==================
Command Line Tools
==================

the `pipeliner` command allows pipeliner functions to be run from the UNIX command line

.. argparse::
   :module: pipeliner.api.pipeliner
   :func: get_arguments
   :prog: pipeliner