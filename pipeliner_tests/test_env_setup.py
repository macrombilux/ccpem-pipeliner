#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch

from pipeliner import env_setup
from pipeliner_tests import test_data


class PipelinerEnvSetupTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_env_setup(self):
        # Remove variable in case set using pop in case not present
        env_var = "CCPEMLOC"
        os.environ.pop(env_var, None)
        assert env_var not in os.environ
        # Load test bash script and reset environment variable
        test_script = os.path.join(self.test_data, "test_env.sh")
        assert os.path.exists(test_script)
        env_setup.setup_python_environment(scripts=[test_script])
        assert env_var in os.environ
        assert os.environ["CCPEMLOC"] == "playing.burden.lavished"
        assert os.environ["CCPEMLOC"] != "r92"

    @patch.dict(os.environ)
    def test_prepend_to_path(self):
        assert self.test_dir not in os.environ["PATH"]
        env_setup.prepend_paths_to_path(paths=[self.test_dir, self.test_data])
        # Check paths added to start of PATH
        assert os.environ["PATH"].startswith(self.test_dir + ":" + self.test_data + ":")

    def test_setup_python_enviroment(self):
        # Check environment setup works avoiding warning
        # Function should run without error.  When used in pipeliner the function
        # is called in pipeliner.__init__ with try/except to throw warning
        # in case of user error with JSON formatted.
        # Here called without to test function executes with no error when testing
        env_setup.setup_python_environment()


if __name__ == "__main__":
    unittest.main()
