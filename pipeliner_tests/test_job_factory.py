#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import pkg_resources
import unittest
import os

from pipeliner.job_factory import get_job_types, job_from_dict
from pipeliner.jobs.ccpem import molrep_job
from pipeliner.jobs.relion import refine3D_job
from pipeliner.jobs.other import cryolo_job
from pipeliner.jobs.relion.motioncorr_job import RelionMotionCorrOwn


class JobFactoryTest(unittest.TestCase):
    def test_all_jobs_have_same_process_name_in_class_and_entry_point_definitions(self):
        # TODO: avoid the need for this test by removing the duplication of names
        for entry_point in pkg_resources.iter_entry_points("ccpem_pipeliner.jobs"):
            job_class = entry_point.load()
            assert job_class.PROCESS_NAME == entry_point.name

    def test_get_job_types_returns_all_jobs(self):
        all_jobs = [
            x.name for x in pkg_resources.iter_entry_points("ccpem_pipeliner.jobs")
        ]
        found_jobs = get_job_types()
        assert len(found_jobs) == len(all_jobs)

    def test_get_job_types_with_empty_search_term_finds_all_jobs(self):
        all_jobs = [
            x.name for x in pkg_resources.iter_entry_points("ccpem_pipeliner.jobs")
        ]
        found_jobs = get_job_types("")
        assert len(found_jobs) == len(all_jobs)

    def test_get_job_types_with_non_empty_search_term(self):
        all_jobs = [
            x.name for x in pkg_resources.iter_entry_points("ccpem_pipeliner.jobs")
        ]
        found_jobs = get_job_types("Relion")
        assert len(found_jobs) < len(all_jobs)
        for job in found_jobs:
            assert "relion" in job.PROCESS_NAME.lower()

    def test_acedrg_job_does_not_leave_files_behind(self):
        assert not os.path.isdir("AcedrgOut_TMP")
        found_jobs = get_job_types("acedrg")
        assert len(found_jobs) == 1
        assert not os.path.isdir("AcedrgOut_TMP")

    def test_some_example_job_types(self):
        assert type(get_job_types("molrep.atomic_model_fit")[0]) is molrep_job.MolrepJob
        assert type(get_job_types("relion.refine3d")[0]) is refine3D_job.RelionRefine3D
        assert type(get_job_types("cryolo.autopick")[0]) is cryolo_job.CrYOLOAutopick

    def test_job_from_dict(self):
        job = job_from_dict(
            {"_rlnJobTypeLabel": "relion.motioncorr.own", "group_frames": 100}
        )
        assert type(job) == RelionMotionCorrOwn
        assert job.joboptions["group_frames"].value == 100

    def test_job_from_dict_with_compatibility_jobop(self):
        job = job_from_dict(
            {
                "_rlnJobTypeLabel": "relion.motioncorr.own",
                "group_frames": 100,
                "do_own_motioncor": "Yes",  # compatibility JobOption
            }
        )
        assert type(job) == RelionMotionCorrOwn
        assert job.joboptions["group_frames"].value == 100

    def test_job_from_dict_with_bad_jobopt(self):
        with self.assertRaises(ValueError):
            job_from_dict(
                {
                    "_rlnJobTypeLabel": "relion.motioncorr.own",
                    "group_frames": 100,
                    "Bad_job_op": "doodoodoodooo",  # bad JobOption
                }
            )


if __name__ == "__main__":
    unittest.main()
