#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e run.err
#$ -o run.out
#$ -P openmpi
#$  -M 101    # put your email address here
#$  -l coproc_v100=4   # GPUS in relion should be left blank

## load modules
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
202
303

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
acedrg "--smi=CC(=O)Nc1ccc(O)cc1" --res=TYL --out=TYL_acedrg
XXXCHECK_COMMAND_PATHXXX . TYL_acedrg.cif TYL_acedrg.pdb

#One more extra command for good measure
404
dedicated nodes: 1
output name: AceDRG/job001/