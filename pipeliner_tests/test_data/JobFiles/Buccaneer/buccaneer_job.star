# version 30001

data_job

_rlnJobTypeLabel             buccaneer.atomic_model_build
_rlnJobIsContinue                    0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
job_title 		"RELION pipeline test with EMD-3488"
input_map 		PostProcess/job010/postprocess_masked.mrc
resolution 		3.3
input_seq		sequences/5ni1.fasta
extend_pdb		""
ncycle 			1 
ncycle_refmac 	20
ncycle_buc1st	1
ncycle_bucnth	1
map_sharpen 	0.0
ncpus 			1
lib_in 			""
keywords 		""
refmac_keywords ""

do_queue        No 
min_dedicated   1 
qsub    		sbatch 
qsubscript 		/home/vol07/scarf957/sub_scripts/slurm_template.sh 
queuename      	scarf 
other_args		""