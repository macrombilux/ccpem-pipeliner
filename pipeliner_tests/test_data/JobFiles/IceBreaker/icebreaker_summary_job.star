# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    icebreaker.micrograph_analysis.summary
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
   'in_mics'           'IceBreaker/job003/corrected_micrographs.star' 
'min_dedicated'            16 
'nr_threads'            4 
'other_args'           '' 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
