
# version 30001

data_job

_rlnJobType                            16
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
adhoc_bfac        -30 
    angpix      1.244 
  do_queue         No 
do_relion_locres        Yes 
do_resmap_locres         No 
     fn_in Refine3D/job029/run_half1_class001_unfil.mrc 
   fn_mask         "" 
    fn_mtf         "" 
 fn_resmap /public/EM/ResMap/ResMap-1.1.4-linux64 
    maxres          0 
min_dedicated         24 
    minres          0 
    nr_mpi         12 
nr_threads          1 
other_args         "" 
      pval       0.05 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
   stepres          1 