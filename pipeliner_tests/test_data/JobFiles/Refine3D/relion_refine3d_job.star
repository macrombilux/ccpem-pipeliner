
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    relion.refine3d
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'auto_faster'           No 
'auto_local_sampling' '1.8 degrees' 
'ctf_intact_first_peak'           No 
'do_combine_thru_disc'           No 
'do_ctf_correction'          Yes 
   'do_pad1'           No 
'do_parallel_discio'          Yes 
'do_preread_images'           No 
  'do_queue'           No 
'do_solvent_fsc'           No 
'do_zero_mask'          Yes 
   'fn_cont'           '' 
    'fn_img'           '' 
   'fn_mask'           '' 
    'fn_ref'           '' 
   'gpu_ids'           '' 
  'ini_high'           60 
'min_dedicated'            1 
'mpi_command' 'mpirun -n XXXmpinodesXXX' 
    'nr_mpi'            1 
   'nr_pool'            3 
'nr_threads'            1 
'offset_range'            5 
'offset_step'            1 
'other_args'           '' 
'particle_diameter'          200 
'ref_correct_greyscale'           No 
 'relax_sym'           '' 
'scratch_dir'           '' 
'skip_gridding'          Yes 
  'sym_name'           C1 
   'use_gpu'           No 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
    sampling '7.5 degrees' 
 
