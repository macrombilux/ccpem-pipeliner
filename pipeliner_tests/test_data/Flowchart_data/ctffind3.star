
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnMicrographPixelSize #8 
opticsGroup1            1 mtf_k2_200kV.star     0.885000   200.000000     1.400000     0.100000     0.885000 
 

# version 30001

data_micrographs

loop_ 
_rlnMicrographName #1 
_rlnOpticsGroup #2 
_rlnCtfImage #3 
_rlnDefocusU #4 
_rlnDefocusV #5 
_rlnCtfAstigmatism #6 
_rlnDefocusAngle #7 
_rlnCtfFigureOfMerit #8 
_rlnCtfMaxResolution #9 
MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00021_frameImage_PS.ctf:mrc 10863.857422 10575.721680   288.135742    77.967194     0.131144     4.809192 
MotionCorr/job002/Movies/20170629_00022_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00022_frameImage_PS.ctf:mrc  9836.475586  9586.718750   249.756836    70.291290     0.168926     3.619038 
MotionCorr/job002/Movies/20170629_00023_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00023_frameImage_PS.ctf:mrc 10678.056641 10365.234375   312.822266    71.958046     0.166332     3.412236 
MotionCorr/job002/Movies/20170629_00024_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00024_frameImage_PS.ctf:mrc 11693.039062 11353.885742   339.153320    74.367035     0.153686     3.495461 
MotionCorr/job002/Movies/20170629_00025_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00025_frameImage_PS.ctf:mrc 10656.021484 10381.144531   274.876953    73.184746     0.156609     3.478493 
MotionCorr/job002/Movies/20170629_00026_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00026_frameImage_PS.ctf:mrc  8346.364258  8101.361816   245.002441    72.656509     0.187263     2.998199 
MotionCorr/job002/Movies/20170629_00027_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00027_frameImage_PS.ctf:mrc  9145.218750  8826.969727   318.249023    78.355713     0.173906     3.213317 
MotionCorr/job002/Movies/20170629_00028_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00028_frameImage_PS.ctf:mrc 10159.675781  9805.832031   353.843750    75.627907     0.164042     3.075406 
MotionCorr/job002/Movies/20170629_00029_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00029_frameImage_PS.ctf:mrc  8303.106445  7963.178711   339.927734    79.366615     0.193207     3.272007 
MotionCorr/job002/Movies/20170629_00030_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00030_frameImage_PS.ctf:mrc  9116.507812  8828.420898   288.086914    76.925766     0.202328     3.348456 
MotionCorr/job002/Movies/20170629_00031_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00031_frameImage_PS.ctf:mrc 10123.724609  9808.489258   315.235352    75.411964     0.198348     3.198971 
MotionCorr/job002/Movies/20170629_00035_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00035_frameImage_PS.ctf:mrc  9292.215820  8923.652344   368.563477    75.864159     0.150946     3.198971 
MotionCorr/job002/Movies/20170629_00036_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00036_frameImage_PS.ctf:mrc 10073.046875  9709.804688   363.242188    73.090691     0.160490     3.170662 
MotionCorr/job002/Movies/20170629_00037_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00037_frameImage_PS.ctf:mrc 11036.773438 10740.718750   296.054688    77.287926     0.141183     3.184754 
MotionCorr/job002/Movies/20170629_00039_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00039_frameImage_PS.ctf:mrc 10135.973633  9799.812500   336.161133    78.814507     0.164658     3.227791 
MotionCorr/job002/Movies/20170629_00040_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00040_frameImage_PS.ctf:mrc 11115.548828 10829.711914   285.836914    80.512001     0.169794     3.049232 
MotionCorr/job002/Movies/20170629_00042_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00042_frameImage_PS.ctf:mrc 10108.393555  9801.315430   307.078125    77.237549     0.197060     3.257134 
MotionCorr/job002/Movies/20170629_00043_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00043_frameImage_PS.ctf:mrc 11089.820312 10819.312500   270.507812    73.001717     0.211295     3.075406 
MotionCorr/job002/Movies/20170629_00044_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00044_frameImage_PS.ctf:mrc 11457.024414 11169.107422   287.916992    71.873207     0.145232     3.115520 
MotionCorr/job002/Movies/20170629_00045_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00045_frameImage_PS.ctf:mrc 12109.853516 11827.894531   281.958984    75.550957     0.155075     3.102033 
MotionCorr/job002/Movies/20170629_00046_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00046_frameImage_PS.ctf:mrc 13065.066406 12759.383789   305.682617    78.980515     0.157159     3.332882 
MotionCorr/job002/Movies/20170629_00047_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00047_frameImage_PS.ctf:mrc 11486.458984 11198.267578   288.191406    70.012390     0.144625     3.142849 
MotionCorr/job002/Movies/20170629_00048_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00048_frameImage_PS.ctf:mrc 12201.394531 11883.540039   317.854492    75.200516     0.164503     2.924774 
MotionCorr/job002/Movies/20170629_00049_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00049_frameImage_PS.ctf:mrc 13118.432617 12777.864258   340.568359    73.466858     0.166647     3.287016 
 
