
# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      2


# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_
_rlnPipeLineProcessName #1
_rlnPipeLineProcessAlias #2
_rlnPipeLineProcessTypeLabel #3
_rlnPipeLineProcessStatusLabel #4
ModelBuild/job001/ None            buccaneer.atomic_model_build            Succeeded



# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_
_rlnPipeLineNodeName #1
_rlnPipeLineNodeTypeLabel #2
input_map.mrc       DensityMap.mrc
ModelBuild/job001/refined1.pdb  AtomCoords.pdb.buccaneer

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_
_rlnPipeLineEdgeFromNode #1
_rlnPipeLineEdgeProcess #2
input_map.mrc       ModelBuild/job001/



# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_
_rlnPipeLineEdgeProcess #1
_rlnPipeLineEdgeToNode #2
ModelBuild/job001/ ModelBuild/job001/refined1.pdb


