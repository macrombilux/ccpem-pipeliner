# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                       4
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
PostProcess/job003/       None           relion.postproces            running 
ChildProcess/job004/	  None			 relion.test.generic          running
ChildProcess/job005/	  None			 relion.test.generic          running

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job002/emd_3488_mask.mrc            Mask3D.mrc 
Import/job001/3488_run_half1_class001_unfil.mrc           DensityMap.mrc.halfmap 
PostProcess/job003/postprocess.mrc           DensityMap.mrc.relion.postprocess 
PostProcess/job003/postprocess_masked.mrc           DensityMap.relion.postprocess.masked
PostProcess/job003/postprocess.star           ProcessData.star.relion.postprocess
PostProcess/job003/logfile.pdf           LogFile.pdf.relion.postprocess
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job002/emd_3488_mask.mrc PostProcess/job003/ 
Import/job001/3488_run_half1_class001_unfil.mrc PostProcess/job003/ 
PostProcess/job003/postprocess.mrc ChildProcess/job004/ 
PostProcess/job003/postprocess.mrc ChildProcess/job005/ 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
PostProcess/job003/ PostProcess/job003/postprocess.mrc 
PostProcess/job003/ PostProcess/job003/postprocess_masked.mrc 
PostProcess/job003/ PostProcess/job003/postprocess.star 
PostProcess/job003/ PostProcess/job003/logfile.pdf 