#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import shutil
import os
import time
import subprocess
import json
from glob import glob
from pathlib import Path
from typing import Mapping, Sequence, Tuple, List, Dict

from pipeliner.node_factory import create_node
from pipeliner import job_factory
from pipeliner.data_structure import SUCCESS_FILE, FAIL_FILE
from pipeliner.job_runner import JobRunner
from pipeliner.project_graph import ProjectGraph
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner_tests.generic_tests import (
    print_coms,
    get_file_structure,
    check_for_file_structure_changes,
)
from pipeliner.utils import get_check_completion_script, touch
from pipeliner.results_display_objects import ResultsDisplayObject

cc_path = get_check_completion_script()


class DummyJob(PipelinerJob):
    """A dummy pipeliner job for testing purposes."""

    PROCESS_NAME = "dummyjob"
    OUT_DIR = "Test"

    def __init__(self, queue_enabled=True, do_mpi=True, do_threads=True):
        PipelinerJob.__init__(self)

        self.jobinfo.programs = [ExternalProgram("touch")]

        if queue_enabled:
            self.make_queue_options()
        self.get_runtab_options(do_mpi, do_threads)

    def get_commands(self):
        outfile = os.path.join(self.output_dir, "test_file.txt")
        self.output_nodes.append(create_node(outfile, "TestFile", ["txt", "dummyjob"]))
        com = ["touch", outfile]
        return [com]

    def post_run_actions(self):
        with open(os.path.join(self.output_dir, "test_file.txt"), "w") as f:
            f.write("post_run_actions() wrote this line")


def job_running_test(
    *,
    test_jobfile: str,
    input_files: Sequence[Tuple[str, str]],
    expected_outfiles: Sequence[str],
    sleep_time=1,
    success=True,
    overwrite_job=None,
    n_metadata_files_expected=1,
    print_run=False,
    print_err=False,
    show_contents=False,
):
    """Runs a job and checks that the expected outputs are produced

    Args:
        test_jobfile (str): The name of the file containing the running parameters
            for the job. A run.job or job.star file
        input_files (list):  A list of tuples for input files to the job (Directory
            the files should be in for the test, path to the file to copy)
        expected_outfiles (list): List of expected output files.  Only needs the
            path past the output dir of the job IE: `Import/job001/movies/test.mrc`
            should be `movies/test.mrc`
        sleep_time (int): Time to wait before checking for the job's outputs, only
            necessary when large files are written and it's a bit slow
        success (bool): Do you expect the job to be successful
        overwrite_job (str): Overwrite this existing job when running the job.  If
            `None` creates the job as job001
        n_metadata_files_expected (int): Expected number of pipeliner metadata files
            to be produced by the job, almost always 1
        print_run (bool): Display the contents of the run.job file, for debugging
        print_err (bool): Display the contents of the run.err file, for debugging
        show_contents (bool): Show the contents of the output directory, for debugging

    """

    # Prepare the directory structure as if previous jobs have been run:
    for infile in input_files:
        import_dir = infile[0]
        if not os.path.isdir(import_dir):
            os.makedirs(import_dir)
        shutil.copy(infile[1], import_dir)

    with ProjectGraph(read_only=False, create_new=not overwrite_job) as pipeline:
        job_runner = JobRunner(pipeline)
        pipeline.job_counter = 998
        job = job_factory.read_job(test_jobfile)
        job_dir = os.path.join(job.OUT_DIR, "job998/")

        extra_files = [
            "job.star",
            "note.txt",
            "job_pipeline.star",
            "run.out",
            "run.err",
        ]

        if success:
            extra_files.append(SUCCESS_FILE)
        else:
            extra_files.append(FAIL_FILE)

        overwrite = False if overwrite_job is None else True
        job_run = job_runner.run_job(job, overwrite_job, False, False, overwrite, False)

    # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
    time.sleep(sleep_time)
    if print_run:
        print((Path(job_dir) / "run.out").read_text())
    if print_err:
        print((Path(job_dir) / "run.err").read_text())

    if show_contents:
        subprocess.run(["ls", "-al"])
        subprocess.run(["ls", "-al", job_dir])
        print(Path("default_pipeline.star").read_text())
        print((Path(job_dir) / "job.star").read_text())
        print((Path(job_dir) / "run.job").read_text())
        print((Path(job_dir) / "job_pipeline.star").read_text())

        # test for output files
    assert os.path.isdir(job_dir)
    for outfile in expected_outfiles:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    for outfile in extra_files:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    assert os.path.isfile(".gui_" + job.PROCESS_NAME.replace(".", "_") + "job.star")

    # look for the metadata file
    md_files = glob(f"{job_dir}/*job_metadata.json")
    assert len(md_files) == n_metadata_files_expected, md_files

    return job_run


def job_generate_commands_test(
    *,
    jobfile: str,
    input_nodes: Mapping[str, str],
    output_nodes: Mapping[str, str],
    expected_commands: Sequence[str],
    show_coms=False,
    show_inputnodes=False,
    show_outputnodes=False,
):
    """Test that executing the get command function returns the expected
    commands and input and output nodes.

    If the commands don't match, a comparison of the expected and actual
    commands will be displayed

    The pipeliner's check completion command will automatically be added,
    as well as some of the wrapping that the pipeliner performs

    Note that this test will not work correctly for jobs that are set to
    be submitted to a queue. That functionality is tested elsewhere.

    Args:
        jobfile (str): The file containing the running parameters for the job.
            job.star or run.job format
        input_nodes (dict): The input nodes expected to be created by the job
            `{node file path: nodetype}`
        output_nodes (dict): The output nodes expected to be created by the job
            Only needs the path past the output dir of the job IE:
            `Import/job001/movies/test.mrc` should be `movies/test.mrc`.
            {node file, nodetype}
        expected_commands (list): The commands the job is expected to generate
            each command string should be one item in the list
        show_coms (bool): Show the commands that were generated, for debugging
        show_inputnodes (bool): Show the input nodes that would be created, for
            debugging
        show_outputnodes (bool): Show the output nodes that would be created,
            for debugging
    """

    job = job_factory.read_job(jobfile)

    output_dir = os.path.join(job.OUT_DIR, "job999/")
    job.output_dir = output_dir
    # get the current state of the directory for comparison
    pre_tree = get_file_structure()
    commandlist = job.get_commands()
    commands = job.prepare_final_command(commandlist, False)[0]
    post_tree = get_file_structure()
    check_for_file_structure_changes(pre_tree, post_tree)

    command_strings = [" ".join(x) for x in commands]

    if job.working_dir:
        check_comp_command = [
            f"python3 {cc_path} {os.path.relpath(output_dir, job.working_dir)} "
            f"{' '.join(list(output_nodes))}"
        ]
    else:
        check_comp_command = [
            f"python3 {cc_path} {output_dir.strip('/')} {' '.join(list(output_nodes))}"
        ]

    all_expected_commands = list(expected_commands)
    if len(output_nodes) > 0:
        all_expected_commands += check_comp_command

    # make sure the expected nodes have been created
    expected_out_nodes = [
        os.path.join(job.OUT_DIR, f"job999/{x}") for x in output_nodes
    ]
    actual_in_nodes = [x.name for x in job.input_nodes]
    actual_out_nodes = [x.name for x in job.output_nodes]

    if show_coms:
        print_coms([" ".join(x) for x in commands], all_expected_commands)

    if show_inputnodes:
        print("\nINPUT NODES: ({}/{})".format(len(job.input_nodes), len(input_nodes)))
        for i in job.input_nodes:
            print(i.name)
    if show_outputnodes:
        print(
            "\nOUTPUT NODES: ({}/{})".format(len(job.output_nodes), len(output_nodes))
        )
        for i in job.output_nodes:
            print(i.name)

    assert command_strings == all_expected_commands, print_coms(
        command_strings, all_expected_commands
    )

    for input_node in input_nodes:
        assert input_node in actual_in_nodes, input_node
    for out_node in expected_out_nodes:
        assert out_node in actual_out_nodes, out_node

    # make sure no extra nodes have been produced
    for actual_in_node in actual_in_nodes:
        assert actual_in_node in input_nodes, "EXTRA NODE: " + actual_in_node
    for actual_out_node in actual_out_nodes:
        assert actual_out_node in expected_out_nodes, "EXTRA NODE: " + actual_out_node

    # make sure all of the nodes are of the correct type
    for node in job.input_nodes:
        assert node.type == input_nodes[node.name], (
            node.name,
            "node error expect/act",
            input_nodes[node.name],
            node.type,
        )
    # using the expected list here, but it has already been checked
    for node in job.output_nodes:
        node_name = node.name.replace(job.output_dir, "")
        assert node.type == output_nodes[node_name], (
            node_name,
            "node error expect/act",
            output_nodes[node_name],
            node.type,
        )


def job_display_object_generation_test(
    *,
    jobfile: str,
    expected_display_objects: List[Tuple[ResultsDisplayObject, str]],
    files_to_create: Dict[str, str],
    print_dispobj: bool = False,
):
    """Test that a job is creating the expected display objects for the GUI

    Args:
        jobfile (str): The file containing the running parameters for the job.
            job.star or run.job format
        expected_display_objects (List[Tuple[ResultsDisplayObject, str]]):
            A list of the expected display objects, in the order the appear.  If the
            object is small it can be created in the test, and the 2nd item in the
            tuple can be an empty string.  If the object is expected to be very large
            it instead provide the ResultsDisplayObject type and the path to a .json
            file that contains its data for it as the 2nd itm.
        files_to_create (Dict[str, str]): Files that needed for correct
            creation of the results display object.  File name is the key. It along with
            any additional directories will be created in 'JobType/job999' If an actual
            file (with real data) needs to be copied in provide a path in the value. If
            the value is an empty string an empty dummy file will be created.
        print_dispobj (bool): Print out the ResultsDisplyObject that was generated (for
            debugging)
    """
    # create the jon
    job = job_factory.read_job(jobfile)
    output_dir = os.path.join(job.OUT_DIR, "job999/")
    job.output_dir = output_dir
    os.makedirs(output_dir)

    # create needed files
    for in_f in files_to_create:
        fdir = os.path.join(output_dir, os.path.dirname(in_f))
        f = os.path.join(job.output_dir, in_f)
        if not os.path.isdir(fdir):
            os.makedirs(fdir)
        if not files_to_create[in_f]:
            touch(f)
        else:
            shutil.copy(files_to_create[in_f], f)

    # get the expected display objects
    comp_disp_objs: List[ResultsDisplayObject] = []
    for ex_dobj in expected_display_objects:
        if not ex_dobj[1]:
            comp_disp_objs.append(ex_dobj[0])
            continue
        with open(ex_dobj[1], "r") as edof:
            dob = json.load(edof)
            rdo = type(ex_dobj[0])
            comp_disp_objs.append(rdo(**dob))

    # create the display objects
    dispobjs = job.create_results_display()
    if print_dispobj:
        for dobj in dispobjs:
            print(dobj.__dict__)
    assert len(dispobjs) == len(comp_disp_objs)
    for n, dispo in enumerate(comp_disp_objs):
        for key in dispo.__dict__:
            assert dispo.__dict__[key] == dispobjs[n].__dict__[key], (
                key,
                dispo.__dict__[key],
                dispobjs[n].__dict__[key],
            )
