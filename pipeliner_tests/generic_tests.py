#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
"""Utility functions to help with running tests."""

import os
import shutil
import time
import subprocess
import warnings
from contextlib import contextmanager
from glob import glob
from pathlib import Path
from typing import Union, List, Sequence

from pipeliner.job_runner import JobRunner
from pipeliner.project_graph import ProjectGraph
from pipeliner import job_factory
from pipeliner.data_structure import SUCCESS_FILE, RELION_GENERAL_PROCS
from pipeliner.utils import get_check_completion_script, touch
from pipeliner_tests import test_data


test_data_dir = os.path.dirname(test_data.__file__)
cc_path = get_check_completion_script()


def get_pipeliner_tests_root() -> Path:
    return Path(__file__).parent


def print_coms(commands, expected_commands):
    comdif = len(commands) - len(expected_commands)
    if comdif < 0:
        commands.extend(["X"] * (-1 * comdif))
    if comdif > 0:
        expected_commands.extend(["X"] * comdif)

    for line in zip(commands, expected_commands):
        print("\n" + line[0])
        print(line[1])
        compare = ""
        for i in range(len(line[0])):
            try:
                if line[0][i] == line[1][i]:
                    compare += " "
                else:
                    compare += "*"
            except IndexError:
                pass
        print(compare)


def get_file_structure() -> List[List[Sequence[str]]]:
    tree = []
    for root, dirs, files in os.walk(os.getcwd()):
        tree.append([root, dirs, files])
    return tree


def check_for_file_structure_changes(pre_tree, post_tree):
    created, deleted, create_errors, delete_errors = [], [], [], []
    for n, fd in enumerate(post_tree):
        if fd not in pre_tree:
            try:
                created.append([fd, pre_tree[n]])
            except IndexError:
                created.append([fd, [[], [], []]])

    for i in created:
        for cdir in i[0][1]:
            if cdir not in i[1][1]:
                create_errors.append(os.path.join(i[0][0], cdir))
        for cfile in i[0][2]:
            if cfile not in i[1][2]:
                create_errors.append(os.path.join(i[0][0], cfile))

    for n, fd in enumerate(pre_tree):
        if fd not in post_tree:
            deleted.append([fd, post_tree[n]])

    for i in deleted:
        for cdir in i[0][1]:
            if cdir not in i[1][1]:
                delete_errors.append(os.path.join(i[0][0], cdir))
        for cfile in i[0][2]:
            if cfile not in i[1][2]:
                delete_errors.append(os.path.join(i[0][0], cfile))
    assert not create_errors, f"get_commands created files/dirs: {create_errors}"
    assert not delete_errors, f"get_commands deleted files/dirs: {delete_errors}"


def general_get_command_test(
    *,
    jobtype,
    jobfile,
    jobnumber,
    input_nodes,
    output_nodes,
    expected_commands,
    show_coms=False,
    show_inputnodes=False,
    show_outputnodes=False,
    jobfiles_dir="JobFiles/{}/{}",
    job_out_dir=None,
    qsub=False,
):
    """Tests that executing the get command function returns the expected
    commands and input and output nodes. Input and output nodes and entered
    as a dict {NodeName:Nodetype}.  The full path is necessary for input nodes
    but only the file name for output nodes as it is assumed they are in the
    output directory."""

    jobfile_path = jobfiles_dir.format(jobtype, jobfile)
    job = job_factory.read_job(os.path.join(test_data_dir, jobfile_path))

    jobout = job_out_dir if job_out_dir is not None else jobtype
    output_dir = jobout + "/job{:03d}/".format(jobnumber)

    job.output_dir = output_dir
    pre_tree = get_file_structure()
    commandlist = job.get_commands()
    commands = job.prepare_final_command(commandlist, False)[0]
    post_tree = get_file_structure()
    check_for_file_structure_changes(pre_tree, post_tree)

    command_strings = [" ".join(x) for x in commands]

    if job.working_dir:
        check_comp_command = [
            f"python3 {cc_path} {os.path.relpath(output_dir, job.working_dir)} "
            f"{' '.join(list(output_nodes))}"
        ]
    else:
        check_comp_command = [
            f"python3 {cc_path} {output_dir.strip('/')} {' '.join(list(output_nodes))}"
        ]

    if not qsub:
        if len(output_nodes) > 0:
            expected_commands = expected_commands + check_comp_command

    assert command_strings == expected_commands, print_coms(
        command_strings, expected_commands
    )

    def print_inputnodes(expected):
        print("\nINPUT NODES: ({}/{})".format(len(job.input_nodes), len(expected)))
        for i in job.input_nodes:
            print(i.name)

    def print_outputnodes(expected):
        print("\nOUTPUT NODES: ({}/{})".format(len(job.output_nodes), len(expected)))
        for i in job.output_nodes:
            print(i.name)

    # make sure the expected nodes have been created
    expected_in_nodes = input_nodes
    expected_out_nodes = [
        jobout + "/job{:03d}/".format(jobnumber) + x for x in output_nodes
    ]
    actual_in_nodes = [x.name for x in job.input_nodes]
    actual_out_nodes = [x.name for x in job.output_nodes]

    if show_coms:
        print_coms([" ".join(x) for x in commands], expected_commands)
    if show_inputnodes:
        print_inputnodes(input_nodes)
    if show_outputnodes:
        print_outputnodes(output_nodes)

    for node in expected_in_nodes:
        assert node in actual_in_nodes, [node, actual_in_nodes]
    for node in expected_out_nodes:
        assert node in actual_out_nodes, [node, actual_out_nodes]

    # make sure no extra nodes have been produced
    for node in actual_in_nodes:
        assert node in expected_in_nodes, "EXTRA NODE: " + node
    for node in actual_out_nodes:
        assert node in expected_out_nodes, "EXTRA NODE: " + node

    # make sure all of the nodes are of the correct type
    for node in job.input_nodes:
        assert node.type == input_nodes[node.name], (
            node.name,
            "node error expect/act",
            input_nodes[node.name],
            node.type,
        )
    # using the expected list here, but it has already been checked
    for node in job.output_nodes:
        node_name = node.name.replace(job.output_dir, "")
        assert node.type == output_nodes[node_name], (
            node_name,
            "node error expect/act",
            output_nodes[node_name],
            node.type,
        )

    return job


# TODO: This test assumes input files are in pipeliner_tests/test_data
#   pipeliner_tests.job_testing_tools.job_running_test() is a better
#   more robust function to use in most instances


def running_job(
    *,
    test_jobfile,
    job_dir_type,
    job_no,
    input_files,  # [(directory,file), ... (directory,file)]
    expected_outfiles,  # (file, file, ... file)
    sleep_time,  # to let the job finish - why is this needed?
    success=True,
    print_run=False,
    print_err=False,
    show_contents=False,
    jobfiles_dir="JobFiles/{}/{}",
    overwrite=False,
    target_job=None,
    n_metadata_files_expected=1,
):
    # Prepare the directory structure as if previous jobs have been run:
    for infile in input_files:
        import_dir = infile[0]
        if import_dir:
            if not os.path.isdir(import_dir):
                os.makedirs(import_dir)
            assert os.path.isfile(os.path.join(test_data_dir, infile[1])), infile[1]
            shutil.copy(
                os.path.join(test_data_dir, infile[1]),
                import_dir,
            )

    with ProjectGraph(read_only=False, create_new=not overwrite) as pipeline:
        job_runner = JobRunner(pipeline)
        pipeline.job_counter = job_no
        jobfile_path = jobfiles_dir.format(job_dir_type, test_jobfile)
        job = job_factory.read_job(os.path.join(test_data_dir, jobfile_path))
        job_dir = job_dir_type + "/job00{}/".format(job_no)

        extra_files = [
            "job.star",
            "note.txt",
            "job_pipeline.star",
            "run.out",
            "run.err",
        ]

        if success:
            extra_files.append(SUCCESS_FILE)

        job_run = job_runner.run_job(job, target_job, False, False, overwrite, False)

    # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
    time.sleep(sleep_time)
    if print_run:
        print((Path(job_dir) / "run.out").read_text())
    if print_err:
        print((Path(job_dir) / "run.err").read_text())

    if show_contents:
        subprocess.run(["ls", "-al"])
        subprocess.run(["ls", "-al", job_dir])
        print(Path("default_pipeline.star").read_text())
        print((Path(job_dir) / "job.star").read_text())
        print((Path(job_dir) / "run.job").read_text())
        print((Path(job_dir) / "job_pipeline.star").read_text())

        # test for output files
    assert os.path.isdir(job_dir)

    for outfile in expected_outfiles:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    for outfile in extra_files:
        assert os.path.isfile(os.path.join(job_dir, outfile)), outfile
    assert os.path.isfile(".gui_" + job.PROCESS_NAME.replace(".", "_") + "job.star")

    # look for the metadata file
    md_files = glob(f"{job_dir}/*job_metadata.json")
    assert len(md_files) == n_metadata_files_expected, md_files

    return job_run


class ShortpipeFileStructure(object):
    def __init__(self, procs_to_make):
        """Builds the entire file structure for test_data/Pipelines/short_full_pipeline.star
        input a list of processes or 'all' to make the entire pipeline"""

        # only making the first few .Nodes dirs for testing deletion later
        self.nodes_files = [
            ".Nodes/0/Import/job001/movies.star",
            ".Nodes/13/MotionCorr/job002/logfile.pdf",
            ".Nodes/1/MotionCorr/job002/corrected_micrographs.star",
            ".Nodes/13/CtfFind/job003/logfile.pdf",
            ".Nodes/1/CtfFind/job003/micrographs_ctf.star",
        ]

        self.nodes_dirs = [
            ".Nodes/0/Import/job001",
            ".Nodes/13/MotionCorr/job002",
            ".Nodes/1/MotionCorr/job002",
            ".Nodes/13/CtfFind/job003",
            ".Nodes/1/CtfFind/job003",
        ]

        self.common_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "run.job",
            "note.txt",
            SUCCESS_FILE,
            "run.err",
            "run.out",
        ]

        self.loop_procs = {
            "MotionCorr": [
                "_Fractions0-Patch-FitCoeff.log",
                "_Fractions0-Patch-Frame.log",
                "_Fractions0-Patch-Full.log",
                "_Fractions0-Patch-Patch.log",
                "_Fractions.com",
                "_Fractions.err",
                "_Fractions.mrc",
                "_Fractions.out",
                "_Fractions_shifts.eps",
                "_Fractions.star",
            ],
            "CtfFind": [
                "_Fractions.mrc",
                "_Fractions.pow",
                "_Fractions.ctf",
                "_Fractions.gctf.log",
            ],
            "AutoPick": ["_Fractions_autopick.star", "_Fractions_autopick.spi"],
            "Extract": ["_Fractions.mrcs", "_Fractions_extract.star"],
            "Class2D": [
                "_data.star",
                "_model.star",
                "_classes.mrcs",
                "_optimiser.star",
                "_sampling.star",
            ],
            "Class3D": [
                "_class001.mrc",
                "_class001_angdist.bild",
                "_class002.mrc",
                "_class002_angdist.bild",
                "_class003.mrc",
                "_class003_angdist.bild",
                "_sampling.star",
                "_data.star",
                "_model.star",
                "_optimiser.star",
            ],
            "Refine3D": [
                "_half1_class001.mrc",
                "_half1_class001_angdist.bild",
                "_half2_class001.mrc",
                "_half2_class001_angdist.bild",
                "_half2_model.star",
                "_half1_model.star",
                "_sampling.star",
                "_data.star",
                "_optimiser.star",
            ],
            "InitialModel": [
                "_data.star",
                "_class001.mrc",
                "_grad001.mrc",
                "_class002.mrc",
                "_grad002.mrc",
                "_sampling.star",
                "_class001_data.star",
                "_class002_data.star",
                "_model.star",
                "_optimiser.star",
            ],
            # need to add optimiser files here
            "MultiBody": [
                "_data.star",
                "_half1_body001_angdist.bild",
                "_half1_body001.mrc",
                "_half1_body002_angdist.bild",
                "_half1_body002.mrc",
                "_half1_model.star",
                "_half2_body001_angdist.bild",
                "_half2_body001.mrc",
                "_half2_body002_angdist.bild",
                "_half2_body002.mrc",
                "_half2_model.star",
            ],
            "CtfRefine": [
                "_fit.star",
                "_fit.eps",
                "_wAcc.mrc",
                "_xyAcc_real.mrc",
                "_xyAcc_imag.mrc",
            ],
            "Polish": [
                "_FCC_cc.mrc",
                "_FCC_w0.mrc",
                "_FCC_w1.mrc",
                "_shiny.mrcs",
                "_shiny.star",
                "_tracks.eps",
                "_tracks.star",
            ],
            "Subtract": ["star", "_opticsgroup1.mrcs", "_opticsgroup2.mrcs"],
        }

        self.single_files = {
            "MotionCorr": [
                "corrected_micrographs_all_rlnAccumMotionEarly.eps",
                "corrected_micrographs_all_rlnAccumMotionLate.eps",
                "corrected_micrographs_all_rlnAccumMotionTotal.eps",
                "corrected_micrographs_hist_rlnAccumMotionEarly.eps",
                "corrected_micrographs_hist_rlnAccumMotionLate.eps",
                "corrected_micrographs_hist_rlnAccumMotionTotal.eps",
                "corrected_micrographs.star",
                "logfile.pdf",
                "logfile.pdf.lst",
            ],
            "CtfFind": [
                "gctf0.err",
                "gctf0.out",
                "gctf1.err",
                "gctf1.out",
                "micrographs_ctf_all_rlnCtfAstigmatism.eps",
                "micrographs_ctf_all_rlnCtfFigureOfMerit.eps",
                "micrographs_ctf_all_rlnDefocusAngle.eps",
                "micrographs_ctf_all_rlnDefocusU.eps",
                "micrographs_ctf_hist_rlnCtfAstigmatism.eps",
                "micrographs_ctf_hist_rlnCtfFigureOfMerit.eps",
                "micrographs_ctf_hist_rlnDefocusAngle.eps",
                "micrographs_ctf_hist_rlnDefocusU.eps",
                "micrographs_ctf.star",
                "logfile.pdf",
            ],
            "Select": [
                "backup_selection.star",
                "particles.star",
                "class_averages.star",
            ],
            "Refine3D": [
                "run_class001.mrc",
                "run_class001_angdist.bild",
                "run_model.star",
                "run_sampling.star",
                "run_class001_half1_unfil.mrc",
                "run_class001_half2_unfil.mrc",
                "run_optimiser.star",
            ],
            "MultiBody": [
                "run_bodies.bild",
                "run_body001_angdist.bild",
                "run_body001_mask.mrc",
                "run_body001.mrc",
                "run_body002_angdist.bild",
                "run_body002_mask.mrc",
                "run_body002.mrc",
                "run_data.star",
                "run_half1_body001_unfil.mrc",
                "run_half1_body002_unfil.mrc",
                "run_half2_body001_unfil.mrc",
                "run_half2_body002_unfil.mrc",
                "run_optimiser.star",
            ],
            "CtfRefine": [
                "aberr_delta-phase_iter-fit_optics-group_1_N-4.mrc",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4.mrc",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4_residual.mrc",
                "aberr_delta-phase_per-pixel_optics-group_1.mrc",
                "asymmetric_aberrations_optics-group_1.eps",
                "beamtilt_delta-phase_iter-fit_optics-group_1_N-3.mrc",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3.mrc",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3_residual.mrc",
                "beamtilt_delta-phase_per-pixel_optics-group_1.mrc]",
                "particles_ctf_refine.star",
            ],
            "MaskCreate": ["mask.mrc.old", "mask.mrc"],
            "Polish": [
                "bfactors.eps",
                "bfactors.star",
                "scalefactors.eps",
                "shiny.star",
            ],
            "PostProcess": [
                "postprocess_fsc.eps",
                "postprocess_fsc.xml",
                "postprocess_guinier.eps",
                "postprocess_masked.mrc",
                "postprocess.mrc",
                "postprocess.star",
                "logfile.pdf",
            ],
            "LocalRes": [
                "relion_locres.mrc",
                "relion_locres_filtered.mrc",
                "relion_locres_fscs.star]",
            ],
        }

        self.jobstar_files = {
            "Import/job001/": "import_movies_job.star",
            "MotionCorr/job002/": "motioncorr_own_job.star",
            "CtfFind/job003/": "ctffind_ctffind4_job.star",
            "AutoPick/job004/": "autopick_log_job.star",
            "Extract/job005/": "extract_job.star",
            "Class2D/job006/": "class2d_em_job.star",
            "Select/job007/": "select_interactive_job.star",
            "InitialModel/job008/": "initialmodel_job.star",
            "Class3D/job009/": "class3d_job.star",
            "Refine3D/job010/": "refine3d_job.star",
            "MultiBody/job011/": "multibody_refine_job.star",
            "CtfRefine/job012/": "ctfrefine_job.star",
            "MaskCreate/job013/": "maskcreate_job.star",
            "Polish/job014/": "polish_job.star",
            "JoinStar/job015/": "joinstar_micrographs_job.star",
            "Subtract/job016/": "subtract_job.star",
            "PostProcess/job017/": "postprocess_job.star",
            "External/job018/": "external_job.star",
            "LocalRes/job019/": "localres_own_job.star",
        }
        self.outfiles = {}
        self.creation_functions = {
            "LocalRes": self.fs_localres,
            "Import": self.fs_import,
            "MotionCorr": self.fs_motioncorr,
            "CtfFind": self.fs_ctffind,
            "AutoPick": self.fs_autopick,
            "Extract": self.fs_extract,
            "Class2D": self.fs_class2d,
            "Select": self.fs_select,
            "InitialModel": self.fs_inimodel,
            "Class3D": self.fs_class3d,
            "Refine3D": self.fs_refine3d,
            "MultiBody": self.fs_multibody,
            "CtfRefine": self.fs_ctfrefine,
            "MaskCreate": self.fs_maskcreate,
            "Polish": self.fs_polish,
            "JoinStar": self.fs_joinstar,
            "Subtract": self.fs_subtract,
            "PostProcess": self.fs_postprocess,
            "External": self.fs_external,
        }
        for proc in RELION_GENERAL_PROCS:  # PROCS will be deprecated
            if proc in procs_to_make or procs_to_make == ["all"]:
                funct = self.creation_functions.get(proc)
                if funct:
                    funct(proc)

    # specific functions for creating specific jobs
    def fs_import(self, proc):
        jobdir = proc + "/job001/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["movies.star"], self.outfiles[proc])

    def fs_motioncorr(self, proc):
        jobdir = proc + "/job002/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_ctffind(self, proc):
        jobdir = proc + "/job003/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        raw_data = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_data,
            self.loop_procs["CtfFind"],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_autopick(self, proc):
        jobdir = proc + "/job004/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(
            jobdir,
            ["coords_suffix_autopick.star", "logfile.pdf"],
            self.outfiles[proc],
        )
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_extract(self, proc):
        jobdir = proc + "/job005/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["particles.star"], self.outfiles[proc])
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_class2d(self, proc):
        jobdir = proc + "/job006/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            25,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_select(self, proc):
        jobdir = proc + "/job007/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def fs_inimodel(self, proc):
        jobdir = proc + "/job008/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            150,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_class3d(self, proc):
        jobdir = proc + "/job009/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            25,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_refine3d(self, proc):
        jobdir = proc + "/job010/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            16,
            "run_it*",
            self.outfiles[proc],
        )

    def fs_multibody(self, proc):
        jobdir = proc + "/job011/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1],
            self.loop_procs[proc],
            12,
            "run_it*",
            self.outfiles[proc],
        )
        for i in range(0, 6):
            h = "analyse_component{:03d}_histogram.eps".format(i)
            hh = os.path.join(jobdir, h)
            touch(hh)
            self.outfiles[proc].append(hh)

            for j in range(1, 11):
                f = "analyse_component{:03d}_bin{:03d}.mrc".format(i, j)
                ff = os.path.join(jobdir, f)
                touch(ff)
                self.outfiles[proc].append(ff)

    def fs_ctfrefine(self, proc):
        jobdir = proc + "/job012/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files["CtfRefine"], self.outfiles[proc])
        raw_data = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_data,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_maskcreate(self, proc):
        jobdir = proc + "/job013/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def fs_polish(self, proc):
        jobdir = proc + "/job014/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])
        raw_dir = jobdir + "Raw_data"
        self.make_loopfiles(
            raw_dir,
            self.loop_procs[proc],
            100,
            "FoilHole_62762_*",
            self.outfiles[proc],
        )

    def fs_joinstar(self, proc):
        jobdir = proc + "/job015/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["join_particles.star"], self.outfiles[proc])

    def fs_subtract(self, proc):
        jobdir = proc + "/job016/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, ["particles_subtract.star"], self.outfiles[proc])
        part_dir = jobdir + "Particles"
        self.make_loopfiles(
            part_dir,
            self.loop_procs[proc],
            10,
            "subtracted_rank*",
            self.outfiles[proc],
        )

    def fs_postprocess(self, proc):
        jobdir = proc + "/job017/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def fs_external(self, proc):
        jobdir = proc + "/job018/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, [], self.outfiles[proc])
        self.make_loopfiles(
            jobdir[:-1], [".file"], 10, "External_file_*", self.outfiles[proc]
        )

    def fs_localres(self, proc):
        jobdir = proc + "/job019/"
        self.outfiles[proc] = [jobdir + x for x in self.common_files]
        self.make_files(jobdir, self.single_files[proc], self.outfiles[proc])

    def make_files(self, jobdir, filelist, out_list):
        os.makedirs(jobdir)

        for node_dir in self.nodes_dirs:
            os.makedirs(node_dir, exist_ok=True)

        for f in self.nodes_files:
            touch(f)
        for f in self.common_files:
            touch(os.path.join(jobdir, f))

        for f in filelist:
            ff = os.path.join(jobdir, f)
            touch(ff)
            out_list.append(ff)
        # copy in an actual job.star file needed for cleanup
        jobstar_dir = os.path.join(test_data_dir, "JobFiles/Default_JobStar/")
        jobstar_name = "default_relion_" + self.jobstar_files[jobdir]
        jobstar_file = os.path.join(jobstar_dir, jobstar_name)
        shutil.copy(jobstar_file, os.path.join(jobdir, "job.star"))
        out_list.append(os.path.join(jobdir, "job.star"))

    @staticmethod
    def make_loopfiles(writedir, file_list, n, fn, out=None):
        if not os.path.isdir(writedir):
            os.makedirs(writedir)

        for i in range(1, n + 1):
            for f in file_list:
                ff = writedir + "/" + fn.replace("*", "{:03d}".format(i)) + f
                touch(ff)
                if out:
                    out.append(ff)


def do_slow_tests():
    """Include slow-running tests if PIPELINER_TEST_SLOW is set"""
    return True if os.environ.get("PIPELINER_TEST_SLOW", False) else False


def do_interactive_tests():
    """Include tests with user interaction if PIPELINER_TEST_INTERACTIVE is set"""
    return True if os.environ.get("PIPELINER_TEST_INTERACTIVE", False) else False


def read_pipeline(pipeline_file):
    """Reads pipeline into a list where each line is a list of the entries on that line
    used for comparing pipeline outputs without having to worry about spaces or
    carriage returns, removes quotes on the entry names so all lines can be compared
    equally"""

    with open(pipeline_file) as pipeline:
        pipe_data = pipeline.readlines()
    split_data = [x.split() for x in pipe_data]
    final_data = []
    # if the first column has any quotes remove them
    for line in split_data:
        if len(line) >= 1:
            newline = []
            for i in line:
                if i[0] in ["'", '"']:
                    i = i[1:]
                if i[-1] in ["'", '"']:
                    i = i[:-1]
                newline.append(i)
            final_data.append(newline)
        else:
            final_data.append(line)
    return final_data


def make_conversion_file_structure():
    """Make all the jobfiles and dirs for testing pipeline conversion
    Each process needs a directory with a job.star file in it"""
    test_data_loc = get_pipeliner_tests_root()
    files_path = os.path.join(test_data_loc, "test_data/JobFiles/Tutorial_Pipeline/")
    jobfiles = glob(files_path + "*_job.star")
    for f in jobfiles:
        fsplit = os.path.basename(f).split("_")
        target_dir = fsplit[0] + "/" + fsplit[1]
        os.makedirs(target_dir)
        shutil.copy(f, os.path.join(target_dir, "job.star"))


def check_for_relion(version_required=3):
    """Check to see if Relion is available for tests
    Returns true if the Relion version is >=  the specified
    version."""

    try:
        check = subprocess.run(["relion", "--version"], stdout=subprocess.PIPE)
    except FileNotFoundError:
        return False

    out_lines = check.stdout.decode().splitlines()
    for out_line in out_lines:
        if out_line.startswith("RELION version:"):
            vers = int(out_line.split()[2].split(".")[0])
            if vers >= version_required:
                return True
    return False


@contextmanager
def expected_warning(type_, msg="", nwarn: int = 1):
    """Simple context manager to catch a single warning (or multiple copies of the same
    warning) and check that it contains a given message.

    Args:
        type_ (type): the type of the warning, for example RuntimeWarning
        msg (str): optional string to check for in the warning message
        nwarn (bool): number of warnings expected
    """
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("default")
        yield
        print([x.__dict__ for x in w])
        assert len(w) == nwarn, len(w)
        assert issubclass(w[-1].category, type_), (w[-1].category, type_, str(w[-1]))
        assert msg in str(w[-1].message), (str(w[-1].message), msg)


def tutorial_data_available():
    """Check if the relion tutorial data is available

    Returns:
        bool: Was the env var found and does the dir it points to
            exist
    """
    tutorialdir = os.environ.get("PIPELINER_TUTORIAL_DATA")
    if tutorialdir is not None:
        return os.path.isdir(tutorialdir)
    else:
        return False


def get_relion_tutorial_data(dirs: Union[list, str] = None):
    """Copy in parts of the relion tutorial so it doesn't need
    to be included with the pipeliner

    Args:
        dirs (list): Which dirs to copy in, if `None` all will be done
    """

    dirs = [] if dirs is None else dirs
    if not tutorial_data_available():
        raise ValueError("Set the path to tutorial data with $PIPELINER_TUTORIAL_DATA")
    tutorialdir = os.environ.get("PIPELINER_TUTORIAL_DATA", "ERROR_NO_DATA")
    if tutorialdir == "ERROR_NO_DATA":
        raise ValueError("Set the path to tutorial data with $PIPELINER_TUTORIAL_DATA")

    if tutorialdir[-1] != "/":
        tutorialdir += "/"
    for d in os.walk(tutorialdir):
        dname = d[0].replace(tutorialdir, "")
        docopy = dname.split("/")[0] in dirs or dirs == []
        if not os.path.isdir(dname) and dname != "" and docopy:
            os.makedirs(dname)
        if len(d[2]) > 0 and docopy:
            for f in d[2]:
                fn = os.path.join(d[0], f)
                if os.path.isfile(fn) and f[0] != ".":
                    ln = fn.replace(tutorialdir, "")
                    os.symlink(fn, ln)
    # always copy in the default pipeline
    if os.path.islink("default_pipeline.star"):
        os.unlink("default_pipeline.star")
    shutil.copy(
        os.path.join(tutorialdir, "default_pipeline.star"), "default_pipeline.star"
    )
