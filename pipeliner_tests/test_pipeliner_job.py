#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from glob import glob
from pipeliner import pipeliner_job
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import touch
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayPending,
)
from pipeliner_tests.job_testing_tools import DummyJob
from pipeliner.job_factory import new_job_of_type, get_job_types
from pipeliner.starfile_handler import JobStar
from pipeliner.data_structure import SUCCESS_FILE, FAIL_FILE
from pipeliner.scripts import check_completion
from pipeliner.job_options import (
    StringJobOption,
    MultiStringJobOption,
    InputNodeJobOption,
    MultiFileNameJobOption,
    FileNameJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
    FloatJobOption,
    BooleanJobOption,
    PathJobOption,
    TRUES,
    FALSES,
)
from pipeliner.utils import get_check_completion_script
from pipeliner.nodes import DensityMapNode

cc_path = get_check_completion_script()


class PipelinerJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_pipeliner_job_cannot_be_instantiated(self):
        with self.assertRaises(NotImplementedError):
            pipeliner_job.PipelinerJob()

    def test_get_default_params_import_movies(self):
        job = new_job_of_type("relion.import.movies")
        params_dict = job.default_params_dict()
        expected_params = {
            "_rlnJobTypeLabel": "relion.import.movies",
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
            "fn_in_raw": "Movies/*.tif",
            "is_multiframe": "Yes",
            "optics_group_name": "opticsGroup1",
            "fn_mtf": "",
            "angpix": 1.4,
            "kV": 300,
            "Cs": 2.7,
            "Q0": 0.1,
            "beamtilt_x": 0.0,
            "beamtilt_y": 0.0,
            "is_synthetic": "No",
        }
        assert params_dict == expected_params

    def test_get_default_params_all_jobs(self):
        defaults_dir = os.path.join(self.test_data, "JobFiles/Default_JobStar")
        default_files = glob(f"{defaults_dir}/*.star")

        # parameters not to check because they are set by env vars or not in the
        # joboptions dict
        params_to_skip = [
            "_rlnJobTypeLabel",
            "_rlnJobIsContinue",
            "_rlnJobIsTomo",
            "scratch_dir",
            "fn_gctf_exe",
            "fn_motioncor2_exe",
            "qsubscript",
            "do_queue",
            "queuename",
            "qsubscript",
            "qsub",
            "fn_ctffind_exe",
            "fn_resmap",
            "fn_topaz_exec",
        ]

        for f in default_files:
            options = JobStar(f).all_options_as_dict()
            jobtype = options["_rlnJobTypeLabel"]
            new_job = new_job_of_type(jobtype)
            new_job.add_compatibility_joboptions()
            params_dict = new_job.default_params_dict()
            for param in options:
                if param not in params_to_skip:
                    try:
                        info = (jobtype, param, str(params_dict[param]), options[param])
                        assert str(params_dict[param]) == options[param], info
                    except KeyError:
                        raise ValueError(f"jobtype {jobtype} missing param: {param}")

            for param in params_dict:
                if param not in params_to_skip:
                    info = (jobtype, param, str(params_dict[param]), options[param])
                    assert str(params_dict[param]) == options[param], info

    def test_joboption_validation_with_errors(self):
        job = new_job_of_type("relion.import.movies")

        # ad hoc set the parameters rather the depending on the job itself
        job.joboptions["fn_in_raw"].is_required = True
        job.joboptions["fn_in_raw"].value = ""
        job.joboptions["optics_group_name"].validation_regex = "x+.x"
        job.joboptions["optics_group_name"].value = "ygroupy"
        job.joboptions["angpix"].suggested_min = 100.0
        job.joboptions["angpix"].value = 99.0
        errors = job.validate_joboptions()

        exp_errors = [
            ("error", "Required parameter empty", "Raw input files:"),
            ("error", "Value format must match pattern 'x+.x'", "Optics group name:"),
            (
                "warning",
                "Value outside suggested range < 100.0",
                "Pixel size (Angstrom):",
            ),
            ("warning", "Value outside suggested range > 3", "Pixel size (Angstrom):"),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)

        for err in exp_errors:
            assert err in formatted_errors, err
        for err in formatted_errors:
            assert err in exp_errors

    def test_joboption_validation_with_warnings(self):
        job = new_job_of_type("relion.import.movies")

        # ad hoc set the parameters rather the depending on the job itself
        job.joboptions["fn_in_raw"].is_required = True
        job.joboptions["fn_in_raw"].value = ""
        job.joboptions["optics_group_name"].validation_regex = "x+.x"
        job.joboptions["optics_group_name"].value = "ygroupy"
        job.joboptions["angpix"].hard_min = 100.0
        job.joboptions["angpix"].value = 99.0
        errors = job.validate_joboptions()

        exp_errors = [
            ("error", "Required parameter empty", "Raw input files:"),
            ("error", "Value format must match pattern 'x+.x'", "Optics group name:"),
            ("error", "Value cannot be less than 100.0", "Pixel size (Angstrom):"),
            ("warning", "Value outside suggested range > 3", "Pixel size (Angstrom):"),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)

        for err in exp_errors:
            assert err in formatted_errors, err
        for err in formatted_errors:
            assert err in exp_errors

    def test_joboption_validation_with_adv_parameter_errors(self):
        job = new_job_of_type("relion.autopick.log")
        job.joboptions["fn_input_autopick"].value = "test.star"
        job.joboptions["log_diam_min"].value = 100
        job.joboptions["log_diam_max"].value = 99
        job.joboptions["do_queue"].value = "No"

        errors = job.validate_joboptions()

        exp_errors = [
            (
                "error",
                "Max diameter of LoG filter (log_diam_max) must be > min diameter "
                "(log_diam_min).",
                [
                    "Min. diameter for LoG filter (A)",
                    "Max. diameter for LoG filter (A)",
                ],
            ),
        ]

        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, [x.label for x in i.raised_by])
            formatted_errors.append(error)

        for err in formatted_errors:
            assert err in exp_errors

        for err in exp_errors:
            assert err in formatted_errors, err

    def test_parse_additional_args_even_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions[
            "other_args"
        ].value = '--1 test --2 test2 --3 "This is complicated"'
        assert job.parse_additional_args() == [
            "--1",
            "test",
            "--2",
            "test2",
            "--3",
            "This is complicated",
        ]

    def test_dynamic_requirement_of_joboptions(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["do_queue"].value = "Yes"
        job.joboptions["qsubscript"].value = ""

        errors = job.validate_dynamically_required_joboptions()

        assert errors[0].message == (
            'Because "Submit to queue?" = True, "Standard submission script:" is'
            " required"
        )
        assert [x.label for x in errors[0].raised_by] == [
            "Standard submission script:",
            "Submit to queue?",
        ]
        assert errors[0].type == "error"

    def test_parse_additional_args_starts_with_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions[
            "other_args"
        ].value = '"1 test" --2 test2 --3 "This is complicated"'

        print(job.parse_additional_args())
        assert job.parse_additional_args() == [
            "1 test",
            "--2",
            "test2",
            "--3",
            "This is complicated",
        ]

    def test_parse_additional_args_starts_no_quotes(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["other_args"].value = "1 test --2 test2 --3 This is simple"

        assert job.parse_additional_args() == [
            "1",
            "test",
            "--2",
            "test2",
            "--3",
            "This",
            "is",
            "simple",
        ]

    def test_grouping_joboptions_complete_list(self):
        job = new_job_of_type("relion.refine3d")
        new_priority = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "mpi_command",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        job.set_joboption_order(new_priority)
        groupings = job.get_joboption_groups()
        exp_groups = {
            "Main": [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "particle_diameter",
                "do_zero_mask",
                "do_solvent_fsc",
                "sampling",
                "offset_range",
                "offset_step",
                "auto_local_sampling",
                "relax_sym",
                "auto_faster",
            ],
            "Compute options": [
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
            ],
            "Running options": ["nr_mpi", "mpi_command", "nr_threads", "other_args"],
            "Queue submission options": [
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
            ],
        }
        assert groupings == exp_groups

    def test_grouping_joboptions_partial_list(self):
        job = new_job_of_type("relion.refine3d")
        priorities = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        job.set_joboption_order(priorities)
        groupings = job.get_joboption_groups()
        exp_groups = {
            "Main": [
                "fn_cont",
                "fn_img",
                "fn_ref",
                "ref_correct_greyscale",
                "fn_mask",
                "ini_high",
                "sym_name",
                "do_ctf_correction",
                "ctf_intact_first_peak",
                "particle_diameter",
                "do_zero_mask",
                "do_solvent_fsc",
                "sampling",
                "offset_range",
                "offset_step",
                "auto_local_sampling",
                "relax_sym",
                "auto_faster",
            ],
            "Compute options": [
                "do_parallel_discio",
                "nr_pool",
                "do_pad1",
                "skip_gridding",
                "do_preread_images",
                "scratch_dir",
                "do_combine_thru_disc",
                "use_gpu",
                "gpu_ids",
            ],
            "Running options": ["nr_mpi", "mpi_command", "nr_threads", "other_args"],
            "Queue submission options": [
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
            ],
        }
        assert groupings == exp_groups

    def test_reordering_joboptions_complete_list(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "mpi_command",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        assert list(job.joboptions) != new_order
        job.set_joboption_order(new_order)
        assert list(job.joboptions) == new_order

    def test_reordering_joboptions_partial_list(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        full_new_order = [
            "fn_cont",
            "fn_img",
            "fn_ref",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
            "sampling",
            "offset_range",
            "offset_step",
            "auto_local_sampling",
            "relax_sym",
            "auto_faster",
            "do_parallel_discio",
            "nr_pool",
            "do_pad1",
            "skip_gridding",
            "do_preread_images",
            "scratch_dir",
            "do_combine_thru_disc",
            "use_gpu",
            "gpu_ids",
            "nr_mpi",
            "mpi_command",
            "nr_threads",
            "do_queue",
            "queuename",
            "qsub",
            "qsubscript",
            "min_dedicated",
            "other_args",
        ]
        assert list(job.joboptions) != full_new_order
        job.set_joboption_order(new_order)
        assert list(job.joboptions) == full_new_order

    def test_reordering_joboptions_error_in_not_present(self):
        job = new_job_of_type("relion.refine3d")
        new_order = [
            "fn_img",
            "fn_ref",
            "Badbadbad",
            "ref_correct_greyscale",
            "fn_mask",
            "ini_high",
            "sym_name",
            "do_ctf_correction",
            "ctf_intact_first_peak",
            "particle_diameter",
            "do_zero_mask",
            "do_solvent_fsc",
        ]
        with self.assertRaises(ValueError):
            job.set_joboption_order(new_order)

    def test_job_withno_outputs_adds_logfile_in_post(self):
        jobdir = "PostProcess/job001/"
        job = new_job_of_type("relion.postprocess")
        job.output_dir = jobdir
        job.output_nodes = []
        assert len(job.output_nodes) == 0
        assert not os.path.isfile(SUCCESS_FILE)

        job.prepare_final_command([["fake", "commands"]], True)
        job.output_dir = jobdir
        oj = os.path.join(jobdir, "run.out")
        touch(oj)

        check_completion.main([jobdir, "run.out"])
        success = os.path.join(jobdir, SUCCESS_FILE)

        assert len(job.output_nodes) == 1
        assert job.output_nodes[0].name == oj, job.output_nodes[0].name
        assert job.output_nodes[0].type == "LogFile.txt"
        assert os.path.isfile(success)

    def test_job_withno_outputs_fails_if_log_not_written(self):
        jobdir = "PostProcess/job001/"
        job = new_job_of_type("relion.postprocess")
        job.output_dir = jobdir
        job.output_nodes = []
        ffile = os.path.join(jobdir, FAIL_FILE)

        assert len(job.output_nodes) == 0
        assert not os.path.isfile(ffile)
        job.output_dir = jobdir
        job.prepare_final_command([["fake", "commands"]], True)
        oj = os.path.join(jobdir, "run.out")
        check_completion.main([jobdir, "run.out"])

        assert len(job.output_nodes) == 1
        assert job.output_nodes[0].name == oj
        assert job.output_nodes[0].type == "LogFile.txt"
        assert os.path.isfile(ffile)

    def test_node_creation_inputnode_joboptions(self):
        """test creation of an input node from an inputnode joboption"""
        the_job = DummyJob()
        jobop = InputNodeJobOption(label="inputnodetype", node_type="Test")
        jobop.value = "testfile.star"
        the_job.joboptions = {"test": jobop}
        the_job.create_input_nodes()
        assert len(the_job.input_nodes) == 1
        node = the_job.input_nodes[0]
        assert node.name == "testfile.star"
        assert node.toplevel_type == "Test"
        assert node.type == "Test.star"
        assert node.format == "star"

    def test_node_creation_filename_joboptions(self):
        """test creation of an input node from an filename joboption"""
        the_job = DummyJob()
        jobop = FileNameJobOption(label="inputnodetype", node_type="Test")
        jobop.value = "testfile.star"
        the_job.joboptions = {"test": jobop}
        the_job.create_input_nodes()
        assert len(the_job.input_nodes) == 1
        node = the_job.input_nodes[0]
        assert node.name == "testfile.star"
        assert node.toplevel_type == "Test"
        assert node.type == "Test.star"
        assert node.format == "star"

    def test_node_creation_multifilename_joboptipns(self):
        """test creation of an input node from an multifile joboption"""
        the_job = DummyJob()
        jobop = MultiFileNameJobOption(label="inputnodetype", node_type="Test")
        jobop.value = "testfile1.star:::testfile2.star"
        the_job.joboptions = {"test": jobop}
        the_job.create_input_nodes()

        assert len(the_job.input_nodes) == 2
        node = the_job.input_nodes[0]
        assert node.name == "testfile1.star"
        assert node.toplevel_type == "Test"
        assert node.type == "Test.star"
        assert node.format == "star"
        node = the_job.input_nodes[1]
        assert node.name == "testfile2.star"
        assert node.toplevel_type == "Test"
        assert node.type == "Test.star"
        assert node.format == "star"

    def test_raw_options_from_jobstar(self):
        job = new_job_of_type("relion.joinstar.micrographs")
        job.read(
            os.path.join(
                self.test_data, "JobFiles/JoinStar/joinstar_mics_relionstyle_job.star"
            )
        )
        exp_jos = {
            "do_mic": "Yes",
            "do_mov": "No",
            "do_part": "No",
            "fn_mic1": "Mics1.star",
            "fn_mic2": "Mics2.star",
        }

        for ejo in exp_jos:
            assert job.raw_options[ejo] == exp_jos[ejo]

    def test_raw_options_from_runjob(self):
        job = new_job_of_type("relion.joinstar.micrographs")
        job.read(
            os.path.join(
                self.test_data, "JobFiles/JoinStar/joinstar_mics_relionstyle_run.job"
            )
        )
        exp_jos = {
            "Combine micrograph STAR files?": "Yes",
            "Combine movie STAR files?": "No",
            "Combine particle STAR files?": "No",
            "Micrograph STAR file 1:": "Mic1.star",
            "Micrograph STAR file 2:": "Mic2.star",
        }
        print(job.raw_options)
        for ejo in exp_jos:
            assert job.raw_options[ejo] == exp_jos[ejo]

    def test_get_mpi_command_substitution(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["nr_mpi"].value = 8
        job.joboptions["mpi_command"].value = "test XXXmpinodesXXX test XXXmpinodesXXX"
        subbed_com = ["test", "8", "test", "8"]
        assert job.get_mpi_command() == subbed_com

    def test_get_mpi_command_substitution_complicated(self):
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["nr_mpi"].value = 16
        job.joboptions["mpi_command"].value = "'A complicated one' XXXmpinodesXXX"
        subbed_com = ["A complicated one", "16"]
        assert job.get_mpi_command() == subbed_com

    def test_all_jobs_joboption_defaults_are_appropriate(self):
        """Make sure all JobOptions for jobs have appropriate data types for
        their default values. This can cause hard to diagnose errors in Doppio"""

        types = {
            StringJobOption: [str],
            MultiStringJobOption: [str],
            InputNodeJobOption: [str],
            MultiFileNameJobOption: [str],
            FileNameJobOption: [str],
            MultipleChoiceJobOption: [str],
            IntJobOption: [int],
            FloatJobOption: [int, float],
            BooleanJobOption: [str],
            PathJobOption: [str],
        }

        for j in get_job_types():
            job = new_job_of_type(j.PROCESS_NAME)
            for jo in job.joboptions:
                exp_type = types[type(job.joboptions[jo])]
                dvtype = type(job.joboptions[jo].default_value)
                assert dvtype in exp_type, (jo, dvtype, exp_type)
                if type(job.joboptions[jo]) == BooleanJobOption:
                    assert job.joboptions[jo].default_value.lower() in TRUES + FALSES

    def test_dropzone_changes_joboptions_and_writes_jobstar(self):
        job = new_job_of_type("relion.refine3d")
        job.joboptions["fn_img"].value = "data.star"
        job.joboptions["fn_mask"].value = "UserFiles/mask.mrc"
        job.joboptions["fn_ref"].value = "reference.mrc"
        job.output_dir = "Refine3D/job002/"
        coms = job.get_commands()
        coms = job.prepare_final_command(commands=coms, do_makedir=True)
        # job option has been updated
        assert job.joboptions["fn_mask"].value == "Refine3D/job002/mask.mrc"

        # nodes have been updated
        in_nodes = [x.name for x in job.input_nodes]
        out_nodes = [x.name for x in job.output_nodes]
        assert in_nodes == ["data.star", "reference.mrc", "Refine3D/job002/mask.mrc"]
        assert out_nodes == [
            "Refine3D/job002/run_data.star",
            "Refine3D/job002/run_optimiser.star",
            "Refine3D/job002/run_half1_class001_unfil.mrc",
            "Refine3D/job002/run_class001.mrc",
        ]

        # make sure the name of the mask file was updated in the commands
        comslist = [
            ["mv", "UserFiles/mask.mrc", "Refine3D/job002/mask.mrc"],
            [
                "relion_refine",
                "--i",
                "data.star",
                "--o",
                "Refine3D/job002/run",
                "--auto_refine",
                "--split_random_halves",
                "--ref",
                "reference.mrc",
                "--firstiter_cc",
                "--ini_high",
                "60",
                "--dont_combine_weights_via_disc",
                "--pool",
                "3",
                "--pad",
                "2",
                "--skip_gridding",
                "--ctf",
                "--particle_diameter",
                "200",
                "--flatten_solvent",
                "--zero_mask",
                "--solvent_mask",
                "Refine3D/job002/mask.mrc",
                "--oversampling",
                "1",
                "--healpix_order",
                "2",
                "--auto_local_healpix_order",
                "4",
                "--offset_range",
                "5",
                "--offset_step",
                "2",
                "--sym",
                "C1",
                "--low_resol_join_halves",
                "40",
                "--norm",
                "--scale",
                "--j",
                "1",
                "--pipeline_control",
                "Refine3D/job002/",
            ],
            [
                "python3",
                cc_path,
                "Refine3D/job002",
                "run_data.star",
                "run_optimiser.star",
                "run_half1_class001_unfil.mrc",
                "run_class001.mrc",
            ],
        ]
        assert coms == [comslist, comslist]

    def test_dropzone_deletes_files_on_error_same_name_as_output_node(self):
        os.makedirs("UserFiles")
        touch("UserFiles/run_class001.mrc")
        job = new_job_of_type("relion.refine3d")
        job.joboptions["fn_img"].value = "Refine3D/job002/data.star"
        job.joboptions["fn_mask"].value = "UserFiles/run_class001.mrc"
        job.joboptions["fn_ref"].value = "reference.mrc"
        job.output_dir = "Refine3D/job002/"
        coms = job.get_commands()
        with self.assertRaises(ValueError):
            job.prepare_final_command(commands=coms, do_makedir=True)
        assert not os.path.isfile("UserFiles/run_class001.mrc")

    def test_dropzone_deletes_files_on_error_same_name_as_input_node(self):
        os.makedirs("UserFiles")
        touch("UserFiles/data.star")
        job = new_job_of_type("relion.refine3d")
        job.joboptions["fn_img"].value = "Refine3D/job002/data.star"
        job.joboptions["fn_mask"].value = "UserFiles/data.star"
        job.joboptions["fn_ref"].value = "reference.mrc"
        job.output_dir = "Refine3D/job002/"
        coms = job.get_commands()
        with self.assertRaises(ValueError):
            job.prepare_final_command(commands=coms, do_makedir=True)
        assert not os.path.isfile("UserFiles/data.star")

    def test_dropzone_changes_joboptions_and_writes_jobstar_multifiles(self):
        # test this with a dummy job
        os.makedirs("UserFiles")
        touch("UserFiles/file_1.txt")
        touch("UserFiles/file_2.txt")
        job = DummyJob()
        job.joboptions["mf"] = MultiFileNameJobOption(
            label="test", node_type="TestNode"
        )
        job.joboptions["mf"].value = "UserFiles/file_1.txt:::UserFiles/file_2.txt"
        job.output_dir = "Test/job001/"
        coms = job.get_commands()
        fincoms = job.prepare_final_command(coms, True)
        assert (
            job.joboptions["mf"].value
            == "Test/job001/file_1.txt:::Test/job001/file_2.txt"
        )
        assert [x.name for x in job.input_nodes] == [
            "Test/job001/file_1.txt",
            "Test/job001/file_2.txt",
        ]
        mvcoms = [
            ["mv", "UserFiles/file_1.txt", "Test/job001/file_1.txt"],
            ["mv", "UserFiles/file_2.txt", "Test/job001/file_2.txt"],
        ]
        for mc in mvcoms:
            assert mc in fincoms[0]
            assert mc in fincoms[1]

    def test_dropzone_changes_joboptions_and_writes_jobstar_multifiles_mixed(self):
        # test this with a dummy job
        os.makedirs("UserFiles")
        touch("UserFiles/file_1.txt")
        touch("file_2.txt")
        job = DummyJob()
        job.joboptions["mf"] = MultiFileNameJobOption(
            label="test", node_type="TestNode"
        )
        job.joboptions["mf"].value = "UserFiles/file_1.txt:::file_2.txt"
        job.output_dir = "Test/job001/"
        coms = job.get_commands()
        fincoms = job.prepare_final_command(coms, True)
        assert job.joboptions["mf"].value == "Test/job001/file_1.txt:::file_2.txt"
        assert [x.name for x in job.input_nodes] == [
            "Test/job001/file_1.txt",
            "file_2.txt",
        ]
        mc = ["mv", "UserFiles/file_1.txt", "Test/job001/file_1.txt"]
        assert mc in fincoms[0]
        assert mc in fincoms[1]

    def test_reference_eq_doi_match(self):
        r1 = pipeliner_job.Ref(
            title="test1", journal="journal numero uno", year="1999", doi="1234567"
        )
        r2 = pipeliner_job.Ref(
            title="test1-dif", journal="journal numero uno", year="1999", doi="1234567"
        )
        assert r1 == r2

    def test_reference_eq_doi_dont_match(self):
        r1 = pipeliner_job.Ref(
            title="test1", journal="journal numero uno", year="1999", doi="1234567"
        )
        r2 = pipeliner_job.Ref(
            title="test1", journal="journal numero uno", year="1999", doi="xxxxxx"
        )
        assert r1 != r2

    def test_reference_eq_exact_match(self):
        r1 = pipeliner_job.Ref(title="test1", journal="journal numero uno", year="1999")
        r2 = pipeliner_job.Ref(title="test1", journal="journal numero uno", year="1999")
        assert r1 == r2

    def test_reference_eq_journal_close_match(self):
        r1 = pipeliner_job.Ref(title="test1", journal="Journal Numero Uno", year="1999")
        r2 = pipeliner_job.Ref(title="test1", journal="j. num. uno", year="1999")
        assert r1 == r2

    def test_reference_eq_title_close_match(self):
        r1 = pipeliner_job.Ref(
            title="test1: a paper", journal="journal numero uno", year="1999"
        )
        r2 = pipeliner_job.Ref(
            title="Test1 - a paper!", journal="journal numero uno", year="1999"
        )
        assert r1 == r2

    def test_reference_eq_title_no_match(self):
        r1 = pipeliner_job.Ref(
            title="test1: a paper", journal="journal numero uno", year="1999"
        )
        r2 = pipeliner_job.Ref(
            title="Test1 - a bad paper!", journal="journal numero uno", year="1999"
        )
        assert r1 != r2

    def test_reference_eq_journal_no_match(self):
        r1 = pipeliner_job.Ref(title="test1", journal="Journal Numero Uno", year="1999")
        r2 = pipeliner_job.Ref(title="test1", journal="j. num. 1", year="1999")
        assert r1 != r2

    def test_validate_files_with_deactivated(self):
        """File is missing but ok because JobOption is deactivated"""
        job = DummyJob()
        job.joboptions = {
            "test": FileNameJobOption(
                label="test", deactivate_if=[("ref", "=", "Yes")]
            ),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "Yes"
        job.joboptions["test"].value = "test_file.txt"

        assert not job.validate_input_files()

    def test_validate_files_with_dynamically_required(self):
        """File is missing and joboption has been toggled to required"""
        job = DummyJob()
        job.joboptions = {
            "test": FileNameJobOption(label="test", required_if=[("ref", "=", "Yes")]),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "Yes"
        job.joboptions["test"].value = "test_file.txt"

        errs = job.validate_input_files()
        assert len(errs) == 1
        assert errs[0].__dict__["message"] == "File test_file.txt not found"

    def test_validate_files_with_value_withfile(self):
        """If jo has a value and hasn't been deactivated then needs an existing file"""
        touch("test_file.txt")
        job = DummyJob()
        job.joboptions = {
            "test": FileNameJobOption(
                label="test", deactivate_if=[("ref", "=", "Yes")]
            ),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "No"
        job.joboptions["test"].value = "test_file.txt"

        assert not job.validate_input_files()

    def test_validate_files_with_value_nofile(self):
        """If jo has a value and hasn't been deactivated then needs an existing file"""
        job = DummyJob()
        job.joboptions = {
            "test": FileNameJobOption(
                label="test", deactivate_if=[("ref", "=", "Yes")]
            ),
            "ref": StringJobOption(label="check"),
        }
        job.joboptions["ref"].value = "No"
        job.joboptions["test"].value = "test_file.txt"

        errs = job.validate_input_files()
        assert len(errs) == 1
        assert errs[0].__dict__["message"] == "File test_file.txt not found"

    def test_default_results_display(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file2.mrc"))

        os.makedirs("outdir")
        node = DensityMapNode(name="file.mrc")
        node2 = DensityMapNode(name="file2.mrc")
        job = DummyJob()
        job.output_nodes = [node, node2]

        dispobjs = job.create_results_display()
        assert len(dispobjs) == 2
        for dob in dispobjs:
            assert type(dob) == ResultsDisplayMapModel
        assert dispobjs[0].title == "Map: file.mrc"
        assert dispobjs[1].title == "Map: file2.mrc"
        assert dispobjs[0].maps == ["Thumbnails/file.mrc"]
        assert dispobjs[1].maps == ["Thumbnails/file2.mrc"]

    def test_default_results_display_onefails(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file2.star"))

        os.makedirs("outdir")
        node = DensityMapNode(name="file.mrc")
        node2 = DensityMapNode(name="file2.mrc")
        job = DummyJob()
        job.output_nodes = [node, node2]

        dispobjs = job.create_results_display()
        assert len(dispobjs) == 2
        assert type(dispobjs[0]) == ResultsDisplayMapModel
        assert type(dispobjs[1]) == ResultsDisplayPending
        assert dispobjs[0].title == "Map: file.mrc"
        assert dispobjs[1].title == "Results pending..."
        assert dispobjs[0].maps == ["Thumbnails/file.mrc"]
        assert dispobjs[1].reason.startswith(
            "Error creating map-model display object: [Errno 2]"
        )


def test_external_program_when_command_is_available_with_version(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)

    def mock_run_subprocess(command, **kwargs):
        class CompletedProcessStub:
            stdout = "1.0.0".encode()

        return CompletedProcessStub()

    monkeypatch.setattr(pipeliner_job, "run_subprocess", mock_run_subprocess)
    prog = pipeliner_job.ExternalProgram(
        "my_command", vers_com=["my_command", "--version"]
    )
    assert prog.command == "my_command"
    assert prog.exe_path == "/path/to/my_command"
    assert prog.get_version() == "1.0.0"


def test_external_program_when_command_is_available_without_version(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)

    prog = pipeliner_job.ExternalProgram("my_command")
    assert prog.command == "my_command"
    assert prog.exe_path == "/path/to/my_command"
    assert prog.get_version() == "No version info available"


def test_external_program_when_command_is_unavailable(monkeypatch):
    def mock_which(command):
        return None

    monkeypatch.setattr(shutil, "which", mock_which)

    prog = pipeliner_job.ExternalProgram("my_command")
    assert prog.command == "my_command"
    assert prog.exe_path is None
    assert prog.get_version() == "No version info available"


def test_external_program_when_version_command_fails(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)

    def mock_run_subprocess(command, **kwargs):
        raise RuntimeError("Version command failed")

    monkeypatch.setattr(pipeliner_job, "run_subprocess", mock_run_subprocess)
    prog = pipeliner_job.ExternalProgram(
        "my_command", vers_com=["my_command", "--version"]
    )
    assert prog.command == "my_command"
    assert prog.exe_path == "/path/to/my_command"
    assert (
        prog.get_version()
        == "Error trying to determine version: Version command failed"
    )


def test_job_info_when_commands_are_available(monkeypatch):
    def mock_which(command):
        return "/path/to/" + command

    monkeypatch.setattr(shutil, "which", mock_which)
    prog1 = pipeliner_job.ExternalProgram("my_command_1")
    prog2 = pipeliner_job.ExternalProgram("my_command_2")
    job_info = pipeliner_job.JobInfo(external_programs=[prog1, prog2])

    assert job_info.is_available is True


def test_job_info_when_commands_are_unavailable(monkeypatch):
    def mock_which(command):
        return None

    monkeypatch.setattr(shutil, "which", mock_which)
    prog1 = pipeliner_job.ExternalProgram("my_command_1")
    prog2 = pipeliner_job.ExternalProgram("my_command_2")
    job_info = pipeliner_job.JobInfo(external_programs=[prog1, prog2])

    assert job_info.is_available is False


def test_job_info_when_no_commands_are_given(monkeypatch):
    job_info = pipeliner_job.JobInfo()
    assert job_info.is_available is True


if __name__ == "__main__":
    unittest.main()
