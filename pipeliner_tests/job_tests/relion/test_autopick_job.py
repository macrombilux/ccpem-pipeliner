#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    get_relion_tutorial_data,
    tutorial_data_available,
)
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import new_job_of_type, active_job_from_proc
from pipeliner.utils import touch
from pipeliner.deposition_tools.pdb_deposition_objects import (
    ParticleSelectionType,
    ParticleSelectionTypeContents,
    SoftwareType,
    SoftwareTypeContents,
)
from pipeliner.deposition_tools.pdb_deposition_objects import DEPOSITION_COMMENT
from pipeliner.nodes import (
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_IMAGE2DSTACK,
    NODE_LOGFILE,
    NODE_MLMODEL,
)

topaz = ExternalProgram("topaz")


class AutoPickTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_2Dref(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --pipeline_control "
                "AutoPick/job011/"
            ],
        )

    def test_get_command_2Dref_helical(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_helical.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --max_stddev_noise -1 "
                "--min_distance 7.62 --helix --helical_tube_outer_diameter 200 "
                "--helical_tube_kappa_max 0.1 --helical_tube_length_min -1 "
                "--gpu 0:1:2:3 --this_one_is_helical --pipeline_control"
                " AutoPick/job011/"
            ],
        )

    def test_get_command_2Dref_amyloid(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_amyloid.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --max_stddev_noise -1"
                " --min_distance 7.62 --helix --amyloid --helical_tube_outer_diameter"
                " 200 --helical_tube_kappa_max 0.1 --helical_tube_length_min -1 "
                "--gpu 0:1:2:3 --this_one_is_helical --pipeline_control "
                "AutoPick/job011/"
            ],
        )

    def test_get_command_2Dref_continue(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_continue.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --only_do_unfinished "
                "--pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_3Dref(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_3dref.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Import/job000/fake_3D_ref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 2 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job011/ "
                "--pickname autopick --ref Import/job000/fake_3D_ref.mrc --sym C1 "
                "--healpix_order 1 --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54"
                " --threshold 0 --min_distance 100 --max_stddev_noise -1 --gpu "
                "0:1 --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_3Dref_helical(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_3dref_helical.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Import/job000/fake_3D_ref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 2 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job011/ "
                "--pickname autopick --ref Import/job000/fake_3D_ref.mrc --sym C1 "
                "--healpix_order 1 --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54"
                " --threshold 0 --min_distance 100 --max_stddev_noise -1"
                " --min_distance -1.0 --helix --helical_tube_outer_diameter 200 "
                "--helical_tube_kappa_max 0.1 --helical_tube_length_min -1 --gpu "
                "0:1 --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_3Dref_relionstylename(self):
        """Name automatically converted from relionstyle relion.autopick to pipeliner
        style relion.autopick.ref3d"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_3dref.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Import/job000/fake_3D_ref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 2 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job011/ "
                "--pickname autopick --ref Import/job000/fake_3D_ref.mrc --sym C1 "
                "--healpix_order 1 --ang 5 --shrink 0 --lowpass 20 --angpix_ref 3.54"
                " --threshold 0 --min_distance 100 --max_stddev_noise -1 --gpu "
                "0:1 --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_LoG(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_LoG.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 16 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job011/ --pickname autopick --LoG --LoG_diam_min 150"
                " --LoG_diam_max 180 --shrink 0 --lowpass 20 --LoG_adjust_threshold 0"
                " --LoG_upper_threshold 5 --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_LoG_helical(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_LoG_helical.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 16 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star --odir AutoPick/job011/ "
                "--pickname autopick --LoG --LoG_diam_min 150 --LoG_diam_max 180"
                " --shrink 0 --lowpass 20 --LoG_adjust_threshold 0 "
                "--LoG_upper_threshold 5 --min_distance -1.0 --helix --amyloid "
                "--helical_tube_outer_diameter 200 --helical_tube_kappa_max 0.1 "
                "--helical_tube_length_min -1 --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_2Dref_writeFOM(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_writeFOM.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --write_fom_maps "
                "--pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_2Dref_writeFOM_continue(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_continue_writeFOM.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --write_fom_maps"
                " --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_readFOM(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_readFOM.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --read_fom_maps "
                "--pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_readFOM_continue(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_2dref_continue_readFOM.job",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --read_fom_maps "
                "--pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_jobstar(self):
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job009/class_averages.star": f"{NODE_IMAGE2DSTACK}.star",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "mpirun -n 4 relion_autopick_mpi --i "
                "CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --ref "
                "Select/job009/class_averages.star --invert --ctf --ang 5 --shrink 0 "
                "--lowpass 20 --angpix_ref 3.54 --threshold 0.05 --min_distance 100 "
                "--max_stddev_noise -1 --gpu 0:1:2:3 --pipeline_control "
                "AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_additional_args(self):
        """train topaz adding additional arguments"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_train_addargs_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "ManualPick/job002/coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}"
                ".star.relion",
            },
            output_nodes={"logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick"},
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --topaz_train --topaz_train_picks "
                "ManualPick/job002/coordinates.star --topaz_args "
                "additional_args_here --gpu  --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_train_helical(self):
        """train topaz adding additional arguments"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_train_helical_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "ManualPick/job002/coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}"
                ".star.relion",
            },
            output_nodes={"logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick"},
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --topaz_train --topaz_train_picks "
                "ManualPick/job002/coordinates.star --topaz_args additional_args_here "
                "--min_distance -1.0 --helix --helical_tube_outer_diameter 200 "
                "--helical_tube_kappa_max 0.1 --helical_tube_length_min -1 --gpu  "
                "--pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_pick_with_diameter(self):
        """Pick with topaz manually entered particle diameter"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_pick_diameter_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "TOPAZ_MODEL/is/here.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --particle_diameter 150 --topaz_extract"
                " --topaz_model TOPAZ_MODEL/is/here.sav --gpu "
                " --pipeline_control AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_pick_with_diameter_helical(self):
        """Pick with topaz manually entered particle diameter"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_pick_diameter_helical_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "TOPAZ_MODEL/is/here.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --particle_diameter 150 --topaz_extract "
                "--topaz_model TOPAZ_MODEL/is/here.sav --min_distance -1.0 --helix "
                "--helical_tube_outer_diameter 200 --helical_tube_kappa_max 0.1 "
                "--helical_tube_length_min -1 --gpu  --pipeline_control "
                "AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_pick_with_no_model(self):
        """Pick with topaz without user defined model"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_pick_nomodel_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --particle_diameter 150 --topaz_extract"
                " --gpu  --pipeline_control"
                " AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_pick_with_nr_parts(self):
        """Pick with topaz manually entered expected nr of parts"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_pick_nrparts_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "TOPAZ_MODEL/is/here.sav": f"{NODE_MLMODEL}.sav.topaz",
            },
            output_nodes={
                "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion.autopick",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --topaz_nr_particles 1003 --topaz_extract"
                " --topaz_model TOPAZ_MODEL/is/here.sav --gpu  --pipeline_control"
                " AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_train_with_coords(self):
        """train topaz using coordinates"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_train_coords_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "ManualPick/job002/coordinates.star": f"{NODE_MICROGRAPHCOORDSGROUP}"
                ".star.relion",
            },
            output_nodes={"logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick"},
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                " --odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --topaz_train --topaz_train_picks "
                "ManualPick/job002/coordinates.star --gpu  --pipeline_control "
                "AutoPick/job011/"
            ],
        )

    def test_get_command_topaz_nogpu_train(self):
        """training topaz without GPU should raise an error"""
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="AutoPick",
                jobfile="autopick_topaz_train_nogpu_job.star",
                jobnumber=11,
                input_nodes={
                    "CtfFind/job003/micrographs_ctf.star": ""
                    f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
                },
                output_nodes={
                    "autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                    "autopick",
                    "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
                },
                expected_commands=[
                    "relion_autopick --i CtfFind/job003/micrographs_ctf.star"
                    " --odir AutoPick/job011/ --pickname autopick --topaz_exe "
                    f"{topaz.exe_path} --topaz_train --topaz_train_picks "
                    "ManualPick/job002/coordinates.star --gpu  --pipeline_control "
                    "AutoPick/job011/"
                ],
            )

    def test_get_command_topaz_train_with_parts(self):
        """train topaz using particles"""
        general_get_command_test(
            jobtype="AutoPick",
            jobfile="autopick_topaz_train_parts_job.star",
            jobnumber=11,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "Select/job010/run_data.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "input_training_coords.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.autopick",
            },
            expected_commands=[
                "relion_autopick --i CtfFind/job003/micrographs_ctf.star "
                "--odir AutoPick/job011/ --pickname autopick --topaz_exe "
                f"{topaz.exe_path} --topaz_train --topaz_train_parts "
                "Select/job010/run_data.star --gpu  --pipeline_control "
                "AutoPick/job011/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_autopick_generate_display_data(self):
        get_relion_tutorial_data(["AutoPick", "CtfFind", "MotionCorr"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("AutoPick/job011/")
            dispobjs = pipeline.get_process_results_display(proc)

        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "AutoPick/job011/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
            ": 409 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "AutoPick/job011/Movies/20170629_00021_frameImage_autopick.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[1].__dict__ == {
            "title": "9191 picked particles",
            "dobj_type": "histogram",
            "bins": [8, 8, 4, 4],
            "bin_edges": [348.0, 369.0, 390.0, 411.0, 432.0],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["AutoPick/job011/autopick.star"],
            "start_collapsed": False,
            "flag": "",
        }

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_results_display_topaz_train(self):
        get_relion_tutorial_data(["AutoPick", "CtfFind", "MotionCorr", "Select"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("AutoPick/job010/")
            dispobjs = pipeline.get_process_results_display(proc)

        expected = {
            "title": "Topaz training complete",
            "dobj_type": "text",
            "display_data": "Topaz training jobs have no graphical output",
            "associated_data": ["AutoPick/job010/topaz_train.txt"],
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[0].__dict__ == expected

    def test_topaz_train_post_run_actions(self):
        ttjob = new_job_of_type("relion.autopick.topaz.train")
        odir = "AutoPick/job001/"
        ttjob.output_dir = odir
        os.makedirs(odir)
        for i in range(8):
            touch(f"{odir}model_epoch{i}.sav")
        assert ttjob.output_nodes == []
        ttjob.post_run_actions()
        assert len(ttjob.output_nodes) == 1
        assert ttjob.output_nodes[0].name == f"{odir}model_epoch7.sav"

    def test_topaz_train_post_run_actions_helical(self):
        ttjob = new_job_of_type("relion.autopick.topaz.train.helical")
        odir = "AutoPick/job002/"
        ttjob.output_dir = odir
        os.makedirs(odir)
        for i in range(10):
            touch(f"{odir}model_epoch{i}.sav")
        assert ttjob.output_nodes == []
        ttjob.post_run_actions()
        assert len(ttjob.output_nodes) == 1
        assert ttjob.output_nodes[0].name == f"{odir}model_epoch9.sav"

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_get_onedep_data_LoG(self):
        get_relion_tutorial_data(["AutoPick"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("AutoPick/job006/")

        job = active_job_from_proc(proc)
        depobjs = job.prepare_onedep_data()

        expected = [
            ParticleSelectionType(
                particle_selection=ParticleSelectionTypeContents(
                    number_selected=215,
                    reference_model=None,
                    method="Laplacian of Gaussian",
                    software=(
                        SoftwareType(
                            software_type=SoftwareTypeContents(
                                name="relion_autopick_mpi",
                                version="RELION version: 3.1.3-commit-8bbf42 ",
                                processing_details=None,
                            )
                        ),
                    ),
                    details=DEPOSITION_COMMENT,
                )
            )
        ]
        assert depobjs == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_get_onedep_data_Topaz(self):
        get_relion_tutorial_data(["AutoPick"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("AutoPick/job011/")

        job = active_job_from_proc(proc)
        depobjs = job.prepare_onedep_data()
        expected = [
            ParticleSelectionType(
                particle_selection=ParticleSelectionTypeContents(
                    number_selected=368,
                    reference_model=None,
                    method="Convolutional neural networks trained from positive and "
                    "unlabeled examples",
                    software=(
                        SoftwareType(
                            software_type=SoftwareTypeContents(
                                name="relion_autopick",
                                version="RELION version: 3.1.3-commit-8bbf42 ",
                                processing_details=None,
                            )
                        ),
                        SoftwareType(
                            software_type=SoftwareTypeContents(
                                name="topaz",
                                version="No version info available",
                                processing_details=None,
                            )
                        ),
                    ),
                    details=DEPOSITION_COMMENT,
                )
            )
        ]
        assert depobjs == expected


if __name__ == "__main__":
    unittest.main()
