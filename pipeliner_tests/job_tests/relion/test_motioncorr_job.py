#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_LOGFILE,
)
from pipeliner.user_settings import get_motioncor2_executable


class MotionCorrTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_own(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32"
                " --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own_relionstyle.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32"
                " --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_jobstar(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own_job.star",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_jobstar_nofloat16(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own_nofloat16_job.star",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_continue(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own_continue.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --only_do_unfinished --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_noDW(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own_noDW.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum"
                " -1 --use_own --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1"
                " --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref"
                " Movies/gain.mrc --gain_rot 0 --gain_flip 0 --grouping_for_ps 4"
                " --float16 --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_own_savenoDW(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_own_save_noDW.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum"
                " -1 --use_own --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1"
                " --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref"
                " Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting "
                "--save_noDW --float16 --pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_mocorr(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_mocorr.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "mpirun -n 2 relion_run_motioncorr_mpi --i "
                "Import/job001/movies.star --o MotionCorr/job002/ "
                "--first_frame_sum 1 --last_frame_sum -1 "
                f"--use_motioncor2 --motioncor2_exe {get_motioncor2_executable()}"
                " --other_motioncor2_args --mocorr_test_arg --gpu 0:1"
                " --bin_factor 1 --bfactor 150 --dose_per_frame 1.5 --preexposure 2"
                " --patch_x 5 --patch_y 5 --eer_grouping 32 --group_frames 3 "
                "--gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --save_noDW "
                "--pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_mocorr_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_mocor2_relionstyle_job.star",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o "
                "MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum 0"
                f" --use_motioncor2 --motioncor2_exe {get_motioncor2_executable()}"
                " --other_motioncor2_args some other args --gpu 0 --bin_factor 1 "
                "--bfactor 150 --dose_per_frame 1.277 --preexposure 0 --patch_x 5"
                " --patch_y 5 --eer_grouping 32 --group_frames 3 --gainref "
                "Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting "
                "--pipeline_control MotionCorr/job002/"
            ],
        )

    def test_get_command_mocorr_defectfile(self):
        general_get_command_test(
            jobtype="MotionCorr",
            jobfile="motioncorr_mocorr_defect.job",
            jobnumber=2,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
                "defect_file.mrc": f"{NODE_MICROSCOPEDATA}.mrc.defectfile",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "mpirun -n 2 relion_run_motioncorr_mpi --i "
                "Import/job001/movies.star --o MotionCorr/job002/"
                " --first_frame_sum 1 --last_frame_sum -1 "
                f"--use_motioncor2 --motioncor2_exe {get_motioncor2_executable()}"
                " --other_motioncor2_args --mocorr_test_arg --gpu 0:1"
                " --defect_file defect_file.mrc --bin_factor 1 --bfactor 150"
                " --dose_per_frame 1.5 --preexposure 2 --patch_x 5 --patch_y 5"
                " --eer_grouping 32 --group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip"
                " 0 --dose_weighting --save_noDW --pipeline_control MotionCorr/job002/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_mocorr_generate_results(self):
        get_relion_tutorial_data(["MotionCorr"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("MotionCorr/job002/")
            dispobjs = pipeline.get_process_results_display(proc)

        expfile = os.path.join(self.test_data, "ResultsFiles/mocorr_results.json")
        with open(expfile, "r") as ef:
            exp = json.load(ef)
        assert dispobjs[0].__dict__ == exp
        assert os.path.isfile(dispobjs[0].img)


if __name__ == "__main__":
    unittest.main()
