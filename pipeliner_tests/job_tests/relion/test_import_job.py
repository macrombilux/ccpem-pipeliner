#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from dataclasses import asdict

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    expected_warning,
    get_relion_tutorial_data,
    tutorial_data_available,
    check_for_relion,
    running_job,
)

from pipeliner.utils import touch
from pipeliner.job_factory import active_job_from_proc, read_job
from pipeliner.api.api_utils import write_default_jobstar, job_default_parameters_dict
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.deposition_tools.pdb_deposition_objects import DEPOSITION_COMMENT
from pipeliner.job_runner import JobRunner
from pipeliner.nodes import (
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
)

OUTPUT_NODE_GENERAL_MICS = f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
OUTPUT_NODE_MOVIES = f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
OUTPUT_NODE_COORDS_DEP = f"{NODE_MICROGRAPHCOORDSGROUP}.star.deprecated_format"


class ImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setup_import_test(self):
        dirs = [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
        ]
        for d in dirs:
            os.makedirs(d, exist_ok=True)

        files = {
            "Import/job001/": os.path.join(
                self.test_data, "Pipelines/import_job_pipeline.star"
            ),
            "MotionCorr/job002/": os.path.join(
                self.test_data, "Pipelines/motioncorr_job_pipeline.star"
            ),
            "CtfFind/job003/": os.path.join(
                self.test_data, "Pipelines/ctffind_job_pipeline.star"
            ),
        }
        for f in files:
            shutil.copy(files[f], f + "job_pipeline.star")

        import_text = (
            " ++++ Executing new job on Fri May 29 11:25:32 2020"
            "\n++++ with the following command(s):"
            "\necho importing..."
            "\nrelion_star_loopheader rlnMicrographMovieName > "
            "Import/job001/movies.star ls -rt Micrographs/*.mrc >> "
            "Import/job001/movies.star \n++++"
        )

        with open("Import/job001/note.txt", "w") as note:
            note.write(import_text)

        mocorr_text = (
            " ++++ Executing new job on Fri Jul 10 13:59:07 2020"
            "\n++++ with the following command(s):"
            "`which relion_run_motioncorr` --i Import/job001/movies.star --o "
            "MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum -1 --use_own"
            "  --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1 "
            "--preexposure 0 --patch_x 1 --patch_y 1 --group_frames 3 "
            "--gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0  "
            "--pipeline_control MotionCorr/job002/\n++++"
        )

        with open("MotionCorr/job002/note.txt", "w") as note:
            note.write(mocorr_text)

        ctffind_text = (
            " ++++ Executing new job on Fri Jul 10 14:00:58 2020"
            "\n++++ with the following command(s):"
            "\n`which relion_run_ctffind` --i "
            "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
            " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
            "--FStep 500 --dAst 100 --use_gctf --gctf_exe /public/EM"
            '/Gctf/bin/Gctf --ignore_ctffind_params --gpu "" '
            "--pipeline_control CtfFind/job003/\n ++++"
        )

        with open("CtfFind/job003/note.txt", "w") as note:
            note.write(ctffind_text)

    def test_import_movies_relionstyle_jobname(self):
        """Automatic conversion of the ambigious relion style jobname"""
        general_get_command_test(
            jobtype="Import",
            jobfile="import_movies_relionstyle.job",
            jobnumber=1,
            input_nodes={"mtf_k2_200kV.star": "MicroscopeData.star.mtf"},
            output_nodes={"movies.star": OUTPUT_NODE_MOVIES},
            expected_commands=[
                "relion_import --do_movies --optics_group_name opticsGroup1 "
                "--optics_group_mtf mtf_k2_200kV.star --angpix 0.885 --kV 200"
                " --Cs 1.4 --Q0 0.1 --beamtilt_x 0 --beamtilt_y 0 --i Movies/*.tiff"
                " --odir Import/job001/ --ofile movies.star "
                "--pipeline_control Import/job001/"
            ],
        )

    def test_import_movies_relionstyle_jobname_noMTF(self):
        """Automatic conversion of the ambigious relion style jobname"""
        general_get_command_test(
            jobtype="Import",
            jobfile="import_movies_relionstyle_nomtf.job",
            jobnumber=1,
            input_nodes={},
            output_nodes={"movies.star": OUTPUT_NODE_MOVIES},
            expected_commands=[
                "relion_import --do_movies --optics_group_name opticsGroup1 "
                "--angpix 0.885 --kV 200 --Cs 1.4 --Q0 0.1 --beamtilt_x 0 "
                "--beamtilt_y 0 --i Movies/*.tiff --odir Import/job001/ "
                "--ofile movies.star --pipeline_control Import/job001/"
            ],
        )

    def test_import_other_relionstyle_jobname(self):
        """Automatic conversion of the ambigious relion style jobname"""
        with self.assertRaises(RuntimeError):
            general_get_command_test(
                jobtype="Import",
                jobfile="import_coords_relionstyle.job",
                jobnumber=1,
                input_nodes={},
                output_nodes={"coords_suffix.box": OUTPUT_NODE_COORDS_DEP},
                expected_commands=[
                    "relion_import --do_coordinates --i mycoords/*.box "
                    "--odir Import/job001/ --ofile coords_suffix.box "
                    "--pipeline_control Import/job001/"
                ],
            )

    def test_import_movies_prepare_deposition_object(self):

        self.setup_import_test()
        # set up the import dirs
        os.makedirs("Import/job001/Movies")
        os.makedirs("Import/job001/Movies2")

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        shutil.copy(
            os.path.join(self.test_data, "movies_mrcs_2dir.star"),
            os.path.join(self.test_dir, "Import/job001/movies.star"),
        )

        # actually a particle stack, but same format and smaller
        movie_file = os.path.join(self.test_data, "20170629_00030_frameImage.mrcs")

        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        for n in range(1, 13):
            target = os.path.join(
                self.test_dir, f"Import/job001/Movies2/20170629_{n:05d}_frameImage.mrcs"
            )
            shutil.copy(movie_file, target)
        write_default_jobstar("relion.import.movies", "Import/job001/job.star")

        # Read pipeline STAR file
        with ProjectGraph() as pipeline:
            import_job = active_job_from_proc(pipeline.process_list[0])

        with expected_warning(RuntimeWarning, "overflow"):
            result = [asdict(x) for x in import_job.prepare_onedep_data()]
        expected_result = [
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies2",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; {DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies2/20170629_000*_"
                "frameImage.mrcs",
            },
            {
                "name": "Multiframe micrograph movies",
                "directory": "Import/job001/Movies",
                "category": "('T2', '')",
                "header_format": "('T2', '')",
                "data_format": "('OT', '16-bit float')",
                "num_images_or_tilt_series": 12,
                "frames_per_image": 215,
                "voxel_type": "('OT', '16-bit float')",
                "pixel_width": 0.885,
                "pixel_height": 0.885,
                "details": "Voltage 1.4; Spherical aberration 1.4; Movie data in file: "
                f"Import/job001/movies.star; {DEPOSITION_COMMENT}",
                "image_width": 64,
                "image_height": 64,
                "micrographs_file_pattern": "Import/job001/Movies/20170629_000*_"
                "frameImage.mrcs",
            },
        ]
        results = [x["multiframe_micrograph_movies"] for x in result]
        for i in expected_result:
            assert i in results
        for i in results:
            assert i in expected_result

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_import_movies_running(self):
        # copy in fake files for the movie stacks
        mn = list(range(21, 32)) + [35, 36, 37] + [39, 40] + list(range(42, 50))
        get_relion_tutorial_data("Import")
        os.makedirs("Movies")
        for n in mn:
            fn = f"Movies/20170629_{n:05d}_frameImage.tiff"
            shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), fn)

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Import/job001/")
            dispobjs = pipeline.get_process_results_display(proc)

        efile = os.path.join(self.test_data, "ResultsFiles/import_movies.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected
        assert os.path.isfile("Import/job001/Thumbnails/montage_f000.png")

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_import_micrographs_running(self):
        get_relion_tutorial_data("MotionCorr")
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = "MotionCorr/job002/corrected_micrographs.star"
        params["node_type"] = "Micrographs STAR file (.star)"
        params["is_relion"] = "True"
        proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process("Import/job032/")
            dispobjs = pipeline.get_process_results_display(impp)

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_mics.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_create_display_import_particles(self):
        get_relion_tutorial_data(["Class2D", "Extract"])
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = "Class2D/job008/run_it025_data.star"
        params["node_type"] = "Particles STAR file (.star)"
        params["is_relion"] = "Yes"
        proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process("Import/job032/")
            dispobjs = pipeline.get_process_results_display(impp)

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_parts.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available() and check_for_relion(), "Needs rln")
    def test_import_2drefs_running(self):
        get_relion_tutorial_data(["Select", "Class2D"])
        # run another import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = "Select/job009/class_averages.star"
        params["node_type"] = "2D references STAR file (.star)"
        params["is_relion"] = "Yes"
        proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process("Import/job032/")
            dispobjs = pipeline.get_process_results_display(impp)

        # check the results
        assert os.path.isfile(dispobjs[0].img)
        efile = os.path.join(self.test_data, "ResultsFiles/import_2drefs.json")
        with open(efile, "r") as exp:
            expected = json.load(exp)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_3dref_running(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = "map.mrc"
        params["pipeliner_node_type"] = "DensityMap"
        params["is_relion"] = "False"
        proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process("Import/job001/")
            dispobjs = pipeline.get_process_results_display(impp)

        assert os.path.isfile(dispobjs[0].maps[0])
        expected = {
            "maps": ["Import/job001/Thumbnails/map.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/map.mrc",
            "maps_data": "Import/job001/map.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/map.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_create_display_import_mask(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = "map.mrc"
        params["pipeliner_node_type"] = "Mask3D"
        params["is_relion"] = "No"
        proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process("Import/job001/")
            dispobjs = pipeline.get_process_results_display(impp)

        assert os.path.isfile(dispobjs[0].maps[0])
        expected = {
            "maps": ["Import/job001/Thumbnails/map.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/map.mrc",
            "maps_data": "Import/job001/map.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/map.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_halfmap_running(self):
        themap = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(themap, "map.mrc")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = "map.mrc"
        params["pipeliner_node_type"] = "DensityMap"
        params["is_relion"] = "False"
        params["kwds"] = "halfmap"
        proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process("Import/job001/")
            dispobjs = pipeline.get_process_results_display(impp)

        assert os.path.isfile(dispobjs[0].maps[0])
        expected = {
            "maps": ["Import/job001/Thumbnails/map.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/map.mrc",
            "maps_data": "Import/job001/map.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/map.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(check_for_relion() and tutorial_data_available(), "Needs rln")
    def test_import_mic_running(self):
        get_relion_tutorial_data("MotionCorr")
        mic = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = mic
        params["pipeliner_node_type"] = "Image2D"
        params["is_relion"] = "False"
        job = proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process(job.output_dir)
            dispobjs = pipeline.get_process_results_display(impp)

        assert dispobjs[0].__dict__ == {
            "title": "20170629_00021_frameImage",
            "dobj_type": "image",
            "image_path": "Import/job032/Thumbnails/20170629_00021_frameImage.png",
            "image_desc": "imported image; Import/job032/20170629_00021_frameImage.mrc;"
            " Image2D.mrc",
            "associated_data": ["Import/job032/20170629_00021_frameImage.mrc"],
            "start_collapsed": False,
            "flag": "",
        }
        assert os.path.isfile(dispobjs[0].image_path)
        assert os.path.isfile(f"Import/job032/{os.path.basename(mic)}")

    @unittest.skipUnless(check_for_relion() and tutorial_data_available(), "Needs rln")
    def test_import_coordsstar_running(self):
        get_relion_tutorial_data("MotionCorr")
        mic = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        shutil.copy(mic, "MotionCorr/job002/micrograph001.mrc")
        ap_file = os.path.join(self.test_data, "autopick.star")
        movs = "AutoPick/job006/Movies/"
        os.makedirs(movs)
        pfile = os.path.join(self.test_data, "micrograph001_autopick.star")
        shutil.copy(pfile, os.path.join(movs, "micrograph001_autopick.star"))
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = ap_file
        params["node_type"] = "RELION coordinates STAR file (.star)"
        params["is_relion"] = "True"
        job = proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process(job.output_dir)
            dispobjs = pipeline.get_process_results_display(impp)

        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Import/job032/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/micrograph001.mrc: 242 particles",
            "associated_data": [
                "MotionCorr/job002/micrograph001.mrc",
                "AutoPick/job006/Movies/micrograph001_autopick.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }
        assert os.path.isfile(dispobjs[0].image_path)
        assert os.path.isfile("Import/job032/autopick.star")

    @unittest.skipUnless(check_for_relion(), "Needs rln")
    def test_import_model_running(self):
        model = os.path.join(self.test_data, "5me2_a.pdb")
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = model
        params["pipeliner_node_type"] = "AtomCoords"
        params["is_relion"] = "No"
        job = proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process(job.output_dir)
            dispobjs = pipeline.get_process_results_display(impp)

        assert dispobjs[0].__dict__ == {
            "maps": [],
            "dobj_type": "mapmodel",
            "maps_opacity": [],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "Model: Import/job001/5me2_a.pdb",
            "maps_data": "",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": ["Import/job001/5me2_a.pdb"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert os.path.isfile("Import/job001/5me2_a.pdb")

    @unittest.skipUnless(check_for_relion() and tutorial_data_available(), "Needs rln")
    def test_import_2Dmask_running(self):
        get_relion_tutorial_data("MotionCorr")
        fake_mask = "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc"
        # run import job
        proj = PipelinerProject(make_new_project=True)
        params = job_default_parameters_dict("relion.import.other")
        params["fn_in_other"] = fake_mask
        params["pipeliner_node_type"] = "Mask2D"
        params["is_relion"] = "No"
        job = proj.run_job(jobinput=params)

        with ProjectGraph() as pipeline:
            impp = pipeline.find_process(job.output_dir)
            dispobjs = pipeline.get_process_results_display(impp)

        assert dispobjs[0].__dict__ == {
            "title": "Imported 2D mask (.mrc)",
            "dobj_type": "image",
            "image_path": "Import/job032/Thumbnails/20170629_00021_frameImage.png",
            "image_desc": "imported image; Import/job032/20170629_00021_frameImage.mrc;"
            " Mask2D.mrc",
            "associated_data": ["Import/job032/20170629_00021_frameImage.mrc"],
            "start_collapsed": False,
            "flag": "",
        }
        assert os.path.isfile(dispobjs[0].image_path)

    def test_import_raw_coords_getcommand(self):
        ctfdir = "CtfFind/job003"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        os.makedirs(ctfdir)
        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )

        os.makedirs("coords")
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.box"
            touch(os.path.join("coords", cf))

        cd = "Import/job004/CoordinateFiles"
        general_get_command_test(
            jobtype="Import",
            jobfile="coords_files_job.star",
            jobnumber=4,
            input_nodes={micsfile: f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"},
            output_nodes={"coordinates.star": "MicrographsCoords.star.relion"},
            expected_commands=[
                "mkdir Import/job004/CoordinateFiles",
                f"cp coords/20170629_00021_frameImage_coords.box {cd}",
                f"cp coords/20170629_00022_frameImage_coords.box {cd}",
                f"cp coords/20170629_00023_frameImage_coords.box {cd}",
                f"cp coords/20170629_00024_frameImage_coords.box {cd}",
                f"cp coords/20170629_00025_frameImage_coords.box {cd}",
                f"cp coords/20170629_00026_frameImage_coords.box {cd}",
                "touch Import/job004/coordinates.star",
            ],
        )

    def test_running_import_coords_with_suffix(self):
        ctfdir = "CtfFind/job003"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        os.makedirs(ctfdir)
        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        os.makedirs("coords")
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.box"
            touch(os.path.join("coords", cf))
            cfs.append(os.path.join("CoordinateFiles", cf))
        running_job(
            test_jobfile="coords_files_job.star",
            job_dir_type="Import",
            job_no=4,
            input_files=[(ctfdir, "micrographs_ctf.star")],
            expected_outfiles=["coordinates.star"] + cfs,
            sleep_time=0,
            print_run=True,
        )

    def test_running_import_coords_with_nosuffix(self):
        ctfdir = "CtfFind/job003"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        os.makedirs(ctfdir)
        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        os.makedirs("coords")
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage.box"
            touch(os.path.join("coords", cf))
            cfs.append(os.path.join("CoordinateFiles", cf))
        running_job(
            test_jobfile="coords_files_job.star",
            job_dir_type="Import",
            job_no=4,
            input_files=[(ctfdir, "micrographs_ctf.star")],
            expected_outfiles=["coordinates.star"] + cfs,
            sleep_time=0,
            print_run=True,
        )

    def test_import_coords_files_create_display_box_format(self):
        ctfdir = "CtfFind/job003"
        movdir = "MotionCorr/job002/Movies"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        for dir_ in (ctfdir, "coords", movdir):
            os.makedirs(dir_)

        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            os.path.join(movdir, "20170629_00021_frameImage.mrc"),
        )
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.box"
            touch(os.path.join("coords", cf))
            with open(os.path.join("coords", cf), "w") as f:
                f.write(f"{n}  {n}\n" * n)
            cfs.append(os.path.join("CoordinateFiles", cf))

        with ProjectGraph(create_new=True) as pipeline:
            job_runner = JobRunner(pipeline)
            pipeline.job_counter = 4
            job = read_job(
                os.path.join(self.test_data, "JobFiles/Import/coords_files_job.star")
            )
            job_runner.run_job(
                job=job,
                current_proc=None,
                is_main_continue=False,
                is_scheduled=False,
            )

        exp_image = {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Import/job004/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc:"
            " 21 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "Import/job004/CoordinateFiles/20170629_00021_frameImage_coords.box",
            ],
            "start_collapsed": False,
            "flag": "",
        }

        with open("Import/job004/.results_display001_image.json") as imgfile:
            actual_image = json.load(imgfile)
        assert exp_image == actual_image

        exp_histo = {
            "title": "141 picked particles",
            "dobj_type": "histogram",
            "bins": [3, 3],
            "bin_edges": [21.0, 23.5, 26.0],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["Import/job004/coordinates.star"],
            "start_collapsed": False,
            "flag": "",
        }

        with open("Import/job004/.results_display000_histogram.json") as hf:
            actual_histo = json.load(hf)
        assert exp_histo == actual_histo

    def test_import_coords_files_create_display_star_format(self):
        ctfdir = "CtfFind/job003"
        movdir = "MotionCorr/job002/Movies"
        micsfile = f"{ctfdir}/micrographs_ctf.star"
        for dir_ in (ctfdir, "coords", movdir):
            os.makedirs(dir_)

        shutil.copy(
            os.path.join(self.test_data, "corrected_micrographs.star"), micsfile
        )
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            os.path.join(movdir, "20170629_00021_frameImage.mrc"),
        )
        cfs = []
        for n in range(21, 27):
            cf = f"20170629_000{n}_frameImage_coords.star"
            touch(os.path.join("coords", cf))
            with open(os.path.join("coords", cf), "w") as f:
                f.write("data_\nloop_\n_rlnCoordinateX\n _rlnCoordinateY\n")
                f.write(f"{n}  {n}\n" * n)
            cfs.append(os.path.join("CoordinateFiles", cf))

        with ProjectGraph(create_new=True) as pipeline:
            job_runner = JobRunner(pipeline)
            pipeline.job_counter = 4
            job = read_job(
                os.path.join(
                    self.test_data, "JobFiles/Import/coords_files_star_job.star"
                )
            )
            job_runner.run_job(
                job=job,
                current_proc=None,
                is_main_continue=False,
                is_scheduled=False,
            )

        exp_image = {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "Import/job004/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc:"
            " 21 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "Import/job004/CoordinateFiles/20170629_00021_frameImage_coords.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }

        with open("Import/job004/.results_display001_image.json") as imgfile:
            actual_image = json.load(imgfile)
        assert exp_image == actual_image

        exp_histo = {
            "title": "141 picked particles",
            "dobj_type": "histogram",
            "bins": [3, 3],
            "bin_edges": [21.0, 23.5, 26.0],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["Import/job004/coordinates.star"],
            "start_collapsed": False,
            "flag": "",
        }

        with open("Import/job004/.results_display000_histogram.json") as hf:
            actual_histo = json.load(hf)
        assert exp_histo == actual_histo


if __name__ == "__main__":
    unittest.main()
