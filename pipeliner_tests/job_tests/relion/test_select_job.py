#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    tutorial_data_available,
    get_relion_tutorial_data,
    general_get_command_test,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_PROCESSDATA,
    NODE_PARTICLESDATA,
)


class SelectTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def print_coms(self, commands, expected_commands):
        """Compare two commands and find the differences"""
        print("\n" + commands)
        print(expected_commands)
        compare = ""
        for i in range(len(commands)):
            try:
                if commands[i] == expected_commands[i]:
                    compare += " "
                else:
                    compare += "*"
            except IndexError:
                pass
        print(compare)

    def test_get_command_select_mics_on_val(self):
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_by_val.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_select_mics_on_val_relionstyle_name(self):
        """Test conversion of ambiguous relion style name"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_by_val_relionstyle.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_select_jobstar(self):
        general_get_command_test(
            jobtype="Select",
            jobfile="select_job.star",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_discard_mics(self):
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_discard.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --discard_on_stats "
                "--discard_label rlnCtfFigureOfMerit --discard_sigma 4"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_discard_mics_relionstyle_jobname(self):
        """Check ambiguous jobname is converted"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_discard_relionstyle.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --discard_on_stats "
                "--discard_label rlnCtfFigureOfMerit --discard_sigma 4"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_mics_regroup_error(self):
        """Ignore illegal regrouping of selected mics"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_regroup_error.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star "
                "--o Select/job010/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_split_mics_defined_number(self):
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_split_defined.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --split --size_split 100"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_split_mics_nr_groups(self):
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_split_nr.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split2.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split3.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split4.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --split --nr_split 4"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_split_mics_nr_groups_relionstyle_name(self):
        """Check interpreting ambiguous jobname"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_split_relionstyle.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split2.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split3.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split4.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --split --nr_split 4"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_split_parts_nr_groups(self):
        general_get_command_test(
            jobtype="Select",
            jobfile="select_parts_split_nr.job",
            jobnumber=10,
            input_nodes={
                "Extract/job300/particles.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={
                "particles_split1.star": f"{NODE_PARTICLESDATA}.star.relion",
                "particles_split2.star": f"{NODE_PARTICLESDATA}.star.relion",
                "particles_split3.star": f"{NODE_PARTICLESDATA}.star.relion",
                "particles_split4.star": f"{NODE_PARTICLESDATA}.star.relion",
            },
            expected_commands=[
                "relion_star_handler --i Extract/job300/particles.star"
                " --o Select/job010/particles.star --split --nr_split 4"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_split_parts_defined_number(self):
        """split particles into n parts per subset"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_mics_split_defined.job",
            jobnumber=10,
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job010/micrographs.star --split --size_split 100"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_remove_duplicates(self):
        """remove duplicate for particles"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_parts_remove_dups.job",
            jobnumber=10,
            input_nodes={
                "Extract/job300/particles.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_star_handler --i Extract/job300/particles.star"
                " --o Select/job010/particles.star --remove_duplicates 30"
                " --pipeline_control Select/job010/"
            ],
        )

    def test_get_command_interactive_from_model(self):
        """get particles from model file with regrouping and recentering"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_averages_job.star",
            jobnumber=8,
            input_nodes={
                "Class2D/job006/run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star"
                ".relion."
                "optimiser"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
            },
            expected_commands=[
                "relion_display --gui --i Class2D/job006/run_it025_optimiser.s"
                "tar --allow_save --fn_parts Select/job008/particles.star --fn_imgs "
                "Select/job008/class_averages.star --recenter --regroup 60 "
                "--pipeline_control Select/job008/"
            ],
        )

    def test_get_command_select_parts_by_class(self):
        """get particles from data file by class"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_parts_by_class.job",
            jobnumber=8,
            input_nodes={
                "Class3D/job002/run_it025_data.star": f"{NODE_PARTICLESDATA}.star."
                "relion"
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_star_handler --i Class3D/job002/run_it025_data.star"
                " --o Select/job008/particles.star --select rlnCtfFigureOfMerit "
                "--minval 3.0 --maxval 3.0 --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_model_norecenter(self):
        """get particles from model file with regrouping and no recentering"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_averages_norecenter_job.star",
            jobnumber=8,
            input_nodes={
                "Class2D/job006/run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star."
                "relion."
                "optimiser"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
            },
            expected_commands=[
                "relion_display --gui --i Class2D/job006/run_it025_optimiser.s"
                "tar --allow_save --fn_parts Select/job008/particles.star --fn_imgs "
                "Select/job008/class_averages.star --regroup 60 "
                "--pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_model_norecenter_noregroup(self):
        """get particles from model file with
        regrouping and no recentering or regrouping"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_averages_norecenter_noregroup_job.star",
            jobnumber=8,
            input_nodes={
                "Class2D/job006/run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star."
                "relion.optimiser"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
            },
            expected_commands=[
                "relion_display --gui --i Class2D/job006/run_it025_optimiser.s"
                "tar --allow_save --fn_parts Select/job008/particles.star --fn_imgs "
                "Select/job008/class_averages.star "
                "--pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_mics(self):
        """get micrographs from mics file"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_micrographs_job.star",
            jobnumber=8,
            input_nodes={
                "CtfFind/job001/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i CtfFind/job001/micrographs_ctf.star"
                " --allow_save --fn_imgs Select/job008/micrographs.star"
                " --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_mics_regroup(self):
        """get micrographs from mics file ignoring illegal regrouping"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_micrographs_regroup_job.star",
            jobnumber=8,
            input_nodes={
                "CtfFind/job001/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i CtfFind/job001/micrographs_ctf.star"
                " --allow_save --fn_imgs Select/job008/micrographs.star"
                " --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_parts(self):
        """get particles from data file"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_parts_job.star",
            jobnumber=8,
            input_nodes={
                "Refine3D/job010/run_data.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_display --gui --i Refine3D/job010/run_data.star"
                " --allow_save --fn_imgs Select/job008/particles.star"
                " --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_parts_relionstyle_jobname(self):
        """get particles from data file, job has ambiguous relion style name"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_parts_relionstyle_job.star",
            jobnumber=8,
            input_nodes={
                "Refine3D/job010/run_data.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_display --gui --i Refine3D/job010/run_data.star"
                " --allow_save --fn_imgs Select/job008/particles.star"
                " --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_parts_regroup(self):
        """get particles from data file ignoring illegal regrouping"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_parts_regroup_job.star",
            jobnumber=8,
            input_nodes={
                "Refine3D/job010/run_data.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_display --gui --i Refine3D/job010/run_data.star"
                " --allow_save --fn_imgs Select/job008/particles.star"
                " --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_interactive_from_model_class3d_norecenter(self):
        """get particles from model file with regrouping and no recentering"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_interactive_averages_class3d_norecenter_job.star",
            jobnumber=8,
            input_nodes={
                "Class3D/job006/run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star."
                "relion.optimiser"
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_display --gui --i "
                "Class3D/job006/run_it025_optimiser.star"
                " --allow_save --fn_parts Select/job008/particles.star"
                " --regroup 60 --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_autorank_relionstyle_jobname(self):
        """get particles from auto rank,
        make sure ambiguous relion style job name is read correctly"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_autorank_relionstyle_job.star",
            jobnumber=8,
            input_nodes={
                "test_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.optimiser."
                "class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.optimiser."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job008/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--python python --fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_autorank(self):
        """get particles from auto rank default jobstar file"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_class2dauto_job.star",
            jobnumber=8,
            input_nodes={
                "test_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.optimiser."
                "class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.optimiser."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job008/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--python python --fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --pipeline_control Select/job008/"
            ],
        )

    def test_get_command_autorank_with_additional_arg(self):
        """get particles from auto rank default jobstar file"""
        general_get_command_test(
            jobtype="Select",
            jobfile="select_class2dauto_addarg_job.star",
            jobnumber=8,
            input_nodes={
                "test_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.optimiser."
                "class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.optimiser."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job008/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--python python --fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --extra arg --pipeline_control "
                "Select/job008/"
            ],
        )

    def test_get_command_autorank_ambiguous_relionstyle_jobname(self):
        """get particles from auto rank with recentering and regrouping,
        raises error because job name cannot be interpreted"""
        with self.assertRaises(RuntimeError):
            general_get_command_test(
                jobtype="Select",
                jobfile="select_relionstyle_none_job.star",
                jobnumber=8,
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_split_mics(self):
        get_relion_tutorial_data(["Select", "MotionCorr"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Select/job005/")
            dispobjs = pipeline.get_process_results_display(proc)

        expected = [
            {
                "title": "Select/job005/micrographs_split1.star; 4/10 images",
                "dobj_type": "montage",
                "xvalues": [0, 1, 2, 3],
                "yvalues": [0, 0, 0, 0],
                "labels": [
                    "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00022_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00023_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00024_frameImage.mrc",
                ],
                "associated_data": ["Select/job005/micrographs_split1.star"],
                "img": "Select/job005/Thumbnails/montage_f000.png",
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Select/job005/micrographs_split2.star; 4/10 images",
                "dobj_type": "montage",
                "xvalues": [0, 1, 2, 3],
                "yvalues": [0, 0, 0, 0],
                "labels": [
                    "MotionCorr/job002/Movies/20170629_00031_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00035_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00036_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00037_frameImage.mrc",
                ],
                "associated_data": ["Select/job005/micrographs_split2.star"],
                "img": "Select/job005/Thumbnails/montage_f001.png",
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Select/job005/micrographs_split3.star; 4/4 images",
                "dobj_type": "montage",
                "xvalues": [0, 1, 2, 3],
                "yvalues": [0, 0, 0, 0],
                "labels": [
                    "MotionCorr/job002/Movies/20170629_00046_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00047_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00048_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00049_frameImage.mrc",
                ],
                "associated_data": ["Select/job005/micrographs_split3.star"],
                "img": "Select/job005/Thumbnails/montage_f002.png",
                "start_collapsed": False,
                "flag": "",
            },
        ]

        for n, do in enumerate(dispobjs):
            assert do.__dict__ == expected[n]

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_select3dclasses(self):
        get_relion_tutorial_data(["Select", "MotionCorr", "Class3D", "Extract"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Select/job017/")
            dispobjs = pipeline.get_process_results_display(proc)

        assert dispobjs[0].__dict__ == {
            "title": "Selected 3D classes",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": ["class004:x", "class004:y", "class004:z"],
            "associated_data": [
                "Select/job017/Thumbnails/run_it025_class004_slices.mrcs"
            ],
            "img": "Select/job017/Thumbnails/montage_m000.png",
            "start_collapsed": False,
            "flag": "",
        }

        resultfile = os.path.join(self.test_data, "ResultsFiles/select_class3d.json")
        with open(resultfile, "r") as res:
            expected = json.load(res)
        assert dispobjs[1].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_select_2dauto(self):
        get_relion_tutorial_data(["Select", "Extract", "Class2D"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Select/job009/")
            dispobjs = pipeline.get_process_results_display(proc)

        assert dispobjs[0].__dict__ == {
            "title": "Selected class averages",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "Class 23: 0.680323",
                "Class 39: 0.644231",
                "Class 40: 0.735355",
                "Class 43: 0.748991",
                "Class 46: 0.58601",
            ],
            "associated_data": ["Select/job009/class_averages.star"],
            "img": "Select/job009/Thumbnails/montage_s000.png",
            "start_collapsed": False,
            "flag": "",
        }

        do_file = os.path.join(self.test_data, "ResultsFiles/select_2dauto.json")
        with open(do_file, "r") as doe:
            expected = json.load(doe)
        assert dispobjs[1].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_select_2dauto_uneven(self):
        get_relion_tutorial_data(["Select", "Extract", "Class2D"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Select/job014/")
            dispobjs = pipeline.get_process_results_display(proc)

        cf = "ResultsFiles/select_2dauto_uneven_classes.json"
        classes_file = os.path.join(self.test_data, cf)
        with open(classes_file, "r") as doe:
            expected = json.load(doe)
        assert dispobjs[0].__dict__ == expected

        pf = "ResultsFiles/select_2dauto_uneven_parts.json"
        parts_file = os.path.join(self.test_data, pf)
        with open(parts_file, "r") as doe:
            expected = json.load(doe)
        assert dispobjs[1].__dict__ == expected

    # TODO: Need tests for
    #  - Interactive selection of 2d classes
    #  - split particles


if __name__ == "__main__":
    unittest.main()
