#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
    expected_warning,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc, read_job
from pipeliner.deposition_tools.pdb_deposition_objects import (
    SoftwareTypeContents,
    Final2DClassificationTypeContents,
    Final2DClassificationType,
    SoftwareType,
)
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EmpiarParticlesTypeContents,
    EmpiarParticlesType,
)
from pipeliner.deposition_tools.pdb_deposition_objects import DEPOSITION_COMMENT


class Class2DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        jobfile = os.path.join(self.test_data, "JobFiles/Class2D/class2D_basic.job")
        job = read_job(jobfile)

        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

        job = read_job(jobfile)
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_class2D_basic_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_basic.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job008/run "
                "--dont_combine_weights_via_disc --preread_images --pool 30 "
                "--pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent --zero_mask"
                " --center_classes --oversampling 1 --psi_step 12.0 --offset_range 5"
                " --offset_step 4.0 --norm --scale --j 6 --gpu 0:1:2:3"
                " --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_basic_jobstar_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_job.star",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job008/run "
                "--dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent "
                "--zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control "
                "Class2D/job008/"
            ],
        )

    def test_get_command_class2D_scratch_comb_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_scratch_comb.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job008/run "
                "--scratch_dir Fake_scratch_dir --pool 30 --pad 2 --ctf "
                "--iter 25 --tau2_fudge 2 --particle_diameter 200 --K 50 "
                "--flatten_solvent --zero_mask --center_classes --oversampling 1"
                " --psi_step 12.0"
                " --offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_corsesample_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_coarsesample.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent --zero_mask"
                " --center_classes --oversampling 1 --psi_step 12.0 "
                "--offset_range 5 --offset_step 4.0"
                " --allow_coarser_sampling --norm --scale --j 6 --gpu 0:1:2:3 "
                "--pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_grad_nozero(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_grad_nozero.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it200_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "relion_refine --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 200 --grad "
                "--class_inactivity_threshold 0.1 --grad_write_iter 10 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control"
                " Class2D/job008/"
            ],
        )

    def test_get_command_class2D_higres10_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_highres10.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --strict_highres_exp 10.0 --center_classes"
                " --oversampling 1 --psi_step 12.0 "
                "--offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --high_res_10_this_one --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_helical_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_helical.job",
            jobnumber=8,
            input_nodes={
                "Extract/job007/particles.star": "ParticlesData.star.relion."
                "helicalsegments"
            },
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion."
                "helicalsegments.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --helical_outer_diameter 200 --bimodal_psi --sigma_psi 2.0"
                " --helix --helical_rise_initial 4.75 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_helical_vdam(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_helical_vdam.job",
            jobnumber=8,
            input_nodes={
                "Extract/job007/particles.star": "ParticlesData.star.relion."
                "helicalsegments"
            },
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it200_data.star": "ParticlesData.star.relion."
                "helicalsegments.class2d",
            },
            expected_commands=[
                "relion_refine --i Extract/job007/particles.star --o "
                "Class2D/job008/run --dont_combine_weights_via_disc "
                "--preread_images --pool 30 --pad 2 --ctf --iter 200 "
                "--grad --class_inactivity_threshold 0.1 --grad_write_iter "
                "10 --tau2_fudge 2 --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --center_classes --oversampling 1 --psi_step 12.0 "
                "--offset_range 5 --offset_step 4.0 --helical_outer_diameter 200 "
                "--bimodal_psi --sigma_psi 2.0 --helix --helical_rise_initial 4.75 "
                "--norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_helical_norestrict_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_helical_norestrict.job",
            jobnumber=8,
            input_nodes={
                "Extract/job007/particles.star": "ParticlesData.star.relion."
                "helicalsegments"
            },
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion."
                "helicalsegments.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50"
                " --flatten_solvent --zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --helical_outer_diameter 200 --bimodal_psi --sigma_psi 2.0"
                " --norm --scale --j 6 --gpu 0:1:2:3 --pipeline_control "
                "Class2D/job008/"
            ],
        )

    def test_get_command_class2D_nogpu_nompi_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_nogpu_nompi.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "relion_refine --i Extract/job007/particles.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --ctf --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --K 50 --flatten_solvent"
                " --zero_mask --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --norm --scale --j 6 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_continue_em(self):
        general_get_command_test(
            jobtype="Class2D",
            jobfile="class2D_continue.job",
            jobnumber=8,
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi "
                "--continue run_it023_optimiser.star"
                " --o Class2D/job008/run --dont_combine_weights_via_disc"
                " --preread_images --pool 30 --pad 2 --iter 25 --tau2_fudge 2"
                " --particle_diameter 200 --center_classes"
                " --oversampling 1 --psi_step 12.0 --offset_range 5 --offset_step 4.0"
                " --j 6 --gpu 0:1:2:3 --pipeline_control Class2D/job008/"
            ],
        )

    def test_get_command_class2D_continue_badfile_em(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="Class2D",
                jobfile="class2D_continue_badfile.job",
                jobnumber=8,
                input_nodes=1,
                output_nodes=2,
                expected_commands="",
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_cl2d(self):
        get_relion_tutorial_data(["Class2D"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Class2D/job008/")
            with expected_warning(RuntimeWarning, "divide", nwarn=1):
                dispobjs = pipeline.get_process_results_display(proc)

        efile = os.path.join(self.test_data, "ResultsFiles/class2d_results.json")
        with open(efile, "r") as ef:
            expected = json.load(ef)
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_get_onedep_data(self):
        get_relion_tutorial_data(["Class2D", "Extract"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("Class2D/job008/")
            job = active_job_from_proc(proc)

        depobjs = job.prepare_onedep_data()

        expected_depobjs = [
            EmpiarParticlesType(
                particle_images=EmpiarParticlesTypeContents(
                    name="Particle images",
                    directory="Extract/job007/Movies",
                    category="('T5', '')",
                    header_format="('T2', '')",
                    data_format="('OT', '16-bit float')",
                    num_images_or_tilt_series=10,
                    frames_per_image=1,
                    voxel_type="('OT', '16-bit float')",
                    pixel_width=0.885,
                    pixel_height=0.885,
                    details="2377 total particles; Voltage 1.4; Spherical "
                    "aberration 1.4; Image data in file: "
                    "Class2D/job008/run_it025_data.star; "
                    f"{DEPOSITION_COMMENT}",
                    image_width=64,
                    image_height=64,
                    micrographs_file_pattern="Extract/job007/Movies/20170629_000*_"
                    "frameImage.mrcs",
                    picked_particles_file_pattern="Class2D/job008/run_it025_data.star",
                )
            ),
            Final2DClassificationType(
                final_two_d_classification=Final2DClassificationTypeContents(
                    number_classes=50,
                    average_number_members_per_class=48,
                    software_list=(
                        SoftwareType(
                            software_type=SoftwareTypeContents(
                                name="relion_refine_mpi",
                                version="RELION version: 3.1.3-commit-8bbf42 ",
                                processing_details=DEPOSITION_COMMENT,
                            )
                        ),
                    ),
                    details=f"{DEPOSITION_COMMENT}",
                ),
            ),
        ]

        assert depobjs[0] == expected_depobjs[0]
        assert depobjs[1] == expected_depobjs[1]


if __name__ == "__main__":
    unittest.main()
