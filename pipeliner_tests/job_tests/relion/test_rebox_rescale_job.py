#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.job_testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import ResultsDisplayMapModel


class ReboxRescaleJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_no_rescale(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_reboxed.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --new_box 100"
                " --o ReboxRescale/job999/test_reboxed.mrc"
                " --pipeline_control ReboxRescale/job999/"
            ],
        )

    def test_get_commands_rebox_rescale(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_rescale_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_reboxed_rescaled.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --new_box 100 --rescale_angpix 1.68"
                " --o ReboxRescale/job999/test_reboxed_rescaled.mrc"
                " --pipeline_control ReboxRescale/job999/"
            ],
        )

    def test_get_commands_rescale(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rescale_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_rescaled.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --rescale_angpix 1.68"
                " --o ReboxRescale/job999/test_rescaled.mrc"
                " --pipeline_control ReboxRescale/job999/"
            ],
        )

    def test_creating_results_display_rebox(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdo = ResultsDisplayMapModel(
            title="Map reboxed to 100^2 px",
            start_collapsed=False,
            associated_data=["ReboxRescale/job999/test_reboxed.mrc"],
            maps=["ReboxRescale/job999/Thumbnails/test_reboxed.mrc"],
            maps_data="ReboxRescale/job999/test_reboxed.mrc",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_job.star"
            ),
            expected_display_objects=[(expected_rdo, None)],
            files_to_create={"test_reboxed.mrc": mrc_file},
        )

    def test_creating_results_display_rescale(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdo = ResultsDisplayMapModel(
            title="Map rescaled to 1.68 \u212B/px",
            start_collapsed=False,
            associated_data=["ReboxRescale/job999/test_rescaled.mrc"],
            maps=["ReboxRescale/job999/Thumbnails/test_rescaled.mrc"],
            maps_data="ReboxRescale/job999/test_rescaled.mrc",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rescale_job.star"
            ),
            expected_display_objects=[(expected_rdo, None)],
            files_to_create={"test_rescaled.mrc": mrc_file},
            print_dispobj=True,
        )

    def test_creating_results_display_rebox_rescale(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdo = ResultsDisplayMapModel(
            title="Map reboxed to 100^2 px rescaled to 1.68 \u212B/px",
            start_collapsed=False,
            associated_data=["ReboxRescale/job999/test_reboxed_rescaled.mrc"],
            maps=["ReboxRescale/job999/Thumbnails/test_reboxed_rescaled.mrc"],
            maps_data="ReboxRescale/job999/test_reboxed_rescaled.mrc",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_rescale_job.star"
            ),
            expected_display_objects=[(expected_rdo, None)],
            files_to_create={"test_reboxed_rescaled.mrc": mrc_file},
        )


if __name__ == "__main__":
    unittest.main()
