#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.job_factory import new_job_of_type
from pipeliner_tests.generic_tests import general_get_command_test
from pipeliner_tests import test_data


class PlugInsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_reconstruct_plugin(self):
        general_get_command_test(
            jobtype="Reconstruct",
            jobfile="reconstruct_plugin_job.star",
            jobnumber=2,
            input_nodes={"test_particles.star": "ParticlesData.star.relion"},
            output_nodes={"reconstruction.mrc": "DensityMap.mrc.relion.reconstruct"},
            expected_commands=[
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job002/reconstruction.mrc --angpix 1.07"
                " --sym C1 --pipeline_control Reconstruct/job002/"
            ],
        )

    def test_get_command_reconstruct_mpi_plugin(self):
        general_get_command_test(
            jobtype="Reconstruct",
            jobfile="reconstruct_plugin_mpi_job.star",
            jobnumber=2,
            input_nodes={"test_particles.star": "ParticlesData.star.relion"},
            output_nodes={"reconstruction.mrc": "DensityMap.mrc.relion.reconstruct"},
            expected_commands=[
                "mpirun -n 8 relion_reconstruct_mpi "
                "--i test_particles.star --o "
                "Reconstruct/job002/reconstruction.mrc --angpix 1.07"
                " --sym C1 --pipeline_control Reconstruct/job002/"
            ],
        )

    def test_get_command_reconstruct_plugin_with_no_angpix_and_maxres(self):
        general_get_command_test(
            jobtype="Reconstruct",
            jobfile="reconstruct_plugin_angpx_maxres_job.star",
            jobnumber=2,
            input_nodes={"test_particles.star": "ParticlesData.star.relion"},
            output_nodes={"reconstruction.mrc": "DensityMap.mrc.relion.reconstruct"},
            expected_commands=[
                "relion_reconstruct --i test_particles.star --o "
                "Reconstruct/job002/reconstruction.mrc --maxres 2.0"
                " --sym C1 --pipeline_control Reconstruct/job002/"
            ],
        )

    def test_create_results_display(self):
        mrc = os.path.join(self.test_data, "emd_3488.mrc")
        os.makedirs("Reconstruct/job001")
        shutil.copy(mrc, "Reconstruct/job001/reconstruction.mrc")
        job = new_job_of_type("relion.reconstruct")
        job.output_dir = "Reconstruct/job001/"
        dispobj = job.create_results_display()
        expected = {
            "maps": ["Reconstruct/job001/reconstruction.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Reconstructed map",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["Reconstruct/job001/reconstruction.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobj[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
