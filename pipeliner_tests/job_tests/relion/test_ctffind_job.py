#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    get_relion_tutorial_data,
    tutorial_data_available,
)

from pipeliner.project_graph import ProjectGraph
from pipeliner.nodes import NODE_MICROGRAPHGROUPMETADATA, NODE_LOGFILE
from pipeliner.user_settings import get_ctffind_executable, get_gctf_executable


class CtfFindTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_ctffind(self):
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_ctffind_run.job",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
                ".ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_ctffind_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style job name"""
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_ctffind_run_relionstyle.job",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_ctffind_jobstar(self):
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_job.star",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_ctffind_continue_jobstar(self):
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_continue_job.star",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --only_do_unfinished --pipeline_control CtfFind/job003/"
            ],
        )

    def test_is_continue_works(self):
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_continue_run.job",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --only_do_unfinished --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_gctf(self):
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_gctf_run.job",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --use_gctf True --gctf_exe"
                f" {get_gctf_executable()} --ignore_ctffind_params --gpu 0:1"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_gctf_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style job name"""
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_gctf_run_relionstyle.job",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --use_gctf True --gctf_exe"
                f" {get_gctf_executable()} --ignore_ctffind_params --gpu 0:1"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_nonmpi(self):
        general_get_command_test(
            jobtype="CtfFind",
            jobfile="ctffind_nompi_run.job",
            jobnumber=3,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "relion_run_ctffind --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job003/"
            ],
        )

    def test_get_command_nofile(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="CtfFind",
                jobfile="ctffind_ctffind_nofile.job",
                jobnumber=3,
                input_nodes=1,
                output_nodes=2,
                expected_commands=" ",
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctffind_generate_display_data(self):
        get_relion_tutorial_data(["CtfFind", "MotionCorr"])

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("CtfFind/job003/")
            dispobjs = pipeline.get_process_results_display(proc)

        expected = [
            {
                "title": "Defocus",
                "dobj_type": "histogram",
                "bins": [4, 7, 6, 4, 3],
                "bin_edges": [
                    0.813312,
                    0.9096186,
                    1.0059252,
                    1.1022318,
                    1.1985384,
                    1.294845,
                ],
                "xlabel": "Defocus (uM)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Astigmatism",
                "dobj_type": "histogram",
                "bins": [1, 0, 0, 0, 0, 0, 2, 8, 7, 6],
                "bin_edges": [
                    51.68,
                    54.569,
                    57.458,
                    60.346999999999994,
                    63.236,
                    66.125,
                    69.014,
                    71.90299999999999,
                    74.792,
                    77.681,
                    80.57,
                ],
                "xlabel": "Astigmatism (Angstrom)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Resolution of good Thon ring fit",
                "dobj_type": "histogram",
                "bins": [8, 6, 5, 2, 2, 0, 0, 0, 0, 0, 0, 1],
                "bin_edges": [
                    3.0,
                    3.15,
                    3.3,
                    3.45,
                    3.6,
                    3.75,
                    3.9,
                    4.05,
                    4.2,
                    4.35,
                    4.5,
                    4.65,
                    4.8,
                ],
                "xlabel": "Resolution (Angstrom)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Per-micrograph defocus",
                "dobj_type": "graph",
                "xvalues": [
                    [
                        1.082769,
                        0.796313,
                        1.038126,
                        0.958674,
                        1.119836,
                        0.979985,
                        0.8923639999999999,
                        1.0365280000000001,
                        1.275936,
                        0.9805530000000001,
                        1.135205,
                        1.057579,
                        1.074288,
                        1.212013,
                        1.1169040000000001,
                        1.081931,
                        0.8828370000000001,
                        0.8101470000000001,
                        0.980125,
                        0.980858,
                        0.882692,
                        0.970859,
                        1.2778049999999999,
                        1.1828,
                    ]
                ],
                "xaxis_label": "Defocus x (μM)",
                "xrange": [0.796313, 1.2778049999999999],
                "yvalues": [
                    [
                        1.111626,
                        0.830311,
                        1.0656,
                        0.983651,
                        1.148599,
                        1.013594,
                        0.929225,
                        1.0678040000000002,
                        1.3065129999999998,
                        1.0159639999999999,
                        1.169346,
                        1.086415,
                        1.103049,
                        1.212013,
                        1.145648,
                        1.108983,
                        0.911666,
                        0.834485,
                        1.010842,
                        1.0122799999999998,
                        0.9144610000000001,
                        1.007535,
                        1.311885,
                        1.210985,
                    ]
                ],
                "yaxis_label": "Defocus y (μM)",
                "yrange": [0.830311, 1.311885],
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "data_series_labels": ["All micrographs"],
                "modes": ["markers"],
                "start_collapsed": False,
                "flag": "",
            },
        ]

        for i in dispobjs:
            if i not in expected:
                print(i.__dict__, expected[-1])
            assert i.__dict__ in expected

        dicts = [x.__dict__ for x in dispobjs]
        for i in expected:
            assert i in dicts

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_ctffind_generate_display_data_nologfiles(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        tdata = os.getenv("PIPELINER_TUTORIAL_DATA")
        get_relion_tutorial_data(["MotionCorr"])
        os.makedirs("CtfFind/job003/Movies")
        shutil.copy(
            os.path.join(tdata, "CtfFind/job003/job.star"), "CtfFind/job003/job.star"
        )

        with ProjectGraph() as pipeline:
            proc = pipeline.find_process("CtfFind/job003/")
            dispobjs = pipeline.get_process_results_display(proc)

        assert dispobjs[0].__dict__ == {
            "title": "Results pending...",
            "dobj_type": "pending",
            "message": "The result not available yet",
            "reason": "No log files found",
            "start_collapsed": False,
            "flag": "",
        }


if __name__ == "__main__":
    unittest.main()
