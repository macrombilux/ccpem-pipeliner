#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import subprocess

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    do_slow_tests,
    check_for_relion,
    general_get_command_test,
    running_job,
)
from pipeliner.job_factory import new_job_of_type

do_full = do_slow_tests()
has_relion = check_for_relion()

has_relion4 = False
if has_relion:
    relion_vers = subprocess.run(
        ["relion", "--version"], stdout=subprocess.PIPE, text=True
    )
    if "RELION version: 4" in relion_vers.stdout:
        has_relion4 = True


class ReprojectJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_project_plugin(self):
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Reproject/reproj_angles.star"),
            self.test_dir,
        )
        general_get_command_test(
            jobtype="Reproject",
            jobfile="project_plugin_job.star",
            jobnumber=3,
            input_nodes={
                "emd_3488.mrc": "DensityMap.mrc",
                "reproj_angles.star": "ProcessData.star.relion.eulerangles",
            },
            output_nodes={
                "reproj.mrcs": "Image2DStack.mrc.relion.projections",
                "reproj.star": "ProcessData.star.relion.eulerangles",
            },
            expected_commands=[
                "relion_project --i emd_3488.mrc --ang reproj_angles.star"
                " --angpix 1.07 --o Reproject/job003/reproj --pipeline_control "
                "Reproject/job003/"
            ],
        )

    # relion_project seems to have a bug in relion 3.1; it's sorted in relion 4.0
    @unittest.skipUnless(
        do_full and has_relion and has_relion4, "Slow test: Only runs in full unittest"
    )
    def test_running_projection_job(self):

        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Reproject/reproj_angles.star"),
            self.test_dir,
        )
        running_job(
            test_jobfile="project_plugin_job.star",
            job_dir_type="Reproject",
            job_no=3,
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "reproj.mrcs",
                "reproj.star",
            ),
            sleep_time=1,
        )

    def test_reproject_results_display(self):
        stack = os.path.join(self.test_data, "image_stack.mrcs")
        star = os.path.join(self.test_data, "reproj.star")
        jobdir = os.path.join("Reproject/job007")
        os.makedirs(jobdir)
        shutil.copy(stack, os.path.join(jobdir, "reproj.mrcs"))
        shutil.copy(star, os.path.join(jobdir, "reproj.star"))
        rp_job = new_job_of_type("relion.reproject")
        rp_job.output_dir = jobdir + "/"
        results = rp_job.create_results_display()

        exp_file = os.path.join(self.test_data, "ResultsFiles/reproject.json")
        with open(exp_file, "r") as exp_f:
            exp = json.load(exp_f)
        assert results[0].__dict__ == exp


if __name__ == "__main__":
    unittest.main()
