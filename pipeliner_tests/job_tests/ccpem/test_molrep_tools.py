#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import textwrap

from pipeliner_tests import test_data
from pipeliner.scripts.job_scripts import molrep_tools


class MolrepTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="molrep_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_molrep_symlink(self):
        input_map = "1ake_10A_molrep.mrc"
        test_data_map = os.path.join(self.test_data, input_map)
        os.mkdir("subdir")
        test_dir_map = os.path.join(self.test_dir, "subdir", input_map)
        shutil.copy(test_data_map, test_dir_map)
        assert os.path.isfile(test_dir_map)
        molrep_tools.symlink_mrc_map(orig_file=test_dir_map)
        assert os.path.islink("1ake_10A_molrep.map")

    def test_molrep_write_keywords_file(self):
        keywords = ["_NMON 1", "NCSM 1", "stick n"]
        molrep_tools.write_keywords_file(keywords=keywords)
        keywords_file = os.path.join(self.test_dir, molrep_tools.keywords_file)
        assert os.path.exists(keywords_file)
        with open(keywords_file, "r") as file:
            first_line = file.readline()
        assert first_line == "_NMON 1\n"

    def test_fix_pdb(self):
        molrep_pdb = "molrep.pdb"
        backup_pdb = "molrep.pdb.orig"
        contents = textwrap.dedent(
            """\
            Fake MOLREP output file
            Line 2
            # Bad separator to be removed
            Line 3
            """
        )
        with open(molrep_pdb, "w") as test_file:
            test_file.write(contents)
        molrep_tools.fix_pdb()
        assert os.path.isfile(molrep_pdb)
        assert os.path.isfile(backup_pdb)
        with open(molrep_pdb) as fixed:
            fixed_contents = fixed.read()
            assert "#" not in fixed_contents
            orig_lines = contents.split("\n")
            fixed_lines = fixed_contents.split("\n")
            assert orig_lines[0] == fixed_lines[0]
            assert orig_lines[1] == fixed_lines[1]
            assert orig_lines[3] == fixed_lines[2]
        with open(backup_pdb) as original:
            lines = original.read()
            assert "#" in lines
            assert lines == contents


if __name__ == "__main__":
    unittest.main()
