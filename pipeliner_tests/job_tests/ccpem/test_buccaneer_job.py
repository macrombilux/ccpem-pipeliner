#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner.utils import touch
from pipeliner_tests.generic_tests import general_get_command_test
from pipeliner.api.api_utils import write_default_jobstar
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc
from pipeliner.scripts.job_scripts import make_buccaneer_args_file


class BuccaneerJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_buccaneer_commands(self):
        os.makedirs("sequences")
        touch(os.path.join(self.test_dir, "sequences/5ni1.fasta"))
        os.makedirs("PostProcess/job010/")
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.mrc"),
            os.path.join(self.test_dir, "PostProcess/job010/postprocess_masked.mrc"),
        )
        script_path = make_buccaneer_args_file.__file__
        general_get_command_test(
            jobtype="Buccaneer",
            jobfile="buccaneer_job.star",
            jobnumber=1502,
            input_nodes={
                "PostProcess/job010/postprocess_masked.mrc": "DensityMap.mrc",
                "sequences/5ni1.fasta": "Sequence.fasta",
            },
            output_nodes={"refined1.pdb": "AtomCoords.pdb.buccaneer"},
            expected_commands=[
                f"python3 {script_path} --job_title RELION pipeline test with EMD-3488"
                f" --input_map"
                f" {os.path.abspath('PostProcess/job010/postprocess_masked.mrc')}"
                f" --resolution 3.3 --input_seq"
                f" {os.path.abspath('sequences/5ni1.fasta')}"
                f" --extend_pdb None --ncycle 1.0 --ncycle_refmac 20.0 --ncycle_buc1st"
                f" 1.0 --ncycle_bucnth 1.0 --map_sharpen 0.0 --ncpus 1.0 --lib_in None"
                f" --keywords  --refmac_keywords ",
                "ccpem-buccaneer --no-gui --args tmp_buccaneer_args.json"
                " --job_location Buccaneer/job1502/",
                "rm tmp_buccaneer_args.json",
            ],
        )

    def test_make_results_display(self):
        # make it look like abuccaneer job has run
        os.makedirs("ModelBuild/job001")
        touch("ModelBuild/job001/refined1.pdb")
        write_default_jobstar(
            "buccaneer.atomic_model_build", "ModelBuild/job001/job.star"
        )
        pipelinefile = os.path.join(self.test_data, "Pipelines/single_buccaneer.star")
        shutil.copy(pipelinefile, "default_pipeline.star")
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mapfile, "map1.mrc")

        # set up the pipeline and get the job
        with ProjectGraph() as pipeline:
            buc_proc = pipeline.find_process("ModelBuild/job001/")

        buc_job = active_job_from_proc(buc_proc)
        buc_job.joboptions["input_map"].value = "map1.mrc"

        dispobjs = buc_job.create_results_display()
        print(dispobjs[0].__dict__)
        assert dispobjs[0].__dict__ == {
            "maps": ["ModelBuild/job001/Thumbnails/map1.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelBuild/job001/refined1.pdb"],
            "title": "Overlaid map: map1.mrc model: ModelBuild/job001/refined1.pdb",
            "maps_data": "map1.mrc",
            "models_data": "ModelBuild/job001/refined1.pdb",
            "associated_data": ["map1.mrc", "ModelBuild/job001/refined1.pdb"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert os.path.isfile("ModelBuild/job001/Thumbnails/map1.mrc")


if __name__ == "__main__":
    unittest.main()
