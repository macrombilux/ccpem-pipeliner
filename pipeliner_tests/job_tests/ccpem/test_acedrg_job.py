#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("acedrg") is None else False


class AceDRGTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="acedrg")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_smiles_string(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/AceDRG/acedrg_create_ligand_run.job"
            ),
            input_nodes={},
            output_nodes={
                "TYL_acedrg.cif": "LigandDescription.cif.acedrg",
                "TYL_acedrg.pdb": "AtomCoords.pdb.acedrg",
            },
            expected_commands=[
                "acedrg --smi=CC(=O)Nc1ccc(O)cc1 --res=TYL --out=TYL_acedrg"
            ],
            show_outputnodes=False,
        )

    def test_get_command_cif_file(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/AceDRG/acedrg_create_ligand_input_cif.job"
            ),
            input_nodes={
                "lig.cif": "AtomCoords.cif",
            },
            output_nodes={
                "LIG_acedrg.cif": "LigandDescription.cif.acedrg",
                "LIG_acedrg.pdb": "AtomCoords.pdb.acedrg",
            },
            expected_commands=[
                "acedrg --mmcif=../../lig.cif --res=LIG --out=LIG_acedrg"
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_create_ligand(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/AceDRG/acedrg_create_ligand_run.job"
            ),
            input_files=[],
            expected_outfiles=[
                "run.out",
                "run.err",
                "TYL_acedrg.cif",
                "TYL_acedrg.pdb",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "TYL_acedrg.pdb",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": ["AceDRG/job998/TYL_acedrg.pdb"],
            "maps_data": "",
            "models_data": "AceDRG/job998/TYL_acedrg.pdb",
            "associated_data": ["AceDRG/job998/TYL_acedrg.pdb"],
        }


if __name__ == "__main__":
    unittest.main()
