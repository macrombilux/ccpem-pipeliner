#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_job_script

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("molrep") is None else False
molrep_tools_script = get_job_script("molrep_tools.py")


class MolrepTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="molrep_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_molrep(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model.job"
            ),
            input_nodes={
                "Import/job001/1ake_10A_molrep.mrc": "DensityMap.mrc",
                "Import/job002/1AKE_cha_molrep_to_move.pdb": "AtomCoords.pdb",
            },
            output_nodes={
                "molrep.pdb": "AtomCoords.pdb",
                "molrep.cif": "AtomCoords.cif",
            },
            expected_commands=[
                f"python3 {molrep_tools_script} --symlink_mrc_map"
                " ../../Import/job001/1ake_10A_molrep.mrc --keywords"
                " _NMON 1\nNCSM 1\nstick\nnp 1\nnpt 1\n",
                "molrep -m ../../Import/job002/1AKE_cha_molrep_to_move.pdb"
                " -f 1ake_10A_molrep.map -k keywords.txt",
                f"python3 {molrep_tools_script} --fix_pdb",
                "gemmi convert molrep.pdb molrep.cif",
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_molrep_fit_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A_molrep.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "1AKE_cha_molrep_to_move.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["Molrep/job998/Thumbnails/1ake_10A_molrep.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Molrep/job998/molrep.pdb"],
            "title": "Molrep docked model",
            "maps_data": "Import/job001/1ake_10A_molrep.mrc",
            "models_data": "Molrep/job998/molrep.pdb",
            "associated_data": [
                "Import/job001/1ake_10A_molrep.mrc",
                "Molrep/job998/molrep.pdb",
            ],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_molrep_fit_model_cif_format(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_cif_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A_molrep.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "1AKE_cha_molrep_to_move.cif"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["Molrep/job998/Thumbnails/1ake_10A_molrep.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Molrep/job998/molrep.pdb"],
            "title": "Molrep docked model",
            "maps_data": "Import/job001/1ake_10A_molrep.mrc",
            "models_data": "Molrep/job998/molrep.pdb",
            "associated_data": [
                "Import/job001/1ake_10A_molrep.mrc",
                "Molrep/job998/molrep.pdb",
            ],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_molrep_fit_fixed_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_fixed_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5me2_chain_a.cif"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5me2_a_to_move.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Molrep docked model",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Molrep/job998/Thumbnails/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["Molrep/job998/molrep.pdb", "Import/job002/5me2_chain_a.cif"],
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "Molrep/job998/molrep.pdb, Import/job002/5me2_chain_a.cif",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "Molrep/job998/molrep.pdb",
                "Import/job002/5me2_chain_a.cif",
            ],
        }


if __name__ == "__main__":
    unittest.main()
