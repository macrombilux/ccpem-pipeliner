#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("gemmi") is None else False


class GemmiConvertTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="gemmi_convert_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_gemmi_convert(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_pdb_run.job",
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes={"5me2_a.cif": "AtomCoords.cif.gemmi.convert"},
            expected_commands=[
                (
                    "gemmi convert --remove-waters ../../Import/job001/5me2_a.pdb "
                    "5me2_a.cif"
                ),
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_gemmi_convert_pdb(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_pdb_run.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "5me2_a.cif"],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Model: GemmiConvert/job998/5me2_a.cif",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": ["GemmiConvert/job998/5me2_a.cif"],
            "maps_data": "",
            "models_data": "GemmiConvert/job998/5me2_a.cif",
            "associated_data": ["GemmiConvert/job998/5me2_a.cif"],
        }

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_gemmi_convert_cif(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_cif_run.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "5ni1_updated.pdb"],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Model: GemmiConvert/job998/5ni1_updated.pdb",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": ["GemmiConvert/job998/5ni1_updated.pdb"],
            "maps_data": "",
            "models_data": "GemmiConvert/job998/5ni1_updated.pdb",
            "associated_data": ["GemmiConvert/job998/5ni1_updated.pdb"],
        }


if __name__ == "__main__":
    unittest.main()
