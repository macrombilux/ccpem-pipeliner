#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import gemmi
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_job_script, get_pipeliner_root
from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("ccpem-flex-em") is None else False
args_script = get_job_script("flex_em_args.py")


class FlexEMTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="flex_em_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("pipeliner.jobs.ccpem.flex_em_job.which")
    def test_get_command(self, mock_which):
        mock_which.return_value = "/test/path/to/ccpem/bin/ccpem-python"
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FlexEM/auto_ribfind_and_refine_run.job"
            ),
            input_nodes={
                "Import/job001/5me2_chain_a.cif": "AtomCoords.cif",
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "1_MD/final1_mdcg.pdb": "AtomCoords.pdb.flex_em.refined",
                "1_MD/final1_mdcg.cif": "AtomCoords.cif.flex_em.refined",
            },
            expected_commands=[
                (
                    "gemmi convert --remove-lig-wat "
                    "../../Import/job001/5me2_chain_a.cif "
                    "5me2_chain_a_no_wat_lig.pdb"
                ),
                "mkdssp 5me2_chain_a_no_wat_lig.pdb ribfind.dssp",
                (
                    "ccpem-python /test/path/to/ccpem/lib/py2/ccpem/src/ccpem_progs"
                    "/ribfind/ribfind_c_SM.pyc"
                    " 5me2_chain_a_no_wat_lig.pdb ribfind.dssp 6.5"
                ),
                (
                    f"python3 {args_script} "
                    "--working_dir  --input_model 5me2_chain_a_no_wat_lig.pdb "
                    "--input_map ../../Import/job002/emd_3488.mrc "
                    "--ribfind_rigid_body_file "
                    "5me2_chain_a_no_wat_lig/protein/"
                    "5me2_chain_a_no_wat_lig_denclust_60.txt"
                    " --density_weight 1.0 --resolution 10.0 --phi_psi"
                ),
                (
                    "ccpem-python /test/path/to/ccpem/lib/py2/ccpem/src/ccpem_progs"
                    "/flex_em/flexem.pyc flex_em_args.json"
                ),
                "gemmi convert 1_MD/final1_mdcg.pdb 1_MD/final1_mdcg.cif",
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    @patch("pipeliner.utils.get_job_script")
    def test_auto_ribfind_and_refine(self, mock_gjs):
        mock_gjs.return_value = os.path.join(
            self.test_data, "Scripts/flex_em_args_test.py"
        )
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/FlexEM/auto_ribfind_and_refine_run.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_chain_a.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(
                        self.test_data,
                        "emd_3488.mrc",
                    ),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "ribfind.dssp",
                "flex_em_args.json",
                (
                    "5me2_chain_a_no_wat_lig/protein/"
                    "5me2_chain_a_no_wat_lig_denclust_60.txt"
                ),
                "1_MD/final1_mdcg.pdb",
                "1_MD/final1_mdcg.cif",
            ],
        )

        # Check ligands are removed
        out_pdb = os.path.join(self.test_dir, "FlexEM/job998/1_MD/final1_mdcg.pdb")
        assert os.path.exists(out_pdb)
        st = gemmi.read_structure(out_pdb)
        for model in st:
            for chain in model:
                for residue in chain:
                    assert residue.name != "HEM"

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Overlaid map/models",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["FlexEM/job998/Thumbnails/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": [
                "Import/job001/5me2_chain_a.cif",
                "FlexEM/job998/1_MD/final1_mdcg.pdb",
            ],
            "maps_data": "Import/job002/emd_3488.mrc",
            "models_data": (
                "Import/job001/5me2_chain_a.cif, FlexEM/job998/1_MD/final1_mdcg.pdb"
            ),
            "associated_data": [
                "Import/job002/emd_3488.mrc",
                "Import/job001/5me2_chain_a.cif",
                "FlexEM/job998/1_MD/final1_mdcg.pdb",
            ],
        }
        mock_gjs.return_value = os.path.join(
            get_pipeliner_root(), "scripts/job_scripts/flex_em_args.py"
        )


if __name__ == "__main__":
    unittest.main()
