#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("lafter") is None else False


class LafterTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="lafter")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_lafter(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Lafter/lafter_local_denoising_run.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job001/3488_run_half2_class001_unfil.mrc": "DensityMap.mrc",
            },
            output_nodes={"LAFTER_filtered.mrc": "DensityMap.mrc.lafter.denoise"},
            expected_commands=[
                "lafter --v1 ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "--v2 ../../Import/job001/3488_run_half2_class001_unfil.mrc "
                "--particle_diameter 75.0"
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_lafter_fit_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Lafter/lafter_local_denoising_run.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "LAFTER_filtered.mrc",
                "noise_suppressed.mrc",
            ],
            print_err=True,
            show_contents=True,
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Lafter denoised map",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Lafter/job998/Thumbnails/LAFTER_filtered.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": "Lafter/job998/LAFTER_filtered.mrc",
            "models_data": "",
            "associated_data": ["Lafter/job998/LAFTER_filtered.mrc"],
        }


if __name__ == "__main__":
    unittest.main()
