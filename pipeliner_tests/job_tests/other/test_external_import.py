#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import general_get_command_test, running_job
from pipeliner.utils import touch
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc


class ExternalProcessingImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_all_nodes_are_unique(self):
        """The basic test all imported outputs are unique so they just need
        to be copied in to the directory"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_simple.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        touch("processed_map.mrc")
        touch("logfile.log")
        mapout = os.path.abspath("processed_map.mrc")
        logout = os.path.abspath("logfile.log")
        general_get_command_test(
            jobtype="Import",
            jobfile="external_import_job.star",
            jobnumber=1,
            input_nodes={
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.star.relion.refine3d",
            },
            output_nodes={
                "processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "logfile.log": "LogFile.txt.externalprog",
            },
            expected_commands=[
                f"cp {mapout} Import/job001/processed" "_map.mrc",
                f"cp {logout} Import/job001/logfile.log",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_single_dir(self):
        """The imported nodes have the same name and a 1-deep directory structure"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_depth1.json"
        )
        shutil.copy(importfile, "test_external_import.json")

        os.makedirs("step1")
        os.makedirs("step2")
        touch("step1/processed_map.mrc")
        touch("step2/processed_map.mrc")
        mapout = os.path.abspath("step1/processed_map.mrc")
        logout = os.path.abspath("step2/processed_map.mrc")

        general_get_command_test(
            jobtype="Import",
            jobfile="external_import_job.star",
            jobnumber=1,
            input_nodes={
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.star.relion.refine3d",
            },
            output_nodes={
                "step1/processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "step2/processed_map.mrc": "LogFile.mrc.externalprog",
            },
            expected_commands=[
                "mkdir -p Import/job001/step1",
                f"cp {mapout} Import/job001/ste" "p1/processed_map.mrc",
                "mkdir -p Import/job001/step2",
                f"cp {logout} Import/job001/ste" "p2/processed_map.mrc",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_longer_dir(self):
        """The imported nodes have the same name and a deeper directory structure"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_depth2.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        os.makedirs("test1/step1")
        os.makedirs("test2/step1")
        touch("test1/step1/processed_map.mrc")
        touch("test2/step1/processed_map.mrc")
        mapout = os.path.abspath("test1/step1/processed_map.mrc")
        logout = os.path.abspath("test2/step1/processed_map.mrc")
        general_get_command_test(
            jobtype="Import",
            jobfile="external_import_job.star",
            jobnumber=1,
            input_nodes={
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.star.relion.refine3d",
            },
            output_nodes={
                "test1/step1/processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "test2/step1/processed_map.mrc": "LogFile.mrc.externalprog",
            },
            expected_commands=[
                "mkdir -p Import/job001/test1/step1",
                f"cp {mapout} Import/job0" "01/test1/step1/processed_map.mrc",
                "mkdir -p Import/job001/test2/step1",
                f"cp {logout} Import/job0" "01/test2/step1/processed_map.mrc",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_variable_dir(self):
        """The imported nodes have the same name and a deeper directory structure
        with varying lengths"""
        importfile = os.path.join(
            self.test_data,
            "ExternalImport/external_processing_depth_multi.json",
        )
        shutil.copy(importfile, "test_external_import.json")
        os.makedirs("test1/step1/")
        os.makedirs("silly/test1/step1")
        touch("test1/step1/processed_map.mrc")
        touch("silly/test1/step1/processed_map.mrc")
        mapout = os.path.abspath("test1/step1/processed_map.mrc")
        logout = os.path.abspath("silly/test1/step1/processed_map.mrc")
        general_get_command_test(
            jobtype="Import",
            jobfile="external_import_job.star",
            jobnumber=1,
            input_nodes={
                "mapfile.mrc": "DensityMap.mrc.relion.refine3d",
                "particles.star": "ParticlesData.star.relion.refine3d",
            },
            output_nodes={
                "test1/step1/processed_map.mrc": "DensityMap.mrc.externalprog.fixed",
                "silly/test1/step1/processed_map.mrc": "LogFile.mrc.externalprog",
            },
            expected_commands=[
                "mkdir -p Import/job001/test1/step1",
                f"cp {mapout} Import/job0" "01/test1/step1/processed_map.mrc",
                "mkdir -p Import/job001/silly/test1/step1",
                f"cp {logout} Import/job001/silly/test1/step1/processed_map.mrc",
                "cp test_external_import.json Import/job001/externa"
                "l_processing.json",
            ],
        )

    def test_running(self):
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_live.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        running_job(
            test_jobfile="external_import_job.star",
            job_dir_type="Import",
            job_no=2,
            input_files=[
                ("Refine3D/job001", "emd_3488.mrc"),
                ("Refine3D/job001", "particles.star"),
                ("SomeOtherProgram", "logfile.log"),
                ("SomeOtherProgram", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=["emd_3488_mask.mrc", "logfile.log"],
            sleep_time=1,
        )

    def test_running_multidir(self):
        importfile = os.path.join(
            self.test_data,
            "ExternalImport/external_processing_live_multi.json",
        )
        shutil.copy(importfile, "test_external_import.json")
        running_job(
            test_jobfile="external_import_job.star",
            job_dir_type="Import",
            job_no=2,
            input_files=[
                ("Refine3D/job001", "emd_3488.mrc"),
                ("Refine3D/job002", "particles.star"),
                ("SomeOtherProgram/test1/", "emd_3488_mask.mrc"),
                ("SomeOtherProgram/test2/", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=["test1/emd_3488_mask.mrc", "test2/emd_3488_mask.mrc"],
            sleep_time=1,
        )

    def test_results_display(self):
        # setup and run the job
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_live.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        running_job(
            test_jobfile="external_import_job.star",
            job_dir_type="Import",
            job_no=2,
            input_files=[
                ("Refine3D/job001", "emd_3488.mrc"),
                ("Refine3D/job001", "particles.star"),
                ("SomeOtherProgram", "logfile.log"),
                ("SomeOtherProgram", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=["emd_3488_mask.mrc", "logfile.log"],
            sleep_time=1,
        )
        with ProjectGraph() as pipeline:
            proc = pipeline.process_list[0]

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        expected = [
            {
                "title": "Inputs",
                "dobj_type": "table",
                "headers": ["File", "NodeType"],
                "header_tooltips": ["File", "NodeType"],
                "table_data": [
                    ["Refine3D/job001/emd_3488.mrc", "DensityMap.mrc.relion.refine3d"],
                    [
                        "Refine3D/job001/particles.star",
                        "ParticlesData.star.relion.refine3d",
                    ],
                ],
                "associated_data": [
                    "Refine3D/job001/emd_3488.mrc",
                    "Refine3D/job001/particles.star",
                ],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Outputs",
                "dobj_type": "table",
                "headers": ["File", "NodeType"],
                "header_tooltips": ["File", "NodeType"],
                "table_data": [
                    [
                        "Import/job002/emd_3488_mask.mrc",
                        "DensityMap.mrc.externalprog.fixed",
                    ],
                    ["Import/job002/logfile.log", "LogFile.txt.externalprog"],
                ],
                "associated_data": [
                    "Import/job002/emd_3488_mask.mrc",
                    "Import/job002/logfile.log",
                ],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Processing Info",
                "dobj_type": "text",
                "display_data": (
                    "\n  Step 1: I did a thing,\n  Step 2: I did another thing,\n  "
                    "Step 3: \n    substep a: A result,\n    substep b: B result\n  "
                    "\n"
                ),
                "associated_data": ["Import/job002/external_processing.json"],
                "start_collapsed": False,
                "flag": "",
            },
        ]
        for n, do in enumerate(dispobjs):
            assert do.__dict__ == expected[n], [do.__dict__, expected[n]]


if __name__ == "__main__":
    unittest.main()
