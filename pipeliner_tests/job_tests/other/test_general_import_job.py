#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    running_job,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.utils import touch
from pipeliner.node_factory import create_node

OUTPUT_NODE_GENERAL_MICS = "MicrographsData.star.relion"
OUTPUT_NODE_MOVIES = "MicrographMoviesData.star.relion"
OUTPUT_NODE_COORDS_DEP = "MicrographsCoords.star.deprecated_format"


class GeneralImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_standard_node(self):
        touch("test_image.mrc")
        general_get_command_test(
            jobtype="Import",
            jobfile="pipeliner_import_single_file_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={"test_image.mrc": "DensityMap.mrc.test.fancy.pretty"},
            expected_commands=["cp test_image.mrc Import/job001/test_image.mrc"],
        )

    def test_get_command_standard_node_file_in_UserFiles(self):
        os.makedirs("UserFiles")
        touch("UserFiles/test_image.mrc")
        general_get_command_test(
            jobtype="Import",
            jobfile="pipeliner_import_single_file_usrfiles_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={"test_image.mrc": "DensityMap.mrc.test.fancy.pretty"},
            expected_commands=[
                "mv UserFiles/test_image.mrc Import/job001/test_image.mrc"
            ],
        )

    def test_get_command_standard_node_no_keywords(self):
        touch("test_image.mrc")
        general_get_command_test(
            jobtype="Import",
            jobfile="pipeliner_import_single_file_no_keywords_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={"test_image.mrc": "DensityMap.mrc"},
            expected_commands=["cp test_image.mrc Import/job001/test_image.mrc"],
        )

    def test_get_command_standard_node_synthetic(self):
        touch("test_image.mrc")
        general_get_command_test(
            jobtype="Import",
            jobfile="pipeliner_import_single_file_sim_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={
                "test_image.mrc": "DensityMap.mrc.test.fancy.pretty.synthetic"
            },
            expected_commands=["cp test_image.mrc Import/job001/test_image.mrc"],
        )

    def test_get_command_standard_node_bad_characters(self):
        general_get_command_test(
            jobtype="Import",
            jobfile="pipeliner_import_single_file_man_node_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={"test_image.mrc": "NewNode.mrc.test.fancy.pretty"},
            expected_commands=["cp test_image.mrc Import/job001/test_image.mrc"],
        )

    def test_run_job_standard_node(self):
        touch("test_image.mrc")
        running_job(
            test_jobfile="pipeliner_import_single_file_job.star",
            job_dir_type="Import",
            job_no=1,
            input_files=[("", "test_image.mrc")],
            expected_outfiles=["test_image.mrc"],
            sleep_time=1,
        )

    def test_run_job_manual_node_name(self):
        touch("test_image.mrc")
        running_job(
            test_jobfile="pipeliner_import_single_file_man_node_job.star",
            job_dir_type="Import",
            job_no=1,
            input_files=[("", "test_image.mrc")],
            expected_outfiles=["test_image.mrc"],
            sleep_time=1,
        )

    def test_run_job_manual_node_name_bad_chars(self):
        touch("test_image.mrc")
        with self.assertRaises(ValueError):
            running_job(
                test_jobfile="pipeliner_import_single_file_bad_chars_job.star",
                job_dir_type="Import",
                job_no=1,
                input_files=[("", "test_image.mrc")],
                expected_outfiles=["test_image.mrc"],
                sleep_time=1,
            )

    def setup_results_test(
        self, results_file, nodetype, nodekwds=None, is_relion=False
    ):
        nodekwds = [] if not nodekwds else nodekwds
        outdir = "Import/job001/"
        os.makedirs(outdir)
        shutil.copy(results_file, outdir)
        thejob = new_job_of_type("relion.import.other")
        thejob.joboptions["pipeliner_node_type"].value = nodetype
        thejob.joboptions["kwds"].value = nodekwds
        thejob.joboptions["is_relion"].value = "Yes" if is_relion else "No"
        thejob.output_dir = outdir
        thejob.output_nodes.append(
            create_node(
                os.path.join("Import/job001", os.path.basename(results_file)),
                nodetype,
                nodekwds,
            )
        )
        return thejob.create_results_display()

    def test_display_image2d_mrc(self):
        rfile = os.path.join(self.test_data, "single.mrc")
        results = self.setup_results_test(rfile, "Image2D")
        exp = {
            "title": "single",
            "dobj_type": "image",
            "image_path": "Import/job001/Thumbnails/single.png",
            "image_desc": "imported image; Import/job001/single.mrc; Image2D.mrc",
            "associated_data": ["Import/job001/single.mrc"],
            "start_collapsed": False,
            "flag": "",
        }
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].image_path)

    def test_display_2stack_mrcs(self):
        rfile = os.path.join(self.test_data, "image_stack.mrcs")
        results = self.setup_results_test(rfile, "Image2DStack")
        with open(
            os.path.join(self.test_data, "ResultsFiles/genimport_2dstack_mrcs.json")
        ) as exps:
            exp = json.load(exps)
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].img)

    def test_display_2stack_tiff(self):
        rfile = os.path.join(self.test_data, "movie_tiff.tiff")
        results = self.setup_results_test(rfile, "Image2DStack")
        with open(
            os.path.join(self.test_data, "ResultsFiles/genimport_2dstack_tiff.json")
        ) as exps:
            exp = json.load(exps)
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].img)

    def test_display_2stack_other(self):
        rfile = os.path.join(self.test_data, "logfile.log")
        results = self.setup_results_test(rfile, "Image2DStack")
        exp = {
            "title": "Error creating results display",
            "dobj_type": "text",
            "display_data": "Illegal file type for " "mini_montage_from_stack(): .log",
            "associated_data": ["Import/job001/logfile.log"],
            "start_collapsed": False,
            "flag": "",
        }

        assert results[0].__dict__ == exp, results[0].__dict__

    def test_display_mapmodel_map(self):
        rfile = os.path.join(self.test_data, "emd_3488.mrc")
        results = self.setup_results_test(rfile, "DensityMap", ["masked", "emdb"])
        exp = {
            "maps": ["Import/job001/Thumbnails/emd_3488.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Map: Import/job001/emd_3488.mrc",
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "",
            "associated_data": ["Import/job001/emd_3488.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].maps[0])

    def test_display_mapmodel_model(self):
        rfile = os.path.join(self.test_data, "5me2_a.pdb")
        results = self.setup_results_test(rfile, "AtomCoords", ["refined"])
        exp = {
            "maps": [],
            "dobj_type": "mapmodel",
            "maps_opacity": [],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "Model: Import/job001/5me2_a.pdb",
            "maps_data": "",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": ["Import/job001/5me2_a.pdb"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert results[0].__dict__ == exp, results[0].__dict__
        assert os.path.isfile(results[0].models[0])

    def test_display_nodisplay_text(self):
        rfile = os.path.join(self.test_data, "micrographs_ctf.star")
        shutil.copy(rfile, "micrographs_ctf.xxx")
        results = self.setup_results_test(rfile, "MicrographsData", ["ctf"])
        exp = {
            "title": "Import/job001/micrographs_ctf.star",
            "start_collapsed": False,
            "dobj_type": "textfile",
            "flag": "",
            "file_path": "Import/job001/micrographs_ctf.star",
        }
        assert results[0].__dict__ == exp, results[0].__dict__


if __name__ == "__main__":
    unittest.main()
