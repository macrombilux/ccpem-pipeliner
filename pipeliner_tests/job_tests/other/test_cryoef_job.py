#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests import generic_tests, test_data
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.utils import get_job_script

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("cryoef") is None else False


class CryoEFTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_cryoEF_plugin_input(self):
        os.makedirs(os.path.join(self.test_dir, "Refine3D/job001/"))
        shutil.copy(
            os.path.join(self.test_data, "run_data.star"),
            os.path.join(self.test_dir, "Refine3D/job001/"),
        )
        input_file_script = get_job_script("make_cryoef_angle_file.py")
        generic_tests.general_get_command_test(
            jobtype="CryoEF",
            jobfile="cryoef_star_job.star",
            jobnumber=2,
            input_nodes={"Refine3D/job001/run_data.star": "ParticlesData.star.relion"},
            output_nodes={
                "cryoef_angles_K.mrc": "Image3D.mrc.cryoef.fourierspace_"
                "transferfunction",
                "cryoef_angles_R.mrc": "Image3D.mrc.cryoef.realspace_powerspectrum",
                "cryoef_angles.log": "LogFile.txt.cryoef",
            },
            expected_commands=[
                f"python3 {input_file_script}"
                " --input_starfile Refine3D/job001/run_data.star"
                " --output_dir CryoEF/job002/",
                "cryoEF -f CryoEF/job002/cryoef_angles.dat -B 150.0 "
                "-D 80.0 -b 128 -r 3.8 -g C1",
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: only run in full unittest"
    )
    def test_run_cryoEF(self):
        os.makedirs(os.path.join(self.test_dir, "Refine3D/job001/"))
        shutil.copy(
            os.path.join(self.test_data, "run_data.star"),
            os.path.join(self.test_dir, "Refine3D/job001/"),
        )
        out_proc = generic_tests.running_job(
            test_jobfile="cryoef_star_job.star",
            job_dir_type="CryoEF",
            job_no=2,
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "cryoef_angles_K.mrc",
                "cryoef_angles_R.mrc",
                SUCCESS_FILE,
            ),
            sleep_time=5,
        )

        # make sure cleanup happened
        assert not os.path.isfile("cryoef_angles.dat")
        dispobjs = active_job_from_proc(out_proc).create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Tilted collection recommendations",
            "dobj_type": "table",
            "headers": ["Tilt angle", "Reccomended particles to collect"],
            "header_tooltips": ["Tilt angle", "Reccomended particles to collect"],
            "table_data": [["42", "~3.2x"], ["39", "~3.0x"]],
            "associated_data": ["CryoEF/job002/cryoef_angles.log"],
            "start_collapsed": False,
            "flag": "",
        }

        hd = os.path.join(self.test_data, "JobFiles/CryoEF/results_histo.json")
        with open(hd, "r") as hdf:
            exp_histo = json.load(hdf)
        assert dispobjs[1].__dict__ == exp_histo

        assert dispobjs[2].__dict__ == {
            "maps": ["CryoEF/job002/Thumbnails/cryoef_angles_K.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Fourier space information density",
            "maps_data": "CryoEF/job002/cryoef_angles_K.mrc",
            "models_data": "",
            "associated_data": ["CryoEF/job002/cryoef_angles_K.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
