#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import requests

from pipeliner_tests import test_data
from pipeliner_tests.job_testing_tools import (
    job_generate_commands_test,
    job_running_test,
)
from pipeliner_tests.generic_tests import do_slow_tests
from pipeliner.job_factory import job_can_run, new_job_of_type
from pipeliner.node_factory import create_node


def check_dbs():
    try:
        ad = "https://files.wwpdb.org/pub/emdb/structures/EMD-0002/map/emd_0002.map.gz"
        response = requests.head(ad)
        emdb = response.status_code == 200
        ad = "https://files.wwpdb.org/pub/pdb/data/structures/all/mmCIF/100d.cif.gz"
        response = requests.head(ad)
        pdb_pdb = response.status_code == 200
        ad = "https://files.wwpdb.org/pub/pdb/data/structures/all/pdb/pdb100d.ent.gz"
        response = requests.head(ad)
        pdb_mmcif = response.status_code == 200
        ad = "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_v4.pdb"
        response = requests.head(ad)
        alphafold = response.status_code == 200
        return all([emdb, pdb_mmcif, pdb_pdb, alphafold])

    except requests.exceptions.ConnectionError:
        return False


if do_slow_tests():
    do_live = check_dbs()
else:
    do_live = False


class FetchImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.jobfiles = os.path.join(self.test_data, "JobFiles")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(do_live, "Only in full tests or can't get data from databases")
    def test_get_commands_pdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_ent_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"pdb7ndo.pdb": "AtomCoords.pdb.from_pdb"},
            expected_commands=[
                "wget https://files.wwpdb.org/pub/pdb/data/structures/all/"
                "pdb/pdb7ndo.ent.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/pdb7ndo.ent.gz",
                "mv Fetch/job999/pdb7ndo.ent Fetch/job999/pdb7ndo.pdb",
            ],
        )

    @unittest.skipUnless(do_live, "Only in full tests or can't get data from databases")
    def test_get_commands_mmcif(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_mmcif_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"7ndo.cif": "AtomCoords.cif.from_pdb"},
            expected_commands=[
                "wget https://files.wwpdb.org/pub/pdb/data/structures/all/"
                "mmCIF/7ndo.cif.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/7ndo.cif.gz",
            ],
        )

    @unittest.skipUnless(do_live, "Only in full tests or can't get data from databases")
    def test_get_commands_emdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_emdb_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"emd_0002.mrc": "DensityMap.mrc.from_emdb"},
            expected_commands=[
                "wget https://files.wwpdb.org/pub/emdb/structures/EMD-0002/"
                "header/emd-0002.xml -P Fetch/job999/",
                "wget https://files.wwpdb.org/pub/emdb/structures/EMD-0002/map/"
                "emd_0002.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_0002.map.gz",
                "mv Fetch/job999/emd_0002.map Fetch/job999/emd_0002.mrc",
            ],
        )

    @unittest.skipUnless(do_live, "Only in full tests or can't get data from databases")
    def test_get_commands_emdb_with_halfmaps(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_emdb_hms_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "emd_28897.mrc": "DensityMap.mrc.from_emdb",
                "emd_28897_half_map_1.mrc": "DensityMap.mrc.halfmap",
                "emd_28897_half_map_2.mrc": "DensityMap.mrc.halfmap",
            },
            expected_commands=[
                "wget https://files.wwpdb.org/pub/emdb/structures/EMD-28897/"
                "header/emd-28897.xml -P Fetch/job999/",
                "wget https://files.wwpdb.org/pub/emdb/structures/EMD-28897/"
                "map/emd_28897.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_28897.map.gz",
                "mv Fetch/job999/emd_28897.map Fetch/job999/emd_28897.mrc",
                "wget https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-"
                "28897/other/emd_28897_half_map_1.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_28897_half_map_1.map.gz",
                "mv Fetch/job999/emd_28897_half_map_1.map Fetch/job999/emd_"
                "28897_half_map_1.mrc",
                "wget https://ftp.ebi.ac.uk/pub/databases/emdb/structures/"
                "EMD-28897/other/emd_28897_half_map_2.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_28897_half_map_2.map.gz",
                "mv Fetch/job999/emd_28897_half_map_2.map Fetch/job999/emd_"
                "28897_half_map_2.mrc",
            ],
        )

    @unittest.skipUnless(
        job_can_run("pipeliner.fetch.pdb") and do_live,
        "Slow tests - Skip unless doing full testing",
    )
    def test_running_pdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_ent_job.star")
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["pdb7ndo.pdb"],
        )

    @unittest.skipUnless(
        job_can_run("pipeliner.fetch.emdb") and do_live,
        "Slow tests - Skip unless doing full testing",
    )
    def test_running_emdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_emdb_job.star")
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["emd_0002.mrc"],
        )
        assert os.path.isfile("Fetch/job998/emd-0002.xml")

    @unittest.skipUnless(
        job_can_run("pipeliner.fetch.pdb") and do_live,
        "Slow tests - Skip unless doing full testing",
    )
    def test_running_mmcif(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_mmcif_job.star")
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["7ndo.cif"],
        )

    def test_parse_emdb_xml(self):
        f = "emd-15285.xml"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.emdb")
        job.output_dir = jobdir
        job.joboptions["db_id"].value = "15285"
        expected = (
            "EMD-15285: Cryo-EM structure of alpha-synuclein filaments from Parkinson's"
            " disease and dementia with Lewy bodies",
            "Yang Y et al.\nStructures of alpha-synuclein filaments from human brains"
            " with Lewy pathology.\nNature (2022) doi:  DOI: "
            "doi:10.1038/s41586-022-05319-3\npubmed:  PubMed: 36108674",
            "256 x 256 x 256",
            "0.727",
            "2.2",
            [],
            [["8a9l"]],
        )
        assert job.parse_emdb_header() == expected

    def test_parse_emdb_xml_with_associated_emdbs(self):
        f = "emd-0001.xml"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.emdb")
        job.output_dir = jobdir
        job.joboptions["db_id"].value = "0001"
        expected = (
            "EMD-0001: Cryo-EM structure of bacterial RNA polymerase-sigma54 holoenzyme"
            " transcription open complex",
            "Glyde R et al.\nStructures of Bacterial RNA Polymerase Complexes Reveal "
            "the Mechanism of DNA Loading and Transcription Initiation.\nMol. Cell "
            "(2018) doi:  DOI: doi:10.1016/j.molcel.2018.05.021\npubmed:  "
            "PubMed: 29932903",
            "256 x 256 x 256",
            "1.06",
            "3.4",
            [["EMD-4397", "None available"]],
            [["6gh5"]],
        )
        assert job.parse_emdb_header() == expected

    def test_parse_pdb_cif_data(self):
        f = "2onv.cif"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.pdb")
        job.output_dir = jobdir
        job.output_nodes = [
            create_node(os.path.join(jobdir, f), "AtomCoords.cif.from_pdb")
        ]
        job.joboptions["db_id"].value = "2onv"
        expected_ref = {
            "authors": [
                "Sawaya, M.R.",
                "Sambashivan, S.",
                "Nelson, R.",
                "Ivanova, M.I.",
                "Sievers, S.A.",
                "et al.",
            ],
            "title": "Atomic structures of amyloid cross-beta spines reveal varied"
            " steric zippers.",
            "journal": "Nature",
            "year": "2007",
            "volume": "447",
            "issue": "",
            "pages": "453",
            "doi": "10.1038/nature05695",
            "other_metadata": {"pubmed": "17468747"},
        }
        assert job.get_pdb_cite()[0][0].__dict__ == expected_ref
        assert job.get_pdb_cite()[1] == "1.600"

    def test_parse_pdb_pdb_data(self):
        f = "2onv.pdb"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.pdb")
        job.output_dir = jobdir
        job.output_nodes = [
            create_node(os.path.join(jobdir, f), "AtomCoords.pdb.from_pdb")
        ]
        job.joboptions["db_id"].value = "2onv"
        expected_ref = {
            "authors": [
                "M.R.Sawaya",
                "S.Sambashivan",
                "R.Nelson",
                "M.I.Ivanova",
                "S.A.Sievers",
                "et al.",
            ],
            "title": "Atomic structures of amyloid cross-beta spines reveal varied"
            " steric zippers. ",
            "journal": "Nature",
            "year": "2007",
            "volume": "447",
            "issue": "",
            "pages": "453",
            "doi": "10.1038/NATURE05695",
            "other_metadata": {"pubmed": "17468747"},
        }
        assert job.get_pdb_cite()[0][0].__dict__ == expected_ref
        assert job.get_pdb_cite()[1] == "1.61"

    @unittest.skipUnless(do_live, "Can't get data from databases")
    def test_get_commands_alphafold_pdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_af_pdb_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "AF-F4HVG8-F1-model_v4.pdb": "AtomCoords.pdb.simulated.alphafold"
            },
            expected_commands=[
                "wget https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_"
                "v4.pdb -P Fetch/job999/",
                "wget https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-predicted"
                "_aligned_error_v4.json -P Fetch/job999/",
            ],
        )

    @unittest.skipUnless(do_live, "Can't get data from databases")
    def test_get_commands_alphafold_mmcif(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_af_mmcif_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "AF-F4HVG8-F1-model_v4.cif": "AtomCoords.cif.simulated.alphafold"
            },
            expected_commands=[
                "wget https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_"
                "v4.cif -P Fetch/job999/",
                "wget https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-predicted"
                "_aligned_error_v4.json -P Fetch/job999/",
            ],
        )
