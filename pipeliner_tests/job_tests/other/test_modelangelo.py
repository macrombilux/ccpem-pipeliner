#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner.job_factory import new_job_of_type
from pipeliner_tests.job_testing_tools import job_generate_commands_test
from pipeliner.utils import touch


class ModelAngeloTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_with_fasta(self):
        infile = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_with_fasta_job.star"
        )

        job_generate_commands_test(
            jobfile=infile,
            input_nodes={
                "my_map.mrc": "DensityMap.mrc",
                "my_sequence.fasta": "Sequence.fasta",
            },
            output_nodes={
                "my_map_model/my_map_model.cif": "AtomCoords.cif.modelangelo.simulated"
            },
            expected_commands=[
                "model_angelo build -v my_map.mrc -f my_sequence.fasta"
                " -o my_map_model"
            ],
        )

    def test_get_commands_no_fasta(self):
        infile = os.path.join(
            self.test_data, "JobFiles/ModelAngelo/mangelo_no_fasta_job.star"
        )

        job_generate_commands_test(
            jobfile=infile,
            input_nodes={
                "my_map.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "my_map_model/my_map_model.cif": "AtomCoords.cif.modelangelo.simulated"
            },
            expected_commands=[
                "model_angelo build_no_seq -v my_map.mrc -o my_map_model"
            ],
        )

    def test_get_results_display(self):
        ma_job = new_job_of_type("modelangelo.atomic_model_build")
        ma_job.joboptions["fn_map"].value = "my_map.mrc"
        ma_job.output_dir = "ModelAngelo/job001/"
        rdo = ma_job.create_results_display()
        expected_rdo = {
            "maps": ["my_map.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelAngelo/job001/my_map_model/my_map_model.cif"],
            "title": "Generated atomic model",
            "maps_data": "",
            "models_data": "",
            "associated_data": ["ModelAngelo/job001/my_map_model/my_map_model.cif"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

        assert len(rdo) == 1
        assert rdo[0].__dict__ == expected_rdo

    def test_postrun_actions(self):
        ma_job = new_job_of_type("modelangelo.atomic_model_build")
        ma_job.joboptions["fn_map"].value = "my_map.mrc"
        ma_job.output_dir = "ModelAngelo/job001/"

        hmmdir = "ModelAngelo/job001/my_map_model/hmm_profiles"
        os.makedirs(hmmdir)
        for chain in ("A", "B", "C"):
            touch(os.path.join(hmmdir, f"{chain}.hmm"))

        ma_job.post_run_actions()
        for n, chain in enumerate(("A", "B", "C")):
            assert f"ModelAngelo/job001/my_map_model/hmm_profiles/{chain}.hmm" in [
                x.name for x in ma_job.output_nodes
            ]
            assert ma_job.output_nodes[n].type == "SequenceAlignment.hmm.model_angelo"
            assert ma_job.output_nodes[n].kwds == ["model_angelo"]

    def test_postrun_actions_no_hmm_files(self):
        ma_job = new_job_of_type("modelangelo.atomic_model_build")
        ma_job.joboptions["fn_map"].value = "my_map.mrc"
        ma_job.joboptions["fn_sequence"].value = "my_seq.fasta"
        ma_job.output_dir = "ModelAngelo/job001/"

        ma_job.post_run_actions()
        assert len(ma_job.output_nodes) == 0


if __name__ == "__main__":
    unittest.main()
