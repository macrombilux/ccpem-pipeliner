#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from gemmi import cif


from pipeliner.utils import touch, get_job_script
from pipeliner_tests import generic_tests, test_data
from pipeliner.job_factory import new_job_of_type
from pipeliner.nodes import NODE_MICROGRAPHGROUPMETADATA, NODE_MLMODEL, NODE_LOGFILE

make_files_script = get_job_script("make_cryolo_links_dir.py")


class CrYOLOTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def set_up_micrographs_file(self, output_dir):
        """Make the micrographs file that the cryolo plugin needs to read for
        doing comparisons at command 3"""
        os.makedirs(os.path.join(output_dir, "Micrographs"))
        os.makedirs("CtfFind/job003")
        infile = os.path.join(self.test_data, "JobFiles/CrYOLO/micrographs_ctf.star")
        file_path = "CtfFind/job003/micrographs_ctf.star"
        shutil.copy(infile, file_path)
        return file_path

    def test_get_command_crYOLO_plugin_with_file_input(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        generic_tests.general_get_command_test(
            jobtype="CrYOLO",
            jobfile="crYOLO_file_input_job.star",
            jobnumber=2,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "cryolomodel.h5": f"{NODE_MLMODEL}.h5.cryolo",
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                f"python3 {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star",
                "cryolo_gui.py config cryolo_config.json 256"
                " --filter LOWPASS --low_pass_cutoff 0.1",
                "cryolo_predict.py -c cryolo_config.json -w"
                " cryolomodel.h5 -i Micrographs/ -g 0,1 -o ."
                " -t 0.3",
            ],
            job_out_dir="AutoPick",
        )

    def test_get_command_crYOLO_plugin_with_file_input_continue(self):
        """Test using a starfile as in input"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        os.makedirs("AutoPick/job002")
        apfile = os.path.join(self.test_data, "JobFiles/CrYOLO/autopick.star")
        shutil.copy(apfile, "AutoPick/job002/autopick.star")
        generic_tests.general_get_command_test(
            jobtype="CrYOLO",
            jobfile="crYOLO_continue_job.star",
            jobnumber=2,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "cryolomodel.h5": f"{NODE_MLMODEL}.h5.cryolo",
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                f"python3 {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star"
                " --is_continue",
                "cryolo_gui.py config cryolo_config.json 256"
                " --filter LOWPASS --low_pass_cutoff 0.1",
                "cryolo_predict.py -c cryolo_config.json -w"
                " cryolomodel.h5 -i Micrographs/ -g 0,1 -o ."
                " -t 0.3",
            ],
            job_out_dir="AutoPick",
        )

    def test_get_command_CrYOLO_plugin_with_file_input_use_JANNI(self):
        """Test using a starfile as in input, using JANNI instead of LOWPASS"""
        os.makedirs("CtfFind/job003/")
        shutil.copy(
            os.path.join(self.test_data, "micrographs_ctf.star"),
            os.path.join(self.test_dir, "CtfFind/job003/"),
        )
        touch("cryolomodel.h5")
        generic_tests.general_get_command_test(
            jobtype="CrYOLO",
            jobfile="crYOLO_file_JANNI_job.star",
            jobnumber=2,
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "path/to/JANNI.h5": f"{NODE_MLMODEL}.h5.janni",
                "cryolomodel.h5": f"{NODE_MLMODEL}.h5.cryolo",
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                f"python3 {make_files_script}"
                " --input_mics_file ../../CtfFind/job003/micrographs_ctf.star",
                "cryolo_gui.py config cryolo_config.json 256"
                " --filter JANNI --janni_model path/to/JANNI.h5",
                "cryolo_predict.py -c cryolo_config.json -w"
                " cryolomodel.h5 -i Micrographs/ -g 0,1 -o ."
                " -t 0.3",
            ],
            job_out_dir="AutoPick",
        )

    def test_get_command_CrYOLO_plugin_file_missing(self):
        """Test using a starfile as in input but file is missing"""
        with self.assertRaises((ValueError, RuntimeError)):
            generic_tests.general_get_command_test(
                jobtype="CrYOLO",
                jobfile="crYOLO_file_input_job.star",
                jobnumber=2,
                input_nodes={},
                output_nodes={},
                expected_commands="",
            )

    def test_counting_and_comparing_files(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_dir = "AutoPick/job003/"
        yolojob.working_dir = yolojob.output_dir
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        stardir = "AutoPick/job003/STAR"
        self.set_up_micrographs_file("AutoPick/job003/")
        os.makedirs(stardir)
        for n in range(21, 50):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            touch(f)
        yolojob.post_run_actions()

        in_star = cif.read_file("autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 45):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 45):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            for i in all_coords:
                print(i)
            assert f in all_coords, f

    def test_counting_and_comparing_files_with_warning(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Raise a warning if only a few files are missing testing with one missing"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_dir = "AutoPick/job003/"
        yolojob.working_dir = yolojob.output_dir
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"

        self.set_up_micrographs_file("AutoPick/job003/")
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        for n in range(21, 44):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        yolojob.post_run_actions()

        in_star = cif.read_file("autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 44):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 44):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_with_warning_multi(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Raise a warning if only a few files are missing testing with multiple
        missing"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_dir = "AutoPick/job003/"
        yolojob.working_dir = yolojob.output_dir
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        self.set_up_micrographs_file("AutoPick/job003/")
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        for n in range(21, 36):
            f = os.path.join(stardir, f"20170629_{n:05d}_frameImage.star")
            open(f, "w").close()
        yolojob.post_run_actions()
        in_star = cif.read_file("autopick.star")
        mics_block = in_star.find_block("coordinate_files")
        all_mics = mics_block.find_loop("_rlnMicrographName")
        for n in range(21, 36):
            f = f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            assert f in all_mics, f
        all_coords = mics_block.find_loop("_rlnMicrographCoordinates")
        for n in range(21, 36):
            f = f"AutoPick/job003/STAR/20170629_{n:05d}_frameImage.star"
            assert f in all_coords, f

    def test_counting_and_comparing_files_fail_if_no_part_files(self):
        """Make sure the bit that runs after CrYOLO runs has functioned properly
        Fail if no Particle files were written"""
        yolojob = new_job_of_type("cryolo.autopick")
        yolojob.output_dir = "AutoPick/job003/"
        yolojob.working_dir = yolojob.output_dir
        yolojob.joboptions["input_file"].value = "CtfFind/job003/micrographs_ctf.star"
        self.set_up_micrographs_file("AutoPick/job003/")
        stardir = "AutoPick/job003/STAR"
        os.makedirs(stardir)
        with self.assertRaises(ValueError):
            yolojob.post_run_actions()


if __name__ == "__main__":
    unittest.main()
