#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests.generic_tests import check_for_relion
from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner_tests import test_data


class TestingToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.test_data = os.path.dirname(test_data.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_test(self):
        job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/PostProcess/postprocess.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "emd_3488_mask.mrc")),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_get_command_class2D_scratch_comb_em(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Class2D/class2D_scratch_comb.job"
            ),
            input_nodes={"Extract/job007/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it025_optimiser.star": "ProcessData.star.relion.optimiser.class2d",
                "run_it025_data.star": "ParticlesData.star.relion.class2d",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job007/particles.star --o Class2D/job999/run "
                "--scratch_dir Fake_scratch_dir --pool 30 --pad 2 --ctf "
                "--iter 25 --tau2_fudge 2 --particle_diameter 200 --K 50 "
                "--flatten_solvent --zero_mask --center_classes --oversampling 1"
                " --psi_step 12.0"
                " --offset_range 5 --offset_step 4.0 --norm --scale --j 6 --gpu"
                " 0:1:2:3 --pipeline_control Class2D/job999/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
