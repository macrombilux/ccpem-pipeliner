#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

""" Tools for visualising projects. An installation of pygraphviz, which is dependent
on GraphVis is necessary.  See the README for info about installation of GraphViz"""

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import networkx as nx
import os

from typing import List, Optional, Tuple, Union

from pipeliner.process import Process

from pipeliner.nodes import Node
from pipeliner.project_graph import ProjectGraph
from pipeliner.starfile_handler import DataStarFile
from pipeliner.star_keys import MOVIES_NODE_NAME, MICS_NODE_NAME, PARTS_NODE_NAME

# for illustrating flowcharts
# TODO: Add more colors for other node types
NODES_COLORS = {
    "MicrographMoviesData": "#000000",
    "MicrographsData": "#000000",
    "MicrographsCoords": "#000000",
    "ParticlesData": "#000000",
    "Image2DStack": "#000000",
    "DensityMap": "#000000",
    "Mask3D": "#000000",
    "Mask2D": "#000000",
    "ProcessData": "#000000",
    "Image3D": "#000000",
}


def count_movs_mics_parts(node):
    """Count the number of objects in a movies, mics or particles starfile

    Args:
        node (:class:`~pipeliner.nodes.Node`): The `Node` object
            for the star file

    Returns:
        int: Count of object if the file contained movies, micrographs, or particles
        returns ``None`` if the file is not found or it does not contain the correct
        data type
    """
    nodetype = node.type.split(".")[0]
    if os.path.isfile(node.name):
        data_labels = {
            MOVIES_NODE_NAME: "movies",
            MICS_NODE_NAME: "micrographs",
            PARTS_NODE_NAME: "particles",
        }
        if nodetype in data_labels:
            nodefile = DataStarFile(node.name)
            if nodetype in data_labels:
                return nodefile.count_block(data_labels[nodetype])
            else:
                return None
    return None


class ProcessFlowchart(object):
    """A class for drawing pretty flowcharts for pipeliner projects

    Attributes:
        pipeline (:class:`~pipeliner.project_graph.ProjectGraph`): The pipeline to use
            in drawing the flowchart
        process (:class:`~pipeliner.process.Process`): The process to start
            drawing from for upstream and/or downstream flowcharts
        do_upstream (bool): Should an upstream flowchart be drawn?
        do_downstream (bool): Should a downstream flowchart be drawn?
        do_full (bool): Should a full project flowchart be drawn?
        save (bool): Should the flowchart be saved as a `.png` file?
        show (bool): Should an interactive flowchart be displayed on screen?
        drew_up (str): The name of the upstream flowchart saved, if any
        drew_down (str): The name of the downstream flowchart saved, if any
        drew_full (str): The name of the full flowchart saved, if any

    """

    def __init__(
        self,
        pipeline: Optional[ProjectGraph] = None,
        process: Optional[Process] = None,
        do_upstream: bool = False,
        do_downstream: bool = False,
        do_full: bool = False,
        save: bool = False,
        show: bool = False,
    ):
        """Create a `ProcessFlowchart` object`

        Args:
            pipeline (:class:`~pipeliner.project_graph.ProjectGraph`): The pipeline to
                use in drawing the flowchart.  A :class:`str` process name is also
                acceptable
            process (:class:`~pipeliner.process.Process`): The process to start
                drawing from for upstream and/or downstream flowcharts
            do_upstream (bool): Should an upstream flowchart be drawn?
            do_downstream (bool): Should a downstream flowchart be drawn?
            do_full (bool): Should a full project flowchart be drawn?
            save (bool): Should the flowchart be saved as a `.png` file?
            show (bool): Should an interactive flowchart be displayed on screen?

        Raises:
            ValueError: If upstream or downstream flowcharts are requested and
                no terminal process specified
        """
        self.pipeline = pipeline
        self.do_save = save
        self.do_show = show
        self.do_upstream = do_upstream
        self.do_downstream = do_downstream
        self.do_full = do_full
        # make it able to use process names or process objects
        if isinstance(process, Process):
            self.process = process
        elif isinstance(process, str):
            self.process = self.pipeline.find_process(process)
        elif process is not None:
            raise ValueError(
                "ERROR: Error parsing process name: {}. It should be a string "
                "or pipeliner.Process object".format(process)
            )
        self.save = save
        self.show = show

        is_error = False
        if (do_upstream or do_downstream) and process is None:
            print("ERROR: No process specified for upstream/downstream flow chart")
            is_error = True
        if not do_full and not do_upstream and not do_downstream:
            print("ERROR: Nothing to draw!")
            is_error = True
        self.drew_up: Optional[str] = None
        self.drew_down: Optional[str] = None
        self.drew_full: Optional[str] = None

        if not is_error:
            if do_upstream:
                self.upstream_process_graph()
            if do_downstream:
                self.downstream_process_graph()
            if do_full:
                self.full_process_graph()

    def make_process_flowchart(
        self, edges_list: list, procname: Optional[str], ntype: str
    ) -> Optional[str]:
        """Draw a flowchart and save and/or display it

        Args:
            edges_list (list): A list of edges, as produced by :meth:`format_edges_list`
            procname (str): The name of the process to draw a flowchart for
            ntype (str): The type of flowchart being drawn; `"upstream"` or
                `"downstream"`

        Returns:
            str: The name of the file created if one was saved otherwise ``None``
        """

        # explicitly define graph to get colors right
        g = nx.DiGraph()
        if len(edges_list) == 0:
            print(f"ERROR: No {ntype} processes found for this job. Cannot draw graph")
            return None
        for edge in edges_list:
            icount = "" if edge[3] is None else edge[3]
            ecolor = NODES_COLORS.get(edge[0].split(".")[0], "#000000")
            g.add_edge(edge[1], edge[2], color=ecolor, itemcount=icount)
        edges = g.edges()
        edges_labels = nx.get_edge_attributes(g, "itemcount")
        colors = [g[u][v]["color"] for u, v in edges]
        layout = nx.layout.kamada_kawai_layout(g)

        nodesize_list: list = []
        nodesize = 50

        # color and shape the nodes
        if procname is not None:
            title = "{} processes for job {}".format(ntype, procname)
            node_list = list(g.nodes)
            proc_index = node_list.index(procname)
            color_list: list = ["#808080"] * (proc_index)
            color_list.append("#ff0000")
            fwdlist: List[Union[str, int]] = ["#808080"] * (
                len(node_list[proc_index:]) - 1
            )
            color_list.extend(fwdlist)

            nodesize_list = [50] * (proc_index)
            nodesize_list.append(250)
            fwdlist = [50] * (len(node_list[proc_index:]) - 1)
            nodesize_list.extend(fwdlist)

        else:
            title = "Full pipeline"
            color_list = ["#808080"]

        plt.figure()

        # draw the network graph
        nx.draw(
            g,
            pos=layout,
            node_size=nodesize_list or nodesize,
            node_color=color_list,
            font_size=6,
            with_labels=True,
            min_source_margin=0,
            min_target_margin=0,
            horizontalalignment="center",
            verticalalignment="top",
            font_weight="bold",
            connectionstyle="arc3,rad=0.1",
            edge_color=colors,
            arrows=True,
        )

        # draw the labels
        nx.draw_networkx_edge_labels(
            g,
            pos=layout,
            edge_labels=edges_labels,
            font_size=4,
            bbox={"alpha": 0},
        )

        # draw the legend
        legend_lines = [
            Line2D([0], [0], color=(0.75, 0.75, 0.75), lw=2),
            Line2D([0], [0], color=(1, 0, 1), lw=2),
            Line2D([0], [0], color=(0, 0, 1), lw=2),
            Line2D([0], [0], color=(0, 1, 1), lw=2),
            Line2D([0], [0], color=(1, 0, 0), lw=2),
            Line2D([0], [0], color=(0, 1, 0), lw=2),
            Line2D([0], [0], color=(1, 1, 0), lw=2),
        ]
        plt.title(title)
        plt.legend(
            legend_lines,
            (
                "micrographs",
                "coordinates",
                "particles",
                "classes",
                "map",
                "mask",
                "halfmap",
            ),
        )
        imgname = "Not saved"
        if self.save:
            if ntype == "full" or not procname:
                imgname = "full_project_flowchart.png"
            else:
                imgname = procname.replace("/", "_") + ntype + ".png"
            plt.savefig(imgname)
            print("Saved image: " + imgname)

        if self.show:
            plt.show()
        plt.close()
        return imgname

    def format_edges_list(
        self, il: List[Tuple[Optional[Node], Optional[Process], Process]]
    ) -> List[Tuple[str, str, str, Optional[int]]]:
        """Convert the output of network finding functions to format for graph drawing

        (node, parent, child) to (Node name, parent name, child name, count)
        formats:
        (Node, Process, Process) to (str, str, str, int)

        Args:
            il (list): list of tuples in the format
            [node, parent, child], [Node, Process, Process]

        Returns:
            list: list of tuples [Node name, parent name, child name, count],
                [str, str, str, int]
        """
        edges = []
        for x in il:
            if x[0] is not None and x[1] is not None:
                edges.append(
                    (x[0].type, x[1].name, x[2].name, count_movs_mics_parts(x[0]))
                )
            elif x[2]:
                edges.append(("", "", x[2].name, None))
        return edges

    def downstream_process_graph(self) -> None:
        """Make a flowchart for a job and all downstream processes"""
        edges: List[Tuple[Optional[Node], Optional[Process], Process]] = []
        if self.pipeline:
            edges = self.pipeline.get_downstream_network(self.process)
        print("Drawing downstream process graph for " + self.process.name)
        edges_list = self.format_edges_list(edges)
        self.drew_down = self.make_process_flowchart(
            edges_list, self.process.name, "child"
        )

    def upstream_process_graph(self) -> None:
        """Make a flowchart for a job and all upstream processes"""
        edges: List[Tuple[Optional[Node], Optional[Process], Process]] = []
        if self.pipeline:
            edges = self.pipeline.get_upstream_network(self.process)
        edges_list = self.format_edges_list(edges)
        self.drew_up = self.make_process_flowchart(
            edges_list, self.process.name, "parent"
        )

    def full_process_graph(self) -> None:
        """Make a flowchart for an entire project"""
        print("Drawing full process graph")
        edges: List[Tuple[Optional[Node], Optional[Process], Process]] = []
        if self.pipeline:
            edges = self.pipeline.get_whole_project_network()
        # convert the edges list to the format the graph drawing functiosn want
        edges_list = self.format_edges_list(edges)
        # count the number of processes in the final graph
        self.drew_full = self.make_process_flowchart(edges_list, None, "full")
