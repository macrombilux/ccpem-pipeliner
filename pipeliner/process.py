#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import Optional
from pipeliner.utils import decompose_pipeline_filename


class Process(object):
    """A Process represents a job that has been run by the Pipeliner

    Attributes:
        name (str): The name of the process.  It should be in the format
            `"<jobtype>/jobxxx/"`.  The trailing slash is required
        alias (str): An alternate name for the process to make it easier to identify
        outdir (str): The directory the process was written into
        p_type (str): The process' type
        status (str): The processes' status 'running, scheduled, successful,
            failed, or aborted'
        input_nodes (list):  `Node` objects for files the process use as inputs
        output_nodes(list): `Node` objects for files the process produces
    """

    def __init__(
        self, name: str, p_type: str, status: str, alias: Optional[str] = None
    ):

        self.name = str(name)
        self.alias = alias
        self.outdir = str(name).split("/")[0]
        self.type = p_type
        self.status = status
        # needs both inputs and outputs
        self.input_nodes: list = []  # nodes used by this process
        self.output_nodes: list = []  # nodes produced by this process
        self.job_number = decompose_pipeline_filename(self.name)[1]

    def clear(self):
        self.input_nodes.clear()
        self.output_nodes.clear()
