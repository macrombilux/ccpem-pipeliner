#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import Union, Tuple, Any, TypedDict
from collections.abc import Mapping
from copy import deepcopy
from glob import glob
from dataclasses import asdict

from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import date_time_tag
from pipeliner.job_factory import active_job_from_proc
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EmpiarParticlesType,
    EmpiarMovieSetType,
    EmpiarRefinedParticlesType,
    EmpiarCorrectedMicsType,
)
from pipeliner.deposition_tools.pdb_deposition_objects import (
    FinalReconstructionType,
    CtfCorrectionType,
    ParticleSelectionType,
    OtherStartupModelType,
    EmdbStartupModelType,
    PdbModelStartupModelType,
    RandomConicalTiltStartupModelType,
    InitialAngleType,
    InSilicoStartupModelType,
    FinalAngleType,
    FinalMultiReferenceAlignmentType,
    Final3DClassificationType,
    Final2DClassificationType,
)


class DepDict(TypedDict):
    """A dict that holds experiment type and a list of the image sets"""

    experiment_type: int
    imagesets: list


def merge(source: dict, overrides: Union[Mapping, dict]) -> dict:
    """Return a new dictionary by merging two dictionaries recursively."""

    result = deepcopy(source)
    if not result:
        return dict(overrides)
    for key, value in overrides.items():
        if value and key:
            if isinstance(value, Mapping):
                result[key] = merge(result.get(key, {}), value)
            elif key == "software_list":
                result[key] = []
                for sware in list(source[key]) + list(overrides[key]):
                    if sware not in result["software_list"]:
                        result[key].append(sware)
            else:
                result[key] = deepcopy(overrides[key])
    return result


def remove_nested_dict_top_key(in_dict: dict) -> dict:
    """Strip the outer key off a dict with a single outer key

    IE: {outer_key:{Key: value, ...}}
    """
    if not len(in_dict) == 1:
        raise ValueError(
            "remove_nested_dict_top_key() requires a nested dict with a single "
            f"top-level key, input dict has {len(in_dict)}"
        )
    key = list(in_dict)[0]
    return in_dict[key]


# method:  work backwards from terminal job
#    for each parent that should contribute run that parents prepare_onedep_info()
#    function (to be written) then prepare json from all using all of the deposition
#    objects.

# TODO: also later do one for helical


class OneDepDeposition(object):
    """A class for preparing depositions using the OneDep system

    Will also be used to generate other depositions in the future (EMPIR)
    replacing the cirrent ad hoc EMPIR deposition preparation function

    Attributes:
        terminal_proc (:class:`~pipeliner.process.Process`): The process
            that was chosen to make the deposition from
        upstream (list[:class:`~pipeliner.process.Process`]): All of the
            processes upstream of the terminal process, in order
        final_depo (dict): A dict that contains all of the info for the final
            deposition
        procs_depobjs (Dict{str:NamedTuple}): Contains all of processes in the upstream
            list and the deposition objects they have produced.

    """

    def __init__(self, pipeline: ProjectGraph, terminal_job: str):
        """create a OneDepDeposition object

        Args:
            pipeline (:class:~pipeliner.project_graph.ProjectGraph): The pipeline
                for the current project
            terminal_job (str): The name of the job to create the deposition from

        """
        tproc = pipeline.find_process(terminal_job)
        if tproc:
            self.terminal_proc = tproc
        self.upstream = pipeline.get_project_procs_list(terminal_job)
        self.final_depo: dict = {}
        self.procs_depobjs = {}
        # get all the deposition objects for the processes in the chain
        print("Preparing deposition info")
        for proc in self.upstream:
            job = active_job_from_proc(proc)
            self.procs_depobjs[proc.name] = job.prepare_onedep_data()

    def gather_depdata(
        self, dep_type: Any, reverse: bool = False, is_empiar: bool = False
    ):
        """Gather deposition data for a specific toplevel depo object type

        Prioritize later jobs over earlier jobs unless reverse is True
        Only takes data from the first job of each type IE: if job001 = ctffind,
        job002 = ctffind, job003 = ctfrefine.

        reverse is false for CtfCorrectionType so job003 will form the basis of the
        depo dict and will be updated with info from job002 but job001 will be ignored

        if reverse were true job001 would form the basis of the depo dict, job002 would
        be ignored, and it would be updated with data from job003 because it is a
        different type

        reverse is used for initial parameters like particle picking and inital angle
        assignment where early jobs are important

        non-reverse is used for final parameters where the later jobs are the important
        ones

        Args:
            dep_type (object): The type of deposition object
            reverse (bool): Switch the priority, use earlier jobs of that type
                over later ones
            is_empiar (bool): Is this an empiar deposition: If so the top level
                dict key needs to be stripped from the dicts

        Returns:
            dict: The merged deposition objects as a nested set of dicts
        """
        done_proctypes, dep_objs = [], []
        procs = self.upstream

        # sort the procs depending on precedence set by reverse
        procs.sort(key=lambda x: x.job_number, reverse=reverse)

        # get all the depo objects in order, only use the first/last one from any
        # given proc type

        for proc in procs:
            for depobj in self.procs_depobjs[proc.name]:
                if type(depobj) == dep_type and proc.type not in done_proctypes:
                    dep_objs.append(depobj)
                    done_proctypes.append(proc.type.split(".")[1])

        # quit if none of the correct dep objs found
        if len(dep_objs) == 0:
            return

        # convert dep objects to nested dicts
        # fix names for dicts that have top level keys that weren't conducive
        # to being used as dataclass attributed
        convert_names = {
            "in_silico_startup_model": 'startup_model type_of_model = "insilico"',
            "other_startup_model": 'startup_model type_of_model = "other"',
            "random_conical_tilt_startup_model": "startup_model type_of_model = "
            '"random_conical_tilt"',
            "emdb_id_starting_model": 'startup_model type_of_model = "emdb_id"',
            "bfactorSharpening": "b-factorSharpening",
            "multiframe_micrograph_movies": "Multiframe micrograph movies",
            "particle_images": "Particle images",
            "per_particle_motion_corrected_particle_images": "Per-particle motion "
            "corrected particle images",
            "corrected_micrographs": "Corrected micrographs",
        }
        dds = []
        for dopob in dep_objs:
            the_dict = asdict(dopob)
            key = list(the_dict)[0]
            if key in convert_names:
                dds.append({convert_names[key]: the_dict[key]})
            else:
                dds.append(the_dict)

        # use the 1st (lowest priority) as the base dict
        depdict = remove_nested_dict_top_key(dds[0]) if is_empiar else dds[0]

        # update dep dict with increasing priority deposition objects
        for dodict in dds[1:]:
            depobj_dict = remove_nested_dict_top_key(dodict) if is_empiar else dodict
            depdict = merge(depdict, depobj_dict)

        # update the main dict with the deposition dict
        depobj_name = list(dep_objs[0].__dict__)[0]
        self.final_depo[depobj_name] = depdict

    def prepare_pdbemdb_deposition_dict(self, deptype: str) -> dict:
        """Prepare the dictionary containing all of the deposition information

        Args:
            deptype (str): The type of deposition to prepare

        Returns:
            dict: The information for the deposition in dict for ready to create a
                json
        """
        # make sure the terminal process has the right sort of deposition objects
        # TO DO: make a similar check for pdb depositions when they are prepared
        termproc_objs = [type(x) for x in self.procs_depobjs[self.terminal_proc.name]]
        if FinalReconstructionType not in termproc_objs and deptype == "emdb":
            raise ValueError(
                f"The job {self.terminal_proc.name} is not of the appropriate type to "
                f"prepare a {deptype} deposition, find a job that returns a final "
                "map such as Refine3D, PostProcess, or LocalRes"
            )

        # some deposition objects should be prioritized in reverse order
        # ie the first time it was done should be included rather than the most recent
        # {depo type: {depo obj: shoud_it_be_reversed?}}
        depotypes = {
            "emdb": {
                CtfCorrectionType: False,
                ParticleSelectionType: True,
                OtherStartupModelType: False,
                EmdbStartupModelType: False,
                PdbModelStartupModelType: False,
                RandomConicalTiltStartupModelType: False,
                InSilicoStartupModelType: False,
                InitialAngleType: True,
                FinalAngleType: False,
                FinalMultiReferenceAlignmentType: False,
                Final2DClassificationType: False,
                Final3DClassificationType: False,
                FinalReconstructionType: False,
            },
            "pdb": {
                CtfCorrectionType: False,
                ParticleSelectionType: True,
                EmdbStartupModelType: False,
                PdbModelStartupModelType: False,
                RandomConicalTiltStartupModelType: False,
                InSilicoStartupModelType: False,
                InitialAngleType: True,
                FinalAngleType: False,
                FinalMultiReferenceAlignmentType: False,
                Final2DClassificationType: False,
                Final3DClassificationType: False,
                FinalReconstructionType: False,
                # plus more as needed
            },
        }

        # go through the different fields and update the main dict for each
        for dep_field in depotypes[deptype]:
            self.gather_depdata(dep_field, depotypes[deptype][dep_field])

        return self.final_depo

    def prepare_empiar_deposition_dict(
        self, do_rawmic=False, do_cormic=False, do_parts=False, do_corparts=False
    ) -> Tuple[DepDict, str]:
        """Prepare the dictionary containing all of the deposition information

        Args:
            do_rawmic (bool): Include raw micrographs in the deposition
            do_cormic (bool): Include motioncorrected micrographs in the deposition
            do_parts (bool): Include particles in the deposition
            do_cormic (bool): Include per-particle motioncorrected
                particles in the deposition
            do_corparts (bool): Include corrected (polished) particles in the deposition

        Returns:
            dict: The information for the deposition in dict for ready to create a
                json
        """
        if all([not x for x in (do_rawmic, do_cormic, do_parts, do_corparts)]):
            raise ValueError(
                "Nothing to do! Select one or more of the deposition fields"
            )

        # which type of imagesets to make:
        imgsets = {
            EmpiarMovieSetType: do_rawmic,
            EmpiarCorrectedMicsType: do_cormic,
            EmpiarParticlesType: do_parts,
            EmpiarRefinedParticlesType: do_corparts,
        }

        # go through the different imageset types and update the main dict for each
        for dep_field in imgsets:
            if imgsets[dep_field]:
                self.gather_depdata(dep_field, is_empiar=True)

        # add the extra info to the final depo dict and make a list of
        # files for the archive
        files_patterns = set()

        depdict: DepDict = {"experiment_type": 3, "imagesets": []}
        for depinfo in self.final_depo:
            depdict["imagesets"].append(self.final_depo[depinfo])

            # get file search strings for archiving
            mfp = self.final_depo[depinfo].get("micrographs_file_pattern")
            ppfp = self.final_depo[depinfo].get("picked_particles_file_pattern")
            for i in (mfp, ppfp):
                if i is not None:
                    files_patterns.add(i)

        # make the archive dir
        depdir = f"{date_time_tag(True)}_EMPIAR_DEPOSITION"
        os.makedirs(depdir)
        print("Preparing deposition files:")
        for fs in files_patterns:
            files = glob(fs)
            fdir = os.path.join(depdir, os.path.dirname(fs))
            if not os.path.isdir(fdir):
                os.makedirs(fdir)
            for f in files:
                linkname = os.path.join(depdir, f)
                os.symlink(f, linkname)

        return depdict, depdir
