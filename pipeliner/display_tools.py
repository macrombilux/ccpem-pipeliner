#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import numpy as np
import os
import math
import mrcfile
import traceback
from typing import List, Optional, Union, Dict, Tuple
from PIL import Image
from gemmi import cif
from ccpem_utils.map.mrcfile_utils import bin_mrc_map
from pipeliner.mrc_image_tools import (
    threed_array_to_montage,
    mrc_thumbnail,
    tiff_thumbnail,
    read_tiff,
)
from pipeliner.mrc_image_tools import bin_array_to_size
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
    ResultsDisplayPending,
    ResultsDisplayGraph,
    ResultsDisplayMontage,
    ResultsDisplayHistogram,
    ResultsDisplayMapModel,
    RESULTS_DISPLAY_OBJECTS,
    get_next_resultsfile_name,
)


def create_results_display_object(dobj_type: str, **kwargs) -> ResultsDisplayObject:
    """Safely create a results display object

    Returns a ResultsDisplayPending if there are any problems.  Give it the type of
    display object as the first argument followed by the kwargs for that specific
    type of ResultsDisplayObject

    Args:
        type (str): The type of DisplayObject to create
    """
    dt = RESULTS_DISPLAY_OBJECTS.get(dobj_type)
    if dt is None:
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Unknown DisplayObject type: {type}",
            start_collapsed=False,
        )

    try:
        return dt(**kwargs)
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating {dt.__name__}: {str(e)} \nTraceback: {tb}",
        )


def graph_from_starfile_cols(
    title: str,
    starfile: str,
    block: str,
    ycols: list,
    xcols: list = None,
    xrange: list = None,
    yrange: list = None,
    data_series_labels: list = None,
    xlabel: str = "",
    ylabel: str = "",
    assoc_data: List[str] = None,
    modes: List[str] = ["lines"],
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayGraph, ResultsDisplayPending]:
    """Automatically generate a ResultsDisplayGraph object from a starfile

    Can use one or two columns and third column for labels if desired

    Args:
        title (str): The title of the final graph
        starfile (str): Path to the star file ot use
        block (str): The block to use in the starfile, use `None` for a starfile
            with only a single block
        ycols (list): Column label(s) from the star file to use for the y data series
        xcols (list): Column label(s) from the star file to use for the y data series
            if `None` a simple count from 1 will be used
        xlabel (str): Label for the x axis, if no x data are specified the label will
            'Count', if x data are specified and the xlabel is `None` the x axis label
            will be the name of the starfile column used
        xrange (list): Range for x vlaues to be displayed, full range if `None`
        yrange (list): Range for y vlaues to be displayed, full range if `None`
        data_series_labels (list): Names for the data series
        ylabel (str): Label for the y axis, if  `None` the y axis label will be the
            name of the starfile column used
        assoc_data (list): List of data file(s) associated with this graph
        modes (list): Controls the appearance of each data series, choose from 'lines',
            'markers' 'or lines+markers'
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayGraph`: A
        ResultsDisplayGraph object for the created graph
    """
    try:
        star = cif.read_file(starfile).find_block(block)
        x_data_series = []
        y_data_series = []
        for ycol in ycols:
            try:
                ydata = [float(x[0]) for x in star.find([ycol])]
            except ValueError:
                ydata = [x[0] for x in star.find([ycol])]
            y_data_series.append(ydata)

        if xcols is None:
            for yds in y_data_series:
                x_data_series.append([float(x) for x in range(1, len(yds) + 1)])
        else:
            for xcol in xcols:
                try:
                    xdata = [float(x[0]) for x in star.find([xcol])]
                except ValueError:
                    xdata = [x[0] for x in star.find([xcol])]
                x_data_series.append(xdata)

        ylabel = ycols[0] if not ylabel else ylabel
        if not xlabel:
            xlabel = "Count" if xcols is None else xcols[0]
        assoc_data = [starfile] if assoc_data is None else assoc_data

        return create_results_display_object(
            "graph",
            title=title,
            xvalues=x_data_series,
            xaxis_label=xlabel,
            xrange=xrange,
            yrange=yrange,
            yvalues=y_data_series,
            yaxis_label=ylabel,
            data_series_labels=data_series_labels,
            associated_data=assoc_data,
            modes=modes,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore  # limited return options  # noqa: E501
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating graph display object: {str(e)} \nTraceback: {tb}",
        )


def histogram_from_starfile_col(
    title: str,
    starfile: str,
    block: str,
    data_col: str,
    xlabel: str = "",
    ylabel: str = "Count",
    assoc_data: Optional[List[str]] = None,
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayHistogram, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501
    """Automatically generate a ResultsDisplayHistogram object from a starfile

    Args:
        title (str): The title of the final graph
        starfile (str): Path to the star file ot use
        block (str): The block to use in the starfile, use `None` for a starfile
            with only a single block
        data_col (str): Column label from the star file to use for the data series
        xlabel (str): Label for the x axis, if no x data are specified the label will
            'Count', if x data are specified and the xlabel is `None` the x axis label
            will be the name of the starfile column used
        ylabel (str): Label for the y axis, if  `None` the y axis label will be the
            name of the starfile column used
        assoc_data (list): List of data file(s) associated with this graph
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    """
    try:
        star = cif.read_file(starfile).find_block(block)

        try:
            alldata = [float(x[0]) for x in star.find([data_col])]
        except ValueError:
            alldata = [x[0] for x in star.find([data_col])]

        xlabel = data_col if not xlabel else xlabel

        assoc_data = [starfile] if not assoc_data else assoc_data

        return create_results_display_object(
            "histogram",
            title=title,
            data_to_bin=alldata,
            xlabel=xlabel,
            ylabel=ylabel,
            associated_data=assoc_data,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore  # limited return options  # noqa: E501

    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating histogram display object: {str(e)} \nTraceback:"
            f"{tb}",
        )


def get_ordered_classes_arrays(
    model_file: str,
    ncols: int,
    boxsize: int,
    outname: str,
    parts_file: Optional[str] = None,
    title: str = "2D class averages",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501
    """Return a 3D array of class averages from a Relion Class2D model file

    Args:
        model_file (str): Name of the model file
        ncols (int): number of columns desired in the file montage
        boxsize (int): Size of the class averages in the final montage
        outname (str): Name of the output image
        parts_file (str): Path of the file containing the particles, for counting
        title (str): A title for the DisplayObject
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message

    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayMontage`: An object
        for the GUI to use to render the graph
    """
    try:
        star = cif.read_file(model_file)
        class_info = star.find_block("model_classes").find(
            ["_rlnReferenceImage", "_rlnClassDistribution"]
        )

        # count the particles if possible
        asoc_data_files = [model_file]
        if parts_file is not None:
            pblock = cif.read_file(parts_file).find_block("particles")
            nparts = int(str(pblock[0].loop).split()[1])
            asoc_data_files.append(parts_file)
        else:
            nparts = 0

        # get the info on each class
        classdict: Dict[int, Tuple[float, str]] = {}
        x, y, xs, ys, labels = 0, 0, [], [], []
        for i in class_info:
            clno = int(i[0].split("@")[0]) - 1
            cldist = float(i[1])
            xs.append(x)
            ys.append(y)
            if x != ncols - 1:
                x += 1
            else:
                x = 0
                y += 1
            pcount = f"; {int(nparts * cldist)} particles" if nparts > 0 else ""
            classdict[clno] = (cldist, f"Class {clno}; {cldist * 100:.02f}%{pcount}")

        # pad the last row if there were unused slots
        square = (ys[-1] + 1) * ncols
        dif = square - len(class_info)
        extralabels = []
        if dif != 0:
            xs.extend([xs[-1] + n for n in range(1, dif + 1)])
            ys.extend([ys[-1]] * dif)
            extralabels = [""] * dif
        # reverse the ys (Assuming 0,0 == bottom left)
        ys.reverse()

        # sort the classes by class distribution
        classlist = list(classdict)
        classlist.sort(key=lambda x: classdict[x][0], reverse=True)

        # write the labels in order
        for clno in classlist:
            labels.append(classdict[clno][1])
        labels.extend(extralabels)

        stackfile = class_info[0][0].split("@")[1]

        with mrcfile.open(stackfile) as stack:
            stack_data = stack.data

        reordered = np.zeros(stack_data.shape)
        n = 0
        for img in classlist:
            reordered[n, :, :] = stack_data[img, :, :]
            n += 1

        # create the montage image
        od = os.path.dirname(outname)
        if not os.path.isdir(od) and od != "":
            os.makedirs(os.path.dirname(outname))
        threed_array_to_montage(reordered, ncols, boxsize, outname)

        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=labels,
            associated_data=asoc_data_files,
            img=outname,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore  # limited return options  # noqa: E501
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def make_map_model_thumb_and_display(
    outputdir: str,
    maps: Optional[List[str]] = None,
    maps_opacity: Optional[List[float]] = None,
    maps_colours: Optional[List[str]] = None,
    models: Optional[List[str]] = None,
    models_colours: Optional[List[str]] = None,
    title: Optional[str] = None,
    thumbsize: int = 64,
    maps_data: str = "",
    models_data: str = "",
    assoc_data: Optional[List] = None,
    start_collapsed: bool = True,
    flag: str = "",
) -> Union[ResultsDisplayMapModel, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501
    """Make a display object for an atomic model overlaid over a map

    Makes a binned map and a ResultsDisplayMapModel display object

    Args:
         outputdir (str): Name of the job's output directory
         maps (list): List of map files to use
         models (list): List of model files to use
         maps_opacity (list): List of opacity for the maps, from 0-1
             if `None` 0.5 is used for all
         title (str): The title for the ResultsDisplayMapModel object, if `None`
             the name of the map and model will be used
         thumbsize (int): Desired dimensions of binned map (must be cubic)
         maps_data (str): Any additional data to be included about the map
         models_data (str): Any additional data to be included about the map
         assoc_data (list): List of associated data, if left as `None`
             then just uses the file itself
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If the results are considered scientifically dubious explain in this
            string

    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayMapModel`: The
        DisplayObject for the map and model
    """
    # make the thumbnails dir if necessary
    try:
        maps = maps if maps is not None else []
        models = models if models is not None else []
        maps_opacity = maps_opacity if maps_opacity is not None else []
        maps_colours = maps_colours if maps_colours is not None else []
        models_colours = models_colours if models_colours is not None else []
        outdir = os.path.join(outputdir, "Thumbnails")
        if not os.path.isdir(outdir) and maps:
            os.makedirs(outdir)

        # prepare the thumbnail
        outmaps = []
        for mapfile in maps:
            outmap = os.path.join(outdir, os.path.basename(mapfile))
            bin_mrc_map(mapfile, new_dim=thumbsize, map_output=outmap)
            outmaps.append(outmap)

        # create the results display object
        ad = maps + models if assoc_data is None else assoc_data

        # figure out the title
        if len(maps) == 0 and len(models) == 1:
            obj_title = f"Model: {models[0]}"
        elif len(maps) == 1 and len(models) == 0:
            obj_title = f"Map: {maps[0]}"
        elif len(maps) == 1 and len(models) == 1:
            obj_title = f"Overlaid map: {maps[0]} model: {models[0]}"
        else:
            tmap = "map" if len(maps) >= 1 else ""
            tmap = tmap + "s" if len(maps) > 1 else tmap
            sp = "/" if len(maps) > 0 and len(models) > 0 else ""
            tmod = "model" if len(models) >= 1 else ""
            tmod = tmod + "s" if len(models) > 1 else tmod
            obj_title = f"Overlaid {tmap}{sp}{tmod}"

        dispobj = create_results_display_object(
            "mapmodel",
            title=obj_title if title is None else title,
            maps=outmaps,
            models=models,
            maps_opacity=[] if maps_opacity is None else maps_opacity,
            maps_colours=maps_colours,
            maps_data=", ".join(maps) if maps_data == "" else maps_data,
            models_data=", ".join(models) if models_data == "" else models_data,
            models_colours=models_colours,
            associated_data=ad,
            start_collapsed=start_collapsed,
            flag=flag,
        )
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating map-model display object: {str(e)} "
            f"\nTraceback: {tb}",
        )  # type: ignore  # limited return options  # noqa: E501
    return dispobj  # type: ignore  # limited return options  # noqa: E501


def mini_montage_from_many_files(
    filelist: List[str],
    outputdir: str,
    nimg: int = 5,
    montagesize: int = 640,
    title: str = "",
    ncols: int = 5,
    montage_n: int = 0,
    associated_data: Optional[List[str]] = None,
    labels: Optional[List[str]] = None,
    cmap: str = "",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501
    """Make a mini montage from a list of images

    Merge and flatten image stacks

    Args:
        filelist (list): A list of the files to use
        outputdir (str): The output dir of the pipeliner job
        nimg (int): Number of images to use in the montage
        montagesize (int): Desired size of the final montage image
        title (str): Title for the ResultsDisplay object that will be output
        ncols (int): Number of columns to make in the montage
        montage_n (int): A number to make a unique name for the montage
        associated_data (list): Data files associated with these images, if `None`
            then all of the selected images
        labels (list): The labels for the items in the montage
        cmap (str): colormap to apply, if any
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message

    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayImage`: The
        DisplayObject for the map
    Raises:
        ValueError: If a non mrc or tiff image is used
    """
    # get the images
    try:
        if nimg < 1 or nimg > len(filelist):
            images = filelist
            nimg = len(images)
        else:
            images = filelist[:nimg]
        nimg = len(images) if len(images) < nimg else nimg
        ncols = nimg if ncols > nimg else ncols
        imgsize = montagesize // ncols
        nrows = math.ceil(nimg / ncols)

        img_arrays = []
        for img in images:
            ext = img.split(".")[-1]
            if ext in ["mrc", "mrcs"]:
                img_arrays.append(mrc_thumbnail(img, imgsize))
            elif ext in ["tif", "tiff"]:
                img_arrays.append(tiff_thumbnail(img, imgsize))
            else:
                raise ValueError("Invalid image type")
        zstack = np.stack(img_arrays, axis=0)
        thumbs_dir = os.path.join(outputdir, "Thumbnails")

        outfile = get_next_resultsfile_name(thumbs_dir, "montage_f*.png")
        if not os.path.isdir(thumbs_dir):
            os.makedirs(thumbs_dir)
        threed_array_to_montage(zstack, ncols, imgsize, outfile, cmap, inpnormed=True)

        xs = list(range(ncols)) * nrows
        ys = []
        for i in list(range(nrows)).__reversed__():
            ys.extend([i] * ncols)

        labels = images if not labels else labels
        associated_data = images if not associated_data else associated_data
        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=labels,
            img=outfile,
            associated_data=associated_data,
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore  # limited return options  # noqa: E501
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def mini_montage_from_stack(
    stack_file: str,
    outputdir: str,
    nimg: int = 40,
    ncols: int = 10,
    montagesize: int = 640,
    title: str = "",
    labels: Optional[List[Union[int, str]]] = None,
    montage_n: int = 0,
    cmap: str = "",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501
    """Make a montage from a mrcs or tiff file

    Args:
        stack_file (str): The path to the stack_file
        outputdir (str): The output dir of the pipeliner job
        nimg (int): Number of images to use in the montage, if < 1 uses all of them
        ncols (int): Number of columns to use
        montagesize (int): Desired size of the final montage image
        title (str): Title for the ResultsDisplay object that will be output
        labels (list): Labels for the images
        montage_n (int): A number to make a unique name for the montage
        cmap (str): colormap to apply, if any
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayImage`: The
        DisplayObject for the map
    Raises:
        ValueError: If a non mrc or tiff image is used
    """
    try:
        # get the images
        imgsize = montagesize // ncols

        ext = os.path.basename(stack_file).split(".")[-1]
        if ext in ["mrcs", "mrc"]:
            with mrcfile.open(stack_file) as mrcs:
                imgstack = mrcs.data
        elif ext in ["tiff", "tif"]:
            imgstack = read_tiff(stack_file)
        else:
            return create_results_display_object(
                "text",
                title="Error creating results display",
                display_data=f"Illegal file type for mini_montage_from_stack(): .{ext}",
                associated_data=[stack_file],
                start_collapsed=start_collapsed,
                flag=flag,
            )  # type: ignore  # limited return options  # noqa: E501
        img_count = imgstack.shape[0]

        # check there are enough images and labels and update accordingly
        nimg = img_count if nimg < 1 else nimg
        nimg = img_count if nimg > img_count else nimg
        ncols = nimg if ncols > nimg else ncols
        labels = [int(x) for x in range(nimg)] if not labels else labels
        stackslice = imgstack[:nimg, :, :]
        # make the montage image
        thumbdir = os.path.join(outputdir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        outfile = get_next_resultsfile_name(thumbdir, "montage_m*.png")
        threed_array_to_montage(stackslice, ncols, imgsize, outfile, cmap)

        nrows = math.ceil(nimg / ncols)
        xs = list(range(ncols)) * nrows
        ys = []
        for i in list(range(nrows)).__reversed__():
            ys.extend([i] * ncols)
        if len(labels) != len(ys):
            labels.extend([""] * (len(ys) - len(labels)))

        return create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=labels,
            img=outfile,
            associated_data=[stack_file],
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore  # limited return options  # noqa: E501
    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def mini_montage_from_starfile(
    starfile: str,
    block: str,
    column: str,
    outputdir: str,
    title: str = "",
    nimg: int = 20,
    montagesize: int = 640,
    ncols: int = 10,
    montage_n: int = 0,
    labels: Optional[List[str]] = None,
    cmap: str = "",
    start_collapsed: bool = False,
    flag: str = "",
) -> Union[ResultsDisplayMontage, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501
    """Make a montage from a list of images in a starfile column

    Merge and flatten image stacks if they are encountered.

    Args:
        starfile (str): The starfile to use
        block (str): The name of the block with the images
        column (str): The name of the column that has the images
        outputdir (str): The output dir of the pipeliner job
        title (str): The title for the object, automatically generated if ""
        nimg (int): Number of images to use in the montage, uses all if < 1
        montagesize (int): Desired size of the final montage image
        ncols (int): number of columns to use
        montage_n (int): A number to make a unique name for the montage
        labels (list): Labels for the images in the montage, in order
        cmap (str): colormap to apply, if any
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    Returns:
        :class:`~pipeliner.results_display_objects.ResultsDisplayImage`: The
        DisplayObject for the map
    Raises:
        ValueError: If a non mrc or tiff image is encountered
    """
    try:
        if block in ("", None):
            stardat = cif.read_file(starfile).sole_block()
        else:
            stardat = cif.read_file(starfile).find_block(block)

        imgdata = [x[0] for x in stardat.find([column])]
        # check the counts of images and columns
        nimg = len(imgdata) if nimg < 1 else nimg
        nimg = len(imgdata) if nimg > len(imgdata) else nimg
        images = imgdata[:nimg]

        ncols = nimg if ncols > nimg else ncols

        # if all the image list is straight images just make the object
        if all([os.path.isfile(x) for x in images]):
            dispobj = mini_montage_from_many_files(
                filelist=images,
                outputdir=outputdir,
                nimg=nimg,
                ncols=ncols,
                montagesize=montagesize,
                title=f"{starfile}; {nimg}/{len(imgdata)} images",
                montage_n=montage_n,
                associated_data=[starfile],
                labels=labels,
                start_collapsed=start_collapsed,
                flag=flag,
            )
            return dispobj

        # if the images are slices from stacks, they need to be handled separately
        # remember they count from 1 not 0
        if not all(["@" in x for x in images]):
            raise ValueError(
                "Unable to parse images in star file. Possibly a mix of stack slices"
                " and single images, or files are missing"
            )

        arrays = []
        files_to_read = set()
        # check all the images get the stacks they came from
        for img in images:
            files_to_read.add(img.split("@")[-1])
        image_arrays = {}
        for f in files_to_read:
            with mrcfile.open(f) as mrc:
                image_arrays[f] = mrc.data
        for img in images:
            imsplit = img.split("@")
            n = int(imsplit[0])
            image = imsplit[1]
            arrays.append(image_arrays[image][n - 1, :, :])

        zstack = np.stack(arrays, axis=0)
        thumbsdir = os.path.join(outputdir, "Thumbnails")
        if not os.path.isdir(thumbsdir):
            os.makedirs(thumbsdir)
        output_image = get_next_resultsfile_name(thumbsdir, "montage_s*.png")
        boxsize = montagesize // ncols
        montage = threed_array_to_montage(
            zstack, ncols=ncols, boxsize=boxsize, outname=output_image, cmap=cmap
        )

        outimg = Image.fromarray(montage)
        outimg = outimg.convert("RGB")
        outimg.save(output_image)

        nrows = math.ceil(nimg / ncols)
        xs = list(range(ncols)) * nrows
        ys = []
        for i in list(range(nrows)).__reversed__():
            ys.extend([i] * ncols)
        if len(images) != len(ys):
            images.extend([""] * (len(ys) - len(images)))
        title = title if title != "" else f"{starfile}; {nimg}/{len(imgdata)} images"
        dispobj = create_results_display_object(
            "montage",
            title=title,
            xvalues=xs,
            yvalues=ys,
            labels=images if not labels else labels,
            img=output_image,
            associated_data=[starfile],
            start_collapsed=start_collapsed,
            flag=flag,
        )  # type: ignore  # limited return options  # noqa: E501

        return dispobj

    except Exception as e:
        tb = str(traceback.format_exc())
        return ResultsDisplayPending(
            message="Error creating results display",
            reason=f"Error creating montage display object: {str(e)} \nTraceback: {tb}",
        )


def make_particle_coords_thumb(
    in_mrc,
    in_coords,
    out_dir,
    thumb_size=640,
    pad=5,
    start_collapsed=False,
    flag: str = "",
) -> Union[ResultsDisplayHistogram, ResultsDisplayPending]:  # type: ignore  # limited return options  # noqa: E501

    """Create a thumbnail of picked particle coords on their micrograph

    Because the extraction box size is not known boxes will be  a % of the
    total image size.

    Args:
        in_mrc (str): Path to the merged micrograph mrc file
        in_coords (str): Path to the .star coordinates file
        out_dir (str): Name of the output directory
        thumb_size (int): Size of the x dimension of the final thumbnail image
        pad (int): Thickness of the particle box borders before binning in px
        start_collapsed (bool): Should the display start out collapsed when displayed
            in the GUI
        flag (str): If this display object contains scientificlly dubious results
            display this message
    """

    # make the micrographs thumbnail
    mic_thumb = mrc_thumbnail(in_mrc, thumb_size)

    # read the particles file
    coordsext = in_coords.split(".")[-1]
    # assumes relion style coordinates files
    if coordsext == "star":
        cd_block = cif.read_file(in_coords).sole_block()
        cd = cd_block.find("_rln", ["CoordinateX", "CoordinateY"])
    elif coordsext == "box":
        with open(in_coords) as ic:
            cd = [x.split() for x in ic.readlines()]
    else:
        raise ValueError("Invalid coordinate file format use .box or .star")

    coords = []
    for i in cd:
        coords.append([float(i[0]), float(i[1])])

    with mrcfile.open(in_mrc) as mrc:
        mic_shape = mrc.data.shape
    coords_array = np.zeros(mic_shape)

    def square_corners(c_coords, size, pct, ppad):
        """Get the corners for the particle boxes

        Args:
            c_coords (list): x and y coordinates
            size (int): desired size of the thumbnail image
            pct (float): percentage of the thumbnail width to make the
                box (because particle boxsize is not known)
            ppad (int): Thickness of the box borders in pixels

        """
        boxshift = 0.5 * pct * size
        x, y = c_coords
        y_top = max(int(math.ceil(y - boxshift)), ppad)
        y_bot = min(int(math.ceil(y + boxshift)), mic_shape[0] - ppad)
        x_left = max(int(math.ceil(x - boxshift)), ppad)
        x_right = min(int(math.ceil(x + boxshift)), mic_shape[1] - ppad)

        return y_top, y_bot, x_left, x_right

    for cpair in coords:
        # get the box coordinates
        ytop, ybot, xleft, xright = square_corners(cpair, thumb_size, 0.2, pad)
        xlength = xright - xleft
        ylength = ybot - ytop

        # draw the boxes array
        coords_array[ytop:ybot, xleft - pad : xleft] = np.full([ylength, pad], 100)
        coords_array[ytop:ybot, xright : xright + pad] = np.full([ylength, pad], 100)
        coords_array[ytop - pad : ytop, xleft:xright] = np.full([pad, xlength], 100)
        coords_array[ybot : ybot + pad, xleft:xright] = np.full([pad, xlength], 100)

    # make an image of the coords
    binned_coords = bin_array_to_size(coords_array, thumb_size)
    binned_coords[binned_coords > 0] = 100
    box_img = Image.fromarray(binned_coords).convert("RGBA")
    boxes_data = box_img.getdata()
    transboxes = []
    for pix in boxes_data:
        if pix[0] == 0 and pix[1] == 0 and pix[2] == 0:
            transboxes.append((255, 255, 255, 0))
        else:
            transboxes.append((255, 0, 0, 255))
    box_img.putdata(transboxes)  # type: ignore  # typing expects Sequence  # noqa: E501

    # make image of the micrograph and merge the two
    mic_img = Image.fromarray(mic_thumb).convert("RGB")
    mic_img.paste(box_img, (0, 0), box_img)
    thumbdir = os.path.join(out_dir, "Thumbnails")
    if not os.path.isdir(thumbdir):
        os.makedirs(thumbdir)
    thumbsdir = os.path.join(out_dir, "Thumbnails")
    out_img = get_next_resultsfile_name(thumbsdir, "picked_coords*.png")

    mic_img.save(out_img)
    description = f"{in_mrc}: {len(coords)} particles"
    # make the ResultsDisplayObject
    dispobj = create_results_display_object(
        "image",
        title="Example picked particles",
        image_path=out_img,
        image_desc=description,
        associated_data=[in_mrc, in_coords],
        start_collapsed=start_collapsed,
        flag=flag,
    )  # type: ignore  # limited return options  # noqa: E501

    return dispobj  # type: ignore  # limited return options  # noqa: E501
