#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
import shutil
import time
import json
import math
from typing import List, Optional, Union, Tuple, Dict


from pipeliner.data_structure import (
    CLASS2D_HELICAL_NAME_EM,
    CLASS2D_PARTICLE_NAME_EM,
    CLASS2D_HELICAL_NAME_VDAM,
    CLASS2D_PARTICLE_NAME_VDAM,
    INIMODEL_JOB_NAME,
    CLASS3D_PARTICLE_NAME,
    CLASS3D_HELICAL_NAME,
    REFINE3D_HELICAL_NAME,
    REFINE3D_PARTICLE_NAME,
    MULTIBODY_REFINE_NAME,
    MULTIBODY_FLEXANALYSIS_NAME,
    JOBSTATUS_ABORT,
    JOBSTATUS_RUN,
    JOBSTATUS_FAIL,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_SCHED,
    JOBINFO_FILE,
)
from pipeliner.job_options import JobOption
from pipeliner.job_runner import JobRunner
from pipeliner.starfile_handler import JobStar, PipelineStarFile
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import (
    read_job,
    job_from_dict,
    active_job_from_proc,
)
from pipeliner.utils import (
    clean_jobname,
    decompose_pipeline_filename,
    date_time_tag,
    touch,
    run_subprocess,
)
from pipeliner.metadata_tools import get_job_metadata, get_metadata_chain
from pipeliner.data_structure import PROJECT_FILE
from pipeliner.pipeliner_job import PipelinerJob

do_flowcharts = True
try:
    from pipeliner.flowchart_illustration import ProcessFlowchart
except ImportError:
    do_flowcharts = False
    ProcessFlowchart = None  # type: ignore


def look_for_project(pipeline_name: str = "default") -> Optional[dict]:
    """See if a pipeliner project exists in the current directory

    Args:
        pipeline_name (str): The name of the pipeline to look for.  This is the same
            as the pipeline file name with _pipeline.star removed

    Returns:
        tuple: (:class:`bool`:was the pipeline found?, :class:`dict` info about the
            project)
    """

    # first look for a project file
    if os.path.isfile(PROJECT_FILE):
        try:
            with open(PROJECT_FILE) as project_file:
                project_info = json.load(project_file)
            new_pipe = project_info["pipeline file"].replace("_pipeline.star", "")
            if pipeline_name != new_pipe:
                print(
                    f"WARNING: Pipeline name '{pipeline_name}' does not match the "
                    f"pipeline name specified in the project file ({new_pipe})."
                    "\nReverting to the name in the project file"
                )
            pipeline_name = new_pipe
            return project_info
        except json.decoder.JSONDecodeError:
            print(
                "WARNING: An outdated CCP-EM Pipeliner project file was found. Removing"
                " it and making a fresh one."
            )

    # If no project file, see if the pipeline file exists. If so, the project was
    # created in Relion rather than the pipeliner.
    if os.path.isfile(f"{pipeline_name}_pipeline.star"):
        project_info = {
            "project name": "Project imported from RELION",
            "description": (
                'This project was imported from RELION. "Date created" is the date it'
                " was imported into the CCP-EM Pipeliner."
            ),
            "pipeline file": f"{pipeline_name}_pipeline.star",
            "date created": date_time_tag(),
        }
        with open(PROJECT_FILE, "w") as pfile:
            pfile.write(json.dumps(project_info, indent=2))

        return project_info

    # otherwise it is a new project
    return None


class PipelinerProject(object):
    """This class forms the basis for a project.

    Attributes:
        pipeline_name (str): The name of the pipeline. Defaults to `default` if
            not set. There is really no good reason to give the pipeline any other
            name.
    """

    def __init__(
        self,
        pipeline_name: str = "default",
        project_name: Optional[str] = None,
        description: Optional[str] = None,
        make_new_project: bool = False,
    ):
        """Create a PipelinerProject object

        Args:
            pipeline_name (str): The name for the pipeline. There is no good reason
                to use anything else except the default.
            project_name (str): A short descriptive name for the project, editable
                by the user, will overwrite current oit the project already exists
            description (str): A verbose description of the project, editable by the
                user will overwrite current if the project already exists
            make_new_project (bool): If an existing project is not found in the current
                directory, should a new one be created? If ``True``, a new empty project
                will be made. If ``False``, an error will be raised instead.

        Raises:
            FileNotFoundError: If no existing project is found and ``make_new_project``
                is ``False``.
        """
        self.pipeline_name = pipeline_name

        # look to see if pipeliner has been run previously and specified a pipeline
        project_info = look_for_project(pipeline_name)

        if project_info is None:
            if not make_new_project:
                # This should not happen. If it does, either the current working
                # directory is wrong, or we are in a situation where it really does make
                # sense to make a new project and the make_new_project flag should have
                # been set.
                raise FileNotFoundError(
                    "Could not find an existing project to open in directory"
                    f" {os.getcwd()}. This is probably caused by a bug in the code."
                    " Please report this error to the CCP-EM developers."
                )
            else:
                project_info = self._make_new_project()

        self.pipeline_name = project_info["pipeline file"].replace("_pipeline.star", "")

        if project_name is not None:
            project_info["project name"] = project_name
        if description is not None:
            project_info["description"] = description

        project_info["last opened"] = date_time_tag()

        with open(PROJECT_FILE, "w") as proj_file:
            proj_file.write(json.dumps(project_info, indent=2))

    def _make_new_project(self):
        # Make a new empty pipeline
        with ProjectGraph(name=self.pipeline_name, create_new=True):
            pass

        # relion related stuff
        # this will be deprecated when support for relion GUI is dropped

        # create the .gui_projectdir file so the gui knows it is initialized
        # this is for relion's benefit
        touch(".gui_projectdir")

        # Create and return default project info for a new project
        project_info = {
            "project name": "New project",
            "description": "A new CCP-EM pipeliner project",
            "pipeline file": f"{self.pipeline_name}_pipeline.star",
            "date created": date_time_tag(),
        }
        return project_info

    def parse_procname(self, in_proc: str, search_trash: bool = False) -> Optional[str]:
        """Find process name with the ability for parse ambiguous input.

        Returns full process names IE: `Import/job001/` from `job001` or `1`
        Can look in both active processes and the Trash Can, accepts
        inputs containing only job number and process type and alias
        IE `Import/my_alias`

        Args:
            in_proc (str): The text that is being checked against the list of processes
            search_trash (bool): Should it return the process name if the process is in
                the trash?

        Returns:
            str: the process name


        Raises:
            ValueError: if the process was in the trash but search_trash is false
            ValueError: if the process name is not in the pipeliner format, jobxxx,
                or a number.  IE: An unrelated string
            ValueError: if the process name is not found

        """
        # first see if the process is in the pipeline or trash
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            in_proc = in_proc.replace(" ", "")
            if in_proc[-1] != "/":
                in_proc += "/"
            found_process = pipeline.find_process(in_proc)

            if found_process:
                return found_process.name

            trashprocs = glob("Trash/*/job*/")
            if in_proc[0:6] == "Trash/":
                in_proc = in_proc[6:]
            if "Trash/" + in_proc in trashprocs:
                found_it = in_proc
                if search_trash:
                    return found_it
                raise ValueError(
                    f"ERROR: Job {found_it} is in the trash. Trashed jobs cannot"
                    " be used for this function.  If you wish to use this job, "
                    "undelete it first"
                )

            # if the user entered just job number IE: job005 or just a number
            in_jobnr = decompose_pipeline_filename(in_proc)[1]
            if not in_jobnr:
                try:
                    in_jobnr = int(in_proc.replace("/", ""))
                except ValueError:
                    raise ValueError(
                        f"ERROR: Could not parse the process name {in_proc}"
                    )

            for pipeproc in pipeline.process_list:
                if pipeproc.job_number == in_jobnr:
                    return pipeproc.name
            for trashproc in trashprocs:
                tp_jobnr = decompose_pipeline_filename(trashproc)[1]
                if tp_jobnr == in_jobnr:
                    if search_trash:
                        return trashproc[6:]
                    raise ValueError(
                        f"ERROR: Job {found_process.name if found_process else None} is"
                        " in the trash. Trashed jobs cannot"
                        " be used for this function.  If you wish to use this job,"
                        " undelete it first"
                    )

            raise ValueError(
                f"ERROR: {in_proc} does not seem to be a job in this project"
            )

    def parse_proclist(self, list_o_procs: list, search_trash: bool = False) -> list:
        """Finds full process names for multiple processes

        Returns full process names IE: `Import/job001/` from `job001` or `1`

        Args:
            list_o_procs (list): A list of string process names
            search_trash (bool): Should the trash also be search?

        Returns:
            list: All of the full process names

        """
        checked = [self.parse_procname(proc, search_trash) for proc in list_o_procs]
        return checked

    def run_cleanup(self, jobs: list, harsh: bool = False) -> bool:
        """Run the cleanup function for multiple jobs

        Each job defines its own method for cleanup and harsh cleanup

        Args:
            jobs (list): List of string job names to operate on
            harsh (bool): Should harsh cleaning be performed

        Returns:
            bool: ``True`` if cleanup is successful, otherwise ``False``

        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            for job in jobs:
                job = clean_jobname(job)
                jobproc = pipeline.find_process(job)
                if jobproc is not None:
                    pipeline.clean_up_job(jobproc, harsh)
                elif jobproc is None:
                    print(f"WARNING: Could not find job {job} to clean up")
                    return False
            pipeline.check_process_completion()
        return True

    def cleanup_all(self, harsh: bool = False):
        """Runs cleanup on all jobs in a project

        Args:
            harsh (bool): Should harsh cleaning be performed?
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            pipeline.cleanup_all_jobs(harsh)
            pipeline.check_process_completion()

    def delete_job(self, job: str) -> bool:
        """Delete a job

        Removes the job from the main project and moves it and its children
        it to the Trash

        Args:
            job (str): The name of the job to be deleted

        Returns:
            bool: ``True`` If a job was deleted, ``False`` if no jobs were deleted

        """
        job = clean_jobname(job)
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            jobproc = pipeline.find_process(job)
            if jobproc is not None:
                pipeline.delete_job(jobproc)
                print(f"Job {job} and child processes were moved to the trash")
                pipeline.check_process_completion()
                return True
            else:
                print(f"ERROR: Could not find job {job} to delete")
        return False

    def undelete_job(self, job: str) -> bool:
        """Restores a job from the Trash back into the project

        Also restores the job's alias if one existed


        Args:
            job (str): The job to undelete

        Returns:
            bool: ``True`` If a job was restored, otherwise ``False``

        """
        job = clean_jobname(job)
        if not os.path.isdir(os.path.join("Trash/", job)):
            print(f"ERROR: Could not find job {job} in the trash, can't undelete it.")
            return False
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            pipeline.undelete_job(job)
            pipeline.check_process_completion()
        return True

    def set_alias(self, job: str, new_alias: Optional[str]):
        """Set the alias for a job

        Args:
            job (str): The name of the job to set the alias for
            new_alias (str):  The new alias

        Raises:
            ValueError: If the alias could not be set for any reason.
        """
        job = clean_jobname(job)
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            jobproc = pipeline.find_process(job)
            if jobproc is None:
                raise ValueError(f"ERROR: could not find job {job}")
            pipeline.set_job_alias(jobproc, new_alias)
            pipeline.check_process_completion()

    def update_job_status(self, job: str, new_status: str) -> bool:
        """Mark a job as finished, failed or aborted

        If is_failed and is_aborted are both ``False`` the job is marked as
        finished.

        Args:
            job (str): The name of the job to update
            new_status (str): The new status for the job; Choose from `"Running"`,
                , `"Scheduled"`, `"Succeeded"`, `"Failed` or `"Aborted"`.
                Status names are not case sensitive

        Returns:
            bool: ``True`` if the status was updated, otherwise ``False``


        Raises:
            ValueError: If the new status is not one of the options
        """
        statuses = (
            JOBSTATUS_ABORT,
            JOBSTATUS_RUN,
            JOBSTATUS_FAIL,
            JOBSTATUS_SUCCESS,
            JOBSTATUS_SCHED,
        )
        if new_status not in statuses:
            raise ValueError(
                f"{new_status} is not an available status.  Select from {statuses}"
            )
        job = clean_jobname(job)
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            jobproc = pipeline.find_process(job)
            if not jobproc:
                return False
            updated = pipeline.update_status(jobproc, new_status)
            pipeline.check_process_completion()
        return updated

    def run_job(
        self,
        jobinput: Union[str, dict, PipelinerJob],
        overwrite: Optional[str] = None,
        wait_for_queued: bool = True,
        comment: Optional[str] = None,
        alias: Optional[str] = None,
    ) -> PipelinerJob:
        """Run a new job in the project

        If a file is specified the job will be created from the parameters in that file
        If a dict is input the job will be created with defaults for all options except
        those specified in the dict.

        If a dict is used for input it MUST contain at minimum
        {"_rlnJobTypeLabel": <the jobtype>}

        Args:
            jobinput (str, dict, :class:`~pipeliner.pipeliner_job.PipelinerJob`):
                The path to a run.job or job.star file that defines the parameters
                for the job or a dict specifying job parameters or a
                :class:`~pipeliner.pipeliner_job.PipelinerJob` object
            overwrite (str): The name of a job to overwrite, if ``None`` a new
                job will be created.  A job can only be overwritten by a job of the
                same type
            wait_for_queued (bool): If the job is being sent to a queue, should the
                pipeliner wait for the job to finish before starting the next job.
                Jobs run locally always wait for the job to finish before the next is
                started
            comment (str):  Comments to be added to the job's info file
            alias (str): Alias to assign to the new job

        Returns:
            str: The name of the job that was run

        Raises:
            ValueError: If this method is used to continue a job
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_runner = JobRunner(pipeline)
            overwrite_option = bool(overwrite)
            # if the job is overwrite get the old job
            if overwrite is not None:
                overwrite_jobname = clean_jobname(overwrite)
                original_proc = pipeline.find_process(overwrite_jobname)
                target_proc = original_proc
                overwrite_option = True
            # otherwise make a new job
            else:
                target_proc = None

            # if input is dict
            if isinstance(jobinput, dict):
                job = job_from_dict(jobinput)

            elif isinstance(jobinput, PipelinerJob):
                job = jobinput

            # if input is file
            elif os.path.isfile(jobinput):
                job = read_job(jobinput)

            # if input is bad
            else:
                raise ValueError(
                    "Can't parse job input; use a job.star, run.job file or a dict "
                    "containing '_rlnJobTypeLabel': <job type> and other parameters"
                    " that will have non-default values"
                )
            if job.is_continue:
                raise ValueError(
                    "ERROR: The job file is for continuing a job "
                    "Use the --continue_job <job name> function instead.\n"
                    "To modify the parameters in a continued job edit the"
                    "continue_job.star file in its job directory"
                )

            job_runner.run_job(
                job,
                target_proc,
                job.is_continue,
                False,
                overwrite_option,
                False,
                wait_for_queued,
                comment,
                alias=alias,
            )
            time.sleep(1)
            pipeline.check_process_completion()

            return job

    def continue_job(
        self, job_to_continue: str, wait_for_queued: bool = True, comment: str = None
    ) -> PipelinerJob:
        """Continue a job that has already been run

        To change the parameters in a continuation the user needs to edit
        the continue_job.star file in the job's directory

        Args:
            job_to_continue (str): The name of the job to continue
            wait_for_queued (bool): If the job is being sent to a queue, should the
                pipeliner wait for the job to finish before starting the next job.
                Jobs run locally always wait for the job to finish before the next is
                started
            comment (str): Comments for the job's jobinfo file

        Returns:
            :class:`~pipeliner.pipeliner_job.PipelinerJob`: The
                :class:`~pipeliner.pipeliner_job.PipelinerJob` object for
                the created job

        Raises:
            ValueError: If the continue_job.star file is not found and there is no
                job.star file in the job's directory to use as a backup
            ValueError: If the job is of a type that needs a optimizer file to continue
                and this file is not found
            ValueError: The job has iterations but the parameters specified would
                result in no additional iterations being run
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_runner = JobRunner(pipeline)
            job_to_continue = clean_jobname(job_to_continue)
            # find the job_to_continue
            curr_proc = job_runner.pipeline.find_process(job_to_continue)
            if curr_proc is None:
                raise ValueError(
                    "ERROR: Asked to continue job: {} but this job does not exist"
                )

            # find and read the continuation file
            continue_file = os.path.join(curr_proc.name, "continue_job.star")
            if not os.path.isfile(continue_file):
                print(
                    f"WARNING: Could not find the file {continue_file}.\nWill instead"
                    " use the job options in the original job.star file."
                )
                continue_options = {}
            else:
                continue_options = JobStar(continue_file).joboptions

            # find and read the original job.star
            original_options_file = os.path.join(curr_proc.name, "job.star")
            if not os.path.isfile(original_options_file):
                raise ValueError(
                    f"ERROR: The file {original_options_file} was not found. There is"
                    " an error with the job and it cannot be continued."
                )

            # make a dummy job to get a job options dict for writing the job.star
            # substitute in the continue options
            djob = read_job(original_options_file)
            for jo in djob.joboptions:
                if jo in continue_options:
                    djob.joboptions[jo].value = continue_options[jo]

            # TO DO: remove relion specific code here move to individual jobs
            # check if an optimiser is needed; if so and missing, raise error..
            needs_optimiser = [
                CLASS2D_PARTICLE_NAME_EM,
                CLASS2D_PARTICLE_NAME_VDAM,
                CLASS2D_HELICAL_NAME_EM,
                CLASS2D_HELICAL_NAME_VDAM,
                INIMODEL_JOB_NAME,
                CLASS3D_PARTICLE_NAME,
                CLASS3D_HELICAL_NAME,
                REFINE3D_HELICAL_NAME,
                REFINE3D_PARTICLE_NAME,
                MULTIBODY_REFINE_NAME,
                MULTIBODY_FLEXANALYSIS_NAME,
            ]
            if curr_proc.type in needs_optimiser:
                optimiser_file = djob.joboptions["fn_cont"].get_string()
                if "_optimiser.star" not in optimiser_file:
                    raise ValueError(
                        "ERROR: No optimiser file was specified to continue from. Edit"
                        f" {continue_file} to list an optimiser.star file in the field"
                        " 'fn_cont'"
                    )

                # check that additional iterations have been added
                optifile = os.path.basename(optimiser_file)
                iternumber = int(optifile.split("_")[1].replace("it", ""))
                if int(djob.joboptions["nr_iter"].get_number()) == iternumber:
                    raise ValueError(
                        "ERROR: The job has been continued from iteration {} but "
                        f"the specified number of iterations is {iternumber} so no"
                        "additional iterations will be run\nUpdate the 'nr_iter'"
                        " parameter in the continue_job.star file for this job."
                    )

            # rename the old job.star file
            jobstars = glob(os.path.join(curr_proc.name, "job.star.*"))
            if len(jobstars) > 0:
                jobstars.sort()
                last_ct_no = int(jobstars[-1].split(".")[-1].replace("ct", ""))
                ct_no = last_ct_no + 1
                new_name = f"{original_options_file}.ct{ct_no:03d}"
                shutil.move(original_options_file, new_name)
            else:
                shutil.move(original_options_file, f"{original_options_file}.ct000")

            # make a new jobstar file and run it
            djob.write_jobstar(curr_proc.name)
            job = read_job(original_options_file)
            job.is_continue = True
            job_runner.run_job(
                job,
                curr_proc,
                job.is_continue,
                False,
                False,
                wait_for_queued=wait_for_queued,
                comment=comment,
            )
            time.sleep(1)
            pipeline.check_process_completion()

            return job

    def schedule_job(
        self,
        job_input: Union[str, Dict[str, Union[str, float, int, bool]]],
        comment: Optional[str] = None,
        alias: Optional[str] = None,
    ) -> PipelinerJob:
        """Schedule a job to run

        Adds the job to the pipeline with scheduled status, does not run it

        Args:
            job_input (str): The path to a run.job or job.star file that defines the
                parameters for the job or a dictionary containing job parameters
            comment (str): Comments to put in the job's jobinfo file
            alias (str): Alias to give to the job
        Returns:
            The :class:`~pipeliner.pipeliner_job.PipelinerJob` object for the scheduled
            job
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_runner = JobRunner(pipeline)

            # if input is dict
            if isinstance(job_input, dict):
                job = job_from_dict(job_input)

            # if input is file
            elif os.path.isfile(job_input):
                job = read_job(job_input)
            else:
                raise ValueError(f"Could not find job for {job_input}")

            if job.is_continue:
                raise ValueError(
                    "Do not use this function to continue jobs! Use"
                    "pipeliner.api.manage_project.schedule_continue_job()"
                )

            job_runner.schedule_job(
                job, None, False, False, False, comment, alias=alias
            )
            # put a copy of the job file in the output directory
            job.write_jobstar(job.output_dir)
            job.write_runjob(job.output_dir)
            # write the continuation file - for continuing the job later
            continue_name = os.path.join(job.output_dir, "continue_")
            job.write_jobstar(continue_name, is_continue=True)

            pipeline.check_process_completion()

            return job

    def schedule_continue_job(
        self,
        job_to_continue: str,
        params_dict: Optional[dict] = None,
        comments: Optional[str] = None,
    ) -> PipelinerJob:
        """Schedule a job to run

        Adds the job to the pipeline with scheduled status, does not run it

        Args:
            job_to_continue (str): the name of the job to continue
            params_dict (dict): Parameters to change in the continuation job.star
                file. {param name: value}
            comments (str): comments to add to the job's jobinfo file

        Returns:
            :class:`~pipeliner.pipeliner_job.PipelinerJob`: The
                :class:`~pipeliner.pipeliner_job.PipelinerJob` object for the
                newly scheduled job
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_runner = JobRunner(pipeline)

            old_job = pipeline.find_process(job_to_continue)
            # if input is bad
            if old_job is None:
                raise ValueError(f"No job named {job_to_continue} found")

            cont_file = os.path.join(old_job.name, "continue_job.star")
            if params_dict is not None:
                js = JobStar(cont_file)
                js.modify(params_dict)
                js.write(cont_file)
            cont_options = JobStar(cont_file).joboptions
            main_file = os.path.join(old_job.name, "job.star")
            run_dict = JobStar(main_file).joboptions
            for param in cont_options:
                run_dict[param] = cont_options[param]

            run_dict["_rlnJobTypeLabel"] = old_job.type
            run_dict["_rlnJobIsContinue"] = "Yes"
            new_job = job_from_dict(run_dict)
            old_proc = pipeline.find_process(old_job.name)
            job_runner.schedule_job(
                new_job,
                old_proc,
                True,
                True,
                False,
                comments,
            )

            # put a copy of the job file in the output directory
            new_job.write_jobstar(new_job.output_dir + "job.star")
            pipeline.check_process_completion()

            return new_job

    def run_schedule(
        self,
        fn_sched: str,
        job_ids: List[str],
        nr_repeat: int = 1,
        minutes_wait: int = 0,
        minutes_wait_before: int = 0,
        seconds_wait_after: int = 5,
    ) -> str:
        """Runs a list of scheduled jobs

        Args:
            fn_sched (str): A name to assign to the schedule
            job_ids (list): A list of string job names to run
            nr_repeat (int): Number of times to repeat the entire schedule
            minutes_wait (int): Minimum number of minutes to wait between running
                each subsequent job
            minutes_wait_before (int): Initial number of minutes to wait before
                starting to run the schedules.
            seconds_wait_after (int): Time to wait after running each job

        Returns:
            str: The name of the schedule that is run

        Raises:
            ValueError: If the schedule name is already in use
        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            job_runner = JobRunner(pipeline)
            runfile_name = "RUNNING_PIPELINER_" + self.pipeline_name + "_" + fn_sched

            if os.path.isfile(runfile_name):
                raise ValueError(
                    f"ERROR: a file called {runfile_name} already exists. \n This "
                    "implies another set of scheduled jobs with this name is already "
                    "running. \n Cancelling job execution..."
                )

            print(
                f"Created schedule: {fn_sched} with {len(job_ids)} jobs repeated "
                f"{nr_repeat} times"
            )

            print("Running " + fn_sched)
            job_runner.run_scheduled_jobs(
                fn_sched,
                job_ids,
                nr_repeat,
                minutes_wait,
                minutes_wait_before,
                seconds_wait_after,
            )
            pipeline.check_process_completion()
            return fn_sched

    @staticmethod
    def empty_trash():
        """Deletes all the files and dirs in the Trash directory

        Returns:
            bool:``True`` if any files were deleted, ``False`` If no files were deleted

        """
        trash_files = glob("Trash/*/*/*")
        if len(trash_files) > 0:
            shutil.rmtree("Trash")
            print(f"Trash has been emptied. {len(trash_files)} files were deleted")
            return True
        else:
            print("WARNING: No files were found in the trash")
            return False

    def draw_flowcharts(
        self,
        job: Optional[str] = None,
        do_upstream: bool = False,
        do_downstream: bool = False,
        do_full: bool = False,
        save: bool = False,
        show: bool = False,
    ) -> Union[bool, Tuple[Optional[str], Optional[str], Optional[str]]]:
        """Prepare flowcharts for visulizing a a project

        Args:
            job (str): The job for the flowchart to start or end on.
            do_upstream (bool): Should an upstream flowchart from the job be prepared?
            do_downstream (bool): Should a downstream flowchart from the job
                be prepared?
            do_full (bool): Should a full project flowchart be drawn?
            save (bool): Should the flowchart be saved as a file?
            show (bool): Should an interactive flowchart be shown?

        Returns:
            tuple: Names of the upstream, downstream, and full flowchart files written.

            The entry is ``"Not saved"`` if the flowchart was drawn but only used the
            interactive viewer and ``None`` no flowchart was draw for this option

        """
        if not do_flowcharts:
            print(
                "ERROR: Flowchart drawing is not enabled. \n to"
                " enable flowcharts install the pipeliner using the command "
                "'pip install -e .[draw_flowcharts]'"
            )
            return False

        if (do_upstream or do_downstream) and job is None:
            print(
                "WARNING: No job specified for flowchart, can't draw"
                " upstream/downstream charts"
            )

        with ProjectGraph(name=self.pipeline_name) as pipeline:
            flowchart = ProcessFlowchart(
                pipeline,
                pipeline.find_process(job) if job else None,
                do_upstream,
                do_downstream,
                do_full,
                save,
                show,
            )

            if (
                flowchart.drew_up is None
                and flowchart.drew_down is None
                and flowchart.drew_full is None
            ):
                print("WARNING: There was nothing to draw! Was a job name specified?")

            return flowchart.drew_up, flowchart.drew_down, flowchart.drew_full

    def stop_schedule(self, schedule_name: str) -> bool:
        """Stops a currently running schedule

        Kills the process running the schedule and marks the currently running
        job as aborted.  Works to stop schedules that were started using the
        RELION GUI or pipeliner.

        Args:
            schedule_name (str): The name of the schedule to stop

        Returns:
            bool: ``True`` If the schedule was stopped, ``False`` if the schedule could
            not be found to stop
        """
        # find RUNNING_PIPELINER_ file
        sched_info = []
        the_file = None
        running_files = glob("RUNNING*")
        for f in running_files:
            sname = f.replace("RUNNING_PIPELINER_", "").split("_")
            pipe = sname[0]
            name = "_".join(sname[1:])
            if name == schedule_name and pipe == self.pipeline_name:
                the_file = f
                with open(the_file) as run_file:
                    sched_info = run_file.readlines()

        # error if the schedule is not found
        if the_file is None:
            print(
                "ERROR: Cannot find file RUNNING_PIPELINER_"
                f"{self.pipeline_name}_{schedule_name}\n"
                f"'{schedule_name}' does not appear to be a running schedule"
            )
            return False

        # if the schedule was started by the GUI delete the file
        if len(sched_info) == 0:
            os.remove(the_file)
            return True

        # if the schedule was started by the ccpem_pipeliner kill it and abort the job
        else:
            running_jobs = []
            for line in sched_info:
                if "RELION_SCHEDULE:" in line:
                    # kill the schedule id it was started by the pipeliner
                    sched_pid = line.split()[1].rstrip()
                    run_subprocess(["kill", "-9", sched_pid])
                else:
                    running_jobs.append(line)
            # abort the current running job
            if len(running_jobs) > 0:
                current_job = running_jobs[-1].rstrip()
                self.update_job_status(current_job, JOBSTATUS_ABORT)

            # remove the file
            os.remove(the_file)
            return True

    def get_job_metadata(self, jobname: str, output_name: Optional[str] = None) -> dict:
        """Runs the gather_metadata function of a single job

        Args:
            jobname: The name of the job to run on
            output_name: File to write json to. If ``None``, no file is written

        Returns:
            dict: Metadata dict for the job
        """
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            proc = pipeline.find_process(jobname)
            metadata_dict = get_job_metadata(proc) if proc else {}
            if output_name is not None:
                if not output_name.endswith(".json"):
                    output_name += ".json"
                with open(output_name, "w") as outfile:
                    outfile.write(json.dumps(metadata_dict))
            return metadata_dict

    def get_network_metadata(
        self, jobname: str, output_name: Optional[str] = None
    ) -> dict:
        """Returns a full metadata trace for a job and all upstream jobs

        Args:
            jobname: The name of the job to run on
            output_name: File to write json to. If ``None``, no file is written

        Returns:
            dict: Metadata dict for all the jobs
        """
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            proc = pipeline.find_process(jobname)
            if not proc:
                return {}
            metadata_dict = get_metadata_chain(pipeline, proc, output_name)
            return metadata_dict

    def create_archive(self, job: str, full: bool = False, tar: bool = True) -> str:
        """Creates an archive

        Archives can be full or simple. Simple archives contain the directory
        structure of the project, the parameter files for each job and a script
        to rerun the project through the terminal job.  The full archive contains the
        full job dirs for the terminal job and all of its children

        Args:
            job (str): The name of the terminal job in the workflow
            full (bool): If ``True`` a full archive is written else a simple archive is
                written
            tar (bool): Should the newly written archive be compressed?

        Returns:
            str: A message telling the type of archive and its name

        """
        with ProjectGraph(name=self.pipeline_name, read_only=False) as pipeline:
            process = pipeline.find_process(job)
            if not process:
                return ""
            message = pipeline.prepare_archive(process, full, tar)
            return message

    def edit_comment(
        self,
        job_name: str,
        comment: Optional[str] = None,
        overwrite: bool = False,
        new_rank: Optional[int] = None,
    ):
        """Edit the comment of a job

        Args:
            job_name (str): The name of the job to eddit the comment for
            comment (str): The comment to add/append
            overwrite (bool): if `True` overwrites otiginal comment, otherwise
                appends it to the current comment
            new_rank (int): New rank to assign to job, use -1 to revert the rank
                to `None`

        Raises:
            ValueError: If the new rank is not `None` or an integer
        """
        if comment is None and new_rank is None:
            raise ValueError("Nothing to do! To clear comment use comment=[]")

        if type(new_rank) != int and new_rank is not None:
            raise ValueError("New rank must be an integer or None")

        with ProjectGraph(name=self.pipeline_name) as pipeline:
            the_job = pipeline.find_process(job_name)
            if not the_job:
                raise ValueError(
                    f"No process found in {self.pipeline_name} pipeline for {job_name}"
                )
            jobinfo_file = os.path.join(the_job.name, JOBINFO_FILE)
            with open(jobinfo_file, "r") as jof:
                job_data = json.load(jof)
            if new_rank is not None:
                job_data["rank"] = new_rank if new_rank > -1 else None
            if comment is not None:
                if overwrite:
                    job_data["comments"] = [comment]
                else:
                    job_data["comments"].append(comment)
            with open(jobinfo_file, "w") as jof:
                json.dump(job_data, jof, indent=2)

    def find_job_by_rank(
        self,
        equals: Optional[int] = None,
        less_than: Optional[int] = None,
        greater_than: Optional[int] = None,
        job_type: Optional[str] = None,
    ) -> List[str]:
        """Find jobs by their rank

        Ignores jobs that are unranked

        Args:
            equals (int): Find jobs with this exact rank
            less_than (int): Find jobs with ranks less then this number
            greater_than (int): Find jobs with ranks higher than this number
            job_type (str): Only consider jobs that contain this string in their
                job type

        Returns:
            list: Names of the matching jobs

        Raises:
            ValueError: If nothing is specified to search for
            ValueError: If both equals and less_than/greater than are specified
        """
        if [equals, less_than, greater_than] == [None, None, None]:
            raise ValueError("Nothing to do specify equals, greater_than, or less_than")
        if equals is not None and (less_than is not None or greater_than is not None):
            raise ValueError("Specifiy a greater_than/less than or equals, not both")

        with ProjectGraph(name=self.pipeline_name) as pipeline:
            jobs = pipeline.process_list
            jobranks = {}
            for job in jobs:
                if str(job_type) in job.type or job_type is None:
                    with open(os.path.join(job.name, JOBINFO_FILE), "r") as notefile:
                        jobinfo = json.load(notefile)
                        jobranks[job.name] = jobinfo.get("rank")

            sel_jobs = []
            range_top = less_than if less_than is not None else math.inf
            range_bottom = greater_than if greater_than is not None else -1
            if equals is not None:
                range_top, range_bottom = equals + 1, equals - 1

            for jobr, val in jobranks.items():
                if val is not None:
                    if range_top > val > range_bottom:
                        sel_jobs.append(jobr)

            return sel_jobs

    # TO DO: rewrite this to use the jobinfo file
    def find_job_by_comment(
        self,
        contains: Optional[List[str]] = None,
        not_contains: Optional[List[str]] = None,
        job_type: Optional[str] = None,
        command: bool = False,
    ) -> List[str]:
        """Find Jobs by their comments or command

        Args:
            contains (list): Find jobs that contain all of the strings in this list
            not_contains (list): Find jobs that do not contain any of these strings
            job_type (str): Only consider jobs who's type contain this string
            command (bool): If `True` searches the job's command history rather than
                its comments

        Returns:
            list: Names of all the jobs found

        Raises:
            ValueError: If nothing is specified for contains and not_contains
        """
        if contains is None and not_contains is None:
            raise ValueError("Must specifiy contains= or not_contains=")

        with ProjectGraph(name=self.pipeline_name) as pipeline:
            jobs = pipeline.process_list
            job_commands, job_comments = {}, {}
            for job in jobs:
                if str(job_type) in job.type or job_type is None:
                    with open(os.path.join(job.name, JOBINFO_FILE), "r") as notefile:
                        jobinfo = json.load(notefile)
                        if jobinfo.get("comments") is not None:
                            job_comments[job] = jobinfo["comments"]
                        if jobinfo.get("command history") is not None:
                            job_commands[job] = jobinfo["command history"]

            sel_jobs = []

            def check_cnc(which_dict: dict) -> bool:
                combined = "\t".join(which_dict[job])
                if contains is not None:
                    in_contains = any([x in combined for x in contains])
                else:
                    in_contains = True

                if not_contains is not None:
                    reject = any([x in combined for x in not_contains])
                else:
                    reject = False

                if job_type is not None:
                    jt_match = job_type in job.type
                else:
                    jt_match = True

                if in_contains and not reject and jt_match:
                    return True
                else:
                    return False

            if command:
                for job in job_commands:
                    if check_cnc(job_commands):
                        sel_jobs.append(job.name)

            else:
                for job in job_comments:
                    if check_cnc(job_comments):
                        sel_jobs.append(job.name)

            return sel_jobs

    def compare_job_parameters(self, jobs_list: List[str]) -> dict:
        """Compare the running parameters of multiple jobs

        Args:
            jobs_list (list): The jobs to compare

        Returns:
            dict: {parameter: [value, value, value]}

        Raises:
            ValueError: If any of the jobs is not found
            ValueError: If the jobs being compared are not of the same type
        """
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            jobs = []
            for job in jobs_list:
                j = pipeline.find_process(job)
                if j is None:
                    raise ValueError(f"job {job} is not a job in the current project")
                jobs.append(j)
            params_dicts = []
            for j in jobs:
                jfile = os.path.join(j.name, "job.star")
                params_dicts.append(JobStar(jfile).all_options_as_dict())

            jts = [x["_rlnJobTypeLabel"] for x in params_dicts]
            print(jts, "****")
            typecheck = len(set(jts))
            if typecheck != 1:
                raise ValueError(
                    "Jobs are not of the same type: "
                    f"{list(zip([x.name for x in jobs], jts))}"
                )
            out_dict = {}
            for param in params_dicts[0]:
                out_dict[param] = [params_dicts[0][param]]
                for other in params_dicts[1:]:
                    val = other[param]
                    out_dict[param].append(val)
            return out_dict

    def get_joboptions(self, job: str) -> List[JobOption]:
        """Get the job options for a job in the project.

        Args:
            job (str): The name of the job to get the job options for
        """
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            proc = pipeline.find_process(job)
            if proc:
                the_job = active_job_from_proc(proc)
            else:
                raise ValueError(f"Process {job} not found")
        return the_job.joboptions

    def get_reference_list(self, terminal_job):
        """Prepare a list of literature references for software used in a project

        Args:
            terminal_job (str): The job to trace backwards from

        Returns:
            dict: {Ref object: [jobs, that, use, reference]}
        """
        with ProjectGraph(name=self.pipeline_name) as pipeline:
            term_proc = pipeline.find_process(terminal_job)
            backtrace = pipeline.get_upstream_network(term_proc)
            back_procs = [term_proc] + [x[1] for x in backtrace]

            refs_list, jobs_list, refs_set = [], [], []
            for bp in back_procs:
                job = active_job_from_proc(bp)
                for ref in job.jobinfo.references:
                    refs_list.append(ref)
                    jobs_list.append(job.output_dir)
                    if ref not in refs_set:
                        refs_set.append(ref)

                # check for the special get_reference_info() function in some jobs
                # IE pipeliner.fetch.pdb, pipeliner.fetch.emdb
                addref = job.get_additional_reference_info()
                if addref:
                    refs_list.extend(addref)
                    jobs_list.extend([job.output_dir] * len(addref))
                    for aref in addref:
                        if aref not in refs_set:
                            refs_set.append(aref)
            final_refs_list = []
            rj_pairs = list(zip(refs_list, jobs_list))
            for ref in refs_set:
                joblist = []
                for rjp in rj_pairs:
                    if rjp[0] == ref:
                        joblist.append(rjp[1])
                final_refs_list.append((ref, joblist))

            return final_refs_list


def convert_pipeline(pipeline_name: str = "default") -> bool:
    """Converts a pipeline file from the RELION 2.0-3.1 format

    This format has integer node, process, and status IDs.  The pipeliner format
    uses string IDs

    Args:
        pipeline_name (str): The name of the pipeline to be converted

    Returns:
        bool: The result of the conversion

        ``True`` if the pipeline was converted, ``False`` if the pipeline was
        already in pipeliner format

    """
    # update the pipeline
    # TODO: fix this, confusing that making a new object actually changes files on disk
    the_pipe = PipelineStarFile(f"{pipeline_name}_pipeline.star")

    # check the pipeline version if it's old style convert
    if the_pipe.been_converted:
        return True
    else:
        print(
            f"ERROR: Pipeline file {pipeline_name}_pipeline.star is already"
            " in pipeliner format"
        )
        return False


def get_commands_and_nodes(job_file: str) -> tuple:
    """Tell what commands a job file would return and nodes that would be created

    Args:
        job_file (str): The path to a run.job or job.star file

    Returns:
        tuple:

        - A list of commands. Each item in the commands
          list is a list of commands arguments. IE:
          ``[[com1-arg1, com1-arg2],[com2-arg1]]``
        - A list of input nodes that would be created.  Each item in the list
          is a tuple: ``[(name, type), (name, type)]``
        - A list of output nodes that would be created. Each item in the list
          is a tuple: ``[(name, type), (name, type)]``
        - A list of any PipelinerWarning raised by joboption validation
        - A list of the ExternalProgram objects used by the job
    """
    is_project = look_for_project() is not None
    job = read_job(job_file)

    # validate the joboptions
    vres = job.validate_joboptions()
    errs = []
    for joe in vres:
        if joe.type == "error":
            errs.append(f"'{joe.raised_by[0].label}': {joe.message}")
    if errs:
        raise ValueError(
            "Job cannot generate command because of the following errors in the "
            f"joboptions: {', '.join(errs)}"
        )

    if is_project:
        with ProjectGraph() as pipeline:
            jobnumber = pipeline.job_counter
            job.output_dir = f"{job.OUT_DIR}/job{jobnumber:03d}/"
            commandlist = job.get_commands()
            commands = job.prepare_final_command(commandlist, False, ignore_queue=True)

    else:
        job.output_dir = f"{job.OUT_DIR}/job000/"
        commandlist = job.get_commands()
        commands = job.prepare_final_command(commandlist, False, ignore_queue=True)

    inputnodes, outputnodes = [], []

    if len(job.input_nodes) > 0:
        for innode in job.input_nodes:
            inputnodes.append((innode.name, innode.type))
    if len(job.output_nodes) > 0:
        for outnode in job.output_nodes:
            outputnodes.append((outnode.name, outnode.type))

    return commands, inputnodes, outputnodes, vres, job.jobinfo.programs
