#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import Optional
import time

from pipeliner.job_factory import new_job_of_type
from pipeliner.starfile_handler import StarFile, JobStar
from pipeliner.star_writer import write_jobstar
from pipeliner.data_structure import SUCCESS_FILE, FAIL_FILE, ABORT_FILE
from pipeliner.pipeliner_job import JobInfo
from pipeliner.utils import get_pipeliner_root


def write_default_jobstar(
    job_type: str, out_fn: Optional[str] = None, relionstyle: bool = False
):
    """
    Write a job.star file for the specified type of job

    The default jobstar contains all the job options with their values
    set as the defaults

    Args:
        job_type (str): The type of job
        out_fn (str): Name of the file to write the output to. If left blank
            defaults to `<job_type>_job.star`
        relionstyle (bool): Should the job.star files be written in the relion
            format? Relion files are compatible with the pipeliner, but the
            pipeliner versions are not back compatible with Relion. If this
            option is selected a Relion job type should be used for job_type
    Returns:
        str: The name of the output file written
    """
    if out_fn is None:
        out_fn = "{}_job.star".format(job_type.lower().replace(".", "_"))

    # pipeliner style jobstar files
    if not relionstyle:
        the_job = new_job_of_type(job_type)
        the_job.write_jobstar("", output_fn=out_fn)
        print(f"Wrote {out_fn}")
        return out_fn

    # relion_style jobstar files
    # TODO: look at whether the relionstyle option and the default_relion_..._job.star
    #  files can be completely removed. The "compatibility" job options should be able
    #  to handle this instead.
    relion_jobtype = job_type.split(".")[-1]
    jobstar_path = os.path.join(
        get_pipeliner_root(),
        f"jobs/relion/relion_jobstars/default_relion_{relion_jobtype}_job.star",
    )
    try:
        js_data = JobStar(jobstar_path).all_options_as_dict()
    except FileNotFoundError:
        raise ValueError("This is not a recognised RELION job type.")

    write_jobstar(js_data, out_fn)


def write_default_runjob(job_type: str, out_fn: Optional[str] = None) -> str:
    """
    Write a run.job file for the specified type of job

    The default runjob contains all the job option labels with their values
    set as the defaults

    Args:
        job_type (str): The type of job
        out_fn (str): Name of the file to write the output to.  If left blank
            defaults to `<job_type>_run.job`

    Returns:
        str: The name of the output file written
    """
    the_job = new_job_of_type(job_type)
    if out_fn is None:
        out_fn = "{}_run.job".format(job_type.lower().replace(".", "_"))
    the_job.write_runjob(out_fn)
    print(f"Wrote {out_fn}")
    return out_fn


def job_default_parameters_dict(jobtype: str) -> dict:
    """
    Get dictionary of a job's parameters

    Args:
        jobtype (str): The type of job to get the dict for

    Returns:
        dict: The parameters dict. Suitable for running a job from
            :meth:`~pipeliner.api.manage_project.PipelinerProject.run_job`

    """

    job = new_job_of_type(jobtype)
    params = job.default_params_dict()
    return params


def validate_starfile(fn_in: str):
    """Checks for inappropriate use of reserved words in starfiles

    Writes a corrected version with proper quotation if possible. The
    original file is saved with a '.old' suffix added.

    Args:
        fn_in (str): The name of the file to check
    """
    StarFile(fn_in)


def get_job_info(job_type: str) -> Optional[JobInfo]:
    """Get information about a job

    Args:
        job_type (str): The type of job to return info on

    Returns:
        :class:`~pipeliner.pipeliner_job.JobInfo`: JobInfo object with
        info about the job and it's references
    """

    the_job = new_job_of_type(job_type)
    try:
        info = the_job.jobinfo

    except AttributeError as e:
        print(str(e))
        print(f"ERROR: jobtype {job_type} has no info attribute")
        return None
    return info


def edit_jobstar(fn_template: str, params_to_change: dict, out_fn: str) -> str:
    """Modify one or more parameters in a job.star file

    Args:
        fn_template (str): The name of the job.star file to use as a template
        params_to_change (dict): The parameters to change in the
            format ``{param_name: new_value}``
        out_fn (str): Name for the new file to be written

    Returns:
        str: The name of the output file written
    """
    js = JobStar(fn_template)
    js.modify(params_to_change)
    js.write(out_fn)
    return out_fn


def job_success(
    job_name: str,
    search_time: float = 0,
    raise_error: bool = False,
    error_message: str = "",
) -> bool:
    """Check that a finished job has produced the expected control files

    Args:
        job_name (str): The name of the job to be checked in the format
            JobType/jobxxx/
        search_time (float): Time in minutes to spend looking for the control files
            before givng up
        raise_error (bool): Should an error be raised if the job is failed/aborted
            rather than returning a bool
        error_message (str): Additional text (if any) to print before the error reason

    Returns:
        bool: `True` if the job was successful, `False` if the job was failed, aborted,
            or no control files have appeared after the set search time and
            `raise_error` is `False`

    Raises:
        RuntimeError: If the job was failed, aborted, or no control files have appeared
            after the set search time and `raise_error` is `True`
    """

    itime = 0
    success_file = os.path.isfile(os.path.join(job_name, SUCCESS_FILE))
    fail_file = os.path.isfile(os.path.join(job_name, FAIL_FILE))
    abort_file = os.path.isfile(os.path.join(job_name, ABORT_FILE))

    while not success_file:
        if fail_file:
            if raise_error:
                raise RuntimeError(f"{error_message}\n{job_name} has failed")
            else:
                return False
        if abort_file:
            if raise_error:
                raise RuntimeError(f"{error_message}\n{job_name} was aborted")
            else:
                return False

        time.sleep(1)
        itime += 1
        if itime > search_time * 60:
            if raise_error:
                raise RuntimeError(
                    f"{error_message}\nhave been waiting {search_time} minutes "
                    f"after {job_name} finished and no status file has appeared"
                )
            else:
                return False
    return True
