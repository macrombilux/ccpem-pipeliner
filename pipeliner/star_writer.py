#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""
--------------------------
Starfile writing utilities
--------------------------

Writes star files in the same style as RELION
"""
import os
import threading
from typing import IO, Union

from gemmi import cif

from pipeliner import __version__


COMMENT_LINE = f"RELION version 4.0 / CCP-EM Pipeliner version {__version__}"


def write(
    doc: cif.Document,
    filename: Union[str, "os.PathLike[str]"],
    commentline: str = COMMENT_LINE,
):
    """Write a Gemmi CIF document to a RELION-style STAR file.

    The file is written as an atomic operation. This ensures that any processes reading
    it will always see a valid version (old or new) and not a half-written new file.

    The document is written to a temporary file first, then after the writing is
    complete and the file has been flushed to disk, the target file is replaced with the
    temporary one. The temporary file will always be removed even if the replacement is
    unsuccessful.

    Note: it is still possible for data to be lost using this function, if two processes
    try to write to the file at the same time. In that case, one of the new versions of
    the file will be kept and the other will not.

    Args:
        doc (:class:`gemmi.cif.Document`): The data to write out
        filename (str or pathlib.Path): The name of the file to write the data to
        commentline (str): The comment line to put in the star file
    """
    temp_filename = f"{filename}.tmp{threading.get_native_id()}"
    try:
        with open(temp_filename, "w") as temp_file:
            # Write the data to a temporary file and flush it to disk
            write_to_stream(doc, temp_file, commentline)
            temp_file.flush()
            os.fsync(temp_file.fileno())

        # Replace the target file with the temporary file
        os.replace(temp_filename, filename)
    finally:
        try:
            # Tidy up by removing the temporary file if it still exists
            os.remove(temp_filename)
        except FileNotFoundError:
            # Ignore the error if the temporary file does not exist anymore
            pass


def write_to_stream(
    doc: cif.Document, out_stream: IO[str], commentline: str = COMMENT_LINE
):
    """Write a Gemmi CIF document to an output stream using RELION's output style.

    Args:
        doc (:class:`gemmi.cif.Document`): The data to write out
        out_stream (str): The name of the file to write the data to
        commentline (str): The comment line to put in the written star file
    """
    for block in doc:
        if commentline is not None:
            out_stream.write("\n# " + commentline + "\n")
        out_stream.write("\ndata_{}\n\n".format(block.name))
        for item in block:
            if item.pair:
                out_stream.write("{}    {}\n".format(*item.pair))
            elif item.loop:
                out_stream.write("loop_ \n")
                loop = item.loop
                for ii, tag in enumerate(loop.tags, start=1):
                    out_stream.write("{} #{} \n".format(tag, ii))
                for row in range(loop.length()):
                    for col in range(loop.width()):
                        value = loop.val(row, col)
                        # Annoyingly, RELION's output uses 12-char wide fields except
                        # if value is "None"
                        if value == "None":
                            out_stream.write("{:>10} ".format(value))
                        # Need to quote empty strings
                        # (can normally use gemmi.cif.quote() for this but not if we
                        # want strict RELION-style output)
                        elif value == "":
                            out_stream.write("{:>12} ".format('""'))
                        else:
                            out_stream.write("{:>12} ".format(value))
                    out_stream.write("\n")
            out_stream.write(" \n")


def write_jobstar(in_dict: dict, out_fn: Union[str, "os.PathLike[str]"]):
    """Write a job.star file from a dictionary of options

    Args:
        in_dict (dict): Dict of job option keys and values
        out_fn (str or :class:`~pathlib.Path`): Name of the file to write to
    """
    jobstar = cif.Document()

    # make the job block - don't know why I have to separate these
    # two functions but Gemmi seg faults if you don't....
    job_block = jobstar.add_new_block("job")

    for param in in_dict:
        val = in_dict[param]
        # get the two that go to the job block
        if param in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            job_block.set_pair(param, val)

    # make the options block
    jobop_block = jobstar.add_new_block("joboptions_values")
    jobop_loop = jobop_block.init_loop(
        "_", ["rlnJobOptionVariable", "rlnJobOptionValue"]
    )

    for param in in_dict:
        val = in_dict[param]
        # get the two that go to the job block
        if param not in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            jobop_loop.add_row([cif.quote(param), cif.quote(str(val))])

    write(jobstar, out_fn)
