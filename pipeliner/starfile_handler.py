#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
import traceback
from pathlib import Path  # noqa: F401 # import needed for docs build but not in code
from typing import Optional, List, Dict, Tuple, Union

from gemmi import cif

from pipeliner.job_options import TRUES
from pipeliner.star_writer import write_jobstar
from pipeliner.star_keys import OUTPUT_EDGE_BLOCK, INPUT_EDGE_BLOCK
from pipeliner.nodes import Node
from pipeliner.process import Process
from pipeliner.utils import smart_strip_quotes
from pipeliner.relion_compatibility import (
    relion31_jobtype_conversion,
    convert_relion3_1_pipeline,
    convert_relion_4_pipeline,
    get_pipeline_version,
)

datalabels = (
    "data_",
    "data_pipeline_general",
    "data_pipeline_processes",
    "data_pipeline_nodes",
    "data_pipeline_input_edges",
    "data_pipeline_output_edges",
    "data_job",
    "data_joboptions_values",
    "data_joboptions_full_definition",
    "data_guinier",
    "data_fsc",
    "data_general",
    "data_optics",
    "data_micrographs",
    "data_particles",
    "data_movies",
    "data_schedule_general",
    "data_schedule_bools",
    "data_schedule_floats",
    "data_schedule_strings",
    "data_schedule_operators",
    "data_schedule_jobs",
    "data_schedule_edges",
    "data_output_nodes",
    "data_filament_splines",
    "data_coordinates",
    "data_model_general",
    "data_model_class",
    "data_class_averages",
)
reserved_words = (
    "stop_",
    "data_",
    "save_",
    "global_",
    "loop_",
)


def check_reserved_words(fn_in):
    """Make sure the starfile doesn't contain any illegally used reserved words

    Overwrites the original file if it is corrected.  The old file is saved as
    ``<filename>.old``
    """

    with open(fn_in) as thefile:
        f = thefile.readlines()

    # go line by line and look for reserved words used badly
    nline = 0
    been_corrected = False
    for line in f:
        if (
            line.rstrip() != "loop_"
            and not any(label in line.rstrip() for label in datalabels)
            and line[0] != "#"
        ):
            line_split = line.split()
            for entry in line_split:
                # quotate them if necessary
                for word in reserved_words:
                    if entry.startswith(word):
                        f[nline] = f[nline].replace(entry, '"' + entry + '"')
                        been_corrected = True
        nline += 1
        # if the file has been corrected write a tmp file with the corrections
    if been_corrected:
        oldname = fn_in + ".old"
        print(f"The file has been overwritten and original saved as {oldname}")
        shutil.move(fn_in, oldname)

        with open(fn_in, "w") as out_file:
            for ln in f:
                out_file.write(ln)
        print(
            "WARNING: Reserved words errors were found in the star "
            "file and have been corrected"
        )

    # check the starfile can be opened
    try:
        cif.read_file(str(fn_in))
    except Exception as e:
        tb = str(traceback.format_exc())
        raise ValueError(f"ERROR: Invalid starfile format: {e}; {tb}")


class StarFile(object):
    """A superclass for all types of starfiles used by the pipeliner

    Attributes:
        file_name (str or :class:`~pathlib.Path`): The star file
    """

    def __init__(self, fn_in: Union[str, "os.PathLike[str]"] = ""):
        if fn_in:
            check_reserved_words(fn_in)
        self.file_name = fn_in

    def pairs_as_dict(self, block: str) -> Dict[str, str]:
        """Returns paired values from a starfile as a dict

        Args:
            block (str): The name of the block to get the data from

        Returns:
            dict: `{parameter: value}`

        Raises:
            ValueError: If the specified block is not found
            ValueError: If the specified block is a loop and not a pair-value
        """
        sf = cif.read(str(self.file_name))
        fblock = sf.find_block(block)
        if fblock is None:
            raise ValueError(f"Block '{block}' not found")
        out_dict = {}
        if fblock[0].pair is None:
            raise ValueError(
                f"Block '{block}' does not contain item-value pairs. "
                "Try using star_loop_as_list() instead"
            )
        for item in fblock:
            out_dict[item.pair[0]] = item.pair[1]
        return out_dict

    def loop_as_list(
        self, block: str, columns: Optional[List[str]] = None
    ) -> Tuple[List[str], List[List[str]]]:
        """Returns a set of columns from a starfile loop as a list

        Args:
            block (str): The name of the block to get the data from
            columns (list): Names of the columns to get, if None then all
                columns are returned

        Returns:
            tuple: (:class:`list`, :class:`list`) [0] The names of the columns in order,
            [c1, c2, c3]. [1] The rows of the column(s) as a list of lists
            [[r1c1, r1c2, r1c3],[r1c1, r1c2, r1c3],[r1c1, r1c2, r1c3]]

        Raises:
            ValueError: If the specified block is not found
            ValueError: If the specified block does not contain a loop
            ValueError: If any of the specified columns are not found
        """
        sf = cif.read(str(self.file_name))
        fblock = sf.find_block(block)
        if fblock is None:
            raise ValueError(f"Block '{block}' not found")

        try:
            loop = fblock[0].loop
        except IndexError:
            loop = None

        if not loop:
            raise ValueError(
                f"Block '{block}' does not contain a loop; Use star_pairs_as_dict()"
                " instead"
            )
        cols = loop.tags[0:] if columns is None else columns
        for col in cols:
            if col not in loop.tags[0:]:
                raise ValueError(f"Column '{col}' not found in block '{block}'")
        data = []
        for row in fblock.find(cols):
            data.append([smart_strip_quotes(x) for x in row])
        return cols, data


class JobStar(StarFile):
    """A class for star files that define a pipeliner job parameters

    Attributes:
        jobtype (str): the job type, converted from the relion nomenclature if
            necessary
        joboptions(dict): The joboptions {name: value} all values are strings regardless
            of joboption type
        is_continue (bool): is the job a continuation
        is_tomo (bool): is the job tomography (might be removed when relon compatobility
            is deprecated)
    """

    def __init__(self, fn_in: str = ""):
        super().__init__(fn_in)
        if not fn_in:
            self.file_name = "job.star"
        self.is_continue = False
        self.is_tomo = False
        self.joboptions: Dict[str, str] = {}
        self.jobtype = ""

        if fn_in:
            data = cif.read_file(str(fn_in))
            job_block = data.find_block("job")
            options_block = data.find_block("joboptions_values")
            options_table = options_block.find(
                ["_rlnJobOptionVariable", "_rlnJobOptionValue"]
            )
            self.joboptions = {}

            for item in options_table:
                key = smart_strip_quotes(item[0])
                val = smart_strip_quotes(item[1])
                self.joboptions[key] = val

            for item in job_block:
                key = item.pair[0]
                val = item.pair[1]
                if key == "_rlnJobTypeLabel":
                    self.jobtype = val
                elif key == "_rlnJobType":
                    self.jobtype = relion31_jobtype_conversion(
                        int(val), self.joboptions
                    )
                elif key == "_rlnJobIsContinue":
                    self.is_continue = val in TRUES
                elif key == "_rlnJobIsTomo":
                    self.is_tomo = val in TRUES

    def all_options_as_dict(self) -> Dict[str, str]:
        """Returns a dict of all the parameters of a jobstar file

        The dict contains both the job options and the running options.
        All values in the dict are strings.
        """
        is_continue = "1" if self.is_continue else "0"
        is_tomo = "1" if self.is_tomo else "0"
        run_dict = {
            "_rlnJobTypeLabel": self.jobtype,
            "_rlnJobIsContinue": is_continue,
            "_rlnJobIsTomo": is_tomo,
        }
        return {**run_dict, **self.joboptions}

    def write(self, outname: Union[str, "os.PathLike[str]"] = ""):
        """Write a star file from the JobStar

        Args:
            outname (str or :class:`~pathlib.Path`): name of the output file; will
                overwrite the original file if none provided
        """

        outfile = self.file_name if not outname else outname
        write_jobstar(self.all_options_as_dict(), outfile)

    def modify(self, params_to_change: dict, allow_add=False):
        """Change multiple values in a jobstar from a dict

        Args:
            params_to_change (dict): The Parameters to change in the template in the
                format {param_name: new_value}
            allow_add (bool): Can new joboptions be added?
        Raises:
            ValueError: If an attempt is made to add new fields with allow_add=False
        """

        # change any necessary parameters

        new_continue = params_to_change.get("_rlnJobIsContinue")
        new_tomo = params_to_change.get("_rlnJobIsTomo")
        new_type = params_to_change.get("_rlnJobTypeLabel")

        self.is_continue = new_continue in TRUES if new_continue else self.is_continue
        self.is_tomo = new_tomo if new_tomo in TRUES else self.is_tomo
        self.jobtype = new_type if new_type else self.jobtype

        for changed_opt in params_to_change:
            if changed_opt not in [
                "_rlnJobIsContinue",
                "_rlnJobIsTomo",
                "_rlnJobTypeLabel",
            ]:
                if not allow_add and changed_opt not in self.joboptions:
                    raise ValueError(f"JobOption {changed_opt} not in the starfile")
                self.joboptions[changed_opt] = params_to_change[changed_opt]


class BodyFile(StarFile):
    """A star file that lists the bodies in a multibody refinement

    Attributes:
       bodycount (int): Number bodies in the file
    """

    def __init__(self, fn_in: str):
        super().__init__(fn_in)
        data = cif.read_file(str(fn_in))
        bodies_block = data.sole_block()
        self.bodycount = len(bodies_block.find_loop("_rlnBodyMaskName"))


class DataStarFile(StarFile):
    """A general class for starfiles that contain data, such as particles files

    Attributes:
        data (:class:`gemmi.cif.Document`): A gemmi cif object containing the data from
            the star file
    """

    def __init__(self, fn_in: str):
        """Create a RelionStarFile object

        Args:
            fn_in (str): The starfile to read
        """
        super().__init__(fn_in)
        # read the starfile
        self.data = cif.read_file(str(fn_in))

    def get_block(self, blockname: Optional[str] = None) -> cif.Block:
        """Get a block from the star file

        Args:
            blockname (str): The name of the block to get.  Use ``None``
                if the file has a single unnamed block

        Returns:
            (:class:`gemmi.cif.Block`): The desired block
        """
        if blockname is not None:
            block = self.data.find_block(blockname)
        else:
            block = self.data.sole_block()
        return block

    def count_block(self, blockname: Optional[str] = None) -> int:
        """Count the number of items in a block that only contains a single loop

        This is the format in most relion data star files

        Args:
            blockname (str): The name of the block to count

        Returns:
            int: The count
        """
        block = self.get_block(blockname)
        item_count = int(str(block[0].loop).split()[1])
        return item_count

    def column_as_list(self, blockname: str, colname: str) -> List[str]:
        """Return a single column from a block as a list

        Args:
            blockname (str): The name of the block to use
            colname (str): The name of the column to get

        Returns:
            list: The values from that column
        """

        coldata = self.get_block(blockname).find_loop(colname)
        return list(coldata)


# pipeline functions
def get_processes(pipeline, data):
    proc_block = data.find_block("pipeline_processes")
    proc_table = proc_block.find(
        "_rlnPipeLineProcess", ["Name", "Alias", "TypeLabel", "StatusLabel"]
    )

    for proc in proc_table:
        po = Process(
            name=proc[0],
            alias=proc[1] if proc[1] != "None" else None,
            p_type=proc[2],
            status=proc[3],
        )
        pipeline.procs.append(po)


def get_nodes(pipeline, data):
    nodes_block = data.find_block("pipeline_nodes")
    nodes_table = nodes_block.find("_rlnPipelineNode", ["Name", "TypeLabel"])
    for nl in nodes_table:
        type_split = nl[1].split(".")
        tl_type = type_split[0]
        kwds = type_split[2:]
        # when validation is added to node_factory don't validate here, too slow
        new_node = Node(nl[0], tl_type, kwds)
        pipeline.nodes.append(new_node)

    for i in pipeline.nodes:
        pipeline.nodes_names[i.name] = i
    for i in pipeline.procs:
        pipeline.procs_names[i.name] = i


def get_input_edges(pipeline, data):
    in_edges_block = data.find_block(INPUT_EDGE_BLOCK)
    if not in_edges_block:
        return
    in_edges_table = in_edges_block.find("_rlnPipelineEdge", ["FromNode", "Process"])
    for inedge in in_edges_table:
        innode = pipeline.nodes_names[inedge[0]]
        intoproc = pipeline.procs_names[inedge[1]]

        if innode not in pipeline.input_edges:
            pipeline.input_edges[innode] = [intoproc]
        else:
            pipeline.input_edges[innode].append(intoproc)

        if innode.name not in pipeline.inputs:
            pipeline.inputs[innode.name] = [intoproc]
        else:
            pipeline.inputs[innode.name].append(intoproc)


def get_output_edges(pipeline, data):
    out_edges_block = data.find_block(OUTPUT_EDGE_BLOCK)
    if not out_edges_block:
        return
    out_edges_table = out_edges_block.find("_rlnPipelineEdge", ["Process", "ToNode"])

    for outedge in out_edges_table:
        outnode = pipeline.nodes_names[outedge[1]]
        proc = pipeline.procs_names[outedge[0]]

        if proc not in pipeline.output_edges:
            pipeline.output_edges[proc] = [outnode]
        else:
            pipeline.output_edges[proc].append(outnode)
        if proc.name not in pipeline.outputs:
            pipeline.outputs[proc.name] = [outnode]
        else:
            pipeline.outputs[proc.name].append(outnode)


def update_process_input_edges(pipeline):
    for proc in pipeline.procs:
        for outedge in pipeline.output_edges:
            if outedge == proc:
                for outnode in pipeline.output_edges[outedge]:
                    proc.input_nodes.append(outnode)


def update_nodes_output_from(pipeline):
    for out_proc_name in pipeline.outputs:
        for node in pipeline.outputs[out_proc_name]:
            node.output_from_process = pipeline.procs_names[out_proc_name]


def update_nodes_input_to(pipeline):
    for node_name in pipeline.inputs:
        for node in pipeline.nodes:
            if node.name == node_name:
                node.input_for_processes_list.append(pipeline.inputs[node_name])


class PipelineStarFile(StarFile):
    """A class for holding a pipeline star file, e.g. 'default_pipeline.star'

    Used for interrogating an existing pipeline file, not for modifying or writing a
    pipeline; use a ProjectGraph for that

    Attributes:
        filename (str or :class:`~pathlib.Path`): the name of the file, usually
            'default_pipeline.star'
        procs (list[Process]): Process objects for the pipeline
        nodes (list[Node]): Node objects for the pipeline
        nodes_names (dict[str, Node]): {node_name: Node object} for easy finding of a
            node by name
        procs_names (dict[str, Process]): {proc_+name: Process object} for easy finding
            of a process by name
        input_edges (dict[Node, List[Process]]): All input edges
        output_edges (dict[Process, List[Node]]): All output edges
        inputs: (dict[str, list[Process]]): {node_name: [Process objects for all procs
            it is an input to]}
        outputs: dict[str, list[Node]] {process_name: list[Node objects for all Nodes
            it output]}
        pipeline_version(str): 'relion3', 'relion4', or 'ccpe-pipeliner' - should be
            removed after relion compatibility is deprecated
    """

    def __init__(self, fn_in: Union[str, "os.PathLike[str]"]):
        super().__init__(fn_in)
        self.procs: List[Process] = []
        self.nodes: List[Node] = []
        self.nodes_names: Dict[str, Node] = {}
        self.procs_names: Dict[str, Process] = {}
        self.input_edges: Dict[Node, List[Process]] = {}
        self.output_edges: Dict[Process, List[Node]] = {}
        self.inputs: Dict[str, List[Process]] = {}
        self.outputs: Dict[str, List[Node]] = {}
        self.filename = fn_in
        self.been_converted = False
        self.job_counter = 1
        data = cif.read_file(str(fn_in))

        self.pipeline_version = get_pipeline_version(data)

        if self.pipeline_version == "relion3":
            convert_relion3_1_pipeline(str(fn_in))
            self.been_converted = True
        if self.pipeline_version == "relion4":
            convert_relion_4_pipeline(str(fn_in))
            self.been_converted = True

        gen_block = data.find_block("pipeline_general")
        jobcount = gen_block.find_pair("_rlnPipeLineJobCounter")
        self.job_counter = int(jobcount[1])

        # reread the data in case the conversion changed the file
        data = cif.read_file(str(fn_in))

        get_processes(self, data)
        get_nodes(self, data)
        get_input_edges(self, data)
        get_output_edges(self, data)
        update_process_input_edges(self)
        update_nodes_output_from(self)
        update_nodes_input_to(self)


def compare_starfiles(starfile1: str, starfile2: str) -> Tuple[bool, bool]:
    """See if two starfiles contain the same information

    Direct comparison can be difficult because the starfile columns or blocks
    can be in different orders

    Args:
        starfile1 (str): Name of the first file
        starfile2 (str): Name of the second file

    Returns:
        tuple: (:class:`bool`, :class:`bool`) [0] True if the starfile structures
        are identical (Names of blocks and columns) [1] True if the data in
        the two files are identical
    """

    sf1, sf2 = StarFile(starfile1), StarFile(starfile2)
    # check the blocks
    sf1_blocks = set([x.name for x in cif.read_file(str(sf1.file_name))])
    sf2_blocks = set([x.name for x in cif.read_file(str(sf2.file_name))])
    if sf1_blocks != sf2_blocks:
        return False, False

    # check loops/pairs within blocks
    for block in sf1_blocks:
        try:
            l1 = set(sf1.loop_as_list(block)[0])
            l2 = set(sf2.loop_as_list(block)[0])

        except ValueError:
            l1 = set(sf1.pairs_as_dict(block))
            l2 = set(sf2.pairs_as_dict(block))
        if l1 != l2:
            return False, False

    # check the data
    for block in sf1_blocks:
        try:
            l1_lists = sf1.loop_as_list(block)[1]
            l2_lists = sf2.loop_as_list(block)[1]
            l1_sets = [set(x) for x in l1_lists]
            l2_sets = [set(x) for x in l2_lists]
            for i in l1_sets:
                if i not in l2_sets:
                    return True, False
            for i in l2_sets:
                if i not in l1_sets:
                    return True, False

        except ValueError:
            d1 = sf1.pairs_as_dict(block)
            d2 = sf2.pairs_as_dict(block)
            l1 = set([d1[x] for x in d1])
            l2 = set([d2[x] for x in d2])
            if l1 != l2:
                return True, False

    return True, True
