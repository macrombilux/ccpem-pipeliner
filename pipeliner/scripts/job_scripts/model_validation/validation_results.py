#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import copy
import pandas as pd
from collections import OrderedDict
import json
import numpy as np
import warnings

# from cootscript_edit import SetCootScript
from ccpem_utils.other.cluster import cluster_coord_features
from ccpem_utils.other.utils import extract_numeric_from_string
from pipeliner.starfile_handler import JobStar

# Resolution label (equivalent to <4SSQ/LL>


class PipelineResultsViewer(object):
    """
    Results viewer for job pipeline
    """

    def __init__(self):
        job_star = JobStar("job.star")
        job_options = job_star.all_options_as_dict()
        input_model = job_options["input_model"]
        self.modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = job_options["input_map"]
        self.mapid = os.path.splitext(os.path.basename(input_map))[0]
        # lists of job processes
        # self.bfactor_process = []
        self.molprobity_process = []
        # self.piscore_process = []
        # self.cablam_process = []
        # self.refmac_process = []
        # self.scores_process = []
        self.smoc_process = []
        # self.fdr_process = []
        # self.fsc_process = []
        # self.diffmap_process = []
        # self.sccc_process = []
        # self.dssp_process = []
        # self.jpred_process = []
        # set coot script
        # cs = SetCootScript(self.job_location)
        # dict_local_processes = {}
        self.list_jobs = []
        # list of chains with outliers
        self.list_chains = []
        # list_summary_methods: methods for summary table
        self.list_summary_methods = []
        # per-residue plots
        self.dict_residue_plot = {}
        # iris plot data
        self.dict_iris_data = {}
        # outlier clusters
        self.dict_outlier_clusters = OrderedDict()
        # dictionary with residue outlier details for each method
        # self.residue_outliers[method][mid][chain][res] = outlier type
        self.residue_outliers = OrderedDict()
        # dictionary with residue coordinates for each model
        self.residue_coordinates = OrderedDict()
        ca_coord_json = self.modelid + "_residue_coordinates.json"
        with open(ca_coord_json, "r") as cj:
            self.dict_ca_coord = json.load(cj)
        # TODO: get b-fact results for plot
        # self.set_bfactor_results()
        # output report for global results
        gr = open("global_report.txt", "w")
        run_molprobity = job_options["run_molprobity"] == "Yes"
        run_smoc = job_options["run_smoc"] == "Yes"
        run_tortoize = job_options["run_tortoize"] == "Yes"
        if run_molprobity:
            self.set_molprobity_results(gr)
        if run_smoc:
            self.set_smoc_results(gr)
        if run_tortoize:
            self.set_tortoize_results(gr)
        self.get_outlier_coordinates()
        self.cluster_outliers()
        self.get_outlier_summary()
        self.save_plot_data()
        if len(self.dict_iris_data) > 0:
            self.save_iris_data()

    def save_plot_data(self):
        """
        Save json for outlier plot
        """
        with open(self.modelid + "_plot_data.json", "w") as j:
            json.dump(self.dict_residue_plot, j)

    def set_bfactor_results(self):
        bfact_json = self.modelid + "_residue_bfactors.json"
        with open(bfact_json, "r") as bj:
            bfact_data = json.load(bj)
        try:
            max_bfact_dev = max(round(bfact_data["max_dev"], 1), 10.0)
        except KeyError:
            max_bfact_dev = 10.0
        bfactor_plotname = "b-factor_dev/" + str(max_bfact_dev)
        for m in bfact_data:
            if m == "max_dev":
                continue  # skip max deviation value stored
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in bfact_data[m]:
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in bfact_data[m][c]:
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        bfactor_plotname,
                        r,
                        round(bfact_data[m][c][r][1] / max_bfact_dev, 3),
                    )
        # gr.write("*Atomic B-factors*\n")
        # gr.write("==================\n")

    def set_molprobity_results(self, gr):
        mp_json = self.modelid + "_molprobity_summary.json"
        with open(mp_json, "r") as mpj:
            mp_data = json.load(mpj)
        gr.write("*Molprobity* model geometry\n")
        gr.write("===========================\n")
        labels = [
            "{:<25}".format("#Metric"),
            "{:<7}".format("Score"),
            "{:<60}".format("Expected/Percentile"),
        ]
        gr.write("\t".join(labels) + "\n")
        molprob_data = []
        for metric in mp_data:
            molp_metric = " ".join(metric.split("_"))
            score = mp_data[metric][0]
            pct = mp_data[metric][1]
            molprob_data.append(
                [
                    "{:<25}".format(molp_metric),
                    "{:<7}".format(score),
                    "{:<60}".format(pct),
                ]
            )
            gr.write("\t".join(molprob_data[-1]) + "\n")
        gr.write("\n\n")

        mp_res_json = self.modelid + "_residue_molprobity_outliers.json"
        with open(mp_res_json, "r") as mpj:
            mp_data = json.load(mpj)
        for m in mp_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in mp_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in mp_data[m][c]:
                    for outlier in mp_data[m][c][r]:
                        try:
                            self.residue_outliers[m][c][r].append(
                                outlier[0] + " (" + outlier[1] + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                outlier[0] + " (" + outlier[1] + ")"
                            ]
                    # save for outlier plots
                    # adding 0.25 as y values for the plot
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c], "molprobity", r, 0
                    )
        iris_json = self.modelid + "_molprobity_iris.json"

        if os.path.isfile(iris_json):
            with open(iris_json, "r") as mpj:
                iris_data = json.load(mpj)
            try:
                self.dict_iris_data["molprobity"] = iris_data["molprobity"]
            except KeyError:
                warnings.warn("Molprobity Iris data not found")

    def add_residue_to_plot(self, dict_residue_plot, outlier_name, residue_num, score):
        """
        Add residue outlier to the outlier plot dictionary
        """
        # TODO : insertion codes not handled
        try:
            residue = int(residue_num)
        except (TypeError, ValueError) as e:
            print(e)
            try:
                residue = extract_numeric_from_string(residue_num)
            except (TypeError, ValueError) as e:
                print(e)
                print(
                    "Skipping {} outlier {} from the plot".format(outlier_name, residue)
                )
                return
        try:
            # overwrite same residue numbers
            if int(residue) not in dict_residue_plot[outlier_name][0]:
                dict_residue_plot[outlier_name][0].append(int(residue))
                dict_residue_plot[outlier_name][1].append(score)
        except KeyError:
            dict_residue_plot[outlier_name] = [
                [int(residue)],
                [score],
            ]

    def set_smoc_results(self, gr):
        smoc_res_json = self.modelid + "_" + self.mapid + "_residue_smoc.json"
        with open(smoc_res_json, "r") as smocj:
            smoc_data = json.load(smocj)
        count_total_res = 0
        count_outlier_res = 0
        for m in smoc_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in smoc_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in smoc_data[m][c]:
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c], "smoc", r, smoc_data[m][c][r][0]
                    )
                    if smoc_data[m][c][r][1] < -1.5:  # z < -1.5
                        try:
                            self.residue_outliers[m][c][r].append(
                                "smoc"
                                + " (raw:"
                                + str(round(float(smoc_data[m][c][r][0]), 3))
                                + ";Z:"
                                + str(round(float(smoc_data[m][c][r][1]), 3))
                                + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "smoc"
                                + " (raw:"
                                + str(round(float(smoc_data[m][c][r][0]), 3))
                                + ";Z:"
                                + str(round(float(smoc_data[m][c][r][1]), 3))
                                + ")"
                            ]
                        count_outlier_res += 1
                        # add outlier to plot
                        self.add_residue_to_plot(
                            self.dict_residue_plot[m][c],
                            "smoc_outlier",
                            r,
                            smoc_data[m][c][r][0],
                        )
                    count_total_res += 1
        gr.write("*SMOC* model fit\n")
        gr.write("================\n")
        labels = ["{:<25}".format("#Metric"), "{:<7}".format("Score")]
        gr.write("\t".join(labels) + "\n")
        gr.write(
            "\t".join(
                [
                    "{:<25}".format("SMOC-Z < -1.5"),
                    "{:<7}".format(
                        str(round(float(count_outlier_res) * 100 / count_total_res, 3))
                        + "%"
                    ),
                ]
            )
            + "\n"
        )
        gr.write("\n\n")

    def set_global_tortoize_results(self, gr):
        tortoize_json = self.modelid + "_tortoize_summary.json"
        with open(tortoize_json, "r") as mpj:
            t_data = json.load(mpj)
        gr.write("*Tortoize* model(1) geometry\n")
        gr.write("===========================\n")
        labels = [
            "{:<25}".format("#Metric"),
            "{:<7}".format("Score"),
            "{:<60}".format("Expected/Percentile"),
        ]
        gr.write("\t".join(labels) + "\n")
        score_data = []
        for model in t_data:
            # list_scores = [model_rama_sd,model_rama_z,model_torsion_sd,
            # model_torsion_z]
            # list_scores = t_data[model]
            # list_metrics = [
            #     "ramachandran-jackknife-sd",
            #     "ramachandran-z",
            #     "torsion-jackknife-sd",
            #     "torsion-z",
            # ]
            for metric in t_data[model]:
                score = round(float(t_data[model][metric][0]), 3)
                pct = t_data[model][metric][1]  # TODO: add expected ranges
                score_data.append(
                    "\t".join(
                        [
                            "{:<25}".format(metric),
                            "{:<7}".format(score),
                            "{:<60}".format(pct),
                        ]
                    )
                )
            gr.write("\n".join(score_data) + "\n")
            break
        gr.write("\n\n")

    def set_tortoize_results(self, gr):
        self.set_global_tortoize_results(gr)

        res_json = self.modelid + "_residue_tortoize.json"
        with open(res_json, "r") as j:
            res_data = json.load(j)
        for m in res_data:
            if m not in self.residue_outliers:
                self.residue_outliers[m] = {}
            if m not in self.dict_residue_plot:
                self.dict_residue_plot[m] = {}
            for c in res_data[m]:
                if c not in self.residue_outliers[m]:
                    self.residue_outliers[m][c] = {}
                if c not in self.dict_residue_plot[m]:
                    self.dict_residue_plot[m][c] = {}
                for r in res_data[m][c]:
                    outlier = res_data[m][c][r]
                    if abs(float(outlier[1])) > 2:
                        try:
                            self.residue_outliers[m][c][r].append(
                                "rama_z" + " (" + str(round(float(outlier[1]), 3)) + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "rama_z" + " (" + str(round(float(outlier[1]), 3)) + ")"
                            ]
                    if abs(float(outlier[2])) > 2:
                        try:
                            self.residue_outliers[m][c][r].append(
                                "torsion_z"
                                + " ("
                                + str(round(float(outlier[2]), 3))
                                + ")"
                            )
                        except KeyError:
                            self.residue_outliers[m][c][r] = [
                                "torsion_z"
                                + " ("
                                + str(round(float(outlier[2]), 3))
                                + ")"
                            ]
                    # save for outlier plots
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        "tortoize_ramaz/2",
                        r,
                        round(float(outlier[1]) / 2.0, 3),
                    )
                    self.add_residue_to_plot(
                        self.dict_residue_plot[m][c],
                        "tortoize_torsionz/2",
                        r,
                        round(float(outlier[2]) / 2.0, 3),
                    )
        iris_json = self.modelid + "_tortoize_iris.json"
        if os.path.isfile(iris_json):
            with open(iris_json, "r") as mpj:
                iris_data = json.load(mpj)
            try:
                self.dict_iris_data["rama_z"] = iris_data["rama_z"]
            except KeyError:
                warnings.warn("Ramachandran-Z Iris data not found")

    def get_outlier_coordinates(self):
        for m in self.residue_outliers:
            for c in self.residue_outliers[m]:
                for r in self.residue_outliers[m][c]:
                    residue_id = "_".join([m, c, r])
                    try:
                        self.residue_coordinates[residue_id] = self.dict_ca_coord[m][c][
                            r
                        ][0]
                    except KeyError:
                        # TODO: mismatch of residue IDs?
                        pass

    def cluster_outliers(self):
        """
        Cluster outlier residues based on spatial coordinates
        Uses sklearn DBSCAN
        """
        list_res_id = []
        list_ca_coord = []
        for rid in self.residue_coordinates:
            list_res_id.append(rid)
            list_ca_coord.append(self.residue_coordinates[rid])
        cluster_labels = cluster_coord_features(
            np.array(list_ca_coord), dbscan_eps=5.5, norm=False
        )
        for c in range(len(cluster_labels)):
            try:
                self.dict_outlier_clusters[cluster_labels[c]].append(list_res_id[c])
            except KeyError:
                self.dict_outlier_clusters[cluster_labels[c]] = [list_res_id[c]]

    def save_iris_data(self):
        iris_json = self.modelid + "_iris_data.json"
        with open(iris_json, "w") as j:
            json.dump(self.dict_iris_data, j)

    def get_outlier_summary(self):
        """
        Merge outlier types for each residue
        """
        self.dict_cluster_number = {}
        self.dict_coot_outlier_clusters = {}
        self.cluster_outlier_file = "outlier_clusters.csv"
        oc = open(self.cluster_outlier_file, "w")
        oc.write(
            "#Molprobity: geometry outliers reported by molprobity.\n"
            "#SMOC: local fit to map outliers reported by TEMPy SMOC.\n"
        )
        list_cluster_outliers = []
        for cl_num in self.dict_outlier_clusters:
            # OrderedDict([(0, ['1_A_101', '1_A_102']), (1, ['1_B_105', '1_B_106'])])
            if cl_num == -1:
                continue  # skip unclustered residues
            for res_id in self.dict_outlier_clusters[cl_num]:
                m, c, r = res_id.split("_")
                try:
                    # smoc (0.521;-2.098)
                    # dihedral_angles (1_A_201_C1A:1_A_201_C2A:1_A_201_CAA:1_A_201_CBA)
                    outlier_string = "|".join(self.residue_outliers[m][c][r])
                    list_cluster_outliers.append(
                        ",".join(
                            [
                                str(cl_num),
                                "".join(res_id.split("_")[1:]),  # skip model number
                                outlier_string,
                            ]
                        )
                    )
                except KeyError:
                    print("Residue ID {} not found in outlier dict".format(res_id))
        cl_num = -1
        try:
            for res_id in self.dict_outlier_clusters[cl_num]:
                m, c, r = res_id.split("_")
                try:
                    # smoc (0.521;-2.098)
                    # dihedral_angles (1_A_201_C1A:1_A_201_C2A:1_A_201_CAA:1_A_201_CBA)
                    outlier_string = "|".join(self.residue_outliers[m][c][r])
                    list_cluster_outliers.append(
                        ",".join(
                            [
                                str(cl_num),
                                "".join(res_id.split("_")[1:]),  # skip model number
                                outlier_string,
                            ]
                        )
                    )
                except KeyError:
                    print("Residue ID {} not found in outlier dict".format(res_id))
        except KeyError:
            pass
        oc.write("\n".join(list_cluster_outliers))
        oc.close()

    def set_df_csv(self, dict_data, data_id):
        df = pd.DataFrame.from_dict(dict_data, orient="index")
        # df1 = df.replace(np.nan,' ')
        df.to_csv(
            data_id + "_outliersummary.txt",
            sep=",",
        )

    def update_coordinates(self, dict_coordinates):
        """
        Update residue CA coordinates for outliers
        """
        for pdbid in dict_coordinates:
            if pdbid not in self.residue_coordinates:
                self.residue_coordinates[pdbid] = dict_coordinates[pdbid]
            else:
                for chain in dict_coordinates[pdbid]:
                    if chain not in self.residue_coordinates[pdbid]:
                        self.residue_coordinates[pdbid][chain] = dict_coordinates[
                            pdbid
                        ][chain]
                    else:
                        for resnum in dict_coordinates[pdbid][chain]:
                            res_coordinate = dict_coordinates[pdbid][chain][resnum]
                            if len(res_coordinate) != 3:
                                continue
                            if resnum not in self.residue_coordinates[pdbid][chain]:
                                self.residue_coordinates[pdbid][chain][
                                    resnum
                                ] = res_coordinate
                            elif (
                                len(self.residue_coordinates[pdbid][chain][resnum]) != 3
                            ):
                                self.residue_coordinates[pdbid][chain][
                                    resnum
                                ] = res_coordinate

    def cluster_outliers_kdtree(self):
        """
        Cluster outlier residues based on spatial coordinates
        Uses sklearn DBSCAN
        """
        list_res_id = []
        list_ca_coord = []
        for rid in self.residue_coordinates:
            list_res_id.append(rid)
            list_ca_coord.append(self.residue_coordinates[rid])
        cluster_labels = cluster_coord_features(
            np.array(list_ca_coord), dbscan_eps=3.83, norm=False
        )
        assert len(cluster_labels) > 0
        list_res_clusters = []
        self.dict_residue_clusters = {}
        # get outlier residues from residue_outliers dict

        # for chain in self.list_chains:
        #     for met in self.list_summary_methods:
        #         for pdbid in self.residue_outliers[met]:
        #             if chain not in self.residue_outliers[met][pdbid]: continue
        #             if pdbid not in self.dict_residue_clusters:
        #                 self.dict_residue_clusters[pdbid] = {}
        #             #check if coordinate data is available
        #             if pdbid not in self.residue_coordinates:
        #                 continue
        #             if chain not in chain in self.residue_coordinates[pdbid]:
        #                 continue
        #             list_coord = []
        #             list_res = []
        #             for res in self.residue_outliers[met][pdbid][chain]:
        #                 list_coord.append(self.residue_outliers[met][pdbid][chain][res])
        #                 list_res.append(str(res))

        # for mapid in self.residue_coordinates:
        #     if mapid not in self.dict_residue_clusters:
        #             self.dict_residue_clusters[mapid] = {}

        for pdbid in self.residue_coordinates:
            if pdbid not in self.dict_residue_clusters:
                self.dict_residue_clusters[pdbid] = {}
            for chain in self.residue_coordinates[pdbid]:
                list_coord = []
                list_res = []
                for res in self.residue_coordinates[pdbid][chain]:
                    if str(res) in list_res:
                        continue
                    list_coord.append(self.residue_coordinates[pdbid][chain][res])
                    list_res.append(str(res))  # resid chain

                if len(list_coord) > 0:
                    list_res_clusters = self.cl_out.get_cluster_outliers(
                        list_res, list_coord
                    )
                    list_res_clusters.sort(key=len, reverse=True)

                self.dict_residue_clusters[pdbid][chain] = copy.deepcopy(
                    list_res_clusters
                )

    def set_coot_dict_clusters(self, dict_model_outlier, chain):
        """
        Save outlier clusters in a dictionary for coot fixing
        """
        for pdbid in self.dict_residue_clusters:
            #                 cl_id = mapid+':'+pdbid
            #                 if mapid == 'nomap': cl_id = pdbid
            if pdbid not in self.dict_coot_outlier_clusters:
                self.dict_coot_outlier_clusters[pdbid] = {}
            try:
                dict_outliers = dict_model_outlier[pdbid]
            except KeyError:
                continue
            try:
                list_clusters = self.dict_residue_clusters[pdbid][chain]
            except KeyError:
                continue
            #             try:
            #                 cl_number = self.dict_cluster_number[pdbid][0]
            #             except KeyError: continue
            for lnum in range(len(list_clusters)):
                if len(list_clusters[lnum]) == 0:
                    continue
                # sorted by cluster size until individuals
                if lnum > 0 and len(list_clusters[lnum - 1]) < len(list_clusters[lnum]):
                    break
                list_res = list_clusters[lnum]

                flag_cluster = False
                for res in list_res:
                    try:
                        residue_coordinate = self.residue_coordinates[pdbid][chain][res]
                    except KeyError:
                        continue
                    if res in dict_outliers:
                        flag_cluster = self.set_cluster_res_outliers(
                            res, dict_outliers, pdbid, chain, residue_coordinate
                        )
                if flag_cluster:
                    self.dict_cluster_number[pdbid][0] += 1  # model
                    self.dict_cluster_number[pdbid][1] += 1  # chain
            # break # compute coot data for the first map based outliers only

    def set_cluster_res_outliers(
        self,
        res,
        dict_outliers,
        pdbid,
        chain,
        residue_coordinate,
    ):
        # res,molprobity outlier string, cablam outlier string, smoc
        outlier_types = dict_outliers[res][:]
        # rename selected outliers with method name
        # and merge with '\n'
        for met_index in range(1, len(outlier_types)):
            # different types within a method
            split_type = outlier_types[met_index].split("<br>")
            outlier_types[met_index] = "\n".join(split_type)
            if outlier_types[met_index] == "-":
                outlier_types[met_index] = ""
                continue
            # add method name to outlier type
            if self.list_summary_methods[met_index] in [
                "cablam",
                "smoc",
                "fdr",
                "jpred",
            ]:
                outlier_types[met_index] = (
                    self.list_summary_methods[met_index]
                    + " "
                    + outlier_types[met_index]
                )
        # remove no outlier methods
        ct_pop = 0
        for met_index in range(len(outlier_types)):
            if met_index > len(outlier_types):
                break
            if len(outlier_types[met_index - ct_pop]) == 0:
                outlier_types.pop(met_index - ct_pop)
                ct_pop += 1

        outlier_types_string = "\n".join(outlier_types[1:])  # 0: 'Residue'

        flag_cluster = True
        try:
            self.dict_coot_outlier_clusters[pdbid]["clusters"].append(
                (
                    chain,
                    res,
                    self.dict_cluster_number[pdbid][1],
                    outlier_types_string,
                    residue_coordinate,
                )
            )
        except KeyError:
            self.dict_coot_outlier_clusters[pdbid]["clusters"] = [
                (
                    chain,
                    res,
                    self.dict_cluster_number[pdbid][1],
                    outlier_types_string,
                    residue_coordinate,
                )
            ]
        return flag_cluster

    def add_cootdata(self):
        """
        Add cluster outliers for fixing in Coot
        """
        if self.cootset is not None:
            for pdbid in self.dict_coot_outlier_clusters:
                self.cootset.add_data_to_cootfile(
                    self.dict_coot_outlier_clusters[pdbid],
                    modelid=pdbid,
                    method="clusters",
                )

    def convert_outlierdict_to_dataframe(self):
        """
        Generate a dataframe of residue outliers
        """
        dict_df = {}
        for met in self.list_summary_methods[1:]:
            #             for mapid in self.residue_outliers[met]:
            #                 self.residue_outliers[met][mapid] = {}
            for pdbid in self.residue_outliers[met]:
                #                 if mapid == 'nomap':
                df_id = pdbid
                #                 else:
                #                     df_id = mapid+':'+pdbid
                if df_id not in dict_df:
                    dict_df[df_id] = {}
                for chain in self.residue_outliers[met][pdbid]:
                    for res in self.residue_outliers[met][pdbid][chain]:
                        outlier_det = self.residue_outliers[met][pdbid][chain][res]
                        if isinstance(outlier_det, list):
                            outlier_string = ":".join(outlier_det)
                        else:
                            outlier_string = outlier_det
                        try:
                            res_key = int(res)
                        except ValueError:
                            res_key = res.strip()

                        try:
                            dict_df[df_id][(chain, res_key)][met] = outlier_string
                        except KeyError:
                            dict_df[df_id][(chain, res_key)] = {}
                            dict_df[df_id][(chain, res_key)][met] = outlier_string
        for df_id in dict_df:
            self.set_df_csv(dict_df[df_id], df_id)

    def add_new_residue_outlier_set(
        self, residue_outliers, dict_outlier, pdbid, method="molprobity"
    ):
        """
        Add outlier set to the dictionary dict_outlier[res][method] = outlier name(s)
        """
        # index for summary table
        met_index = self.list_summary_methods.index(method)
        len_table = len(self.list_summary_methods)
        for res in residue_outliers:
            outlier_det = residue_outliers[res]
            if isinstance(outlier_det, list):
                outlier_string = "<br>".join(outlier_det)
            else:
                outlier_string = outlier_det
            res_key = res.strip()
            #             try:
            #                 res_key = int(res)
            #             except ValueError:
            #                 res_key = res.strip()
            # set outlier dictionary:
            # [res,outlier type1,outlier type 2,..]
            try:
                dict_outlier[res_key][met_index] = outlier_string
            except (KeyError, IndexError):
                dict_outlier[res_key] = [res]
                dict_outlier[res_key].extend(["-"] * (len_table - 1))
                dict_outlier[res_key][met_index] = outlier_string


def main():
    PipelineResultsViewer()


if __name__ == "__main__":
    main()
