#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import argparse
import pandas as pd
import json
from ccpem_utils.other import cluster, calc
from ccpem_utils.model import gemmi_model_utils
from typing import OrderedDict, Any


def parse_args():
    parser = argparse.ArgumentParser(description="get SMOC score results")
    parser.add_argument(
        "-smoc",
        "--smoc",
        required=True,
        help="Input smoc scores (.csv)",
    )
    parser.add_argument(
        "-coord",
        "--coord",
        required=True,
        help="Input c-alpha coords (.json)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="pdbid",
        help="ID used to save results json file",
    )

    return parser.parse_args()


def parse_smoc_json(
    smoc_json: str,
    ca_coord_json: str,
    model_id: str,
):
    with open(ca_coord_json, "r") as j:
        dict_pdb_ca_coord = json.load(j)
    list_ids, list_coords = gemmi_model_utils.convert_dict_attributes_to_list(
        dict_pdb_ca_coord
    )
    model_kdtree = cluster.generate_kdtree(list_coords)

    # Get data
    dict_resnum: dict = {}
    dict_coord: dict = {}
    dict_smoc: dict = {}

    with open(smoc_json, "r") as j:
        smoc_scores = json.load(j)
    dict_smoc["1"] = {}
    dict_resnum["1"] = {}
    dict_coord["1"] = {}
    for chain in smoc_scores["chains"]:
        dict_smoc["1"][chain] = {}
        list_coord_chain = []
        list_resnum = []
        for resnum in smoc_scores["chains"][chain]:
            dict_smoc["1"][chain][resnum] = [smoc_scores["chains"][chain][resnum]]
            list_resnum.append(resnum)
            ca_coord = dict_pdb_ca_coord["1"][chain][str(resnum)][0]
            list_coord_chain.append(ca_coord)
        dict_resnum["1"][chain] = list_resnum
        dict_coord["1"][chain] = list_coord_chain
    smoc_z(dict_smoc, dict_resnum, dict_coord, model_kdtree, list_ids)
    out_json = model_id + "_residue_smoc.json"
    with open(out_json, "w") as j:
        json.dump(dict_smoc, j)


def parse_smoc_csv(
    smoc_csv: str,
    ca_coord_json: str,
    model_id: str,
    list_chain_model: list = [],
):
    smoc_df: Any = pd.read_csv(smoc_csv, skipinitialspace=True, skip_blank_lines=True)
    with open(ca_coord_json, "r") as j:
        dict_pdb_ca_coord = json.load(j)
    list_ids, list_coords = gemmi_model_utils.convert_dict_attributes_to_list(
        dict_pdb_ca_coord
    )
    model_kdtree = cluster.generate_kdtree(list_coords)

    # Get data
    dict_smoc: dict = OrderedDict()
    dict_resnum: dict = {}
    dict_score: dict = {}
    dict_coord: dict = {}
    list_pdbs: list = []

    for pdb in smoc_df:
        if "_" not in pdb:
            continue

        # add plots for each chain
        chain = pdb.split("_")[-1]
        pdb_id = "_".join(pdb.split("_")[:-1])
        if pdb_id not in list_pdbs:
            dict_smoc["1"] = {}
            dict_resnum["1"] = {}
            dict_score["1"] = {}
            dict_coord["1"] = {}
            list_pdbs.append(pdb_id)  # multi pdb
        # ct_data_index = 0
        if "." in chain:
            chain = chain.split(".")[0]
            # pdbname = ".".join(pdb.split(".")[:-1])
        if chain == "":
            chain = " "
        if chain not in list_chain_model:
            list_chain_model.append(chain)
            dict_smoc["1"][chain] = {}
        list_coord_chain = []
        list_resnum = []
        # ct_res_chain = 0
        pdb_df = smoc_df[pdb].dropna(axis=0, how="any")
        if pdb_df[0] == "resnum":
            for val in pdb_df[1:]:
                try:
                    residue_num = str(int(float(val)))
                except TypeError:
                    residue_num = val
                list_resnum.append(residue_num)
                ca_coord = dict_pdb_ca_coord["1"][chain][str(residue_num)][0]
                list_coord_chain.append(ca_coord)
            dict_coord["1"][chain] = list_coord_chain
            dict_resnum["1"][chain] = list_resnum
        list_smoc = []
        if pdb_df[0] == "smoc":
            for val in pdb_df[1:]:
                list_smoc.append(round(float(val), 3))
            dict_score["1"][chain] = list_smoc
    for m in dict_resnum:
        for c in dict_resnum[m]:
            list_resnum = dict_resnum[m][c]
            try:
                list_smoc = dict_score[m][c]
            except KeyError:
                print("SMOC score not calculated for chain {}".format(c))
                continue
            assert len(list_resnum) == len(list_smoc)
            for lc in range(len(list_resnum)):
                resnum = list_resnum[lc]
                smoc_score = list_smoc[lc]
                dict_smoc[m][c][resnum] = [smoc_score]  # fill dict_smoc
    smoc_z(dict_smoc, dict_resnum, dict_coord, model_kdtree, list_ids)
    out_json = model_id + "_residue_smoc.json"
    with open(out_json, "w") as j:
        json.dump(dict_smoc, j)


def smoc_z(
    dict_smoc,
    dict_resnum,
    dict_coord,
    model_kdtree,
    list_ids,
):
    for m in dict_resnum:
        for c in dict_resnum[m]:
            list_resnum = dict_resnum[m][c]
            try:
                list_coord_chain = dict_coord[m][c]
            except KeyError:
                print("Coordinate data not found for chain {}".format(c))
                continue
            list_neighbors = cluster.get_neighbors_kdtree(
                coords=list_coord_chain, kdtree=model_kdtree, distance=12.0
            )
            assert len(list_resnum) == len(list_neighbors)
            count_res = 0
            for neighbors in list_neighbors:  # neighbors of each input coordinate
                list_smoc_neigh = []
                for n in neighbors:
                    residue_id = list_ids[n]  # find residue id of neighbor
                    mn, cn, rn = residue_id.split("_")
                    try:
                        list_smoc_neigh.append(dict_smoc[mn][cn][rn][0])
                    except KeyError:
                        pass
                if len(list_smoc_neigh) > 2:
                    smoc_z = calc.calc_z(
                        list_smoc_neigh, dict_smoc[m][c][list_resnum[count_res]][0]
                    )
                    dict_smoc[m][c][list_resnum[count_res]].append(
                        round(float(smoc_z), 3)
                    )
                else:
                    dict_smoc[m][c][list_resnum[count_res]].append(0.0)
                count_res += 1


def main():
    args = parse_args()
    # read input
    smoc_outputfile = args.smoc
    coord_json = args.coord
    model_id = args.id
    parse_smoc_json(
        smoc_json=smoc_outputfile, ca_coord_json=coord_json, model_id=model_id
    )


if __name__ == "__main__":
    main()
