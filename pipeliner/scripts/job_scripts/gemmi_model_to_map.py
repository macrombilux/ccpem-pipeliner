#! /usr/bin/env python3
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import gemmi
import mrcfile
import argparse
from pathlib import Path

# Gemmi read the docs:
# https://gemmi.readthedocs.io/en/latest/index.html

# Gemmi Python API:
# https://project-gemmi.github.io/python-api/index.html


def translate_model(structure, trans):
    for model in structure:
        for chain in model:
            for residue in chain:
                for atom in residue:
                    atom.pos = gemmi.Position(
                        atom.pos.x + trans[0],
                        atom.pos.y + trans[1],
                        atom.pos.z + trans[2],
                    )


def calculate_map(
    pdb_in, pdb_out, map_ref, cell_dims, map_out, resolution, ref_map_aligned=True
):
    # Read structure
    structure = gemmi.read_structure(pdb_in)

    # Remove hydrogens
    print("Removing hydrogens")
    structure.remove_hydrogens()

    # Remove waters
    print("Removing waters")
    structure.remove_waters()

    # Remove waters and ligands
    # structure.remove_ligands_and_waters()

    # Remove alternative conformations
    # structure.remove_alternative_conformations()

    # Calculate a P1 box with margin of 10 Angstroms
    margin = 10.0
    print("Move coordinates to centre of P1 cubic box")
    spacegroup = "P1"
    structure.spacegroup_hm = spacegroup
    box = structure.calculate_box(margin=margin)

    dims = (
        box.maximum[0] - box.minimum[0],
        box.maximum[1] - box.minimum[1],
        box.maximum[2] - box.minimum[2],
    )
    box_origin = None  # float origin
    box_nstart = None  # nstart
    # the centre of the model needs to be aligned with the centre of the box
    # TODO: expect geometric centre to work better than com, need to check
    # gc = box.minimum + (box.maximum - box.minimum) / 2.0
    com = structure[0].calculate_center_of_mass()

    # Calculate Density w/ Electron form factors
    # See Density for FFT:
    #   https://gemmi.readthedocs.io/en/latest/hkl.html
    dencalc = gemmi.DensityCalculatorE()
    # Set resolution in Angstrom
    dencalc.d_min = resolution
    # Typical sampling rate
    dencalc.rate = 1.5

    # If reference map provided set dimensions from map
    if map_ref is not None:
        with mrcfile.open(
            map_ref, mode="r", permissive=False, header_only=False
        ) as mrc:
            print("Reference map: {}".format(map_ref))
            if (
                mrc.header.cella.x < dims[0] - 20.0
                or mrc.header.cella.y < dims[1] - 20.0
                or mrc.header.cella.z < dims[2] - 20.0
            ):
                raise ValueError(
                    "One or more of the reference map dimensions are smaller than the "
                    "model extent along each axis"
                )

            print(
                "Set unit cell by reference map to: "
                "{:.3f} {:.3f} {:.3f} {} {} {} {}".format(
                    mrc.header.cella.x,
                    mrc.header.cella.y,
                    mrc.header.cella.z,
                    90,
                    90,
                    90,
                    spacegroup,
                )
            )

            if ref_map_aligned:
                # move coordinates to the centre of the ref map box (with origin 0)
                ref_map_origin = (
                    mrc.header.origin.x.item(),
                    mrc.header.origin.y.item(),
                    mrc.header.origin.z.item(),
                )
                trans = (-ref_map_origin[0], -ref_map_origin[1], -ref_map_origin[2])
                translate_model(structure, trans)
                # copy origin and nstart records
                box_origin = ref_map_origin
                box_nstart = (
                    mrc.header.nxstart.item(),
                    mrc.header.nystart.item(),
                    mrc.header.nzstart.item(),
                )
            else:
                # move coordinates to the centre of the ref map box (with origin 0)
                offset = (
                    mrc.header.cella.x / 2.0,
                    mrc.header.cella.y / 2.0,
                    mrc.header.cella.z / 2.0,
                )
                trans = (offset[0] - com[0], offset[1] - com[1], offset[2] - com[2])
                translate_model(structure, trans)
                box_origin = (-trans[0], -trans[1], -trans[2])
            # set unitcell and calculate map
            structure.cell = gemmi.UnitCell(
                mrc.header.cella.x, mrc.header.cella.y, mrc.header.cella.z, 90, 90, 90
            )
            dencalc.set_grid_cell_and_spacegroup(structure)
            dencalc.put_model_density_on_grid(structure[0])
            # set grid size / voxel spacing
            dencalc.grid.set_size(
                mrc.header.nx.item(), mrc.header.ny.item(), mrc.header.nz.item()
            )
            if not ref_map_aligned:
                # set nstart based on origin
                box_nstart = (
                    int(round(box_origin[0] / dencalc.grid.spacing[0])),
                    int(round(box_origin[1] / dencalc.grid.spacing[1])),
                    int(round(box_origin[2] / dencalc.grid.spacing[2])),
                )

    elif cell_dims.count(None) == 0:
        if (
            cell_dims[0] < dims[0] - 20.0
            or cell_dims[1] < dims[1] - 20.0
            or cell_dims[2] < dims[2] - 20.0
        ):
            raise ValueError(
                "One or more of the reference map dimensions are smaller than the "
                "model extent along each axis"
            )

        print(
            "Set unit cell specified by x,y,z to: "
            "{:.3f} {:.3f} {:.3f} {} {} {} {}".format(
                cell_dims[0], cell_dims[1], cell_dims[2], 90, 90, 90, spacegroup
            )
        )
        # move coordinates to the centre of the box (with origin 0)
        offset = (cell_dims[0] / 2.0, cell_dims[1] / 2.0, cell_dims[2] / 2.0)
        trans = (offset[0] - com[0], offset[1] - com[1], offset[2] - com[2])
        translate_model(structure, trans)
        box_origin = (-trans[0], -trans[1], -trans[2])
        # set unitcell and calculate map
        structure.cell = gemmi.UnitCell(
            cell_dims[0], cell_dims[1], cell_dims[2], 90, 90, 90
        )
        dencalc.set_grid_cell_and_spacegroup(structure)
        dencalc.put_model_density_on_grid(structure[0])
        # set nstart based on origin
        box_nstart = (
            int(round(box_origin[0] / dencalc.grid.spacing[0])),
            int(round(box_origin[1] / dencalc.grid.spacing[1])),
            int(round(box_origin[2] / dencalc.grid.spacing[2])),
        )

    else:
        # Move coordinates to centre of the P1 box (with origin 0)
        offset = (dims[0] / 2.0, dims[1] / 2.0, dims[2] / 2.0)
        trans = (offset[0] - com[0], offset[1] - com[1], offset[2] - com[2])
        translate_model(structure, trans)
        box_origin = (-trans[0], -trans[1], -trans[2])
        # Better to create new cell rather than set_cell
        # In some rare cases (e.g. 5me2) scalen is set this needs to overwritten
        structure.cell = gemmi.UnitCell(dims[0], dims[1], dims[2], 90, 90, 90)
        print(
            "Set unit cell with a margin of {} to: "
            "{} {:.3f} {:.3f} {:.3f} {} {} {}".format(
                margin, dims[0], dims[1], dims[2], 90, 90, 90, spacegroup
            )
        )
        dencalc.set_grid_cell_and_spacegroup(structure)
        dencalc.put_model_density_on_grid(structure[0])
        # set nstart based on origin
        box_nstart = (
            int(round(box_origin[0] / dencalc.grid.spacing[0])),
            int(round(box_origin[1] / dencalc.grid.spacing[1])),
            int(round(box_origin[2] / dencalc.grid.spacing[2])),
        )

    # Save modified PDB.
    # structure.write_pdb(pdb_out)
    # print("Write modified PDB: {}".format(pdb_out))

    denmap = gemmi.Ccp4Map()
    denmap.grid = dencalc.grid
    # Set to map to mode 2 (32-bit signed real) for details see:
    #   https://www.ccpem.ac.uk/mrc_format/mrc2014.php
    denmap.update_ccp4_header(2, True)

    if box_origin:
        # set map ORIGIN
        denmap.set_header_float(50, box_origin[0])
        denmap.set_header_float(51, box_origin[1])
        denmap.set_header_float(52, box_origin[2])
    if box_nstart:
        # set map NSTART
        denmap.set_header_i32(5, box_nstart[0])
        denmap.set_header_i32(6, box_nstart[1])
        denmap.set_header_i32(7, box_nstart[2])
    denmap.write_ccp4_map(map_out)
    print("Write map: {}".format(map_out))


def main():
    """
    Load coordinate file in PDB or mmCIF format and generates density map
    with electron form factors at given resolution
    """
    print("-" * 80)
    print("\nGemmi model to map\n")
    print("-" * 80)

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pdb", type=str, required=True)
    parser.add_argument("-m", "--map_ref", type=str, required=False)
    parser.add_argument(
        "-ref_notaligned",
        "--ref_notaligned",
        required=False,
        default=False,
        action="store_true",
    )
    parser.add_argument("-r", "--resolution", type=float, default=3.0)
    parser.add_argument("-x", "--dim_x", type=float, required=False)
    parser.add_argument("-y", "--dim_y", type=float, required=False)
    parser.add_argument("-z", "--dim_z", type=float, required=False)

    args = parser.parse_args()

    # Set output paths
    pdb_out = "gemmi_" + Path(args.pdb).stem + ".pdb"
    map_out = "gemmi_" + Path(args.pdb).stem + ".mrc"
    cell_dims = [
        args.dim_x,
        args.dim_y,
        args.dim_z,
    ]
    if cell_dims.count(None) in [1, 2]:
        raise RuntimeError("Error: x, y and z dimensions must be given")

    print("Map ref:", args.map_ref)

    calculate_map(
        pdb_in=args.pdb,
        pdb_out=pdb_out,
        map_ref=args.map_ref,
        cell_dims=cell_dims,
        map_out=map_out,
        resolution=args.resolution,
        ref_map_aligned=not args.ref_notaligned,
    )


if __name__ == "__main__":
    main()
