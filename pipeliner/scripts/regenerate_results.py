#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
from glob import glob
import sys
from pipeliner.utils import make_pretty_header
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.project_graph import ProjectGraph


def main():
    print(make_pretty_header("CCPEM-pipeliner regenerate results files"))
    proj = PipelinerProject()

    with ProjectGraph(name=proj.pipeline_name) as pipeline:
        if len(sys.argv) == 1:
            jobs = [x.name for x in pipeline.process_list]
        else:
            jobs = [proj.parse_procname(x) for x in sys.argv[1:]]

    results_files = []
    for j in jobs:
        rf = glob(j + ".results*.json")
        results_files += rf

    print(f"{len(results_files)} old results files from {len(jobs)} jobs found")
    count = 0
    for job in jobs:
        print(
            make_pretty_header(
                f"Regenerating results files for {job}:", bottom=False, char="-"
            )
        )
        old_results_files = glob(job + ".results*.json")
        for of in old_results_files:
            os.remove(of)
            print(f"Removed old file: {of}")
        thumbs_dir = os.path.join(job, "Thumbnails")
        if os.path.isdir(thumbs_dir):
            print(f"Removing {thumbs_dir}")
            shutil.rmtree(thumbs_dir)
        with ProjectGraph(name=proj.pipeline_name) as pipeline:
            proc = pipeline.find_process(job)
            new_results = pipeline.get_process_results_display(
                proc=proc, forceupdate=True
            )
        count += len(new_results)
        for new_rf in new_results:
            print(f"Created new {new_rf.dobj_type}")

    print(make_pretty_header(f"Created {count} new results files"))


if __name__ == "__main__":
    main()
