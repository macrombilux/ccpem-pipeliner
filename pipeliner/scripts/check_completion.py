#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
"""Used to check completion of pipeliner jobs by looking for the expected outputs

This is normally run as the last command in a series of commands run by the JobRunner
It has been set up to run on remote systems independently of the pipeliner, requiring
minimal imports
"""

import os
import sys

from typing import List, Optional

# Copied from pipeliner.data_structure to avoid dependency on pipeliner import
RELION_SUCCESS_FILE = "RELION_JOB_EXIT_SUCCESS"
RELION_FAIL_FILE = "RELION_JOB_EXIT_FAILURE"
RELION_ABORT_FILE = "RELION_JOB_EXIT_ABORTED"

SUCCESS_FILE = "PIPELINER_JOB_EXIT_SUCCESS"
FAIL_FILE = "PIPELINER_JOB_EXIT_FAILURE"
ABORT_FILE = "PIPELINER_JOB_EXIT_ABORTED"

# Copied from pipeliner.utils to avoid dependency on pipeliner import


def touch(filename: str) -> None:
    """Create an empty file

    This is an identical function to :meth:`pipeliner.utils.touch`.  If is reproduced
    here to that check completion can run independently of the pipeliner.

    Args:
        filename (str): The name for the file to create
    """
    open(filename, "w").close()


def main(in_args: Optional[List[str]] = None):
    """Assesses the completion of a job that has been run.

    The check_completion script looks for the specified output files, if all of them are
    present it marks the job as completed successfully, otherwise it marks the job
    failed.

    If RELION has already marked a job successful, failed, or aborted the script defers
    to
    how RELION has marked the job.

    Args:
        in_args (list): The input arguments, generally received from STDIN
    """
    print("-" * 24)
    print("Checking job completion")
    print("-" * 24)
    if in_args is None:
        if len(sys.argv) > 1:
            in_args = sys.argv[1:]
        else:
            print(
                "ERROR: Script needs input arguments: check_completion"
                " <input_dir> <expected_file> ... <expected_file>"
            )
            return

    out_dir = in_args[0]
    out_files = in_args[1:]

    # check for previous status files

    r_success = os.path.isfile(os.path.join(out_dir, RELION_SUCCESS_FILE))
    r_fail = os.path.isfile(os.path.join(out_dir, RELION_FAIL_FILE))
    r_abort = os.path.isfile(os.path.join(out_dir, RELION_ABORT_FILE))

    if r_success:
        print("RELION reported successful completion of the job")
        touch(os.path.join(out_dir, SUCCESS_FILE))
        return

    elif r_fail:
        print("RELION reported job failure")
        touch(os.path.join(out_dir, FAIL_FILE))
        return

    elif r_abort:
        print("RELION reported job aborted")
        touch(os.path.join(out_dir, ABORT_FILE))
        return

    # look for expected files
    exp_outs = [os.path.join(out_dir, x) for x in out_files]
    f_count = 0
    n_exp_out = len(exp_outs)
    print("Looking for {} expected outputs:".format(n_exp_out))
    for f in exp_outs:
        if os.path.isfile(f):
            f_count += 1
            print("{}... FOUND".format(f))
        else:
            print("{}... MISSING".format(f))

    # check all are present
    if f_count == len(exp_outs):
        touch(os.path.join(out_dir, SUCCESS_FILE))
        print("All outputs found, job completed successfully")
    else:
        touch(os.path.join(out_dir, FAIL_FILE))
        print(
            "{} expected output(s) missing. Job marked failed".format(
                n_exp_out - f_count
            )
        )


if __name__ == "__main__":
    main()
