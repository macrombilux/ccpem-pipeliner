{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "PARAMS: relion.class3d.helical.multiref",
    "description": "Heterogeneous classification of overlapping helical segments in 3D, using multiple references",
    "type": "object",
    "properties": {
        "fn_cont": {
            "description": "Continue from here:",
            "type": "string"
        },
        "fn_img": {
            "description": "Input images STAR file:",
            "type": "string"
        },
        "fn_ref": {
            "description": "Reference maps:",
            "type": "string"
        },
        "ref_correct_greyscale": {
            "description": "Ref. map is on absolute greyscale?",
            "type": "boolean"
        },
        "fn_mask": {
            "description": "Reference mask (optional):",
            "type": "string"
        },
        "ini_high": {
            "description": "Initial low-pass filter (A):",
            "type": "number"
        },
        "sym_name": {
            "description": "Symmetry:",
            "type": "string"
        },
        "do_ctf_correction": {
            "description": "Do CTF-correction?",
            "type": "boolean"
        },
        "ctf_intact_first_peak": {
            "description": "Ignore CTFs until first peak?",
            "type": "boolean"
        },
        "tau_fudge": {
            "description": "Regularisation parameter T:",
            "type": "number"
        },
        "nr_iter": {
            "description": "Number of iterations:",
            "type": "integer"
        },
        "particle_diameter": {
            "description": "Mask diameter (A):",
            "type": "number"
        },
        "do_zero_mask": {
            "description": "Mask individual particles with zeros?",
            "type": "boolean"
        },
        "highres_limit": {
            "description": "Limit resolution E-step to (A):",
            "type": "number"
        },
        "dont_skip_align": {
            "description": "Perform image alignment?",
            "type": "boolean"
        },
        "sampling": {
            "description": "Angular sampling interval:",
            "type": "string"
        },
        "offset_range": {
            "description": "Offset search range (pix):",
            "type": "number"
        },
        "offset_step": {
            "description": "Offset search step (pix):",
            "type": "number"
        },
        "do_local_ang_searches": {
            "description": "Perform local angular searches?",
            "type": "boolean"
        },
        "sigma_angles": {
            "description": "Local angular search range:",
            "type": "number"
        },
        "allow_coarser": {
            "description": "Allow coarser sampling?",
            "type": "boolean"
        },
        "helical_tube_inner_diameter": {
            "description": "Tube diameter - inner (A):",
            "type": "number"
        },
        "helical_tube_outer_diameter": {
            "description": "Tube diameter - outer (A):",
            "type": "number"
        },
        "range_rot": {
            "description": "Angular search range - rot (deg):",
            "type": "number"
        },
        "range_tilt": {
            "description": "Angular search range - tilt (deg):",
            "type": "number"
        },
        "range_psi": {
            "description": "Angular search range - psi (deg):",
            "type": "number"
        },
        "do_apply_helical_symmetry": {
            "description": "Apply helical symmetry?",
            "type": "boolean"
        },
        "helical_nr_asu": {
            "description": "Number of unique asymmetrical units:",
            "type": "integer"
        },
        "helical_twist_initial": {
            "description": "Initial helical twist (deg):",
            "type": "number"
        },
        "helical_rise_initial": {
            "description": "Initial helical rise (A):",
            "type": "number"
        },
        "helical_z_percentage": {
            "description": "Central Z length (%):",
            "type": "number"
        },
        "do_local_search_helical_symmetry": {
            "description": "Do local searches of symmetry?",
            "type": "boolean"
        },
        "helical_twist_min": {
            "description": "Helical twist search (deg) - Min:",
            "type": "number"
        },
        "helical_twist_max": {
            "description": "Helical twist search (deg) - Max:",
            "type": "number"
        },
        "helical_twist_inistep": {
            "description": "Helical twist search (deg) - Step:",
            "type": "number"
        },
        "helical_rise_min": {
            "description": "Helical rise search (A) - Min:",
            "type": "number"
        },
        "helical_rise_max": {
            "description": "Helical rise search (A) - Max:",
            "type": "number"
        },
        "helical_rise_inistep": {
            "description": "Helical rise search (A) - Step:",
            "type": "number"
        },
        "helical_range_distance": {
            "description": "Range factor of local averaging:",
            "type": "number"
        },
        "keep_tilt_prior_fixed": {
            "description": "Keep tilt-prior fixed:",
            "type": "boolean"
        },
        "do_parallel_discio": {
            "description": "Use parallel disc I/O?",
            "type": "boolean"
        },
        "nr_pool": {
            "description": "Number of pooled particles:",
            "type": "integer"
        },
        "do_pad1": {
            "description": "Skip padding?",
            "type": "boolean"
        },
        "skip_gridding": {
            "description": "Skip gridding?",
            "type": "boolean"
        },
        "do_preread_images": {
            "description": "Pre-read all particles into RAM?",
            "type": "boolean"
        },
        "scratch_dir": {
            "description": "Copy particles to scratch directory:",
            "type": "string"
        },
        "do_combine_thru_disc": {
            "description": "Combine iterations through disc?",
            "type": "boolean"
        },
        "use_gpu": {
            "description": "Use GPU acceleration?",
            "type": "boolean"
        },
        "gpu_ids": {
            "description": "Which GPUs to use:",
            "type": "string"
        },
        "nr_threads": {
            "description": "Number of threads:",
            "type": "integer"
        },
        "do_queue": {
            "description": "Submit to queue?",
            "type": "boolean"
        },
        "queuename": {
            "description": "Queue name:",
            "type": "string"
        },
        "qsub": {
            "description": "Queue submit command:",
            "type": "string"
        },
        "qsubscript": {
            "description": "Standard submission script:",
            "type": "string"
        },
        "min_dedicated": {
            "description": "Minimum dedicated cores per node:",
            "type": "integer"
        },
        "other_args": {
            "description": "Additional arguments:",
            "type": "string"
        },
        "nr_mpi": {
            "description": "Number of MPI procs:",
            "type": "integer"
        },
        "mpi_command": {
            "description": "MPI run command:",
            "type": "string"
        }
    }
}