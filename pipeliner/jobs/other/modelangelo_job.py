#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List
from glob import glob

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FileNameJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import create_results_display_object
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_SEQUENCE,
    NODE_ATOMCOORDS,
    NODE_SEQUENCEALIGNMENT,
)


class ModelAngelo(PipelinerJob):
    PROCESS_NAME = "modelangelo.atomic_model_build"
    OUT_DIR = "ModelAngelo"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "ModelAngelo"
        self.jobinfo.short_desc = "Automated atomic model building with ModelAngelo"
        self.jobinfo.long_desc = (
            "Generates an atomic model with or without additional sequence input"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("model_angelo")]
        self.jobinfo.references = [
            Ref(
                authors=["Jamali K", "Kimanius D", "Scheres S"],
                title="ModelAngelo: Automated Model Building in Cryo-EM Maps",
                journal="arXiv",
                year="2022",
                volume="",
                pages="",
                doi="https://doi.org/10.48550/arXiv.2210.00006",
            )
        ]
        self.jobinfo.documentation = "https://arxiv.org/abs/2210.00006"

        self.joboptions["fn_map"] = InputNodeJobOption(
            label="Input EM map",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("MRC map", [".mrc"]),
            help_text="An EM map to build the model into",
            is_required=True,
        )

        self.joboptions["fn_sequence"] = FileNameJobOption(
            label="Input FASTA sequence",
            node_type=NODE_SEQUENCE,
            pattern=files_exts("FASTA sequence file", [".fasta", ".txt"]),
            help_text="Protein sequence in FASTA format",
        )

        self.get_runtab_options()

    def get_commands(self) -> List[List[str]]:
        command = ["model_angelo"]
        fn_seq = self.joboptions["fn_sequence"].get_string()
        fn_in = self.joboptions["fn_map"].get_string()
        if fn_seq:
            command.extend(["build", "-v", fn_in, "-f", fn_seq])
        else:
            command.extend(["build_no_seq", "-v", fn_in])
        oname = os.path.basename(fn_in).split(".")[0] + "_model"
        command.extend(["-o", oname])
        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, oname, oname + ".cif"),
                NODE_ATOMCOORDS,
                ["modelangelo", "simulated"],
            )
        )
        return [command]

    def create_results_display(self) -> list:
        fn_map = self.joboptions["fn_map"].get_string()
        fn_mod = os.path.join(
            self.output_dir,
            os.path.basename(fn_map).split(".")[0] + "_model",
            os.path.basename(fn_map).split(".")[0] + "_model.cif",
        )

        return [
            create_results_display_object(
                "mapmodel",
                title="Generated atomic model",
                associated_data=[fn_mod],
                maps=[fn_map],
                models=[fn_mod],
                start_collapsed=False,
            )
        ]

    def post_run_actions(self):
        fn_map = self.joboptions["fn_map"].get_string()
        hmm_dir = os.path.join(
            self.output_dir,
            os.path.basename(fn_map).split(".")[0] + "_model",
            "hmm_profiles",
        )
        hmm_files = glob(f"{hmm_dir}/*.hmm")
        if hmm_files:
            for hmmf in hmm_files:
                self.output_nodes.append(
                    create_node(hmmf, NODE_SEQUENCEALIGNMENT, ["model_angelo"])
                )
