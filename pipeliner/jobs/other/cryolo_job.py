#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from gemmi import cif
from glob import glob

from pipeliner.utils import (
    truncate_number,
    decompose_pipeline_filename,
    get_job_script,
)
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FileNameJobOption,
    StringJobOption,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.star_writer import write
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MLMODEL,
    NODE_MICROGRAPHCOORDSGROUP,
)
from pipeliner.starfile_handler import DataStarFile


class CrYOLOAutopick(PipelinerJob):
    PROCESS_NAME = "cryolo.autopick"
    OUT_DIR = "AutoPick"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "crYOLO"
        self.jobinfo.short_desc = "Automated particle picking"
        self.jobinfo.long_desc = (
            "Fast and accurate cryo-EM particle picking based on convolutional neural"
            " networks and utilizes the popular You Only Look Once (YOLO) object"
            " detection system."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        yologui = ExternalProgram("cryolo_gui.py")
        yolopred = ExternalProgram("cryolo_predict.py")
        self.jobinfo.programs = [yologui, yolopred]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Wagner T",
                    "Merino F",
                    "Stabrin M",
                    "Moriya T",
                    "Antoni C",
                    "Apelbaum A",
                    "Hagel P",
                    "Sitsel O",
                    "Raisch T",
                    "Prumbaum D",
                    "Quentin D",
                    "Roderer D",
                    "Tacke S",
                    "Siebolds B",
                    "Schubert E",
                    "Shaikh TR",
                    "Lill P",
                    "Gatsogiannis C",
                    "Raunser S",
                ],
                title=(
                    "SPHIRE-crYOLO is a fast and accurate fully automated "
                    "particle picker for cryo-EM."
                ),
                journal="Commun. Biol.",
                year="2019",
                volume="2",
                pages="218",
                doi="10.1038/s42003-019-0437-z",
            )
        ]
        self.jobinfo.documentation = "https://cryolo.readthedocs.io/en/stable/"

        self.always_continue_in_schedule = True

        self.joboptions["input_file"] = InputNodeJobOption(
            label="Input micrographs file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern="Input Micrographs (*.star)",
            help_text=(
                "Input STAR file (preferably with CTF information) with all micrographs"
                " to pick from."
            ),
            is_required=True,
        )

        self.joboptions["model_path"] = FileNameJobOption(
            label="CrYOLO model:",
            node_type=NODE_MLMODEL,
            node_kwds=["cryolo"],
            default_value="",
            directory="",
            pattern="Cryolo model (*.h5)",
            help_text="Download a crYOLO model or train your own",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["box_size"] = IntJobOption(
            label="Box size:",
            default_value=128,
            suggested_min=50,
            suggested_max=1000,
            step_value=1,
            help_text="Box size in pixels",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["gpus"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text=(
                "GPUs should be entered comma separated, unlike relion's notation"
            ),
        )

        self.joboptions["confidence_threshold"] = FloatJobOption(
            label="Confidence threshold",
            default_value=0.3,
            suggested_min=0.01,
            suggested_max=1,
            step_value=0.01,
            help_text="CrYOLO confidence threshold",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["use_JANNI_denoise"] = BooleanJobOption(
            label="Use JANNI denoising?",
            default_value=False,
            help_text="Use JANNI to denoise micrographs.  A model is required",
            in_continue=True,
        )

        self.joboptions["JANNI_model_path"] = InputNodeJobOption(
            label="Path to JANNI noise model:",
            node_type=NODE_MLMODEL,
            node_kwds=["janni"],
            default_value="",
            directory="",
            pattern="JANNI model (*.h5)",
            help_text="Download the model or train your own",
            in_continue=True,
            deactivate_if=[("use_JANNI_denoise", "=", False)],
            required_if=[("use_JANNI_denoise", "=", True)],
        )

        self.get_runtab_options()

    def get_commands(self):
        self.working_dir = self.output_dir
        input_fn = self.joboptions["input_file"].get_string(
            True, "No input file specified"
        )
        if not os.path.isfile(input_fn):
            raise ValueError(f"Input file {input_fn} not found")

        cryolo_model = self.joboptions["model_path"].get_string(
            True, "No crYOLO model specified"
        )
        if not os.path.isfile(cryolo_model):
            raise ValueError(f"crYOLO model file {cryolo_model} not found")

        # add links dir to script here

        links_com = [
            "python3",
            get_job_script("make_cryolo_links_dir.py"),
            "--input_mics_file",
            os.path.relpath(input_fn, self.working_dir),
        ]
        if self.is_continue:
            links_com.append("--is_continue")

        # make the cryolo config file
        box_size = self.joboptions["box_size"].get_number()
        config_command = ["cryolo_gui.py"]
        config_command += ["config", "cryolo_config.json", truncate_number(box_size, 0)]

        # pick the filtering option
        if not self.joboptions["use_JANNI_denoise"].get_boolean():
            config_command += ["--filter", "LOWPASS", "--low_pass_cutoff", "0.1"]
        else:
            config_command += ["--filter", "JANNI", "--janni_model"]
            jannimodel = self.joboptions["JANNI_model_path"].get_string(
                True, "ERROR: No JANNI model specified"
            )
            config_command.append(jannimodel)
        commands = [links_com, config_command]

        # make the crYOLO command
        cryolo_com = ["cryolo_predict.py", "-c", "cryolo_config.json"]

        cryolo_model = self.joboptions["model_path"].get_string(
            True, "ERROR: No crYOLO model specified"
        )
        cryolo_com += ["-w", cryolo_model]

        cryolo_com += ["-i", "Micrographs/"]

        # check the GPUs entry
        gpus = (
            self.joboptions["gpus"]
            .get_string(True, "ERROR: No GPUs specified")
            .replace(" ", "")
        )
        allowed = "0123456789,"
        if not all(x in allowed for x in gpus):
            raise ValueError("ERROR: GPUs must be specified comma separated")

        cryolo_com += [
            "-g",
            gpus,
            "-o",
            ".",
            "-t",
            str(self.joboptions["confidence_threshold"].get_number()),
        ]

        commands.append(cryolo_com)
        return commands

    def post_run_actions(self):
        """After a crYOLO job has been run assemble the autopick.star file for relion
        this file contains name and coordinate file for each micrograph
        """
        infile = self.joboptions["input_file"].get_string()
        in_star = cif.read_file(infile)
        mics_block = in_star.find_block("micrographs")
        all_mics = mics_block.find_loop("_rlnMicrographName")

        # make sure that particle files were written
        missing_files = []
        for f in all_mics:
            fn = os.path.basename(f).replace(".mrc", ".star")
            starfile = os.path.join(self.output_dir, "STAR", fn)
            if not os.path.isfile(starfile):
                missing_files.append(f)
        if len(missing_files) > 0 and len(missing_files) != len(all_mics):
            print(
                "WARNING: No particle coordinate files were produced for the following"
                " files:"
            )
            for f in missing_files:
                print(f" - {f}")

        elif len(missing_files) == len(all_mics):
            raise ValueError("No particle coordinate files were written")

        output_file = "autopick.star"
        self.output_nodes.append(
            create_node(output_file, NODE_MICROGRAPHCOORDSGROUP, ["cryolo", "autopick"])
        )

        d = cif.Document()
        block = d.add_new_block("coordinate_files")
        loop = block.init_loop("", ["_rlnMicrographName", "_rlnMicrographCoordinates"])
        for mic in all_mics:
            coord_fn = os.path.basename(mic).split(".")[0] + ".star"
            coord_file = os.path.join(os.path.join(self.output_dir, "STAR/", coord_fn))
            loop.add_row([mic, coord_file])
        write(d, output_file)

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join("run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "particles in total" in line:
                metadata_dict["TotalParticles"] = int(line.split()[0])
            if "fully immersed" in line:
                metadata_dict["NotFullyImmersed"] = int(line.split()[1])
            if "specified size range" in line:
                metadata_dict["OutOfSizeRange"] = int(line.split()[1])
            if "Particle diameter distribution" in line:
                metadata_dict["SizeDistribution"] = {}
                metadata_dict["SizeDistribution"]["MEAN"] = int(
                    outlines[i + 1].split()[1]
                )
                metadata_dict["SizeDistribution"]["SD"] = int(
                    outlines[i + 2].split()[1]
                )
                metadata_dict["SizeDistribution"]["Q25"] = int(
                    outlines[i + 3].split()[1]
                )
                metadata_dict["SizeDistribution"]["Q50"] = int(
                    outlines[i + 3].split()[1]
                )
                metadata_dict["SizeDistribution"]["Q75"] = int(
                    outlines[i + 4].split()[1]
                )

        summaryfile = os.path.join("DISTR/confidence_distribution_summary_*.txt")
        with open(glob(summaryfile)[0], "r") as sf:
            summarylines = sf.readlines()

        metadata_dict["ConfidenceDisribution"] = {}

        for line in summarylines[1:]:
            metadata_dict["ConfidenceDisribution"][line.split(",")[0]] = float(
                line.split(",")[1]
            )

            return metadata_dict

    def create_results_display(self):
        """count the individual particle starfiles rather than the summary
        star file for on-the-fly updating"""
        # make the histogram
        # get the star files
        instar = DataStarFile(self.joboptions["input_file"].get_string())
        mdat = instar.get_block("micrographs").find(["_rlnMicrographName"])[0][0]
        logsdir = os.path.join(
            self.output_dir, os.path.dirname(decompose_pipeline_filename(mdat)[2])
        )
        partsfiles = glob(logsdir + "/*.star")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(DataStarFile(pf).count_block())
        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_dir, "autopick.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_dir, "autopick.star")
        fb = DataStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [image, graph]
