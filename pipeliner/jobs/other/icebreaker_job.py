#!/usr/bin/env python

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import csv

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import InputNodeJobOption
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.nodes import NODE_MICROGRAPHGROUPMETADATA, NODE_PARTICLESDATA


IB_REFS = [
    Ref(
        authors=[
            "Olek, M",
            "Cowtan, K",
            "Webb, D",
            "Chaban, Y",
            "Zhang, P",
        ],
        title=(
            "Icebreaker: Software for High Resolution Single Particle CryoEM With"
            " Nonuniform Ice"
        ),
        journal="Structure",
        year="2021",
        status="In press",
    )
]

IB_DOCS = "https://github.com/DiamondLightSource/python-icebreaker"


class IceBreakerGroupMics(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.micrographs"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Icebreaker group micrographs"
        self.jobinfo.short_desc = "Assign ice thickness values to micrographs"
        self.jobinfo.long_desc = (
            "Icebreaker estimates the relative ice gradient for each micrograph and"
            " assigns them to groups based on this parameter"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = [ExternalProgram("ib_job")]
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern="*.star",
            help_text="Star file containing micrographs",
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_job", "--j", threads, "--mode", "group", "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)
        coms += ["--o", self.output_dir]

        outfile = os.path.join(self.output_dir, "grouped_micrographs.star")
        self.output_nodes.append(
            create_node(
                outfile,
                NODE_MICROGRAPHGROUPMETADATA,
                ["relion", "icebreaker", "grouped"],
            )
        )

        return [coms]


class IceBreakerGroupParts(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.particles"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Icebreaker group particles"
        self.jobinfo.short_desc = "Assign ice thickness values to particles"
        self.jobinfo.long_desc = (
            "Icebreaker estimates the relative ice gradient for each micrograph and"
            " assigns values to particles based on the micrograph they were extracted"
            " from"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = [ExternalProgram("ib_group")]
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion", "icebreaker", "grouped"],
            default_value="",
            directory="",
            pattern="STAR file (*.star)",
            help_text=(
                "Star file containing micrographs that have been grouped using"
                " Icebreaker"
            ),
            is_required=True,
        )

        self.joboptions["in_parts"] = InputNodeJobOption(
            label="Input particles star file:",
            node_type=NODE_PARTICLESDATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern="STAR file (*.star)",
            help_text="Star file containing particles",
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_group", "--j", threads, "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)

        in_parts = self.joboptions["in_parts"].get_string(
            True, "Particles input file not found"
        )
        coms += ["--in_parts", in_parts]

        coms += ["--o", self.output_dir]

        outfile = os.path.join(self.output_dir, "ib_icegroups.star")
        self.output_nodes.append(
            create_node(outfile, NODE_PARTICLESDATA, ["relion", "icebreaker"])
        )

        return [coms]


class IceBreakerFlatten(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.enhancecontrast"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Icebreaker contrast enhancement"
        self.jobinfo.short_desc = "Flatten ice gradients to enhance micrograph contrast"
        self.jobinfo.long_desc = (
            "Icebreaker estimates the relative ice gradient for each micrograph  and"
            " flattens it based on the K-Means clustering algorithm, thus equalizing"
            " the local contrast"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = [ExternalProgram("ib_job")]
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion", "icebreaker", "grouped"],
            default_value="",
            directory="",
            pattern="STAR file (*.star)",
            help_text=(
                "Star file containing micrographs that have been grouped "
                "using Icebreaker"
            ),
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_job", "--j", threads, "--mode", "flatten", "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)
        coms += ["--o", self.output_dir]

        outfile = os.path.join(self.output_dir, "flattened_micrographs.star")
        self.output_nodes.append(
            create_node(
                outfile,
                NODE_MICROGRAPHGROUPMETADATA,
                ["relion", "icebreaker", "flattened"],
            )
        )

        return [coms]


class IceBreakerFiveFigureSummary(PipelinerJob):
    PROCESS_NAME = "icebreaker.micrograph_analysis.summary"
    OUT_DIR = "IceBreaker"

    def __init__(self):
        super().__init__()
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza, Dan Hatton"
        self.jobinfo.programs = [ExternalProgram("ib_5fig")]
        self.jobinfo.display_name = "Icebreaker 5-figure summary"
        self.jobinfo.references = IB_REFS
        self.jobinfo.documentation = IB_DOCS
        self.jobinfo.short_desc = (
            "Calculate five figure summary of the"
            " distribution of relative ice thickness"
        )
        self.jobinfo.long_desc = (
            "Ice breaker estimates the relative ice gradient for each micrograph "
            " and produces a five figure summary of the resulting distribution "
        )

        self.joboptions["in_mics"] = InputNodeJobOption(
            label="Input micrographs star file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion", "icebreaker", "grouped"],
            default_value="",
            directory=".",
            pattern="STAR file (*.star)",
            help_text=(
                "Star file containing micrographs that have been grouped "
                "using Icebreaker"
            ),
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=True)

    def get_commands(self):
        threads = int(self.joboptions["nr_threads"].get_number())
        coms = ["ib_5fig", "--j", threads, "--in_mics"]
        in_mics = self.joboptions["in_mics"].get_string(
            True, "Micrographs input file not found"
        )
        coms.append(in_mics)
        coms += ["--o", self.output_dir]

        outfile = os.path.join(self.output_dir, "five_figs_test.csv")
        self.output_nodes.append(
            create_node(outfile, NODE_MICROGRAPHGROUPMETADATA, ["icebreaker"])
        )

        return [coms]

    def gather_metadata(self):

        metadata_dict = {}

        csvfile = open(os.path.join("five_figs_test.csv"))
        csvread = csv.reader(csvfile)

        rows = []
        for row in csvread:
            rows.append(row)

        for mic in rows[1:]:
            metadata_dict["Path"] = mic[0]
            metadata_dict["Min"] = int(mic[1])
            metadata_dict["Q1"] = int(mic[2])
            metadata_dict["Median"] = int(mic[3])
            metadata_dict["Q3"] = int(mic[4])
            metadata_dict["Max"] = int(mic[5])

            return metadata_dict
