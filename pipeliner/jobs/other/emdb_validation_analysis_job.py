#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os

from PIL import Image

from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_many_files,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
    StringJobOption,
    JobOptionValidationResult,
)
from pipeliner.node_factory import create_node
from pipeliner.nodes import NODE_DENSITYMAP, NODE_PROCESSDATA, NODE_ATOMCOORDS
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, Ref
from pipeliner.utils import get_job_script
from typing import List


class MapModelValidate(PipelinerJob):
    PROCESS_NAME = "pipeliner.validation.emdb_validation.evaluate"
    OUT_DIR = "EMDB_validation"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "EMDB Validation Analysis"
        self.jobinfo.short_desc = "EMDB map and model validation"
        self.jobinfo.long_desc = (
            "EMDB map and atomic model validation and visual analysis. "
            "Reports multiple features, metrics and statistics.\n"
            "To install the EMDB validation analysis tool, use: \n"
            "pip install va \n"
            "Source and builds are available here:\n"
            "https://pypi.org/project/va/#files"
            "\n\n To generate map surface views: "
            "install ChimeraX and add the executable to the system PATH"
        )
        self.jobinfo.programs = [
            ExternalProgram("va"),
        ]
        self.version = "0.0.1.dev43"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Wang Z et al."],
                title=("Validation analysis of EMDB entries"),
                journal="Acta Cryst D",
                year="2022",
                volume="78",
                issue="5",
                pages="542-552",
                doi="10.1107/S205979832200328X",
            ),
        ]
        self.jobinfo.documentation = "https://www.ebi.ac.uk/emdb/va/"

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to be evaluated",
            is_required=True,
        )
        self.joboptions["exp_method"] = StringJobOption(
            label="Reconstruction method",
            default_value="singleparticle",
            help_text=(
                "Choose from tomography,twodcrystal,crystallography,singleparticle,"
                "subtomogramaveraging,helical"
            ),
            is_required=False,
        )
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".mmcif"]),
            default_value="",
            directory="",
            help_text="The input model to be evaluated against the map",
            is_required=False,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in Angstrom",
            required_if=[("input_model", "!=", "")],
        )

        self.joboptions["contour"] = FloatJobOption(
            label="ContourLevel",
            default_value=-1000,
            step_value=0.05,
            help_text="Suggested contour level",
            required_if=[("input_model", "!=", "")],
        )

        self.joboptions["half_map1"] = InputNodeJobOption(
            label="Input half map 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Half map 1",
            is_required=False,
        )

        self.joboptions["half_map2"] = InputNodeJobOption(
            label="Input half map 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Half map 2",
        )

        # TODO : add optional metrics to run
        # self.joboptions["run_smoc"] = BooleanJobOption(
        #     label="TEMPy SMOC",
        #     default_value=True,
        #     help_text="Run SMOC model-map fit evaluation",
        # )

        self.get_runtab_options(mpi=False, threads=False)

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        errors = []
        if self.joboptions["exp_method"].get_string() not in [
            "tomography",
            "twodcrystal",
            "crystallography",
            "singleparticle",
            "subtomogramaveraging",
            "helical",
        ]:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["exp_method"]],
                    message=(
                        "Choose from [tomography,twodcrystal,crystallography,"
                        "singleparticle,subtomogramaveraging,helical]"
                    ),
                )
            )
        return errors

    def encode(self):
        return self.__dict__

    def get_commands(self):

        # # Run in the job output directory
        self.working_dir = self.output_dir

        commands = []
        # copy inputs to output dir
        # TODO: check if big file copying can be avoided with symlinks?
        # Get parameters
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        self.input_map = os.path.relpath(input_map, self.working_dir)
        commands.append(["cp", self.input_map, "."])
        # get map info
        commands.append(
            [
                "python3",
                get_job_script("get_map_parameters.py"),
                "-m",
                self.input_map,
                "-odir",
                ".",
            ]
        )
        input_model = self.joboptions["input_model"].get_string(
            False, "Input file missing"
        )

        resolution = self.joboptions["resolution"].get_number()
        input_hmap1 = self.joboptions["half_map1"].get_string(
            False, "Input file missing"
        )

        input_hmap2 = self.joboptions["half_map2"].get_string(
            False, "Input file missing"
        )

        contour = self.joboptions["contour"].get_number()
        # run_smoc = self.joboptions["run_smoc"].get_boolean()

        # emdb validation analysis
        emdb_va_command = [
            self.jobinfo.programs[0].command,
            "-m",
            os.path.basename(self.input_map),
        ]
        emdb_va_command += ["-met", self.joboptions["exp_method"].get_string()]
        emdb_va_command += ["-d", "."]
        # outputs for map
        input_map_basename = os.path.basename(self.input_map)
        self.va_all_output = os.path.join(
            self.output_dir, input_map_basename + "_all.json"
        )
        self.output_nodes.append(
            create_node(
                self.va_all_output, NODE_PROCESSDATA, ["emdb_va", "all_results"]
            )
        )
        self.va_raps_output = os.path.join(
            self.output_dir, input_map_basename + "_raps.json"
        )
        self.output_nodes.append(
            create_node(self.va_raps_output, NODE_PROCESSDATA, ["emdb_va", "raps"])
        )
        self.va_map_dist = os.path.join(
            self.output_dir, input_map_basename + "_density_distribution.json"
        )
        self.output_nodes.append(
            create_node(
                self.va_map_dist, NODE_PROCESSDATA, ["emdb_va", "mapdistribution"]
            )
        )
        if input_model != "":
            self.input_model = os.path.relpath(input_model, self.working_dir)
            commands.append(["cp", self.input_model, "."])
            # TODO: check if -i option needs to be used
            emdb_va_command += ["-f", os.path.basename(input_model), "-i", "True"]
            map_model_fsc = os.path.join(
                self.output_dir, input_map_basename + "_mmfsc.json"
            )
            self.output_nodes.append(
                create_node(map_model_fsc, NODE_PROCESSDATA, ["emdb_va", "mapmodelfsc"])
            )
        if input_hmap1 != "" and input_hmap2 != "":
            self.input_hmap1 = os.path.relpath(input_hmap1, self.working_dir)
            self.input_hmap2 = os.path.relpath(input_hmap2, self.working_dir)
            commands.append(["cp", self.input_hmap1, "."])
            commands.append(["cp", self.input_hmap2, "."])
            emdb_va_command += ["-hmeven", os.path.basename(input_hmap1)]
            emdb_va_command += ["-hmodd", os.path.basename(input_hmap2)]
            self.out_halfmap_fsc = os.path.join(
                self.output_dir, input_map_basename + "_fsc.json"
            )
            self.output_nodes.append(
                create_node(
                    self.out_halfmap_fsc, NODE_PROCESSDATA, ["emdb_va", "halfmapfsc"]
                )
            )
        if resolution != -1:
            emdb_va_command += ["-s", resolution]
        if contour != -1000:
            emdb_va_command += ["-cl", contour]
            # outputs
            atom_inc = os.path.join(
                self.output_dir, input_map_basename + "_atom_inclusion.json"
            )
            residue_inc = os.path.join(
                self.output_dir, input_map_basename + "_residue_inclusion.json"
            )
            self.output_nodes.append(
                create_node(atom_inc, NODE_PROCESSDATA, ["emdb_va", "atominclusion"])
            )
            self.output_nodes.append(
                create_node(
                    residue_inc,
                    NODE_PROCESSDATA,
                    ["emdb_va", "residueinclusion"],
                )
            )

        commands += [emdb_va_command]
        return commands

    def gather_metadata(self):
        metadata_dict = {}
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        all_output = os.path.join(
            self.output_dir, os.path.basename(input_map) + "_all.json"
        )
        with open(all_output, "r") as j:
            dict_all_out = json.load(j)
        dict_all_out = dict_all_out[os.path.basename(input_map)]
        metadata_dict["version"] = dict_all_out["version"]
        metadata_dict["volume_estimate"] = dict_all_out["volume_estimate"]["estvolume"]
        contour = self.joboptions["contour"].get_number()
        if contour != -1000:
            try:
                metadata_dict["contour_level"] = dict_all_out[
                    "recommended_contour_level"
                ]["recl"]
            except KeyError:
                print("Contour level data missing in: {}".format(all_output))
        input_model = self.joboptions["input_model"].get_string(
            False, "Input file missing"
        )
        if input_model != "":
            if "rmmccc" in dict_all_out:
                if (
                    os.path.basename(input_model) + "_modelmap.map"
                    in dict_all_out["rmmccc"]
                ):
                    metadata_dict[
                        "rmmccc_" + os.path.splitext(os.path.basename(input_model))[0]
                    ] = dict_all_out["rmmccc"][
                        os.path.basename(input_model) + "_modelmap.map"
                    ]
            if contour != -1000:
                try:
                    metadata_dict["atom_outside_contour"] = dict_all_out[
                        "atom_inclusion_by_level"
                    ]["0"]["atomoutside"]

                    for c in dict_all_out["atom_inclusion_by_level"]["0"][
                        "chainaiscore"
                    ]:
                        metadata_dict[
                            "chain_inclusion_score: {}".format(c)
                        ] = dict_all_out["atom_inclusion_by_level"]["0"][
                            "chainaiscore"
                        ][
                            c
                        ][
                            "value"
                        ]
                except KeyError:
                    print("Atom inclusion data missing in: {}".format(all_output))
        return metadata_dict

    def create_results_display(self):
        display_objects = []
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        input_map = os.path.join(self.output_dir, os.path.basename(input_map))
        # half map inputs?
        input_hmap1 = self.joboptions["half_map1"].get_string(
            False, "Input file missing"
        )
        input_hmap2 = self.joboptions["half_map2"].get_string(
            False, "Input file missing"
        )
        contour = self.joboptions["contour"].get_number()
        resolution = self.joboptions["resolution"].get_number()
        n_montage = 0
        # the output paths are relative to project dir
        map_basename = os.path.splitext(os.path.basename(input_map))[0]
        self.get_map_parameters_tableobj(map_basename, contour, display_objects)
        # map images
        n_montage = self.add_map_image_objects(
            n_montage, input_map, contour, display_objects
        )
        # half map inputs?
        input_hmap1_basename = None
        input_hmap2_basename = None
        if input_hmap1 != "" and input_hmap2 != "":
            input_hmap1_basename = os.path.basename(input_hmap1)
            input_hmap2_basename = os.path.basename(input_hmap2)
            n_montage = self.add_halfmap_result_objects(
                input_hmap1_basename,
                input_hmap2_basename,
                n_montage,
                contour,
                display_objects,
            )
        # voxel value distribution plot
        self.add_volume_distribution_plot(
            input_map,
            input_hmap1_basename,
            input_hmap2_basename,
            contour,
            display_objects,
        )
        # RAPS plot
        self.add_raps_plot(
            input_map,
            input_hmap1_basename,
            input_hmap2_basename,
            display_objects,
            resolution,
        )

        if input_hmap1 != "" and input_hmap2 != "":
            self.add_fsc_plot(input_map, display_objects, resolution)

        input_model = self.joboptions["input_model"].get_string(
            False, "Input file missing"
        )
        input_map_basename = os.path.basename(input_map)
        if input_model:
            input_model = os.path.join(self.output_dir, os.path.basename(input_model))
            # map with model views
            x_modelsurf = (
                input_model + "_" + input_map_basename + "_scaled_xsurface.tif"
            )
            y_modelsurf = (
                input_model + "_" + input_map_basename + "_scaled_ysurface.tif"
            )
            z_modelsurf = (
                input_model + "_" + input_map_basename + "_scaled_zsurface.tif"
            )
            modelsurf_display = mini_montage_from_many_files(
                [x_modelsurf, y_modelsurf, z_modelsurf],
                self.output_dir,
                nimg=3,
                title="Primary map with model views",
                ncols=3,
                labels=["X", "Y", "Z"],
                montage_n=n_montage,
            )
            n_montage += 1
            display_objects.append(modelsurf_display)
            # atom inclusion versus level
            self.add_inclusion_plot(input_map, contour, display_objects)
            # model views
            x_modelfit = (
                input_model + "_" + input_map_basename + "_scaled_xfitsurface.tif"
            )
            y_modelfit = (
                input_model + "_" + input_map_basename + "_scaled_yfitsurface.tif"
            )
            z_modelfit = (
                input_model + "_" + input_map_basename + "_scaled_zfitsurface.tif"
            )
            modelfit_display = mini_montage_from_many_files(
                [x_modelfit, y_modelfit, z_modelfit],
                self.output_dir,
                nimg=3,
                title="Model colored by atom inclusion per residue",
                ncols=3,
                labels=["X", "Y", "Z"],
                montage_n=n_montage,
            )
            n_montage += 1
            display_objects.append(modelfit_display)
            if contour != -1000:
                self.add_residue_inclusion_plot(input_map, contour, display_objects)
        return display_objects

    def add_fsc_plot(self, input_map, display_objects, resolution):
        out_halfmap_fsc = os.path.join(
            self.output_dir, os.path.basename(input_map) + "_fsc.json"
        )
        with open(out_halfmap_fsc, "r") as m:
            dict_fsc_results = json.load(m)
        dict_fsc_plot = dict_fsc_results["fsc"]["curves"]

        list_data = []
        series_args = []
        # fsc
        fsc_data = {
            "x": dict_fsc_plot["level"],
            "y": dict_fsc_plot["fsc"],
        }
        list_data.append(fsc_data)
        series_args.append(
            {
                "name": "Calculated FSC",
                "mode": "lines",
                "line": {"color": "orange"},
            }
        )  # plot trace name (legend)
        associated_data = [out_halfmap_fsc]
        # fsc 0.5
        line_05 = {
            "x": [0.0, max(fsc_data["x"])],
            "y": [0.5, 0.5],
        }
        list_data.append(line_05)
        series_args.append(
            {
                "name": "0.5",
                "mode": "lines",
                "line": {"color": "black", "dash": "dashdot"},
            }
        )  # plot trace name (legend)
        # fsc 0.143
        line_0143 = {
            "x": [0.0, max(fsc_data["x"])],
            "y": [0.143, 0.143],
        }
        list_data.append(line_0143)
        series_args.append(
            {
                "name": "0.143",
                "mode": "lines",
                "line": {"color": "black", "dash": "dash"},
            }
        )  # plot trace name (legend)
        if resolution != -1.0:
            resolution_line = {
                "x": [1.0 / resolution, 1.0 / resolution],
                "y": [
                    min(fsc_data["y"]),
                    max(fsc_data["y"]),
                ],
            }
            list_data.append(resolution_line)
            series_args.append(
                {
                    "name": "Resolution {}Å".format(round(resolution, 3)),
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        # half bit
        halfbit_data = {
            "x": dict_fsc_plot["level"],
            "y": dict_fsc_plot["halfbit"],
        }
        list_data.append(halfbit_data)
        series_args.append(
            {
                "name": "Half bit",
                "mode": "lines",
                "line": {"color": "pink", "dash": "dashdot"},
            }
        )  # plot trace name (legend)

        halfmap_fsc_plot = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            subplot_args=series_args,
            title="FSC",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Spatial frequency (1/Angstroms)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "FSC",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(halfmap_fsc_plot)

    def add_residue_inclusion_plot(self, input_map, contour, display_objects):
        # residue inclusion plot
        res_inc = input_map + "_residue_inclusion.json"
        with open(res_inc, "r") as m:
            dict_residue_inc_results = json.load(m)
        list_x = []
        list_y = []
        list_color = []
        list_chains = []
        list_chain_res = []
        list_chain_scores = []
        dict_res_inc = dict_residue_inc_results["residue_inclusion"]["0"][str(contour)]
        for n in range(len(dict_res_inc["residue"])):
            residue = dict_res_inc["residue"][n]
            residue_split = residue.split(":")
            chain = residue_split[0]
            # append last residue and score before plot
            if n == len(dict_res_inc["residue"]) - 1:
                list_chain_res.append(residue_split[1][:-3])
                list_chain_scores.append(round(float(dict_res_inc["inclusion"][n]), 3))
                chain_last = chain
            if len(list_chains) == 0:
                chain_last = chain
                list_chains.append(chain)
            # end of each chain
            elif chain not in list_chains or n == len(dict_res_inc["residue"]) - 1:
                # TODO: chain splitting is currently disabled. This needs to reworked
                list_chain_color = list_chain_scores
                list_chain_x = list_chain_res
                list_chain_y = [
                    "Chain {}".format(
                        chain_last,
                    )
                ] * len(list_chain_scores)
                list_x = list_chain_x + list_x
                list_y = list_chain_y + list_y
                list_color = list_chain_color + list_color
                if n == len(dict_res_inc["residue"]) - 1:
                    break  # break at the last residue
                list_chains.append(chain)
                list_chain_res = []
                list_chain_scores = []
            chain_last = chain
            list_chain_res.append(residue_split[1][:-3])
            list_chain_scores.append(round(float(dict_res_inc["inclusion"][n]), 3))
        plot_args = {
            "mode": "markers",
            "marker": {
                "symbol": "square",
                "size": 10,
                "color": list_color,
                "colorscale": [[0, "red"], [1, "cyan"]],  # "Viridis",
                "cmax": 1.0,
                "cmin": 0.0,
                "showscale": True,
            },
        }
        res_inc_plot = create_results_display_object(
            "plotlyobj",
            data=[{"x": list_x, "y": list_y}],
            plot_type="Scatter",
            multi_series=True,
            subplot_args=[plot_args],
            title="Atom inclusion by residue (contour: {})".format(contour),
            associated_data=[res_inc],
            xaxes_args={
                "title_text": "Residue",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={"gridcolor": "lightgrey", "linecolor": "black"},
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(res_inc_plot)

    def add_inclusion_plot(
        self,
        input_map,
        contour,
        display_objects,
    ):
        # Atom inclusion vs level plot
        map_data = input_map + "_atom_inclusion.json"
        with open(map_data, "r") as m:
            dict_map_data = json.load(m)
        list_data = []
        series_args = []
        data1 = {
            "x": dict_map_data["atom_inclusion_by_level"]["0"]["level"],
            "y": dict_map_data["atom_inclusion_by_level"]["0"]["all_atom"],
        }
        list_data.append(data1)
        series_args.append(
            {
                "name": "All atom inclusion",
                "mode": "lines",
                "line": {"color": "orange"},
            }
        )  # plot trace name (legend)
        data2 = {
            "x": dict_map_data["atom_inclusion_by_level"]["0"]["level"],
            "y": dict_map_data["atom_inclusion_by_level"]["0"]["backbone"],
        }
        list_data.append(data2)
        series_args.append(
            {
                "name": "Backbone atom inclusion",
                "mode": "lines",
                "line": {"color": "blue"},
            }
        )  # plot trace name (legend)
        associated_data = [map_data]
        if contour != -1000:
            contour_line = {
                "x": [contour, contour],
                "y": [0.0, 1.0],
            }
            list_data.append(contour_line)
            series_args.append(
                {
                    "name": "Contour level",
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        map_plotobj = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            subplot_args=series_args,
            title="Atom inclusion versus contour level",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Contour level",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "Fraction of model inside the map",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(map_plotobj)

    def add_raps_plot(
        self,
        input_map,
        input_hmap1_basename,
        input_hmap2_basename,
        display_objects,
        resolution,
    ):
        # RAPS plot
        map_raps = input_map + "_raps.json"
        with open(map_raps, "r") as m:
            dict_map_raps = json.load(m)
        list_data = []
        series_args = []
        ps_data = {
            "x": dict_map_raps["rotationally_averaged_power_spectrum"]["x"],
            "y": dict_map_raps["rotationally_averaged_power_spectrum"]["y"],
        }
        list_data.append(ps_data)
        series_args.append(
            {
                "name": "RAPS",
                "mode": "lines",
                "line": {"color": "blue"},
            }
        )  # plot trace name (legend)
        associated_data = [map_raps]
        if resolution != -1:
            resolution_line = {
                "x": [1.0 / resolution, 1.0 / resolution],
                "y": [
                    min(dict_map_raps["rotationally_averaged_power_spectrum"]["y"]),
                    max(dict_map_raps["rotationally_averaged_power_spectrum"]["y"]),
                ],
            }
            list_data.append(resolution_line)
            series_args.append(
                {
                    "name": "Resolution {}Å".format(round(resolution, 3)),
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        if input_hmap1_basename and input_hmap2_basename:
            hmap_basename = "_".join(
                [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
            )
            hmap_path = os.path.join(self.output_dir, hmap_basename)
            rawmap_dist = hmap_path + "_raps.json"
            with open(rawmap_dist, "r") as m:
                dict_rawmap_dist = json.load(m)
            rawmap_raps_data = {
                "x": dict_rawmap_dist["rawmap_rotationally_averaged_power_spectrum"][
                    "x"
                ],
                "y": dict_rawmap_dist["rawmap_rotationally_averaged_power_spectrum"][
                    "y"
                ],
            }
            list_data.append(rawmap_raps_data)
            series_args.append(
                {
                    "name": "Raw map RAPS",
                    "mode": "lines",
                    "line": {"color": "orange"},
                }
            )  # plot trace name (legend)
            associated_data.append(rawmap_dist)
        raps_plot = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            subplot_args=series_args,
            title="Rotationally averaged power spectrum",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Spatial frequency (1/Angstroms)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "Intensity (log10)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(raps_plot)

    def add_volume_distribution_plot(
        self,
        input_map,
        input_hmap1_basename,
        input_hmap2_basename,
        contour,
        display_objects,
    ):
        # voxel value distribution
        map_dist = input_map + "_density_distribution.json"
        with open(map_dist, "r") as m:
            dict_map_dist = json.load(m)
        associated_data = [map_dist]
        list_data = []
        series_args = []
        voxel_data = {
            "x": dict_map_dist["density_distribution"]["x"],
            "y": dict_map_dist["density_distribution"]["y"],
        }
        list_data.append(voxel_data)
        series_args.append(
            {
                "name": "Primary map value distribution",
                "mode": "lines",
                "line": {"color": "blue"},
            }
        )  # plot trace name (legend)
        associated_data = [map_dist]
        if contour != -1000:
            contour_line = {
                "x": [contour, contour],
                "y": [0.0, max(dict_map_dist["density_distribution"]["y"])],
            }
            list_data.append(contour_line)
            series_args.append(
                {
                    "name": "Contour level",
                    "mode": "lines",
                    "line": {"color": "red"},
                }
            )  # plot trace name (legend)
        if input_hmap1_basename and input_hmap2_basename:
            hmap_basename = "_".join(
                [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
            )
            hmap_path = os.path.join(self.output_dir, hmap_basename)
            rawmap_dist = hmap_path + "_density_distribution.json"
            with open(rawmap_dist, "r") as m:
                dict_rawmap_dist = json.load(m)
            rawmap_voxel_data = {
                "x": dict_rawmap_dist["rawmap_density_distribution"]["x"],
                "y": dict_rawmap_dist["rawmap_density_distribution"]["y"],
            }
            list_data.append(rawmap_voxel_data)
            series_args.append(
                {
                    "name": "Raw map value distribution",
                    "mode": "lines",
                    "line": {"color": "orange"},
                }
            )  # plot trace name (legend)
            associated_data.append(rawmap_dist)
        map_dist_plot = create_results_display_object(
            "plotlyobj",
            data=list_data,
            plot_type="Scatter",
            multi_series=True,
            subplot_args=series_args,
            title="Voxel-value distribution",
            associated_data=associated_data,
            xaxes_args={
                "title_text": "Voxel value",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            yaxes_args={
                "title_text": "Number of voxels (log10)",
                "gridcolor": "lightgrey",
                "linecolor": "black",
            },
            layout_args={
                "plot_bgcolor": "white",
            },
        )
        display_objects.append(map_dist_plot)

    def add_map_image_objects(self, n_montage, input_map, contour, display_objects):
        # orthogonal projections
        x_proj = input_map + "_xprojection.tif"
        y_proj = input_map + "_yprojection.tif"
        z_proj = input_map + "_zprojection.tif"
        proj_display = mini_montage_from_many_files(
            [x_proj, y_proj, z_proj],
            self.output_dir,
            nimg=3,
            title="Orthogonal projections",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(proj_display)
        # orthogonal max value projections
        x_projmax = input_map + "_xmax.tif"
        y_projmax = input_map + "_ymax.tif"
        z_projmax = input_map + "_zmax.tif"
        projmax_display = mini_montage_from_many_files(
            [x_projmax, y_projmax, z_projmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projmax_display)
        # orthogonal false color max value projections
        x_projglowmax = input_map + "_glow_xmax.tif"
        y_projglowmax = input_map + "_glow_ymax.tif"
        z_projglowmax = input_map + "_glow_zmax.tif"
        projglowmax_display = mini_montage_from_many_files(
            [x_projglowmax, y_projglowmax, z_projglowmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections (False-colour)",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projglowmax_display)
        # orthogonal std dev projections
        x_projstd = input_map + "_xstd.tif"
        y_projstd = input_map + "_ystd.tif"
        z_projstd = input_map + "_zstd.tif"
        projstd_display = mini_montage_from_many_files(
            [x_projstd, y_projstd, z_projstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projstd_display)
        # orthogonal false color std dev projections
        x_projglowstd = input_map + "_glow_xstd.tif"
        y_projglowstd = input_map + "_glow_ystd.tif"
        z_projglowstd = input_map + "_glow_zstd.tif"
        projglowstd_display = mini_montage_from_many_files(
            [x_projglowstd, y_projglowstd, z_projglowstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections (False-colour)",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projglowstd_display)
        # central slices
        x_slicecentral = input_map + "_scaled_xcentral_slice.tif"
        y_slicecentral = input_map + "_scaled_ycentral_slice.tif"
        z_slicecentral = input_map + "_scaled_zcentral_slice.tif"
        slicecentral_display = mini_montage_from_many_files(
            [x_slicecentral, y_slicecentral, z_slicecentral],
            self.output_dir,
            nimg=3,
            title="Central slices",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(slicecentral_display)
        # largest variance slices
        x_slicelargevar = input_map + "_xlargestvariance_slice.tif"
        y_slicelargevar = input_map + "_ylargestvariance_slice.tif"
        z_slicelargevar = input_map + "_zlargestvariance_slice.tif"
        slicelargevar_display = mini_montage_from_many_files(
            [x_slicelargevar, y_slicelargevar, z_slicelargevar],
            self.output_dir,
            nimg=3,
            title="Largest variance slices",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(slicelargevar_display)
        # surface views
        x_surface = input_map + "_scaled_xsurface.tif"
        if os.path.isfile(x_surface) and contour != -1000:
            y_surface = input_map + "_scaled_ysurface.tif"
            z_surface = input_map + "_scaled_zsurface.tif"
            surface_display = mini_montage_from_many_files(
                [x_surface, y_surface, z_surface],
                self.output_dir,
                nimg=3,
                title="Orthogonal surface views of map (at contour {})".format(contour),
                ncols=3,
                labels=["X", "Y", "Z"],
                montage_n=n_montage,
            )
            n_montage += 1
            display_objects.append(surface_display)
        return n_montage

    def add_halfmap_result_objects(
        self,
        input_hmap1_basename,
        input_hmap2_basename,
        n_montage,
        contour,
        display_objects,
    ):
        hmap_basename = "_".join(
            [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
        )
        hmap_path = os.path.join(self.output_dir, hmap_basename)
        x_proj = hmap_path + "_xprojection.tif"
        y_proj = hmap_path + "_yprojection.tif"
        z_proj = hmap_path + "_zprojection.tif"
        proj_display = mini_montage_from_many_files(
            [x_proj, y_proj, z_proj],
            self.output_dir,
            nimg=3,
            title="Orthogonal projections of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(proj_display)
        # orthogonal max-value projections
        x_projmax = hmap_path + "_xmax.tif"
        y_projmax = hmap_path + "_ymax.tif"
        z_projmax = hmap_path + "_zmax.tif"
        proj_display = mini_montage_from_many_files(
            [x_projmax, y_projmax, z_projmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(proj_display)
        # orthogonal false color max value projections
        x_projglowmax = hmap_path + "_glow_xmax.tif"
        y_projglowmax = hmap_path + "_glow_ymax.tif"
        z_projglowmax = hmap_path + "_glow_zmax.tif"
        projglowmax_display = mini_montage_from_many_files(
            [x_projglowmax, y_projglowmax, z_projglowmax],
            self.output_dir,
            nimg=3,
            title="Orthogonal maximum-value projections (False-colour) of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projglowmax_display)
        # orthogonal false color max value projections
        x_projstd = hmap_path + "_xstd.tif"
        y_projstd = hmap_path + "_ystd.tif"
        z_projstd = hmap_path + "_zstd.tif"
        projstd_display = mini_montage_from_many_files(
            [x_projstd, y_projstd, z_projstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projstd_display)
        # orthogonal false color std dev projections
        x_projglowstd = hmap_path + "_glow_xstd.tif"
        y_projglowstd = hmap_path + "_glow_ystd.tif"
        z_projglowstd = hmap_path + "_glow_zstd.tif"
        projglowstd_display = mini_montage_from_many_files(
            [x_projglowstd, y_projglowstd, z_projglowstd],
            self.output_dir,
            nimg=3,
            title="Orthogonal standard-deviation projections (False-colour) \
                of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(projglowstd_display)
        # central slices
        x_slicecentral = hmap_path + "_scaled_xcentral_slice.tif"
        y_slicecentral = hmap_path + "_scaled_ycentral_slice.tif"
        z_slicecentral = hmap_path + "_scaled_zcentral_slice.tif"
        slicecentral_display = mini_montage_from_many_files(
            [x_slicecentral, y_slicecentral, z_slicecentral],
            self.output_dir,
            nimg=3,
            title="Central slices of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(slicecentral_display)
        # largest variance slices
        x_slicelargevar = hmap_path + "_xlargestvariance_slice.tif"
        y_slicelargevar = hmap_path + "_ylargestvariance_slice.tif"
        z_slicelargevar = hmap_path + "_zlargestvariance_slice.tif"
        slicelargevar_display = mini_montage_from_many_files(
            [x_slicelargevar, y_slicelargevar, z_slicelargevar],
            self.output_dir,
            nimg=3,
            title="Largest variance slices of raw map",
            ncols=3,
            labels=["X", "Y", "Z"],
            montage_n=n_montage,
        )
        n_montage += 1
        display_objects.append(slicelargevar_display)
        # surface views
        x_surface = hmap_path + "_scaled_xsurface.tif"
        rawmap_contour_json = hmap_path + "_rawmapcl.json"
        rawmap_contour = contour
        if os.path.exists(rawmap_contour_json):
            with open(rawmap_contour_json, "r") as j:
                dict_contour = json.load(j)
                try:
                    rawmap_contour = dict_contour["rawmap_contour_level"]["cl"]
                except KeyError:
                    pass
        if os.path.isfile(x_surface) and contour != -1000:
            y_surface = hmap_path + "_scaled_ysurface.tif"
            z_surface = hmap_path + "_scaled_zsurface.tif"
            surface_display = mini_montage_from_many_files(
                [x_surface, y_surface, z_surface],
                self.output_dir,
                nimg=3,
                title="Orthogonal surface views of raw map (at contour {})".format(
                    rawmap_contour
                ),
                ncols=3,
                labels=["X", "Y", "Z"],
                montage_n=n_montage,
            )
            n_montage += 1
            display_objects.append(surface_display)
        return n_montage

    def get_map_parameters_tableobj(self, map_basename, contour, display_objects):
        # map parameters
        map_parameters_json = os.path.join(
            self.output_dir, map_basename + "_map_parameters.json"
        )
        map_parameters_data = []
        if contour != -1000:
            map_parameters_data.append(["Selected contour level", contour])
        with open(map_parameters_json, "r") as j:
            dict_map_parameters = json.load(j)
            map_parameters_data.append(
                [
                    "Number of grid points",
                    "x".join(
                        [
                            str(dict_map_parameters["nx"]),
                            str(dict_map_parameters["ny"]),
                            str(dict_map_parameters["nz"]),
                        ]
                    ),
                ]
            )
            map_parameters_data.append(
                [
                    "Voxel size",
                    "x".join(
                        [
                            str(dict_map_parameters["apix"][0]),
                            str(dict_map_parameters["apix"][1]),
                            str(dict_map_parameters["apix"][2]),
                        ]
                    )
                    + "Å",
                ]
            )
            map_parameters_data.append(["Minimum value", dict_map_parameters["min"]])
            map_parameters_data.append(["Maximum value", dict_map_parameters["max"]])
            map_parameters_data.append(["Average value", dict_map_parameters["mean"]])
            map_parameters_data.append(
                ["Standard deviation", dict_map_parameters["std"]]
            )
        map_parameters = create_results_display_object(
            "table",
            title="Map parameters",
            headers=["Parameters", "values"],
            table_data=map_parameters_data,
            associated_data=[map_parameters_json],
        )
        display_objects.append(map_parameters)

    def post_run_actions(self):
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        input_map = os.path.join(self.output_dir, os.path.basename(input_map))
        input_map_basename = os.path.basename(input_map)
        input_model = self.joboptions["input_model"].get_string(
            False, "Input file missing"
        )
        input_model = os.path.join(self.output_dir, os.path.basename(input_model))
        list_images_to_tif = []
        # the output paths are relative to project dir
        # orthogonal projections
        x_proj = input_map + "_xprojection.jpeg"
        y_proj = input_map + "_yprojection.jpeg"
        z_proj = input_map + "_zprojection.jpeg"
        list_images_to_tif.extend([x_proj, y_proj, z_proj])
        # orthogonal max-value projections
        x_projmax = input_map + "_xmax.jpeg"
        y_projmax = input_map + "_ymax.jpeg"
        z_projmax = input_map + "_zmax.jpeg"
        list_images_to_tif.extend([x_projmax, y_projmax, z_projmax])
        # orthogonal false color max value projections
        x_projglowmax = input_map + "_glow_xmax.jpeg"
        y_projglowmax = input_map + "_glow_ymax.jpeg"
        z_projglowmax = input_map + "_glow_zmax.jpeg"
        list_images_to_tif.extend([x_projglowmax, y_projglowmax, z_projglowmax])
        # orthogonal false color max value projections
        x_projstd = input_map + "_xstd.jpeg"
        y_projstd = input_map + "_ystd.jpeg"
        z_projstd = input_map + "_zstd.jpeg"
        list_images_to_tif.extend([x_projstd, y_projstd, z_projstd])
        # orthogonal false color std dev projections
        x_projglowstd = input_map + "_glow_xstd.jpeg"
        y_projglowstd = input_map + "_glow_ystd.jpeg"
        z_projglowstd = input_map + "_glow_zstd.jpeg"
        list_images_to_tif.extend([x_projglowstd, y_projglowstd, z_projglowstd])
        # central slices
        x_slicecentral = input_map + "_scaled_xcentral_slice.jpeg"
        y_slicecentral = input_map + "_scaled_ycentral_slice.jpeg"
        z_slicecentral = input_map + "_scaled_zcentral_slice.jpeg"
        list_images_to_tif.extend([x_slicecentral, y_slicecentral, z_slicecentral])
        # largest variance slices
        x_slicelargevar = input_map + "_xlargestvariance_slice.jpeg"
        y_slicelargevar = input_map + "_ylargestvariance_slice.jpeg"
        z_slicelargevar = input_map + "_zlargestvariance_slice.jpeg"
        list_images_to_tif.extend([x_slicelargevar, y_slicelargevar, z_slicelargevar])
        # surface views
        x_surface = input_map + "_scaled_xsurface.jpeg"
        y_surface = input_map + "_scaled_ysurface.jpeg"
        z_surface = input_map + "_scaled_zsurface.jpeg"
        list_images_to_tif.extend([x_surface, y_surface, z_surface])
        # map with model views
        x_modelsurf = input_model + "_" + input_map_basename + "_scaled_xsurface.jpeg"
        y_modelsurf = input_model + "_" + input_map_basename + "_scaled_ysurface.jpeg"
        z_modelsurf = input_model + "_" + input_map_basename + "_scaled_zsurface.jpeg"
        list_images_to_tif.extend([x_modelsurf, y_modelsurf, z_modelsurf])
        # model views
        x_modelfit = input_model + "_" + input_map_basename + "_scaled_xfitsurface.jpeg"
        y_modelfit = input_model + "_" + input_map_basename + "_scaled_yfitsurface.jpeg"
        z_modelfit = input_model + "_" + input_map_basename + "_scaled_zfitsurface.jpeg"
        list_images_to_tif.extend([x_modelfit, y_modelfit, z_modelfit])
        # half map input?
        input_hmap1 = self.joboptions["half_map1"].get_string(
            False, "Input file missing"
        )
        input_hmap2 = self.joboptions["half_map2"].get_string(
            False, "Input file missing"
        )
        if input_hmap1 != "" and input_hmap2 != "":
            input_hmap1_basename = os.path.basename(input_hmap1)
            input_hmap2_basename = os.path.basename(input_hmap2)
            hmap_basename = "_".join(
                [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
            )
            hmap_path = os.path.join(self.output_dir, hmap_basename)
            x_proj = hmap_path + "_xprojection.jpeg"
            y_proj = hmap_path + "_yprojection.jpeg"
            z_proj = hmap_path + "_zprojection.jpeg"
            list_images_to_tif.extend([x_proj, y_proj, z_proj])
            # orthogonal max-value projections
            x_projmax = hmap_path + "_xmax.jpeg"
            y_projmax = hmap_path + "_ymax.jpeg"
            z_projmax = hmap_path + "_zmax.jpeg"
            list_images_to_tif.extend([x_projmax, y_projmax, z_projmax])
            # orthogonal false color max value projections
            x_projglowmax = hmap_path + "_glow_xmax.jpeg"
            y_projglowmax = hmap_path + "_glow_ymax.jpeg"
            z_projglowmax = hmap_path + "_glow_zmax.jpeg"
            list_images_to_tif.extend([x_projglowmax, y_projglowmax, z_projglowmax])
            # orthogonal false color max value projections
            x_projstd = hmap_path + "_xstd.jpeg"
            y_projstd = hmap_path + "_ystd.jpeg"
            z_projstd = hmap_path + "_zstd.jpeg"
            list_images_to_tif.extend([x_projstd, y_projstd, z_projstd])
            # orthogonal false color std dev projections
            x_projglowstd = hmap_path + "_glow_xstd.jpeg"
            y_projglowstd = hmap_path + "_glow_ystd.jpeg"
            z_projglowstd = hmap_path + "_glow_zstd.jpeg"
            list_images_to_tif.extend([x_projglowstd, y_projglowstd, z_projglowstd])
            # central slices
            x_slicecentral = hmap_path + "_scaled_xcentral_slice.jpeg"
            y_slicecentral = hmap_path + "_scaled_ycentral_slice.jpeg"
            z_slicecentral = hmap_path + "_scaled_zcentral_slice.jpeg"
            list_images_to_tif.extend([x_slicecentral, y_slicecentral, z_slicecentral])
            # largest variance slices
            x_slicelargevar = hmap_path + "_xlargestvariance_slice.jpeg"
            y_slicelargevar = hmap_path + "_ylargestvariance_slice.jpeg"
            z_slicelargevar = hmap_path + "_zlargestvariance_slice.jpeg"
            list_images_to_tif.extend(
                [x_slicelargevar, y_slicelargevar, z_slicelargevar]
            )
            # surface views
            x_surface = hmap_path + "_scaled_xsurface.jpeg"
            y_surface = hmap_path + "_scaled_ysurface.jpeg"
            z_surface = hmap_path + "_scaled_zsurface.jpeg"
            list_images_to_tif.extend([x_surface, y_surface, z_surface])

        # loop through and convert
        for img in list_images_to_tif:
            if os.path.exists(img):
                self.convert_img_tifflzw(img)

    def convert_img_tifflzw(self, img):
        with Image.open(img) as im:
            im.save(os.path.splitext(img)[0] + ".tif", "TIFF", compression="tiff_lzw")
