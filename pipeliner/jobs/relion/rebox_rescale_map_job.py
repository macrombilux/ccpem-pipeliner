#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    InputNodeJobOption,
    IntJobOption,
    BooleanJobOption,
    FloatJobOption,
    JobOptionValidationResult,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.node_factory import create_node


class RelionReboxRescaleMap(RelionJob):
    OUT_DIR = "ReboxRescale"
    PROCESS_NAME = "relion.map_utilities.rebox_rescale_map"

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Change map box and pixel size"
        self.jobinfo.short_desc = (
            "Modify the box size of a map and change the pixel size if desired"
        )

        self.jobinfo.long_desc = (
            "Modify the box size of a map and change the pixel size if desired.  If the"
            " map is rescaled the new pixel box size must be an even number."
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to rescale or rebox",
            is_required=True,
            default_value="",
        )

        self.joboptions["do_rebox"] = BooleanJobOption(
            label="Rebox the map?",
            default_value=False,
            help_text=(
                "Select this option to trim or pad the map box to a different size."
                " If the map is also to be rescaled, the new box size must be even."
            ),
        )

        self.joboptions["box_size"] = IntJobOption(
            label="New box size (px):",
            default_value=100,
            hard_min=2,
            required_if=[("do_rebox", "=", True)],
            deactivate_if=[("do_rebox", "=", False)],
        )

        self.joboptions["do_rescale"] = BooleanJobOption(
            label="Rescale the map?",
            default_value=False,
            help_text=(
                "Select this option to resample the map with a different pixel size."
                " If the map is also to be reboxed, the new box size must be even."
            ),
        )

        self.joboptions["rescale_angpix"] = FloatJobOption(
            label="Rescaled pixel size:",
            default_value=1.0,
            hard_min=0.1,
            help_text="New pixel size for the map",
            required_if=[("do_rescale", "=", True)],
            deactivate_if=[("do_rescale", "=", False)],
        )

    def parameter_validation(self):
        if (
            not self.joboptions["do_rescale"].get_boolean()
            and not self.joboptions["do_rebox"].get_boolean()
        ):
            return [
                JobOptionValidationResult(
                    "error",
                    [self.joboptions["do_rescale"], self.joboptions["do_rebox"]],
                    (
                        "No action will be performed; Select either rescale, rebox, "
                        "or both"
                    ),
                )
            ]
        if (
            self.joboptions["do_rescale"].get_boolean()
            and self.joboptions["do_rebox"].get_boolean()
        ):
            if self.joboptions["box_size"].get_number() % 2:
                return [
                    JobOptionValidationResult(
                        "error",
                        [self.joboptions["box_size"], self.joboptions["do_rescale"]],
                        (
                            "If the map is to be rescaled the new box size must be an"
                            " even number"
                        ),
                    )
                ]

    def get_commands(self) -> List[List[str]]:
        inmap = self.joboptions["input_map"].get_string(True, "Input map not found")
        command = ["relion_image_handler", "--i", inmap]

        suffix = ""
        if self.joboptions["do_rebox"].get_boolean():
            box_size = self.joboptions["box_size"].get_number()
            command.extend(["--new_box", box_size])
            suffix += "_reboxed"

        if self.joboptions["do_rescale"].get_boolean():
            newpx = self.joboptions["rescale_angpix"].get_number()
            command.extend(["--rescale_angpix", newpx])
            suffix += "_rescaled"

        outfile_name = os.path.splitext(os.path.basename(inmap))[0] + suffix + ".mrc"
        outfile_path = os.path.join(self.output_dir, outfile_name)
        self.output_nodes.append(create_node(outfile_path, NODE_DENSITYMAP))

        command.extend(["--o", outfile_path])

        return [command]

    def create_results_display(self) -> list:
        box_size = self.joboptions["box_size"].get_number()
        px_size = self.joboptions["rescale_angpix"].get_number()
        suffix = ""
        title = "Map"
        if self.joboptions["do_rebox"].get_boolean():
            suffix += "_reboxed"
            title += f" reboxed to {box_size}^2 px"
        if self.joboptions["do_rescale"].get_boolean():
            suffix += "_rescaled"
            title += f" rescaled to {px_size} \u212B/px"

        inmap = self.joboptions["input_map"].get_string()
        outfile_name = os.path.splitext(os.path.basename(inmap))[0] + suffix + ".mrc"
        outfile_path = os.path.join(self.output_dir, outfile_name)
        return [
            make_map_model_thumb_and_display(
                title=title,
                outputdir=self.output_dir,
                maps=[outfile_path],
                start_collapsed=False,
            )
        ]
