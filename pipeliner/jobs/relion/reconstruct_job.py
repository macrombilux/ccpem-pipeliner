#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os

from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.jobs.relion.relion_job import relion_program

from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
)
from pipeliner.display_tools import create_results_display_object
from pipeliner.nodes import NODE_PARTICLESDATA, NODE_DENSITYMAP
from pipeliner.node_factory import create_node


class RelionReconstruct(RelionJob):
    PROCESS_NAME = "relion.reconstruct"
    OUT_DIR = "Reconstruct"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "RELION Reconstruct"

        self.jobinfo.short_desc = (
            "Generate a map from a set of particles using relion_reconstruct"
        )
        self.jobinfo.long_desc = (
            "For more information see the help screen by running: 'relion_reconstruct"
            " --h'"
        )
        self.jobinfo.programs = [relion_program("relion_reconstruct")]

        self.joboptions["input_particles"] = InputNodeJobOption(
            label="Input particles file:",
            node_type=NODE_PARTICLESDATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern="Particles star file (.star)",
            help_text=(
                "Input STAR file with the projection images and their orientations"
            ),
            is_required=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size:",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Pixel size in the reconstruction (take from first optics group by"
                " default)"
            ),
            is_required=True,
        )

        self.joboptions["sym"] = StringJobOption(
            label="Symmetry:",
            default_value="C1",
            help_text="Symmetry to apply IE: C2",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["maxres"] = FloatJobOption(
            label="Maximum resolution to consider:",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Maximum resolution (in Angstrom) to consider in Fourier space (default"
                " Nyquist)"
            ),
            in_continue=True,
            is_required=True,
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)

    def get_commands(self):

        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi > 1:
            command = self.get_mpi_command() + ["relion_reconstruct_mpi"]
        else:
            command = ["relion_reconstruct"]

        input_file = self.joboptions["input_particles"].get_string()
        command += ["--i", input_file]

        output_file = "{}reconstruction.mrc".format(self.output_dir)
        command += ["--o", output_file]
        self.output_nodes.append(
            create_node(output_file, NODE_DENSITYMAP, ["relion", "reconstruct"])
        )

        max_res = self.joboptions["maxres"].get_number()
        if max_res > 0:
            command += ["--maxres", str(max_res)]

        angpix = self.joboptions["angpix"].get_number()
        if angpix > 0:
            command += ["--angpix", str(angpix)]

        sym = self.joboptions["sym"].get_string()
        sym = "C1" if sym == "" else sym
        command += ["--sym", sym]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        commands = [command]
        return commands

    def create_results_display(self):
        outmap = os.path.join(self.output_dir, "reconstruction.mrc")
        return [
            create_results_display_object(
                dobj_type="mapmodel",
                title="Reconstructed map",
                associated_data=[outmap],
                maps=[outmap],
                start_collapsed=False,
            )
        ]
