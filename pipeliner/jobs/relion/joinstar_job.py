#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from .relion_job import RelionJob
from pipeliner.jobs.relion.relion_job import relion_program

from pipeliner.job_options import (
    MultiFileNameJobOption,
    BooleanJobOption,
    FileNameJobOption,
    files_exts,
    EXT_STARFILE,
)
from pipeliner.data_structure import (
    JOINSTAR_MOVIES_NAME,
    JOINSTAR_MICS_NAME,
    JOINSTAR_PARTS_NAME,
    JOINSTAR_DIR,
)
from pipeliner.nodes import (
    NODE_PARTICLESDATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
)
from pipeliner.node_factory import create_node
from pipeliner.starfile_handler import DataStarFile
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_raw_mics,
    prepare_empiar_mics_parts,
)
from pipeliner.display_tools import mini_montage_from_starfile


class RelionJoinStarJob(RelionJob):
    OUT_DIR = JOINSTAR_DIR

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_star_handler")]
        self.jobinfo.long_desc = (
            "Join together starfiles that contain the same type of data. The files must"
            " contain the same metadata columns in the same order to be joined"
            " successfully"
        )

    def make_join_command(self, files, ftype):
        if len(files) < 2:
            raise ValueError(
                "ERROR: Not enough files selected, select at least two to join"
            )
        filetypes = {
            "particles": ("rlnImageName", NODE_PARTICLESDATA, ["relion"]),
            "movies": (
                "rlnMicrographMovieName",
                NODE_MICROGRAPHMOVIEGROUPMETADATA,
                ["relion"],
            ),
            "mics": ("rlnMicrographName", NODE_MICROGRAPHGROUPMETADATA, ["relion"]),
        }
        command = ["relion_star_handler", "--combine", "--i", " ".join(files)]
        command += ["--check_duplicates", filetypes[ftype][0]]
        outputfile = self.output_dir + "join_{}.star".format(ftype)
        command += ["--o", outputfile]
        self.output_nodes.append(
            create_node(outputfile, filetypes[ftype][1], filetypes[ftype][2])
        )
        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            command += self.parse_additional_args()
        return command

    def add_common_compatibility_joboptions(self, jtype: str):
        # add the do_ jobops
        self.joboptions["do_mic"] = BooleanJobOption(
            label="Combine micrograph STAR files?",
            default_value=jtype == "mic",
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_mov"] = BooleanJobOption(
            label="Combine movie STAR files?",
            default_value=jtype == "mov",
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_part"] = BooleanJobOption(
            label="Combine particle STAR files?",
            default_value=jtype == "part",
            jobop_group="Relion compatibility options",
        )

        # add the file joboptions
        for n in range(1, 5):
            self.joboptions[f"fn_mov{n}"] = FileNameJobOption(
                label=f"Movie STAR file {n}:",
                default_value="",
                jobop_group="Relion compatibility options",
                node_type="MicrographMoviesData",
            )
            self.joboptions[f"fn_mic{n}"] = FileNameJobOption(
                label=f"Micrograph STAR file {n}:",
                default_value="",
                jobop_group="Relion compatibility options",
                node_type="MicrographsData",
            )
            self.joboptions[f"fn_part{n}"] = FileNameJobOption(
                label=f"Particle STAR file {n}:",
                default_value="",
                jobop_group="Relion compatibility options",
                node_type="ParticlesData",
            )
        # put the values in the appropriate ones
        fjobop = self.joboptions[f"fn_{jtype}s"]

        files = fjobop.get_list()
        for n, f in enumerate(files):
            self.joboptions[f"fn_{jtype}{n+1}"].value = f

        # set the do_ jobops
        self.joboptions[f"do_{jtype}"].value = "Yes"

    def handle_relionstyle_jobfiles(self, jtype, longname):
        """Input files from relion have different fields from the pipeliner version"""

        if not self.joboptions[f"fn_{jtype}s"].get_string():
            relionstyle_names = [
                f"fn_{jtype}1",
                f"fn_{jtype}2",
                f"fn_{jtype}3",
                f"fn_{jtype}4",
                f"{longname} STAR file 1:",
                f"{longname} STAR file 2:",
                f"{longname} STAR file 3:",
                f"{longname} STAR file 4:",
            ]
            the_files = [self.raw_options.get(x) for x in relionstyle_names]
            the_files = list(filter(lambda x: x not in [None, ""], the_files))
            self.joboptions[f"fn_{jtype}s"].value = ":::".join(the_files)

    def general_metadata_gathering(self, filetype):

        metadata_dict = {}

        starfile = DataStarFile(
            os.path.join(self.output_dir, "join_" + filetype + ".star")
        )
        nOpticsGroups = starfile.count_block("optics")
        metadata_dict["NumberOfOpticsGroups"] = nOpticsGroups
        if filetype == "movies":
            metadata_dict["NumberOfMovies"] = starfile.count_block("movies")
        if filetype == "mics":
            metadata_dict["NumberOfMicrographs"] = starfile.count_block("micrographs")
        if filetype == "particles":
            metadata_dict["NumberOfParticles"] = starfile.count_block("particles")

        return metadata_dict


class RelionJoinStarMovies(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_MOVIES_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionJoinStarJob.__init__(self)
        self.jobinfo.display_name = "RELION join movie STAR files"

        self.jobinfo.short_desc = "Join multiple movie STAR files into a single file"

        # TODO: This should really by a MultiInputNodeJobOption, but that type doesn't
        #  exist yet
        self.joboptions["fn_movs"] = MultiFileNameJobOption(
            label="Movie STAR files:",
            node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR file", EXT_STARFILE),
            help_text="The micrograph movie STAR files to be combined.",
        )
        self.get_runtab_options(addtl_args=True)
        self.set_joboption_order(["fn_movs"])

    def get_commands(self):
        self.handle_relionstyle_jobfiles("mov", "Movie")
        command = self.make_join_command(
            self.joboptions["fn_movs"].get_list(), "movies"
        )
        return [command]

    def prepare_onedep_data(self):
        movfile = os.path.join(self.output_dir, "join_movies.star")
        return prepare_empiar_raw_mics(movfile)

    def gather_metadata(self):
        metadata = self.general_metadata_gathering("movies")
        return metadata

    def create_results_display(self):
        odir = os.path.join(self.output_dir, "Thumbnails")
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="movies",
                column="_rlnMicrographMovies",
                outputdir=odir,
                nimg=4,
            )
        ]

    def add_compatibility_joboptions(self):
        self.add_common_compatibility_joboptions(jtype="mov")


class RelionJoinStarMicrographs(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_MICS_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionJoinStarJob.__init__(self)
        self.jobinfo.display_name = "RELION join micrograph STAR files"

        self.jobinfo.short_desc = (
            "Join multiple micrograph STAR files into a single file"
        )

        # TODO: This should really by a MultiInputNodeJobOption, but that type doesn't
        #  exist yet
        self.joboptions["fn_mics"] = MultiFileNameJobOption(
            label="Micrograph STAR files:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="The the micrograph STAR files to be combined.",
        )
        self.get_runtab_options(addtl_args=True)
        self.set_joboption_order(["fn_mics"])

    def get_commands(self):
        self.handle_relionstyle_jobfiles("mic", "Micrograph")
        command = self.make_join_command(self.joboptions["fn_mics"].get_list(), "mics")
        return [command]

    # needs to return an EMPIAR correctedmics obj
    def prepare_onedep_data(self):
        mpfile = os.path.join(self.output_dir, "join_mics.star")
        return prepare_empiar_mics_parts(mpfile, is_parts=False, is_cor_parts=False)

    def gather_metadata(self):
        metadata = self.general_metadata_gathering("mics")
        return metadata

    def create_results_display(self):
        odir = os.path.join(self.output_dir, "Thumbnails")
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="micrographs",
                column="_rlnMicrographName",
                outputdir=odir,
            )
        ]

    def add_compatibility_joboptions(self):
        self.add_common_compatibility_joboptions(jtype="mic")


class RelionJoinStarParticles(RelionJoinStarJob):

    PROCESS_NAME = JOINSTAR_PARTS_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionJoinStarJob.__init__(self)
        self.jobinfo.display_name = "RELION join particle STAR files"

        self.jobinfo.short_desc = "Join multiple particle STAR files into a single file"

        # TODO: This should really by a MultiInputNodeJobOption, but that type doesn't
        #  exist yet
        self.joboptions["fn_parts"] = MultiFileNameJobOption(
            label="Particle STAR files:",
            node_type=NODE_PARTICLESDATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text="The particle STAR files to be combined.",
        )

        self.get_runtab_options(addtl_args=True)
        self.set_joboption_order(["fn_parts"])

    def get_commands(self):
        self.handle_relionstyle_jobfiles("part", "Particle")
        command = self.make_join_command(
            self.joboptions["fn_parts"].get_list(), "particles"
        )
        return [command]

    # needs to return an EMPIAR Particles obj
    def prepare_onedep_data(self):
        mpfile = os.path.join(self.output_dir, "join_particles.star")
        return prepare_empiar_mics_parts(mpfile, is_parts=False, is_cor_parts=False)

    def gather_metadata(self):
        metadata = self.general_metadata_gathering("particles")
        return metadata

    def create_results_display(self):
        odir = os.path.join(self.output_dir, "Thumbnails")
        return [
            mini_montage_from_starfile(
                starfile=self.output_nodes[0].name,
                block="particles",
                column="_rlnImageName",
                outputdir=odir,
                nimg=20,
            )
        ]

    def add_compatibility_joboptions(self):
        self.add_common_compatibility_joboptions(jtype="part")
