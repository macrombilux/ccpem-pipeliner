#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    InputNodeJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.node_factory import create_node


class RelionFlipMap(RelionJob):
    OUT_DIR = "Flip"
    PROCESS_NAME = "relion.map_utilities.flip_handedness"

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Flip map handedness"
        self.jobinfo.short_desc = "Flip the handedness of a map"

        self.jobinfo.long_desc = (
            "Biological macromolecules are chiral! Reconstruction from 2D projections"
            " does not preserve chiral information and may result in a map with"
            " incorrect handedness. Use this job to flip the handedness of a map."
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to flip",
            is_required=True,
            default_value="",
        )

    def get_commands(self) -> List[List[str]]:
        inmap = self.joboptions["input_map"].get_string(True, "Input map not found")
        outfile_name = os.path.splitext(os.path.basename(inmap))[0] + "_flipped.mrc"
        outfile_path = os.path.join(self.output_dir, outfile_name)
        self.output_nodes.append(create_node(outfile_path, NODE_DENSITYMAP))

        command = [
            "relion_image_handler",
            "--i",
            inmap,
            "--invert_hand",
            "--o",
            outfile_path,
        ]

        return [command]

    def create_results_display(self) -> list:
        inmap = self.joboptions["input_map"].get_string()
        outfile_name = os.path.splitext(os.path.basename(inmap))[0] + "_flipped.mrc"
        outfile_path = os.path.join(self.output_dir, outfile_name)
        return [
            make_map_model_thumb_and_display(
                title="Flipped map",
                outputdir=self.output_dir,
                maps=[outfile_path],
                start_collapsed=False,
            )
        ]
