#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os

from pipeliner.jobs.relion.relion_job import relion_program
from pipeliner import user_settings
from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FileNameJobOption,
    PathJobOption,
    files_exts,
    EXT_RELION_HALFMAP,
    EXT_MRC_MAP,
    BooleanJobOption,
    FloatJobOption,
)

from pipeliner.data_structure import (
    LOCALRES_DIR,
    LOCALRES_OWN_NAME,
    LOCALRES_RESMAP_NAME,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_IMAGE3D,
    NODE_MASK3D,
    NODE_LOGFILE,
    NODE_MICROSCOPEDATA,
)
from pipeliner.node_factory import create_node
from pipeliner.starfile_handler import JobStar
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)
from pipeliner.utils import get_job_script


class RelionLocalResJob(RelionJob):
    OUT_DIR = LOCALRES_DIR

    def __init__(self):
        super().__init__()
        self.jobinfo.long_desc = (
            "The estimated resolution from the post-processing program is a global"
            " estimate. However, a single number cannot describe the variations in"
            " resolution that are often observed in reconstructions of macromolecular"
            " complexes"
        )

        self.joboptions["fn_in"] = InputNodeJobOption(
            label="One of the 2 unfiltered half-maps:",
            node_type=NODE_DENSITYMAP,
            node_kwds=["halfmap"],
            default_value="",
            directory="",
            pattern=files_exts("Halfmap file", EXT_RELION_HALFMAP),
            help_text=(
                "Provide one of the two unfiltered half-reconstructions that were"
                " output upon convergence of a 3D auto-refine run."
            ),
            is_required=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Calibrated pixel size (A)",
            default_value=1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Provide the final, calibrated pixel size in Angstroms. This value may"
                " be different from the pixel-size used thus far, e.g. when you have"
                " recalibrated the pixel size using the fit to a PDB model. The X-axis"
                " of the output FSC plot will use this calibrated value."
            ),
            is_required=True,
        )

        self.joboptions["fn_mask"] = InputNodeJobOption(
            label="User-provided solvent mask:",
            node_type=NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("Mask MRC file", EXT_MRC_MAP),
            help_text=(
                "Provide a mask with values between 0 and 1 around all domains of the"
                " complex. ResMap uses this mask for local resolution calculation."
                " RELION does NOT use this mask for calculation, but makes a histogram"
                " of local resolution within this mask."
            ),
            in_continue=True,
            is_required=True,
        )

    def relion_back_compatibility_joboptions(self, do_resmap=False, do_own=False):
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_resmap_locres"] = BooleanJobOption(
            label="Use ResMap?",
            default_value=do_resmap,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_relion_locres"] = BooleanJobOption(
            label="Use Relion?",
            default_value=do_own,
            jobop_group="Relion compatibility options",
        )

        resmap_exe = user_settings.get_resmap_executable()
        self.joboptions["fn_resmap"] = PathJobOption(
            label="ResMap executable:",
            help_text=(
                "Location of the ResMap executable. You can control the default"
                " of this field by setting environment variable"
                " PIPELINER_RESMAP_EXECUTABLE. Note that the ResMap wrapper"
                " cannot use MPI."
            ),
            default_value=resmap_exe,
        )

    def common_commands(self):
        fn_half1 = self.joboptions["fn_in"].get_string(
            True, "Empty field for input half-map"
        )
        if "half1" not in os.path.basename(fn_half1):
            raise ValueError("Cannot find 'half' substring in the input filename")

        fn_half2 = fn_half1.replace("half1", "half2")
        return fn_half1, fn_half2

    def additional_args(self):
        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()


class RelionLocalResResMap(RelionLocalResJob):
    PROCESS_NAME = LOCALRES_RESMAP_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionLocalResJob.__init__(self)
        self.jobinfo.display_name = "RELION ResMap"

        self.jobinfo.short_desc = "Calculate local resolution with ResMap"
        self.jobinfo.long_desc += (
            ". Local resolution is calculated using the program ResMap"
        )
        self.jobinfo.references.append(
            Ref(
                authors=["Kucukelbi A", "Sigworth FJ", "Tagare HD"],
                title="Quantifying the local resolution of cryo-EM density maps.",
                journal="Nature methods",
                year="2014",
                volume="11",
                issue="1",
                pages="63–65",
                doi="10.1038/nmeth.2727",
            )
        )

        self.jobinfo.programs = [ExternalProgram(user_settings.get_resmap_executable())]

        self.joboptions["pval"] = FloatJobOption(
            label="P-value:",
            default_value=0.05,
            suggested_min=0.0,
            suggested_max=1.0,
            step_value=0.01,
            help_text=(
                "This value is typically left at 0.05. If you change it, report the"
                " modified value in your paper!"
            ),
            is_required=True,
        )

        self.joboptions["minres"] = FloatJobOption(
            label="Highest resolution (A):",
            default_value=0.0,
            suggested_min=0.0,
            suggested_max=10.0,
            step_value=0.1,
            help_text=(
                "ResMaps minRes parameter. By default (0), the program will start at"
                " just above 2x the pixel size"
            ),
            is_required=True,
        )

        self.joboptions["maxres"] = FloatJobOption(
            label="Lowest resolution (A):",
            default_value=0.0,
            suggested_min=0.0,
            suggested_max=10.0,
            step_value=0.1,
            help_text=(
                "ResMaps maxRes parameter. By default (0), the program will stop at 4x"
                " the pixel size"
            ),
            is_required=True,
        )

        self.joboptions["stepres"] = FloatJobOption(
            label="Resolution step size (A)",
            default_value=1.0,
            suggested_min=0.1,
            suggested_max=3,
            step_value=0.1,
            help_text="ResMaps stepSize parameter.",
            is_required=True,
        )

        self.make_additional_args()

    def get_commands(self):
        self.joboptions["fn_mask"].get_string(
            True,
            "ERROR: Please provide an input mask for ResMap local-resolution"
            " estimation.",
        )

        fn_half1, fn_half2 = self.common_commands()
        hm_command = [
            "python3",
            get_job_script("relion/make_localres_halfmap_symlinks.py"),
            "--hm1",
            fn_half1,
            "--hm2",
            fn_half2,
            "--output_dir",
            self.output_dir,
        ]

        self.output_nodes.append(
            create_node(
                self.output_dir + "half1_resmap.mrc",
                NODE_IMAGE3D,
                ["resmap", "localresmap"],
            )
        )
        self.command = [user_settings.get_resmap_executable()]
        self.command += [
            "--maskVol=" + self.joboptions["fn_mask"].get_string(),
            "--noguiSplit",
            self.output_dir + "half1.mrc",
            self.output_dir + "half2.mrc",
            "--vxSize=" + self.joboptions["angpix"].get_string(),
            "--pVal=" + self.joboptions["pval"].get_string(),
            "--minRes=" + self.joboptions["minres"].get_string(),
            "--maxRes=" + self.joboptions["maxres"].get_string(),
            "--stepRes=" + self.joboptions["stepres"].get_string(),
        ]
        self.additional_args()
        commands = [hm_command, self.command]
        return commands

    def add_compatibility_joboptions(self):
        self.relion_back_compatibility_joboptions(do_resmap=True)

    # TODO: Need to write a results display function for this, need to see
    #   what the results look like first...


class RelionLocalResOwn(RelionLocalResJob):
    PROCESS_NAME = LOCALRES_OWN_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionLocalResJob.__init__(self)
        self.jobinfo.programs = [relion_program("relion_postprocess")]
        self.jobinfo.display_name = "RELION LocalRes"

        self.jobinfo.short_desc = "Calculate local resolution using RELION"
        self.jobinfo.long_desc += (
            ". Relion uses a post-processing-like procedure with a soft spherical mask"
            " that is moved around the entire map"
        )

        self.joboptions["adhoc_bfac"] = FloatJobOption(
            label="User-provided B-factor:",
            default_value=-100,
            suggested_min=-500,
            suggested_max=0,
            step_value=-25,
            help_text=(
                "Probably, the overall B-factor as was estimated in the postprocess is"
                " a useful value for here. Use negative values for sharpening. Be"
                " careful: if you over-sharpen your map, you may end up interpreting"
                " noise for signal!"
            ),
            is_required=True,
        )

        self.joboptions["fn_mtf"] = FileNameJobOption(
            label="MTF of the detector (STAR file)",
            default_value="",
            pattern="STAR Files (*.star)",
            node_type=NODE_MICROSCOPEDATA,
            node_kwds=["mtf"],
            directory=".",
            help_text=(
                "The MTF of the detector is used to complement the user-provided"
                " B-factor in the sharpening. If you don't have this curve, you can"
                " leave this field empty."
            ),
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)

    def get_commands(self):

        fn_half1 = self.common_commands()[0]

        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi > 1:
            self.command = self.get_mpi_command() + ["relion_postprocess_mpi"]
        else:
            self.command = ["relion_postprocess"]

        self.command += [
            "--locres",
            "--i",
            fn_half1,
            "--o",
            self.output_dir + "relion",
            "--angpix",
            self.joboptions["angpix"].get_string(),
        ]

        # these were commented out in the relion code
        # locres_sampling = self.joboptions["locres_sampling"].get_string()
        # command += " --locres_sampling " + locres_sampling
        # randomize_at = self.joboptions["randomize_at"].get_string()
        # command += " --locres_randomize_at " + randomize_at

        self.command += ["--adhoc_bfac", self.joboptions["adhoc_bfac"].get_string()]

        fn_mtf = self.joboptions["fn_mtf"].get_string()
        if len(fn_mtf) > 0:
            self.command += ["--mtf", fn_mtf]

        fn_mask = self.joboptions["fn_mask"].get_string()
        if len(fn_mask) > 0:
            self.command += ["--mask", fn_mask]

        self.output_nodes.append(
            create_node(
                self.output_dir + "histogram.pdf",
                NODE_LOGFILE,
                ["relion", "localres"],
            )
        )

        self.output_nodes.append(
            create_node(
                self.output_dir + "relion_locres_filtered.mrc",
                NODE_DENSITYMAP,
                ["relion", "localresfiltered"],
            )
        )

        self.output_nodes.append(
            create_node(
                self.output_dir + "relion_locres.mrc",
                NODE_IMAGE3D,
                ["relion", "localresmap"],
            )
        )

        self.additional_args()
        commands = [self.command]
        return commands

    def add_compatibility_joboptions(self):
        self.relion_back_compatibility_joboptions(do_own=True)

    def gather_metadata(self):
        jobstar = JobStar(os.path.join(self.output_dir, "job.star"))
        jobops = jobstar.all_options_as_dict()
        metadata_dict = {
            "Local res type": "Relion",
            "ad hoc b-factor applied": jobops["adhoc_bfac"],
        }

        if jobops["fn_mask"] is not None:
            metadata_dict["Mask"] = jobops["fn_mask"]

        if jobops["fn_mtf"] is not None:
            metadata_dict["Mtf file"] = jobops["fn_mtf"]

        runout_file = os.path.join(self.output_dir, "run.out")
        with open(runout_file, "r") as runout:
            runout_data = runout.readlines()
        for line in runout_data:
            if "+ randomize phases beyond:" in line:
                metadata_dict["Phases randomized beyond"] = line.split()[-1]

        return metadata_dict

    def create_results_display(self):
        # # make the hisogram
        logstar = os.path.join(self.output_dir, "relion_locres_fscs.star")
        runout = os.path.join(self.output_dir, "run.out")
        with open(runout, "r") as ro:
            loglines = ro.readlines()
        hbin, hval = [], []
        for line in loglines:
            if line[0] == "[" and len(line.split()) == 3:
                val, count = line.split()[0], line.split()[2]
                if val != "[-INF,":
                    hbin.append(float(val[1:].strip(",")))
                hval.append(float(count))
        hbin.append(hbin[-1] + 0.1)
        hbin = [hbin[0] - 0.1] + hbin
        histo = create_results_display_object(
            "histogram",
            title="Local resolution distribution",
            xlabel="Resolution",
            ylabel="Number of voxels",
            associated_data=[logstar],
            bins=hval,
            bin_edges=hbin,
        )
        mapfile = os.path.join(self.output_dir, "relion_locres_filtered.mrc")
        the_map = make_map_model_thumb_and_display(
            maps=[mapfile],
            outputdir=self.output_dir,
            title="Local resolution filtered map",
        )

        return [histo, the_map]
