#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    InputNodeJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.node_factory import create_node


class RelionApplySymmetry(RelionJob):
    OUT_DIR = "Symmetrize"
    PROCESS_NAME = "relion.map_utilities.apply_symmetry"

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Apply symmetry"
        self.jobinfo.short_desc = "Apply symmetry to a map"

        self.jobinfo.long_desc = "Apply the chosen symmetry to a map"

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to apply B-factor correction to",
            is_required=True,
            default_value="",
        )

        self.joboptions["sym"] = MultipleChoiceJobOption(
            label="Symmetry type",
            choices=[
                "Cyclic (C)",
                "Dihedral (D)",
                "Tetrahedral (T)",
                "Octahedral (O)",
                "Icosahedral (I)",
            ],
            default_value_index=0,
            help_text="Choose the type of symmetry to apply",
            in_continue=True,
        )

        self.joboptions["fold"] = IntJobOption(
            label="Symmetry fold",
            default_value=1,
            required_if=[("sym", "=", "Cyclic (C)"), ("sym", "=", "Dihedral (D)")],
            deactivate_if=[
                ("sym", "=", "Tetrahedral (T)"),
                ("sym", "=", "Octahedral (O)"),
                ("sym", "=", "Icosahedral (I)"),
            ],
        )

    def get_commands(self) -> List[List[str]]:
        symtype = self.joboptions["sym"].get_string()[0]
        symfold = self.joboptions["fold"].get_number()
        sym = symtype + str(symfold) if symtype in ["C", "D"] else symtype

        inmap = self.joboptions["input_map"].get_string(True, "Input map not found")
        outfile_name = (
            os.path.splitext(os.path.basename(inmap))[0] + f"_{sym.lower()}sym.mrc"
        )
        outfile_path = os.path.join(self.output_dir, outfile_name)
        self.output_nodes.append(
            create_node(outfile_path, NODE_DENSITYMAP, [f"{sym.lower()}sym"])
        )

        command = [
            "relion_image_handler",
            "--i",
            inmap,
            "--sym",
            sym,
            "--o",
            outfile_path,
        ]

        return [command]

    def create_results_display(self) -> list:
        symtype = self.joboptions["sym"].get_string()[0]
        symfold = self.joboptions["fold"].get_number()
        sym = symtype + str(symfold) if symtype in ["C", "D"] else symtype
        inmap = self.joboptions["input_map"].get_string()
        outfile_name = (
            os.path.splitext(os.path.basename(inmap))[0] + f"_{sym.lower()}sym.mrc"
        )
        outfile_path = os.path.join(self.output_dir, outfile_name)
        return [
            make_map_model_thumb_and_display(
                title=f"Map with {sym} symmetry applied",
                outputdir=self.output_dir,
                maps=[outfile_path],
                start_collapsed=False,
            )
        ]
