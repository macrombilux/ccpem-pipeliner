#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_MASK3D


class LafterJob(PipelinerJob):
    """Local de-noising of cryo-EM maps"""

    PROCESS_NAME = "lafter.postprocess.local_denoising"
    OUT_DIR = "Lafter"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "LAFTER"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.short_desc = "Local de-noising of SPA reconstructions"
        self.jobinfo.long_desc = "Local de-noising of cryo-EM maps."
        self.jobinfo.documentation = (
            "https://github.com/StructuralBiology-ICLMedicine/lafter"
        )
        self.jobinfo.programs = [
            ExternalProgram("lafter"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["K. Ramlaul", "C. M. Palmer", "C. H. S. Aylett"],
                title=(
                    "A Local Agreement Filtering Algorithm for Transmission "
                    "EM Reconstructions"
                ),
                journal="Journal of Structural Biology",
                year="2019",
                volume="205",
                issue="1",
                pages="30-40",
                doi="10.1016/j.jsb.2018.11.011",
            ),
        ]
        self.joboptions["half_map_1"] = InputNodeJobOption(
            label="Input map 1 (half map 1)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="First half map for denoising (mrc format)",
            is_required=True,
        )
        self.joboptions["half_map_2"] = InputNodeJobOption(
            label="Input map 2 (half map 2)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Second half map for denoising (mrc format)",
            is_required=True,
        )
        self.joboptions["mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Mask to define particle area (mrc mode 2 format)",
        )
        self.joboptions["particle_diameter"] = FloatJobOption(
            label="Particle diameter",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=1000,
            hard_min=0,
            step_value=0.1,
            help_text="Particle diameter in voxels (if no mask available)",
            required_if=[("mask", "=", False)],
        )
        self.joboptions["sharp"] = BooleanJobOption(
            label="Additional sharpening",
            default_value=False,
            help_text=("Additional sharpening (usually not necessary)"),
            jobop_group="Advanced options",
        )
        self.joboptions["fsc"] = FloatJobOption(
            label="FSC cut-off threshold",
            default_value=-1,
            suggested_min=0.143,
            suggested_max=0.5,
            step_value=0.001,
            help_text="FSC cut-off threshold (default 0.143)",
            jobop_group="Advanced options",
        )
        self.joboptions["downsample"] = BooleanJobOption(
            label="No over-sampling",
            default_value=False,
            help_text=("Do not over-sample output maps"),
            jobop_group="Advanced options",
        )
        self.joboptions["overfitting"] = BooleanJobOption(
            label="Compensate for overfitting",
            default_value=False,
            help_text=(
                "Attempt to mitigate against the effects of overfitting from "
                "3D refinement"
            ),
            jobop_group="Advanced options",
        )
        self.get_runtab_options()

    def get_commands(self):
        # lafter --v1 3488_run_half1_class001_unfil.mrc
        #   --v2 3488_run_half2_class001_unfil.mrc --particle_diameter 75.0

        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get inputs
        half_map_1 = self.joboptions["half_map_1"].get_string(
            True, "Input file missing"
        )
        half_map_2 = self.joboptions["half_map_2"].get_string(
            True, "Input file missing"
        )
        mask = self.joboptions["mask"].get_string()
        particle_diameter = self.joboptions["particle_diameter"].get_number()
        fsc = self.joboptions["fsc"].get_number()
        sharp = self.joboptions["sharp"].get_boolean()
        downsample = self.joboptions["downsample"].get_boolean()
        overfitting = self.joboptions["overfitting"].get_boolean()

        # Create command arguments
        command = [self.jobinfo.programs[0].command]
        command += [
            "--v1",
            os.path.relpath(half_map_1, self.working_dir),
            "--v2",
            os.path.relpath(half_map_2, self.working_dir),
        ]
        if mask != "":
            command += ["--mask", os.path.relpath(mask, self.working_dir)]
        if particle_diameter > 0:
            command += ["--particle_diameter", str(particle_diameter)]
        if fsc > 0:
            command += ["--fsc", str(particle_diameter)]
        if sharp:
            command += ["--sharp"]
        if downsample:
            command += ["--downsample"]
        if overfitting:
            command += ["--overfitting"]

        # Output map
        output_file = os.path.join(self.output_dir, "LAFTER_filtered.mrc")
        self.output_nodes.append(
            create_node(output_file, NODE_DENSITYMAP, ["lafter", "denoise"])
        )
        return [command]

    def gather_metadata(self):
        # XXX to do
        metadata_dict = {}
        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        output_map = os.path.join(self.output_dir, "LAFTER_filtered.mrc")
        maps = [output_map]
        return [
            make_map_model_thumb_and_display(
                maps=maps,
                maps_opacity=[0.5],
                models=[],
                title="Lafter denoised map",
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
