#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
from glob import glob
from pandas import read_csv

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FileNameJobOption,
    StringJobOption,
    FloatJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS, NODE_SEQUENCE
from pipeliner.utils import get_job_script


class EMPlacement(PipelinerJob):
    """
    Phaser EM Placement Job
    Fits model into cryoEM map.
    """

    PROCESS_NAME = "em_placement.atomic_model_fit"
    OUT_DIR = "EMPlacement"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "em_placement"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.short_desc = "Fit model in map with Phaser em_placement"
        self.jobinfo.long_desc = (
            "Molrep fits one or more atomic models into a cryoEM map by the technique"
            " of Molecular Replacement. The spherically averaged phased translation "
            "function locates the model in the cryoEM map using a spherically averaged"
            " calculated map, then determines its optimal orientation and refines the "
            "position. The rotation translation function is the more traditional "
            "molecular replacement method. The latter is generally faster but less "
            "sensitive."
        )
        self.jobinfo.documentation = (
            "https://gitlab.developers.cam.ac.uk/scm/haematology/readgroup/phasertng"
        )
        self.jobinfo.programs = [
            ExternalProgram("phaser"),
            ExternalProgram("phenix.voyager.em_placement"),  # For initial testing
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "A.J.McCoy",
                    "R.W.Grosse-Kunstleve",
                    "P.D.Adams",
                    "M.D.Winn",
                    "L.C.Storoni",
                    "R.J.Read",
                ],
                title="Phaser Crystallographic Software",
                journal="2007",
                year="2007",
                volume="40",
                issue="1",
                pages="658-674",
                doi="10.1107/S0021889807021206",
            ),
        ]

        self.joboptions["model"] = InputNodeJobOption(
            label="Model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            directory="",
            help_text="Initial atomic model",
            is_required=True,
        )

        self.joboptions["sequence"] = FileNameJobOption(
            label="Sequence composition",
            pattern=files_exts("Sequence", [".txt", ".fasta"]),
            node_type=NODE_SEQUENCE,
            default_value="",
            directory="",
            help_text="Sequence file with composition for full reconstruction",
            is_required=True,
        )
        self.joboptions["full_map"] = InputNodeJobOption(
            label="Full map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Full map from 3D reconstruction",
            is_required=True,
        )

        self.joboptions["half_map_1"] = InputNodeJobOption(
            label="Half map 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Half map one from 3D recontruction",
            is_required=True,
        )

        self.joboptions["half_map_2"] = InputNodeJobOption(
            label="Half map 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Half map two from 3D recontruction",
            is_required=True,
        )

        self.joboptions["best_resolution"] = FloatJobOption(
            label="Best resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Best resolution in Angstrom of full map",
            is_required=True,
        )

        self.joboptions["symmetry"] = StringJobOption(
            label="Point group symmetry",
            default_value="None",
            help_text="Specify point group symmetry of input maps",
        )

        self.joboptions["starting_model_vrms"] = FloatJobOption(
            label="Starting model vrms",
            default_value=1.5,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text=(
                "1.0 should be used for an identical structure, typically 1.2 for "
                "an AlphaFold or similarly predicted structure or the expected "
                "rms for an experimentally derived homologue"
            ),
            is_required=True,
        )

        self.joboptions["molecule_name"] = StringJobOption(
            label="Molecule name",
            default_value="",
            help_text="Molecule name for output files",
        )

        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # CCP-EM gui creates:
        # phenix.voyager.em_placement
        #   map_or_model_file=3rkoM.pdb
        #   full_map=emd_12654.map
        #   half_map=emd_12654_half_map_1.map
        #   half_map=emd_12654_half_map_2.map
        #   best_resolution=3.8
        #   point_group_symmetry=C1
        #   sequence_composition=7nyu.seq
        #   molecule_name=3rkoM
        #   starting_model_vrms=1.5

        model_file = self.joboptions["model"].get_string(True, "Input file missing")
        full_map_file = self.joboptions["full_map"].get_string(
            True, "Input file missing"
        )
        half_map_1_file = self.joboptions["half_map_1"].get_string(
            True, "Input file missing"
        )
        half_map_2_file = self.joboptions["half_map_2"].get_string(
            True, "Input file missing"
        )
        best_resolution = self.joboptions["best_resolution"].get_number()
        symmetry = self.joboptions["symmetry"].get_string()
        sequence_file = self.joboptions["sequence"].get_string(
            True, "Input file missing"
        )
        molecule_name = self.joboptions["molecule_name"].get_string()
        if molecule_name == "":
            molecule_name = Path(model_file).stem
        starting_model_vrms = self.joboptions["starting_model_vrms"].get_number()

        phil_name = "em_placement.phil"

        config_command = [
            "python3",
            get_job_script("em_placement_config.py"),
            os.path.relpath(full_map_file, self.working_dir),
            os.path.relpath(half_map_1_file, self.working_dir),
            os.path.relpath(half_map_2_file, self.working_dir),
            str(best_resolution),
            symmetry,
            os.path.relpath(sequence_file, self.working_dir),
            molecule_name,
            os.path.relpath(model_file, self.working_dir),
            str(starting_model_vrms),
            phil_name,
        ]

        em_placement_command = ["phenix.voyager.em_placement", phil_name]
        commands = [config_command, em_placement_command]
        return commands

    def post_run_actions(self):
        # Rename output directory to remove time stamp name for predictable
        # output
        emplaced_dirs = os.path.join(self.output_dir, "emplaced_files*")
        emplaced_dir = glob(emplaced_dirs)[0]
        os.rename(emplaced_dir, os.path.join(self.output_dir, "emplaced_files"))

        # Find all PDB in split output directories and add as node type
        search_pdbs = os.path.join(self.output_dir, "emplaced_files/*.pdb")
        pdbs = glob(search_pdbs)
        for pdb in sorted(pdbs):
            self.output_nodes.append(
                create_node(pdb, NODE_ATOMCOORDS, ["em_placement"])
            )

    def gather_metadata(self):
        """
        em_placement generates 'output_table_docking.csv' which contains summary
        metadata.
        """
        # Find path, required as em_placement directory can not be specified
        for dir in os.listdir(self.output_dir):
            if dir.startswith("emplaced_files"):
                csv_path = os.path.join(
                    self.output_dir, dir, "output_table_docking.csv"
                )
                break
        else:
            raise FileNotFoundError("Could not find emplaced_files directory")

        # Read csv as dataframe and convert to dict
        docking_output = read_csv(csv_path)
        metadata_dict = docking_output.to_dict()
        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        models = [self.joboptions["model"].get_string()]
        for dir in os.listdir(self.output_dir):
            if dir.startswith("emplaced_files"):
                for pdb in sorted(glob(os.path.join(self.output_dir, dir, "*.pdb"))):
                    models.append(pdb)

        maps = [self.joboptions["full_map"].get_string()]
        return [
            make_map_model_thumb_and_display(
                maps=maps,
                maps_opacity=[0.5],
                models=models,
                title="em_placement fitted models",
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
