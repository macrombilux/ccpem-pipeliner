#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    FileNameJobOption,
    StringJobOption,
    JobOptionValidationResult,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.utils import run_subprocess
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_LIGANDDESCRIPTION, NODE_SEQUENCE
from pipeliner.node_factory import create_node


class AceDRGCreateLigand(PipelinerJob):
    PROCESS_NAME = "acedrg.create_ligand"
    OUT_DIR = "AceDRG"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "AceDRG"
        self.jobinfo.short_desc = "Create ligand dictionary"
        self.jobinfo.long_desc = "A stereo-chemical description generator for ligands."
        acedrg = ExternalProgram(
            command="acedrg", vers_com=["acedrg", "--version"], vers_lines=[8, 10, 11]
        )
        self.jobinfo.programs = [acedrg]
        self.version = "246"
        self.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Long F",
                    "Nicholls RA",
                    "Emsley P",
                    "GraZulis S",
                    "Merkys A",
                    "Vaitkus A",
                    "Murshudov GN",
                ],
                title="ACEDRG: A stereo-chemical description generator for ligands",
                journal="Acta Cryst. D",
                year="2017",
                volume="73",
                issue="1",
                pages="112-122",
                doi="10.1107/S2059798317000067",
            )
        ]
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/acedrg/acedrg.html"
        )
        self.joboptions["smiles_string"] = StringJobOption(
            label="SMILES string",
            default_value="",
            help_text="Simplified Molecular Input Line Entry System text",
            is_required=False,
            deactivate_if=[
                ("smiles_file", "!=", ""),
                ("mol_file", "!=", ""),
                ("mmcif_file", "!=", ""),
            ],
        )
        self.joboptions["smiles_file"] = FileNameJobOption(
            label="OR: SMILES file",
            default_value="",
            pattern=files_exts("SMILES", [".smi"]),
            directory=".",
            help_text="Simplified Molecular Input Line Entry System text file",
            is_required=False,
            deactivate_if=[
                ("smiles_string", "!=", ""),
                ("mol_file", "!=", ""),
                ("mmcif_file", "!=", ""),
            ],
            node_type=NODE_SEQUENCE,
            node_kwds=["smiles"],
        )
        self.joboptions["mol_file"] = FileNameJobOption(
            label="OR: MOL file",
            default_value="",
            pattern=files_exts("MDL", [".mol"]),
            directory=".",
            help_text="Input MDL Mol file",
            is_required=False,
            deactivate_if=[
                ("smiles_string", "!=", ""),
                ("smiles_file", "!=", ""),
                ("mmcif_file", "!=", ""),
            ],
            node_type=NODE_ATOMCOORDS,
        )
        self.joboptions["mmcif_file"] = FileNameJobOption(
            label="OR: mmCIF file",
            default_value="",
            pattern=files_exts("mmCIF", [".cif", ".mmcif"]),
            directory=".",
            help_text="Input mmCIF file",
            is_required=False,
            deactivate_if=[
                ("smiles_string", "!=", ""),
                ("smiles_file", "!=", ""),
                ("mol_file", "!=", ""),
            ],
            node_type=NODE_ATOMCOORDS,
        )
        self.joboptions["monomer_code"] = StringJobOption(
            label="Monomer Code",
            default_value="DRG",
            help_text="Three-letter code for output monomer (e.g. GLU)",
            is_required=False,
        )
        self.get_runtab_options()

    def parameter_validation(self):
        """
        Check only one restraint definition is provided
        """
        smiles_string = self.joboptions["smiles_string"].get_string()
        smiles_file = self.joboptions["smiles_file"].get_string()
        mol_file = self.joboptions["mol_file"].get_string()
        mmcif_file = self.joboptions["mmcif_file"].get_string()

        if [smiles_string, smiles_file, mol_file, mmcif_file].count("") != 3:
            return [
                JobOptionValidationResult(
                    "error",
                    [
                        self.joboptions["smiles_string"],
                        self.joboptions["smiles_file"],
                        self.joboptions["mol_file"],
                        self.joboptions["mmcif_file"],
                    ],
                    "Please input only one ligand description",
                ),
            ]

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        command = [self.jobinfo.programs[0].command]

        # Get parameters
        smiles_string = self.joboptions["smiles_string"].get_string()
        smiles_file = self.joboptions["smiles_file"].get_string()
        mol_file = self.joboptions["mol_file"].get_string()
        mmcif_file = self.joboptions["mmcif_file"].get_string()
        monomer_code = self.joboptions["monomer_code"].get_string()

        # Set command
        if smiles_string:
            command.append(f"--smi={smiles_string}")
        elif smiles_file:
            command.append(f"--smi={os.path.relpath(smiles_file, self.working_dir)}")
        elif mol_file:
            command.append(f"--mol={os.path.relpath(mol_file, self.working_dir)}")
        elif mmcif_file:
            command.append(f"--mmcif={os.path.relpath(mmcif_file, self.working_dir)}")
        else:
            raise ValueError("No input file selected")

        command.append(f"--res={monomer_code}")
        out_root = f"{monomer_code}_acedrg"
        command.append(f"--out={out_root}")

        # Set AceDRG restraints output
        # <monomer_code>_acedrg.cif
        cif = f"{out_root}.cif"
        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, cif), NODE_LIGANDDESCRIPTION, ["acedrg"]
            )
        )
        # Set AceDRG structures output
        # <monomer_code>_acedrg.pdb
        cif = f"{out_root}.pdb"
        self.output_nodes.append(
            create_node(os.path.join(self.output_dir, cif), NODE_ATOMCOORDS, ["acedrg"])
        )
        commands = [command]
        return commands

    def post_run_actions(self):
        run_subprocess(["rm", "-r", "AcedrgOut_TMP/"])

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        ligand = self.joboptions["monomer_code"].get_string() + "_acedrg.pdb"
        output_model = os.path.join(self.output_dir, ligand)
        return [
            make_map_model_thumb_and_display(
                maps=[],
                maps_opacity=[],
                models=[output_model],
                title=ligand,
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
