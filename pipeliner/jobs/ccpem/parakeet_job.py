#!/usr/bin/env python3

#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import os
import contextlib
from typing import Dict, List

import yaml
import numpy as np

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
from pipeliner.job_options import (
    FileNameJobOption,
    IntJobOption,
    StringJobOption,
    FloatJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    MultiStringJobOption,
    MultiFileNameJobOption,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.display_tools import mini_montage_from_starfile
from pipeliner.utils import get_job_script
from pipeliner.nodes import (
    Node,
    NODE_PROCESSDATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_ATOMCOORDS,
)


class ParakeetConfig:
    def __init__(self, yaml_filename="config.yaml") -> None:
        # initialise class to hold a dictionary with the same
        # values as parakeet.config.new --full True
        self.config_dict = self.initialise_config_dict()

        self.yaml_filename = yaml_filename

    def read_yaml(self, infile: str) -> None:
        """Read a Parakeet yaml config file

        Args:
            infile (str, optional): Parakeet config file

        Returns:
            dict: Dictionary containing Parakeet configuration
        """
        with open(infile, "r") as read_file:
            self.config_dict = yaml.safe_load(read_file)

    def write_yaml(self) -> None:
        with open(self.yaml_filename, "w") as writefile:
            yaml.dump(self.config_dict, writefile)

    def update_cluster(self, cluster):
        self.config_dict["cluster"] = cluster

    def update_device(self, device):
        self.config_dict["device"] = device

    def update_microscope(self, microscope):
        self.config_dict["microscope"] = microscope

    def update_sample(self, sample):
        self.config_dict["sample"] = sample

    def update_scan(self, scan):
        self.config_dict["scan"] = scan

    def update_simulation(self, simulation):
        self.config_dict["simulation"] = simulation

    def initialise_config_dict(self):
        config_dict = {}

        # cluster
        config_dict["cluster"] = {"max_workers": 1, "method": None}
        # device
        config_dict["device"] = "gpu"

        # microscope
        config_dict["microscope"] = {
            "beam": {
                "acceleration_voltage_spread": 8.0e-07,
                "electrons_per_angstrom": 45.0,
                "energy": 300.0,
                "energy_spread": 2.66e-06,
                "phi": 0,
                "illumination_semiangle": 0.02,
                "theta": 0,
            },
            "detector": {
                "dqe": False,
                "nx": 1000,
                "ny": 1000,
                "origin": [0, 0],
                "pixel_size": 1.0,
            },
            "lens": {
                "c_10": -20000,
                "c_12": 0,
                "c_21": 0,
                "c_23": 0,
                "c_30": 2.7,
                "c_32": 0,
                "c_34": 0,
                "c_41": 0,
                "c_43": 0,
                "c_45": 0,
                "c_50": 0,
                "c_52": 0,
                "c_54": 0,
                "c_56": 0,
                "c_c": 2.7,
                "current_spread": 3.3e-07,
                "phi_12": 0,
                "phi_21": 0,
                "phi_23": 0,
                "phi_32": 0,
                "phi_34": 0,
                "phi_41": 0,
                "phi_43": 0,
                "phi_45": 0,
                "phi_52": 0,
                "phi_54": 0,
                "phi_56": 0,
            },
            "model": None,
            "phase_plate": False,
        }

        # sample
        config_dict["sample"] = {
            "box": [1000, 1000, 500],
            "centre": [500, 500, 250],
            "ice": None,
            "molecules": {
                "pdb": [
                    {
                        "id": "4v1w.pdb",
                        "instances": 1,
                    },
                ]
            },
            "shape": {
                "cube": {
                    "length": 1000.0,
                },
                "cuboid": {
                    "length_x": 1000.0,
                    "length_y": 1000.0,
                    "length_z": 500.0,
                },
                "cylinder": {
                    "length": 1000.0,
                    "radius": 500.0,
                    "offset_x": None,
                    "offset_z": None,
                    "axis": [0, 1, 0],
                },
                "margin": [0, 0, 0],
                "type": "cuboid",
            },
            "sputter": None,
        }

        # scan (not supported)
        config_dict["scan"] = {
            "angles": None,
            "axis": [0, 1, 0],
            "drift": None,
            "exposure_time": 1,
            "mode": "still",
            "num_images": 1,
            "positions": None,
            "start_angle": 0,
            "start_pos": 0,
            "step_angle": 0,
            "step_pos": "auto",
        }
        # scan->angles (not supported)
        # scan->axis
        # scan->exposure_time (not supported)
        # scan->mode
        # scan->num_images (not supported)
        # scan->positions (not supported)
        # scan->start_angle
        # scan->start_pos
        # scan->step_pos

        # simulation
        config_dict["simulation"] = {
            "division_thickness": 100,
            "ice": False,
            "inelastic_model": None,
            "margin": 100,
            "mp_loss_position": "peak",
            "mp_loss_width": 50,
            "padding": 100,
            "radiation_damage_model": False,
            "sensitivity_coefficient": 0.022,
            "slice_thickness": 3.0,
        }
        # simulation->division_thickness (not supported)
        # simulation->ice (fast ice)
        # simulation->inelastic_model
        # simulation->margin
        # simulation->mp_loss_position
        # simulation->mp_loss_width
        # simulation->padding
        # simulation->radiation_damage_model
        # simulation->sensitivity_coefficient
        # simulation->slice_thickness
        return config_dict


class ParakeetJob(PipelinerJob):
    """
    Parakeet job for ccpem-pipeliner
    """

    PROCESS_NAME = "parakeet.simulate"
    OUT_DIR = "Parakeet"

    # (required) Instantiate the object: define info about it and its input parameters
    def __init__(self):
        # super(self.__class__, self).__init__()
        super().__init__()
        self.jobinfo.display_name = "Parakeet micrograph simulation"
        self.jobinfo.version = "0.6.1"
        self.jobinfo.job_author = "Joel Greer"
        self.jobinfo.programs = [
            ExternalProgram(command="parakeet.sample.new"),
            ExternalProgram(command="parakeet.sample.add_molecules"),
            ExternalProgram(command="parakeet.metadata.export"),
            ExternalProgram(command="parakeet.simulate.exit_wave"),
            ExternalProgram(command="parakeet.simulate.optics"),
            ExternalProgram(command="parakeet.simulate.image"),
            ExternalProgram(command="parakeet.export"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Parkhurst James M.",
                    "Dumoux Maud",
                    "Basham Mark",
                    "Clare Daniel",
                    "Siebert C. Alistair",
                    "Varslot Trond",
                    "Kirkland Angus",
                    "Naismith James H.",
                    "Evans Gwyndaf ",
                ],
                title=(
                    "Parakeet: a digital twin software pipeline to assess the impact"
                    " of experimental parameters on tomographic reconstructions"
                    " for cryo-electron tomography"
                ),
                journal="Royal Society Open Biology",
                year="2021",
                volume="11",
                issue="10",
                pages="",
                doi="http://doi.org/10.1098/rsob.210160",
            )
        ]
        self.jobinfo.short_desc = "Single particle micrograph simulation"
        self.jobinfo.long_desc = (
            "Micrograph simulation. Requires Parakeet. Allows the generation"
            " of a sythetic single particle dataset which is created in"
            " a subdirectory of the job called 'Micrographs'. Users may either provide"
            " a Parakeet configuration YAML or provide individual parameters."
        )
        self.jobinfo.documentation = (
            "https://rosalindfranklininstitute.github.io/amplus-digital-twin/"
        )
        self.display_name = "Simulate synthetic micrograph(s) using Parakeet software"
        self.images = 0
        self.particles_per_image = 0

        # job options grouped into:
        # Main
        # Sample
        # Microscope
        # Simulation
        self.joboptions["use_conf_yaml"] = BooleanJobOption(
            label="Set Parakeet simulation parameters via YAML configuration?",
            default_value=True,
            help_text=(
                "If set to Yes, then parakeet will read configuration from the"
                " user-provided yaml file. Otherwise, specify the parameters"
                " using the options below"
            ),
            in_continue=False,
        )

        self.joboptions["conf"] = FileNameJobOption(
            label="Parakeet configuration file (YAML format):",
            pattern="",
            directory="",
            default_value="",
            help_text="Config YAML name",
            in_continue=False,
            is_required=False,
            required_if=[("use_conf_yaml", "=", True)],
            node_type=NODE_PROCESSDATA,
            node_kwds=["parakeet", "config"],
            deactivate_if=[("use_conf_yaml", "=", False)],
        )

        self.joboptions["keep_confs"] = BooleanJobOption(
            label="Keep the configuration file (YAML) for every image:",
            default_value=True,
            help_text=(
                "If set to Yes, outputs the Parakeet configuration file used"
                " to generate every micrograph to Micrographs directory."
                " Otherwise these are deleted."
            ),
            in_continue=False,
        )

        self.joboptions["opticsGroupName"] = StringJobOption(
            label="Optics group name",
            default_value="opticsGroup1",
            help_text="Name of optics group for the generated micrographs",
            in_continue=False,
            is_required=False,
        )

        self.joboptions["particles"] = IntJobOption(
            label="Total number of particles to simulate",
            default_value=1,
            suggested_min=1,
            suggested_max=10000000,
            step_value=1000,
            help_text=("Number of particles to generate micrographs for."),
            hard_min=0,
            is_required=True,
        )

        # sample->molecules->local->filenames
        self.joboptions["pdb_filepaths"] = MultiFileNameJobOption(
            label="The filename(s) of the atomic coordinates to use (*.pdb, *.cif)",
            default_value="",
            pattern="(*.pdb, *.cif)",
            directory="~/",
            help_text=(
                "The filename(s) of the atomic coordinates to use (*.pdb, *.cif)"
            ),
            in_continue=False,
            node_type=NODE_ATOMCOORDS,
            required_if=[("use_conf_yaml", "=", False)],
            deactivate_if=[("use_conf_yaml", "=", True)],
        )
        # sample->molecules->pdb->instances
        self.joboptions["pdb_instances"] = MultiStringJobOption(
            label=(
                "The number of particles of the corresponding molecule to simulate"
                " per micrograph"
            ),
            default_value="",
            help_text=(
                "The number of particles of corresponding molecule to generate per "
                " micrograph simulated. If only 1 particle per micrograph, it is"
                " placed centrally in the sample. If >1 is generated per micrograph,"
                " they are placed and oriented randomly in the sample."
            ),
            in_continue=False,
            required_if=[("use_conf_yaml", "=", False)],
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # device
        self.joboptions["device"] = MultipleChoiceJobOption(
            label="Whether to run on GPU or CPU",
            choices=["gpu", "cpu"],
            default_value_index=0,
            help_text=(
                "Whether to run on GPU or CPU."
                " Options are 'cpu' or 'gpu'. Defaults to gpu."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # simulation->ice
        self.joboptions["fast_ice"] = BooleanJobOption(
            label="Use the Gaussian Random Field ice model?",
            default_value=False,
            help_text=(
                "Use the Gaussian Random Field ice model." " Defaults to False."
            ),
            in_continue=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # microscope->detector->dqe
        self.joboptions["dqe"] = BooleanJobOption(
            label="Use the DQE model?",
            default_value=False,
            help_text=(
                "Use the DQE model to model detector quantum efficiency?"
                " Defaults to False."
            ),
            in_continue=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # simulation->radiation_damage_model
        self.joboptions["radiation_damage_model"] = BooleanJobOption(
            label="Use the radiation damage model?",
            default_value=False,
            help_text=("Use the radiation damage model? Defaults to False."),
            in_continue=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # simulation->sensitivity_coefficient
        self.joboptions["sensitivity_coefficient"] = FloatJobOption(
            label="The radiation damage model sensitivity coefficient",
            default_value=0.022,
            help_text=(
                "The radiation damage model sensitivity coefficient."
                " Defaults to 0.022."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("radiation_damage_model", "!=", True),
            ],
        )

        self.joboptions["c_10"] = FloatJobOption(
            label="Defocus value (A) (input a negative number for underfocus)",
            default_value=-20000.0,
            help_text=(
                "Average defocus value (A) (input a negative number for"
                " underfocus) to use in Parakeet simulation."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )
        self.joboptions["c_10_sigma"] = FloatJobOption(
            label="Standard deviation (A) for defocus",
            default_value=5000,
            help_text=(
                "Standard deviation (A) for defocus"
                " for Gaussian distribution centred on c_10."
                " Allows that reconstructed density map to"
                " sample contrast transfer more fully."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        self.joboptions["c_30"] = FloatJobOption(
            label="Spherical aberration (A)",
            default_value=2.7,
            help_text=("Spherical aberration (A). Defaults to 2.7."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        self.joboptions["c_c"] = FloatJobOption(
            label="Chromatic aberration (A)",
            default_value=2.7,
            help_text=("Chromatic aberration (A). Defaults to 2.7."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        self.joboptions["electrons_per_angstrom"] = FloatJobOption(
            label="Electron dose per square angstrom",
            default_value=45.0,
            help_text=(
                "Dose of electrons per square angstrom to use in Parakeet simulation."
                " Defaults to 45.0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        self.joboptions["energy"] = FloatJobOption(
            label="Electron beam energy (kV)",
            default_value=300.0,
            help_text=(
                "Electron beam energy (kV) to use in Parakeet simulation."
                " Defaults to 300.0kV"
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        self.joboptions["nx"] = IntJobOption(
            label="X axis pixels",
            default_value=1000,
            help_text=(
                "Number of pixels along the x"
                " axis of the image(s) to be"
                " simulated using Parakeet."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )
        self.joboptions["ny"] = IntJobOption(
            label="Y axis pixels",
            default_value=1000,
            help_text=(
                "Number of pixels along the y"
                " axis of the image(s) to be"
                " simulated using Parakeet."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )
        self.joboptions["pixel_size"] = FloatJobOption(
            label="Pixel size (Angstroms)",
            default_value=1.0,
            help_text=(
                "Pixel size (Angstroms) to use"
                " in Parakeet simulation."
                " All pixels are square."
                " Defaults to 1.0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # microscope->phase_plate
        self.joboptions["phase_plate"] = BooleanJobOption(
            label="Use phase plate?",
            default_value=False,
            help_text=(
                "Whether to use a"
                " phase plate in"
                " Parakeet simulation."
                " Defaults to False."
            ),
            in_continue=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # sample->box
        self.joboptions["box_x"] = FloatJobOption(
            label="Sample box size along x axis (Angstroms)",
            default_value=1000.0,
            help_text=("Sample box size along x axis (Angstroms). Defaults to 1000."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
            jobop_group="Sample",
        )
        self.joboptions["box_y"] = FloatJobOption(
            label="Sample box size along y axis (Angstroms)",
            default_value=1000.0,
            help_text=("Sample box size along y axis (Angstroms). Defaults to 1000."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
            jobop_group="Sample",
        )
        self.joboptions["box_z"] = FloatJobOption(
            label="Sample box size along z axis (Angstroms)",
            default_value=500.0,
            help_text=("Sample box size along z axis (Angstroms). Defaults to 500."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
            jobop_group="Sample",
        )

        # sample->type
        self.joboptions["shape"] = MultipleChoiceJobOption(
            label="Sample shape",
            choices=["cube", "cuboid", "cylinder"],
            default_value_index=1,
            help_text=(
                "An enumeration of sample shape types."
                " Options are 'cube', 'cuboid' or 'cylinder'."
                " Defaults to cuboid."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
            jobop_group="Sample",
        )
        # sample->shape->cuboid
        self.joboptions["cuboid_length_x"] = FloatJobOption(
            label="The cuboid X side length (A)",
            default_value=1000.0,
            help_text=("The cuboid X side length (A). Defaults to 1000."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True), ("shape", "!=", "cuboid")],
            jobop_group="Sample",
        )
        self.joboptions["cuboid_length_y"] = FloatJobOption(
            label="The cuboid Y side length (A)",
            default_value=1000.0,
            help_text=("The cuboid Y side length (A). Defaults to 1000."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True), ("shape", "!=", "cuboid")],
            jobop_group="Sample",
        )
        self.joboptions["cuboid_length_z"] = FloatJobOption(
            label="The cuboid Z side length (A)",
            default_value=500.0,
            help_text=("The cuboid Z side length (A). Defaults to 500."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True), ("shape", "!=", "cuboid")],
            jobop_group="Sample",
        )
        # sample->shape->cube
        self.joboptions["cube_length"] = FloatJobOption(
            label="The cube side length (A)",
            default_value=1000.0,
            help_text=("The cube side length (A). Defaults to 1000."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True), ("shape", "!=", "cube")],
            jobop_group="Sample",
        )
        # sample->shape->cylinder
        self.joboptions["cylinder_length"] = FloatJobOption(
            label="The cylinder length (A)",
            default_value=1000.0,
            help_text=("The cylinder length (A). Defaults to 1000."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True), ("shape", "!=", "cylinder")],
            jobop_group="Sample",
        )
        self.joboptions["cylinder_radius"] = FloatJobOption(
            label="The cylinder radius (A)",
            default_value=500.0,
            help_text=("The cylinder radius (A). Defaults to 500."),
            in_continue=False,
            is_required=False,
            deactivate_if=[("use_conf_yaml", "=", True), ("shape", "!=", "cylinder")],
            jobop_group="Sample",
        )

        # enable advanced options
        self.joboptions["advanced_options"] = BooleanJobOption(
            label="Enable advanced options",
            default_value=False,
            help_text=("Enable advanced configuration options"),
            in_continue=False,
            deactivate_if=[("use_conf_yaml", "=", True)],
        )

        # microscope
        # microscope->beam (ignoring defocus_drift and drift terms in config file)
        self.joboptions["acceleration_voltage_spread"] = FloatJobOption(
            label="The acceleration voltage spread (dV/V)",
            default_value=8.0e-07,
            help_text=("The acceleration voltage spread (dV/V). Defaults to 8.0e-07."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["energy_spread"] = FloatJobOption(
            label="Electron beam energy spread (dE/E)",
            default_value=2.66e-6,
            help_text=("Electron beam energy spread (dE/E). Defaults to 2.66e-6."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )

        self.joboptions["current_spread"] = FloatJobOption(
            label="The current spread (dI/I)",
            default_value=0.33e-6,
            help_text=("The current spread (dI/I). Defaults to 0.33e-6."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )

        self.joboptions["illumination_semiangle"] = FloatJobOption(
            label="The illumination semiangle (mrad)",
            default_value=0.02,
            help_text=("The illumination semiangle (mrad). Defaults to 0.02."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi"] = FloatJobOption(
            label="The beam tilt phi angle (deg)",
            default_value=0.0,
            help_text=("The beam tilt phi angle (deg). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["theta"] = FloatJobOption(
            label="The beam tilt theta angle (deg)",
            default_value=0.0,
            help_text=("The beam tilt theta angle (deg). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )

        # microscope->detector
        self.joboptions["Detector_origin_x"] = IntJobOption(
            label="Detector origin along x axis",
            default_value=0,
            help_text=(
                "Detector origin along x axis"
                " in pixels wrt lab frame."
                " Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["Detector_origin_y"] = IntJobOption(
            label="Detector origin along y axis",
            default_value=0,
            help_text=(
                "Detector origin along y axis"
                " in pixels wrt lab frame."
                " Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )

        # sample
        # sample->centre
        self.joboptions["centre_x"] = FloatJobOption(
            label="Center of tomographic rotation around sample x axis (A)",
            default_value=500.0,
            help_text=(
                "Center of tomographic rotation around sample x axis (A)"
                " Defaults to 500."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Sample",
        )
        self.joboptions["centre_y"] = FloatJobOption(
            label="Center of tomographic rotation around sample y axis (A)",
            default_value=500.0,
            help_text=(
                "Center of tomographic rotation around sample y axis (A)"
                " Defaults to 500."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Sample",
        )
        self.joboptions["centre_z"] = FloatJobOption(
            label="Center of tomographic rotation around sample z axis (A)",
            default_value=250.0,
            help_text=(
                "Center of tomographic rotation around sample z axis (A)"
                " Defaults to 250."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Sample",
        )

        # sample->shape->margin
        self.joboptions["margin_x"] = FloatJobOption(
            label=(
                "The x axis margin used to define how close to the edges particles"
                " should be placed (A)"
            ),
            default_value=0.0,
            help_text=(
                "The x axis margin used to define how close to the edges particles"
                " should be placed (A)"
                " Default value is 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Sample",
        )
        self.joboptions["margin_y"] = FloatJobOption(
            label=(
                "The y axis margin used to define how close to the edges particles"
                " should be placed (A)"
            ),
            default_value=0.0,
            help_text=(
                "The y axis margin used to define how close to the edges particles"
                " should be placed (A)"
                " Default value is 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Sample",
        )
        self.joboptions["margin_z"] = FloatJobOption(
            label=(
                "The z axis margin used to define how close to the edges particles"
                " should be placed (A)"
            ),
            default_value=0.0,
            help_text=(
                "The z axis margin used to define how close to the edges particles"
                " should be placed (A)"
                " Default value is 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Sample",
        )

        # simulation
        # simulation->margin
        self.joboptions["simulation_margin"] = IntJobOption(
            label="The margin around the image",
            default_value=100,
            help_text=("The margin around the image. Defaults to 100."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Simulation",
        )

        # simulation->padding
        self.joboptions["simulation_padding"] = IntJobOption(
            label="Additional edge padding",
            default_value=100,
            help_text=("Additional padding. Defaults to 100."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Simulation",
        )

        # simulation->slice_thickness
        self.joboptions["slice_thickness"] = FloatJobOption(
            label="The multislice algorithm thickness (A)",
            default_value=3.0,
            help_text=("The multislice thickness (A). Defaults to 3.0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Simulation",
        )

        # microscope->lens
        self.joboptions["c_12"] = FloatJobOption(
            label="The 2-fold astigmatism (A)",
            default_value=0,
            help_text=("The 2-fold astigmatism (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_12"] = FloatJobOption(
            label="The azimuthal angle of 2-fold astigmatism (rad)",
            default_value=0,
            help_text=(
                "The azimuthal angle of 2-fold astigmatism (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_21"] = FloatJobOption(
            label="The axial coma (A)",
            default_value=0.0,
            help_text=("The axial coma (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_21"] = FloatJobOption(
            label="The azimuthal angle of axial coma (rad)",
            default_value=0.0,
            help_text=("The azimuthal angle of axial coma (rad). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_23"] = FloatJobOption(
            label="The 3-fold astigmatism (A)",
            default_value=0.0,
            help_text=("The 3-fold astigmatism (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_23"] = FloatJobOption(
            label="The azimuthal angle of 3-fold astigmatism (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 3-fold astigmatism (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_32"] = FloatJobOption(
            label="The axial star aberration (A)",
            default_value=0.0,
            help_text=("The axial star aberration (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_32"] = FloatJobOption(
            label="The azimuthal angle of axial star aberration (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of axial star aberration (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_34"] = FloatJobOption(
            label="The 4-fold astigmatism (A)",
            default_value=0.0,
            help_text=("The 4-fold astigmatism (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_34"] = FloatJobOption(
            label="The azimuthal angle of 4-fold astigmatism (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 4-fold astigmatism (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_41"] = FloatJobOption(
            label="The 4th order axial coma (A)",
            default_value=0.0,
            help_text=("The 4th order axial coma (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_41"] = FloatJobOption(
            label="The azimuthal angle of 4th order axial coma (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 4th order axial coma (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_43"] = FloatJobOption(
            label="The 3-lobe aberration (A)",
            default_value=0.0,
            help_text=("The 3-lobe aberration (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_43"] = FloatJobOption(
            label="The azimuthal angle of 3-lobe aberration (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 3-lobe aberration (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_45"] = FloatJobOption(
            label="The 5-fold astigmatism (A)",
            default_value=0.0,
            help_text=("The 5-fold astigmatism (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_45"] = FloatJobOption(
            label="The azimuthal angle of 5-fold astigmatism (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 5-fold astigmatism (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_50"] = FloatJobOption(
            label="The 5th order spherical aberration (A)",
            default_value=0.0,
            help_text=("The 5th order spherical aberration (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_52"] = FloatJobOption(
            label="The 5th order axial star aberration (A)",
            default_value=0.0,
            help_text=("The 5th order axial star aberration (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_52"] = FloatJobOption(
            label="The azimuthal angle of 5th order axial star aberration (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 5th order axial star aberration (rad)."
                " Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_54"] = FloatJobOption(
            label="The 5th order rosette aberration (A)",
            default_value=0,
            help_text=("The 5th order rosette aberration (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_54"] = FloatJobOption(
            label="The azimuthal angle of 5th order rosette aberration (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 5th order rosette aberration (rad)."
                " Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["c_56"] = FloatJobOption(
            label="The 6-fold astigmatism (A)",
            default_value=0.0,
            help_text=("The 6-fold astigmatism (A). Defaults to 0."),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )
        self.joboptions["phi_56"] = FloatJobOption(
            label="The azimuthal angle of 6-fold astigmatism (rad)",
            default_value=0.0,
            help_text=(
                "The azimuthal angle of 6-fold astigmatism (rad). Defaults to 0."
            ),
            in_continue=False,
            is_required=False,
            deactivate_if=[
                ("use_conf_yaml", "=", True),
                ("advanced_options", "=", False),
            ],
            jobop_group="Microscope",
        )

        self.get_runtab_options(mpi=False)

        # Keep track of output micrographs in a list
        self.micrograph_name_list = []

        # Create a parakeet config object
        # to be updated either from
        # user-provided yaml or GUI inputs.
        # This object is updated during iteration
        # over images
        self.parakeet_config = ParakeetConfig()

        # default names for intermediate files
        # name for sample file
        self.sample = "sample.h5"
        # name for metadata mtf file
        self.exit_wave = "exit_wave.h5"
        # name for optics file
        self.optics = "optics.h5"
        # name for image file
        self.image = "image.h5"
        # np random seed
        self.seed = 0

    def get_particles_per_image_from_conf(self) -> int:
        """Determine the number of particle to put in each
        parakeet generated image from the supplied config file

        Returns:
            int: particles to generate per parakeet image
        """
        n_particles = 0
        # Parakeet has two types of input: LocalMolecule or PDBMolecule
        # check for LocalMolecules first
        if "local" in self.parakeet_config.config_dict["sample"]["molecules"]:
            # grab the list of dicts
            for list_entry in self.parakeet_config.config_dict["sample"]["molecules"][
                "local"
            ]:
                n_particles += self.get_particle_number_from_dict(list_entry)

        # do the same but for the pdb accessed molecules
        if "pdb" in self.parakeet_config.config_dict["sample"]["molecules"]:
            for list_entry in self.parakeet_config.config_dict["sample"]["molecules"][
                "pdb"
            ]:
                n_particles += self.get_particle_number_from_dict(list_entry)
        return n_particles

    @staticmethod
    def get_particle_number_from_dict(input_dict: Dict) -> int:
        """
        Sum number of particles to generate in a given image for this molecule type
        Args:
            input_dict (dict): yaml config file interpreted as dict

        Raises:
            TypeError: Checking Molecule dict in yaml config file is interpretable

        Returns:
            int: Summed count of this type of molecule
        """
        n_particles = 0
        # check if the Union is a int or a list of MoleculePose(s)
        if isinstance(input_dict["instances"], int):
            n_particles += input_dict["instances"]
        # otherwise its got to be a list
        elif isinstance((input_dict["instances"]), list):
            n_particles += len(input_dict["instances"])
        # otherwise something's gone wrong
        else:
            raise TypeError(
                "List of molecules (particles) was not an int or a list!"
                " It is {}".format(type(input_dict["instances"]))
            )
        return n_particles

    def determine_molecules(self) -> Dict[str, List[Dict]]:
        """Parse the molecules and number of molecules per micrograph provided and
        ensure that the lists provided are of the same length. Then parse these
        lists and put them into a Parakeet configuration file compatible dict.

        Returns:
            Dict[str, List[Dict]]: Dictionary of molecules and instances of each.
        """
        molecules_list = []
        # use pdb_filepaths if they are filled
        if self.joboptions["pdb_filepaths"].value != "":
            molecules_list = self.joboptions["pdb_filepaths"].get_list()

        # grab the instances list and convert to int from string
        instances_list = self.joboptions["pdb_instances"].get_list()
        instance_list_convert = []
        for instance in instances_list:
            instance_list_convert.append(int(instance))
        instances_list = instance_list_convert
        # check you have a number of instances for each molecule used
        assert len(molecules_list) > 0
        assert len(molecules_list) == len(instances_list)

        molecules_label = "local"
        molecules_outgoing_list = []
        for (molecule, instances) in zip(molecules_list, instances_list):
            molecules_outgoing_list.append(
                {
                    "filename": molecule,
                    "instances": instances,
                },
            )

        molecules = {molecules_label: molecules_outgoing_list}

        return molecules

    def generate_defocus_value(self):
        """Sample from a Gaussian distribution (used for sampling defoci)

        Returns:
            float: defocus value
        """
        defocus_val = np.random.normal(
            loc=self.joboptions["c_10"].get_number(True, "c_10 value is missing"),
            scale=self.joboptions["c_10_sigma"].get_number(
                True, "c_10_sigma value is missing"
            ),
            size=1,
        )
        return defocus_val

    @contextlib.contextmanager
    def set_np_seed(self):
        """Make defoci pseudorandom"""
        state = np.random.get_state()
        np.random.seed(self.seed)
        try:
            yield
        finally:
            np.random.set_state(state)

    def get_commands(self):

        # Create the micrographs dir
        micrograph_dir = os.path.join(self.output_dir, "Micrographs")
        # Create the list of commands to run and micrographs subdir
        final_commands_list = [["mkdir", micrograph_dir]]

        # Update intermediate filename to be in output dir
        self.sample = os.path.join(self.output_dir, "sample.h5")
        # name for metadata mtf file
        self.exit_wave = os.path.join(self.output_dir, "exit_wave.h5")
        # name for optics file
        self.optics = os.path.join(self.output_dir, "optics.h5")
        # name for image file
        self.image = os.path.join(self.output_dir, "image.h5")

        # Read in Parakeet configuration yaml if provided;
        if self.joboptions["use_conf_yaml"].get_boolean():
            # grab config file in dict form
            self.parakeet_config.read_yaml(
                self.joboptions["conf"].get_string(True, "Input file missing")
            )

        # if no config yaml is provided
        else:
            # update the parakeet config object
            # read in the GUI inputs to make the configuration file from

            # work out the molecules from the 2 lists
            molecules = self.determine_molecules()

            # device
            self.parakeet_config.update_device(self.joboptions["device"].value)

            # microscope
            self.parakeet_config.update_microscope(
                {
                    "beam": {
                        "acceleration_voltage_spread": self.joboptions[
                            "acceleration_voltage_spread"
                        ].get_number(
                            True, "acceleration_voltage_spread input is missing"
                        ),
                        "defocus_drift": None,
                        "drift": None,
                        "electrons_per_angstrom": self.joboptions[
                            "electrons_per_angstrom"
                        ].get_number(True, "electrons_per_angstrom input is missing"),
                        "energy": self.joboptions["energy"].get_number(
                            True, "energy input is missing"
                        ),
                        "energy_spread": self.joboptions["energy_spread"].get_number(
                            True, "energy_spread input is missing"
                        ),
                        "phi": self.joboptions["phi"].get_number(
                            True, "phi input is missing"
                        ),
                        "illumination_semiangle": self.joboptions[
                            "illumination_semiangle"
                        ].get_number(True, "illumination_semiangle input is missing"),
                        "theta": self.joboptions["theta"].get_number(
                            True, "theta input is missing"
                        ),
                    },
                    "detector": {
                        "dqe": self.joboptions["dqe"].get_boolean(),
                        "nx": self.joboptions["nx"].get_number(
                            True, "nx input is missing"
                        ),
                        "ny": self.joboptions["ny"].get_number(
                            True, "ny input is missing"
                        ),
                        "origin": [
                            self.joboptions["Detector_origin_x"].get_number(
                                True, "Detector_origin_x input is missing"
                            ),
                            self.joboptions["Detector_origin_y"].get_number(
                                True, "Detector_origin_y input is missing"
                            ),
                        ],
                        "pixel_size": self.joboptions["pixel_size"].get_number(
                            True, "pixel_size input is missing"
                        ),
                    },
                    "lens": {
                        "c_10": self.joboptions["c_10"].get_number(
                            True, "c_10 input is missing"
                        ),
                        "c_12": self.joboptions["c_12"].get_number(
                            True, "c_12 input is missing"
                        ),
                        "c_21": self.joboptions["c_21"].get_number(
                            True, "c_21 input is missing"
                        ),
                        "c_23": self.joboptions["c_23"].get_number(
                            True, "c_23 input is missing"
                        ),
                        "c_30": self.joboptions["c_30"].get_number(
                            True, "c_30 input is missing"
                        ),
                        "c_32": self.joboptions["c_32"].get_number(
                            True, "c_32 input is missing"
                        ),
                        "c_34": self.joboptions["c_34"].get_number(
                            True, "c_34 input is missing"
                        ),
                        "c_41": self.joboptions["c_41"].get_number(
                            True, "c_41 input is missing"
                        ),
                        "c_43": self.joboptions["c_43"].get_number(
                            True, "c_43 input is missing"
                        ),
                        "c_45": self.joboptions["c_45"].get_number(
                            True, "c_45 input is missing"
                        ),
                        "c_50": self.joboptions["c_50"].get_number(
                            True, "c_50 input is missing"
                        ),
                        "c_52": self.joboptions["c_52"].get_number(
                            True, "c_52 input is missing"
                        ),
                        "c_54": self.joboptions["c_54"].get_number(
                            True, "c_54 input is missing"
                        ),
                        "c_56": self.joboptions["c_56"].get_number(
                            True, "c_56 input is missing"
                        ),
                        "c_c": self.joboptions["c_c"].get_number(
                            True, "c_c input is missing"
                        ),
                        "current_spread": self.joboptions["current_spread"].get_number(
                            True, "current_spread input is missing"
                        ),
                        "phi_12": self.joboptions["phi_12"].get_number(
                            True, "phi_12 input is missing"
                        ),
                        "phi_21": self.joboptions["phi_21"].get_number(
                            True, "phi_21 input is missing"
                        ),
                        "phi_23": self.joboptions["phi_23"].get_number(
                            True, "phi_23 input is missing"
                        ),
                        "phi_32": self.joboptions["phi_32"].get_number(
                            True, "phi_32 input is missing"
                        ),
                        "phi_34": self.joboptions["phi_34"].get_number(
                            True, "phi_34 input is missing"
                        ),
                        "phi_41": self.joboptions["phi_41"].get_number(
                            True, "phi_41 input is missing"
                        ),
                        "phi_43": self.joboptions["phi_43"].get_number(
                            True, "phi_43 input is missing"
                        ),
                        "phi_45": self.joboptions["phi_45"].get_number(
                            True, "phi_45 input is missing"
                        ),
                        "phi_52": self.joboptions["phi_52"].get_number(
                            True, "phi_52 input is missing"
                        ),
                        "phi_54": self.joboptions["phi_54"].get_number(
                            True, "phi_54 input is missing"
                        ),
                        "phi_56": self.joboptions["phi_56"].get_number(
                            True, "phi_56 input is missing"
                        ),
                    },
                    # Letting this be None as setting microscope_model to "" throws
                    # ValueError instead of allowing None to be passed
                    "model": None,
                    "phase_plate": self.joboptions["phase_plate"].get_boolean(),
                }
            )

            # sample
            self.parakeet_config.update_sample(
                {
                    "box": [
                        self.joboptions["box_x"].get_number(
                            True, "box_x input is missing"
                        ),
                        self.joboptions["box_y"].get_number(
                            True, "box_y input is missing"
                        ),
                        self.joboptions["box_z"].get_number(
                            True, "box_z input is missing"
                        ),
                    ],
                    "centre": [
                        self.joboptions["centre_x"].get_number(
                            True, "centre_x input is missing"
                        ),
                        self.joboptions["centre_y"].get_number(
                            True, "centre_y input is missing"
                        ),
                        self.joboptions["centre_z"].get_number(
                            True, "centre_z input is missing"
                        ),
                    ],
                    "molecules": molecules,
                    "shape": {
                        "cube": {
                            "length": self.joboptions["cube_length"].get_number(
                                True, "cube_length input is missing"
                            )
                        },
                        "cuboid": {
                            "length_x": self.joboptions["cuboid_length_x"].get_number(
                                True, "cuboid_length_x input is missing"
                            ),
                            "length_y": self.joboptions["cuboid_length_y"].get_number(
                                True, "cuboid_length_y input is missing"
                            ),
                            "length_z": self.joboptions["cuboid_length_z"].get_number(
                                True, "cuboid_length_z input is missing"
                            ),
                        },
                        "cylinder": {
                            "length": self.joboptions["cylinder_length"].get_number(
                                True, "cylinder_length input is missing"
                            ),
                            "radius": self.joboptions["cylinder_radius"].get_number(
                                True, "cylinder_radius input is missing"
                            ),
                        },
                        "margin": [
                            self.joboptions["margin_x"].get_number(
                                True, "margin_x input is missing"
                            ),
                            self.joboptions["margin_y"].get_number(
                                True, "margin_y input is missing"
                            ),
                            self.joboptions["margin_z"].get_number(
                                True, "margin_z input is missing"
                            ),
                        ],
                        "type": self.joboptions["shape"].value,
                    },
                    "sputter": None,
                }
            )

            # simulation
            self.parakeet_config.update_simulation(
                {
                    "division_thickness": 100,
                    "ice": self.joboptions["fast_ice"].get_boolean(),
                    "margin": self.joboptions["simulation_margin"].get_number(
                        True, "simulation_margin input is missing"
                    ),
                    "padding": self.joboptions["simulation_padding"].get_number(
                        True, "simulation_padding input is missing"
                    ),
                    "radiation_damage_model": self.joboptions[
                        "radiation_damage_model"
                    ].get_boolean(),
                    "sensitivity_coefficient": self.joboptions[
                        "sensitivity_coefficient"
                    ].get_number(True, "sensitivity_coefficient input is missing"),
                    "slice_thickness": self.joboptions["slice_thickness"].get_number(
                        True, "slice_thickness input is missing"
                    ),
                }
            )

        self.dataset_particles = self.joboptions["particles"].get_number(
            True, "Total particles in dataset input missing"
        )
        # calculate the particles per image from the configuration dict
        self.particles_per_image = self.get_particles_per_image_from_conf()

        assert (
            self.particles_per_image <= self.dataset_particles
        ), "Particles per image is greater than particles per dataset. Correct this!"

        # get the number of images to make with parakeet
        self.images = round(
            float(self.dataset_particles) / float(self.particles_per_image)
        )

        # update the expected name for default
        # mtf metadata file
        self.mtf_filename = os.path.join(
            self.output_dir,
            os.path.join(
                "relion",
                "mtf_{}kV.star".format(
                    int(
                        self.parakeet_config.config_dict["microscope"]["beam"]["energy"]
                    ),
                ),
            ),
        )

        # Loop through images to create,
        # collecting names for micrographs starfile and
        # adding them to commands list
        # Use a temporary np seed to ensure reproducibility
        with self.set_np_seed():
            for image_n in range(1, self.images + 1, 1):
                # sample a defocus value for this image
                # make a dict of c_10 values indexed by image index
                defocus_val = float(self.generate_defocus_value())

                # alter the parakeet config defocus for each individual micrograph
                self.parakeet_config.config_dict["microscope"]["lens"][
                    "c_10"
                ] = defocus_val
                # give the temporary configuration file a name
                self.parakeet_config.yaml_filename = os.path.join(
                    micrograph_dir, "image_{}.yaml".format(str(image_n).zfill(6))
                )
                create_working_conf = [
                    "python3",
                    get_job_script("parakeet/make_configuration_yaml.py"),
                    "--device",
                    self.parakeet_config.config_dict["device"],
                    "--acceleration_voltage_spread",
                    self.parakeet_config.config_dict["microscope"]["beam"][
                        "acceleration_voltage_spread"
                    ],
                    "--electrons_per_angstrom",
                    self.parakeet_config.config_dict["microscope"]["beam"][
                        "electrons_per_angstrom"
                    ],
                    "--energy",
                    self.parakeet_config.config_dict["microscope"]["beam"]["energy"],
                    "--energy_spread",
                    self.parakeet_config.config_dict["microscope"]["beam"][
                        "energy_spread"
                    ],
                    "--illumination_semiangle",
                    self.parakeet_config.config_dict["microscope"]["beam"][
                        "illumination_semiangle"
                    ],
                    "--phi",
                    self.parakeet_config.config_dict["microscope"]["beam"]["phi"],
                    "--theta",
                    self.parakeet_config.config_dict["microscope"]["beam"]["theta"],
                    "--dqe",
                    self.parakeet_config.config_dict["microscope"]["detector"]["dqe"],
                    "--nx",
                    self.parakeet_config.config_dict["microscope"]["detector"]["nx"],
                    "--ny",
                    self.parakeet_config.config_dict["microscope"]["detector"]["ny"],
                    "--pixel_size",
                    self.parakeet_config.config_dict["microscope"]["detector"][
                        "pixel_size"
                    ],
                    "--Detector_origin_x",
                    self.parakeet_config.config_dict["microscope"]["detector"][
                        "origin"
                    ][0],
                    "--Detector_origin_y",
                    self.parakeet_config.config_dict["microscope"]["detector"][
                        "origin"
                    ][1],
                    "--phase_plate",
                    self.parakeet_config.config_dict["microscope"]["phase_plate"],
                    "--c_10",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_10"],
                    "--c_12",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_12"],
                    "--phi_12",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_12"],
                    "--c_21",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_21"],
                    "--phi_21",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_21"],
                    "--c_23",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_23"],
                    "--phi_23",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_23"],
                    "--c_30",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_30"],
                    "--c_32",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_32"],
                    "--phi_32",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_32"],
                    "--c_34",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_34"],
                    "--phi_34",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_34"],
                    "--c_41",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_41"],
                    "--phi_41",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_41"],
                    "--c_43",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_43"],
                    "--phi_43",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_43"],
                    "--c_45",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_45"],
                    "--phi_45",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_45"],
                    "--c_50",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_50"],
                    "--c_52",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_52"],
                    "--phi_52",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_52"],
                    "--c_54",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_54"],
                    "--phi_54",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_54"],
                    "--c_56",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_56"],
                    "--phi_56",
                    self.parakeet_config.config_dict["microscope"]["lens"]["phi_56"],
                    "--c_c",
                    self.parakeet_config.config_dict["microscope"]["lens"]["c_c"],
                    "--current_spread",
                    self.parakeet_config.config_dict["microscope"]["lens"][
                        "current_spread"
                    ],
                    "--box_x",
                    self.parakeet_config.config_dict["sample"]["box"][0],
                    "--box_y",
                    self.parakeet_config.config_dict["sample"]["box"][1],
                    "--box_z",
                    self.parakeet_config.config_dict["sample"]["box"][2],
                    "--centre_x",
                    self.parakeet_config.config_dict["sample"]["centre"][0],
                    "--centre_y",
                    self.parakeet_config.config_dict["sample"]["centre"][1],
                    "--centre_z",
                    self.parakeet_config.config_dict["sample"]["centre"][2],
                    "--cube_length",
                    self.parakeet_config.config_dict["sample"]["shape"]["cube"][
                        "length"
                    ],
                    "--cuboid_length_x",
                    self.parakeet_config.config_dict["sample"]["shape"]["cuboid"][
                        "length_x"
                    ],
                    "--cuboid_length_y",
                    self.parakeet_config.config_dict["sample"]["shape"]["cuboid"][
                        "length_y"
                    ],
                    "--cuboid_length_z",
                    self.parakeet_config.config_dict["sample"]["shape"]["cuboid"][
                        "length_z"
                    ],
                    "--cylinder_length",
                    self.parakeet_config.config_dict["sample"]["shape"]["cylinder"][
                        "length"
                    ],
                    "--cylinder_radius",
                    self.parakeet_config.config_dict["sample"]["shape"]["cylinder"][
                        "radius"
                    ],
                    "--margin_x",
                    self.parakeet_config.config_dict["sample"]["shape"]["margin"][0],
                    "--margin_y",
                    self.parakeet_config.config_dict["sample"]["shape"]["margin"][1],
                    "--margin_z",
                    self.parakeet_config.config_dict["sample"]["shape"]["margin"][2],
                    "--type",
                    self.parakeet_config.config_dict["sample"]["shape"]["type"],
                    "--fast_ice",
                    self.parakeet_config.config_dict["simulation"]["ice"],
                    "--simulation_margin",
                    self.parakeet_config.config_dict["simulation"]["margin"],
                    "--simulation_padding",
                    self.parakeet_config.config_dict["simulation"]["padding"],
                    "--radiation_damage_model",
                    self.parakeet_config.config_dict["simulation"][
                        "radiation_damage_model"
                    ],
                    "--sensitivity_coefficient",
                    self.parakeet_config.config_dict["simulation"][
                        "sensitivity_coefficient"
                    ],
                    "--slice_thickness",
                    self.parakeet_config.config_dict["simulation"]["slice_thickness"],
                    "--config_yaml_filename",
                    self.parakeet_config.yaml_filename,
                ]
                # the filenames and instances are lists of dict, but we need
                # these in argparse nargs="+" acceptable format
                for (pdb_label, pdb_source) in zip(
                    ["id", "filename"], ["pdb", "local"]
                ):
                    if (
                        pdb_source
                        in self.parakeet_config.config_dict["sample"]["molecules"]
                    ):
                        create_working_conf.append("--pdb_source")
                        create_working_conf.append(pdb_source)
                        create_working_conf.append("--pdb_filepaths")
                        for filepath in self.parakeet_config.config_dict["sample"][
                            "molecules"
                        ][pdb_source]:
                            create_working_conf.append(filepath[pdb_label])
                        create_working_conf.append("--pdb_instances")
                        for instance in self.parakeet_config.config_dict["sample"][
                            "molecules"
                        ][pdb_source]:
                            create_working_conf.append(instance["instances"])

                # run parakeet simulation
                sample_new = [
                    "parakeet.sample.new",
                    "-c",
                    self.parakeet_config.yaml_filename,
                    "-s",
                    self.sample,
                ]
                sample_add_mols = [
                    "parakeet.sample.add_molecules",
                    "-c",
                    self.parakeet_config.yaml_filename,
                    "-s",
                    self.sample,
                ]
                simulate_exitwave = [
                    "parakeet.simulate.exit_wave",
                    "-c",
                    self.parakeet_config.yaml_filename,
                    "-s",
                    self.sample,
                    "-e",
                    self.exit_wave,
                    "-d",
                    self.parakeet_config.config_dict["device"],
                ]
                simulate_optics = [
                    "parakeet.simulate.optics",
                    "-c",
                    self.parakeet_config.yaml_filename,
                    "-e",
                    self.exit_wave,
                    "-o",
                    self.optics,
                    "-d",
                    self.parakeet_config.config_dict["device"],
                ]
                simulate_image = [
                    "parakeet.simulate.image",
                    "-c",
                    self.parakeet_config.yaml_filename,
                    "-o",
                    self.optics,
                    "-i",
                    self.image,
                ]
                mrcfile = "image_{:06d}.mrc".format(image_n)
                output_file = os.path.join(micrograph_dir, mrcfile)
                export = ["parakeet.export", self.image, "-o", output_file]
                self.micrograph_name_list.append(output_file)

                final_commands_list.append(create_working_conf)
                final_commands_list.append(sample_new)
                final_commands_list.append(sample_add_mols)
                final_commands_list.append(simulate_exitwave)
                final_commands_list.append(simulate_optics)
                final_commands_list.append(simulate_image)
                final_commands_list.append(export)

                # Create the metadata from the intermediate
                # Parakeet files on the first image
                if image_n == 1:
                    # Add mtf file to output nodes
                    self.output_nodes.append(
                        Node(self.mtf_filename, NODE_PROCESSDATA, ["parakeet", "mtf"])
                    )
                    parakeet_metadata = [
                        "parakeet.metadata.export",
                        "-c",
                        self.parakeet_config.yaml_filename,
                        "-s",
                        self.sample,
                        "--directory",
                        self.output_dir,
                    ]
                    final_commands_list.append(parakeet_metadata)

                # Remove config with defocus for generated image if required
                if not self.joboptions["keep_confs"].get_boolean():
                    remove_temp_config = ["rm", self.parakeet_config.yaml_filename]
                    final_commands_list.append(remove_temp_config)

        # Finally, create the micrographs starfile
        mics_star = os.path.join(self.output_dir, "synthetic_micrographs.star")
        make_micrographs_starfile = [
            "python3",
            get_job_script("parakeet/make_micrograph_starfile.py"),
            "--mtf_star",
            self.mtf_filename,
            "--original_pixel_size",
            self.parakeet_config.config_dict["microscope"]["detector"]["pixel_size"],
            "--pixel_size",
            self.parakeet_config.config_dict["microscope"]["detector"]["pixel_size"],
            "--voltage",
            self.parakeet_config.config_dict["microscope"]["beam"]["energy"],
            "--c_30",
            self.parakeet_config.config_dict["microscope"]["lens"]["c_30"],
            "--micrograph_name_dir",
            micrograph_dir,
            "--micrograph_search_string",
            "image_*.mrc",
            "--optics_group_name",
            self.joboptions["opticsGroupName"].get_string(
                True, "opticsGroupName missing"
            ),
            "--amplitude_contrast",
            self.parakeet_config.config_dict["microscope"]["beam"][
                "illumination_semiangle"
            ],
            "--micrograph_starfile_name",
            mics_star,
        ]
        # Add creation of micrographs starfile to commands
        final_commands_list.append(make_micrographs_starfile)

        # Add micrographs starfile to output nodes
        self.output_nodes.append(
            Node(mics_star, NODE_MICROGRAPHGROUPMETADATA, ["parakeet", "synthetic"])
        )

        return final_commands_list

    # Return objects the GUI will use to produce graphical display of results
    def create_results_display(self):
        outfile = os.path.join(self.output_dir, "synthetic_micrographs.star")
        return [
            mini_montage_from_starfile(
                starfile=outfile,
                block="micrographs",
                column="_rlnMicrographName",
                nimg=4,
                outputdir=self.output_dir,
            )
        ]

    # Any tasks that are performed after the job's commands have been
    # executed IE: deleting tmp files
    def post_run_actions(self):
        # delete the background outputs from each part of the simulation process
        if self.images >= 1:
            os.remove(self.sample)
            os.remove(self.exit_wave)
            os.remove(self.optics)
            os.remove(self.image)
            os.remove(os.path.join(self.output_dir, "relion/relion_input.star"))
            os.remove(
                os.path.join(self.output_dir, "relion/corrected_micrographs.star")
            )
            os.remove(os.path.join(self.output_dir, "relion/particle.star"))

    # Returns a dictionary of metadata for the job
    def gather_metadata(self):
        datafile = DataStarFile(
            os.path.join(self.output_dir, "synthetic_micrographs.star")
        )
        metadata = {
            "MicrographCount": datafile.count_block("micrographs"),
        }
        return metadata
