#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from typing import List
import xml.etree.ElementTree as et
from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
from pipeliner.job_options import (
    FileNameJobOption,
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_SEQUENCE,
    NODE_RESTRAINTS,
    NODE_ATOMCOORDS,
)
from pipeliner.utils import get_job_script


class BuccaneerJob(PipelinerJob):
    PROCESS_NAME = "buccaneer.atomic_model_build"
    OUT_DIR = "Buccaneer"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Buccaneer"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.short_desc = "Automated atomic model building with Buccaneer"
        self.jobinfo.long_desc = (
            "Buccaneer performs statistical chain"
            " tracing by identifying connected alpha-carbon positions using a "
            "likelihood-based density target. The target distributions are generated"
            "by a simulation calculation using a known reference structure for which"
            " calculated phases are available. The success of the method is dependent"
            " on the features of the reference structure matching those of the "
            "unsolved, work structure. For almost all cases, a single reference "
            "structure can be used, with modifications automatically applied to "
            "the reference structure to match its features to the work structure."
            " N.B. requires CCP4."
        )
        self.jobinfo.documentation = "https://www.ccp4.ac.uk/html/cbuccaneer.html"
        self.jobinfo.programs = [ExternalProgram("ccpem-buccaneer")]
        self.jobinfo.references = [
            Ref(
                authors=["Cowtan K"],
                title=(
                    "The Buccaneer software for automated model building."
                    "1. Tracing protein chains"
                ),
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2006",
                volume="62",
                issue="Pt 9",
                pages="1002-11",
                doi="10.1107/S0907444906022116",
            ),
            Ref(
                authors=["Hoh SW", "Burnley T", "Cowtan K"],
                title=(
                    "Current approaches for automated model building"
                    " into cryo-EM maps using Buccaneer with CCP-EM."
                ),
                journal="Acta Crystallogr D Struct Biol",
                year="2020",
                volume="76",
                issue="Pt 6",
                pages="531-541",
                doi="10.1107/S2059798320005513",
            ),
        ]

        self.joboptions["job_title"] = StringJobOption(
            label="Job title",
            default_value="",
            help_text=(
                "This is the title of the job.  It can be a long string with spaces"
            ),
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map",
            is_required=True,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Input map resolution",
            default_value=3,
            suggested_min=1,
            suggested_max=10,
            step_value=0.1,
            help_text="The resolution of the input map",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["input_seq"] = FileNameJobOption(
            label="Input sequence",
            default_value="",
            pattern=files_exts("Sequence", [".fasta"]),
            directory=".",
            help_text="The input sequence",
            is_required=True,
            node_type=NODE_SEQUENCE,
        )

        # what is this job option exactly??
        self.joboptions["extend_pdb"] = FileNameJobOption(
            label="Extend existing pdb",
            default_value="",
            pattern=files_exts("pdb atomic model", [".pdb"]),
            directory=".",
            help_text="The pdb to be extended",
            node_type="AtomCoords",
        )

        self.joboptions["ncycle"] = IntJobOption(
            label="Number of cycles",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text="The number of cycles to perform",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncycle_refmac"] = IntJobOption(
            label="Number of refmac cycles",
            default_value=20,
            suggested_min=1,
            suggested_max=60,
            step_value=1,
            help_text="How many cycles of refmac to run",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncycle_buc1st"] = IntJobOption(
            label="Number of 1st cycle iterations",
            default_value=-1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of internal buccaneer cycles to run on the first "
                "iteration. Default 3"
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncycle_bucnth"] = IntJobOption(
            label="Number of subsequent cycle iterations",
            default_value=-1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of internal buccaneer cycles to run on subsequent iterations."
                " Default 2."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["map_sharpen"] = FloatJobOption(
            label="Map sharpening",
            default_value=0.0,
            suggested_min=0,
            suggested_max=-200,
            step_value=1,
            help_text="Sharpening to apply to the map",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["ncpus"] = IntJobOption(
            label="Number of CPUS to use",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text="Number of CPUS",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["lib_in"] = FileNameJobOption(
            label="Restraints library",
            default_value="",
            help_text="Restraints library",
            in_continue=True,
            node_type=NODE_RESTRAINTS,
        )

        self.joboptions["keywords"] = StringJobOption(
            label="Additional keyword arguments for Buccaneer",
            default_value="",
            help_text="See the Buccaneer documentation",
        )

        self.joboptions["refmac_keywords"] = StringJobOption(
            label="Additional keyword arguments for refmac",
            default_value="",
            help_text="See the refmac documentation",
        )

        self.get_runtab_options(addtl_args=True)

    def get_commands(self) -> List[List[str]]:

        # write the args file
        jobops_dict = {}

        # some requirements for parameters
        nulls = ["extend_pdb", "lib_in"]  # values that need None for empty value
        abspaths = ["input_map", "input_seq"]  # does extend pdb need to be here too?

        # relion job options not written to the buccaneer_args json file
        nonwritten = [
            "do_queue",
            "min_dedicated",
            "qsub",
            "qsubscript",
            "queuename",
            "other_args",
        ]

        for jobop in self.joboptions:
            if jobop not in nonwritten:
                val = self.joboptions[jobop].get_string()

                # format the values
                try:
                    val = float(val)
                except ValueError:
                    pass

                # files need to abs paths?
                if jobop in abspaths:
                    val = os.path.abspath(val)

                # some fields need null for empties, some need ""
                if jobop in nulls and val == "":
                    val = None

                jobops_dict[jobop] = val

        file_script_com = ["python3", get_job_script("make_buccaneer_args_file.py")]
        for i in jobops_dict:
            file_script_com.extend([f"--{i}", jobops_dict[i]])

        # assemble commands
        bucc_command = [
            "ccpem-buccaneer",
            "--no-gui",
            "--args",
            "tmp_buccaneer_args.json",
            "--job_location",
            self.output_dir,
        ]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            bucc_command.extend(self.parse_additional_args())

        commands = [file_script_com, bucc_command, ["rm", "tmp_buccaneer_args.json"]]

        self.output_nodes.append(
            create_node(
                self.output_dir + "refined1.pdb", NODE_ATOMCOORDS, ["buccaneer"]
            )
        )

        return commands

    def gather_metadata(self):

        metadata_dict = {}

        root = et.parse(self.output_dir + "program1.xml").getroot()

        for x in root.findall("Final"):
            metadata_dict["CompletenessByResidues"] = float(
                x.find("CompletenessByResiduesBuilt").text
            )
            metadata_dict["CompletenessByChains"] = float(
                x.find("CompletenessByChainsBuilt").text
            )
            metadata_dict["ChainsBuilt"] = int(x.find("ChainsBuilt").text)
            metadata_dict["FragmentsBuilt"] = int(x.find("FragmentsBuilt").text)
            metadata_dict["ResiduesUnique"] = int(x.find("ResiduesUnique").text)
            metadata_dict["ResiduesBuilt"] = int(x.find("ResiduesBuilt").text)
            metadata_dict["ResiduesSequenced"] = int(x.find("ResiduesSequenced").text)
            metadata_dict["ResiduesLongestFragment"] = int(
                x.find("ResiduesLongestFragment").text
            )

        with open(self.output_dir + "refined1_summary.json", "r") as jf:
            refined_summary = json.load(jf)

        for k, v in refined_summary["cycles"][-1].items():
            if k != "cycle":
                refined_summary["cycles"][-1][k] = float(v)

        metadata_dict["RefineStatistics"] = refined_summary["cycles"][-1]

        return metadata_dict

    def create_results_display(self):
        model = self.output_dir + "refined1.pdb"
        map_ = self.joboptions["input_map"].get_string()
        dispobj = make_map_model_thumb_and_display(
            maps=[map_],
            models=[model],
            outputdir=self.output_dir,
            start_collapsed=False,
        )
        return [dispobj]
