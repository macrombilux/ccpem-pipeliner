#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.utils import run_subprocess
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_LIGANDDESCRIPTION,
)


class Refine(PipelinerJob):
    PROCESS_NAME = "refmac_servalcat.atomic_model_refine"
    OUT_DIR = "RefmacServalcat"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Refmac Servalcat"
        self.jobinfo.short_desc = "Atomic structure refinement"
        self.jobinfo.long_desc = (
            "Macromolecular refinement program. Servalcat is a wrapper for"
            " REFMAC5 that adds map sharpening, symmetry handling, difference"
            " maps and other functions. REFMAC5 is a program designed for"
            " REFinement of MACromolecular structures. It uses maximum"
            " likelihood and some elements of Bayesian statistics."
            " N.B. requires CCP4."
        )
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        self.jobinfo.programs = [
            ExternalProgram(
                command="servalcat", vers_com=["servalcat", "--version"], vers_lines=[0]
            ),
            ExternalProgram(command="gemmi", vers_com=["gemmi", "-V"], vers_lines=[0]),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
                title=(
                    "Cryo-EM single particle structure refinement and map "
                    "calculation using Servalcat."
                ),
                journal="Acta Cryst. D",
                year="2022",
                volume="77",
                issue="1",
                pages="1282-1291",
                doi="10.1107/S2059798321009475",
            )
        ]
        self.jobinfo.documentation = "https://github.com/keitaroyam/servalcat"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be refined",
            is_required=True,
        )
        self.joboptions["input_ligand"] = InputNodeJobOption(
            label="Input ligand",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("Ligand definition", [".cif"]),
            default_value="",
            directory="",
            help_text="The input model",
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )

        self.joboptions["input_map1"] = InputNodeJobOption(
            label="Input map 1 (half map 1 or full map)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to refine the model against",
            is_required=True,
        )
        self.joboptions["half_map_refinement"] = BooleanJobOption(
            label="Half map refinement",
            default_value=True,
            help_text=(
                "Use half maps for refinement, alternative is to use single full map"
            ),
        )
        self.joboptions["input_half_map2"] = InputNodeJobOption(
            label="Input map 2 (half map 2)",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to refine the model against",
            required_if=[("half_map_refinement", "=", True)],
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input mask required for difference map calculation",
        )
        self.joboptions["masked_refinement"] = BooleanJobOption(
            label="Masked Refinement",
            default_value=True,
            help_text=(
                "Trim map around supplied model file and perform refinement "
                "with resultant sub-volume"
            ),
        )
        self.joboptions["mask_radius"] = FloatJobOption(
            label="Mask radius",
            default_value=3.0,
            help_text="Distance around molecule the map should be cut",
            required_if=[("masked_refinement", "=", True)],
            deactivate_if=[("masked_refinement", "=", False)],
        )
        self.joboptions["b_factor"] = FloatJobOption(
            label="Set model B-factors",
            default_value=40.0,
            help_text="Reset all atomic B-factors in input model to given value",
            is_required=True,
        )
        self.joboptions["n_cycle"] = IntJobOption(
            label="Refmac cycles",
            default_value=8,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of refmac cycles used. User should assess if structure"
                "converges"
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["nucleotide_restraints"] = BooleanJobOption(
            label="Nucleotide restraints",
            default_value=False,
            help_text="Use LIBG to generate restraints for nucleic acids",
        )
        self.joboptions["jelly_body"] = BooleanJobOption(
            label="Jelly body restraints",
            default_value=True,
            help_text="Use jelly body restraints",
        )
        self.joboptions["jelly_body_sigma"] = FloatJobOption(
            label="Jelly body sigma",
            default_value=0.02,
            help_text="Sigma value for jelly body restraints",
            deactivate_if=[("jelly_body", "=", False)],
            required_if=[("jelly_body", "=", True)],
        )
        self.joboptions["jelly_body_dmax"] = FloatJobOption(
            label="Jelly body dmax",
            default_value=4.2,
            help_text="Dmax value for jelly body restraints",
            deactivate_if=[("jelly_body", "=", False)],
            required_if=[("jelly_body", "=", True)],
        )
        self.joboptions["auto_weight"] = BooleanJobOption(
            label="Auto weight",
            default_value=True,
            help_text=(
                "Use Servcalcat to automatically determine the relative "
                "weight of data vs stereochemical restraints"
            ),
        )
        self.joboptions["weight"] = FloatJobOption(
            label="Weight",
            default_value=1.0,
            suggested_min=0.01,
            suggested_max=100,
            step_value=0.01,
            help_text=(
                "Specific relative weight of data vs stereochemical restraints. m"
                "Smaller values result in stricter stereochemical restraints"
            ),
            deactivate_if=[("auto_weight", "=", True)],
            required_if=[("auto_weight", "=", False)],
        )
        # XXX # TODO: add description of local vs global in tooltip
        self.joboptions["auto_symmetry"] = MultipleChoiceJobOption(
            label="Auto symmetry",
            choices=["None", "Local", "Global"],
            default_value_index=1,
            help_text="Automatically detect and apply symmetry (recommended). ",
        )
        self.joboptions["strict_symmetry"] = StringJobOption(
            label="Strict symmetry point group",
            default_value="",
            help_text="Impose strict symmetry using RELION point group convention",
        )
        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # /Users/tom/code/ccpem_distro/ccpem-20211201/bin/ccpem-python -m servalcat.command_line  # noqa: E501
        # refine_spa --show_refmac_log --model /Users/tom/code/ccpem_distro/ccpem-20211201/lib/py2/ccpem/src/ccpem_core/test_data/pdb/5me2_a.pdb # noqa: E501
        # --halfmaps /Users/tom/code/ccpem_distro/ccpem-20211201/lib/py2/ccpem/src/ccpem_core/test_data/map/mrc/3488_run_half1_class001_unfil.mrc /Users/tom/code/ccpem_distro/ccpem-20211201/lib/py2/ccpem/src/ccpem_core/test_data/map/mrc/3488_run_half2_class001_unfil.mrc # noqa: E501
        # --mask_radius 3.0
        # --ncycle 8
        # --ncsr local
        # --bfactor 40.0
        # --resolution 3.2
        # --jellybody --jellybody_params 0.01 4.2 --output_prefix refined --cross_validation # noqa: E501

        # XXX Todo - should this refer to specific version i.e. explicitly point to
        # CCP-EM distribution?
        command = [self.jobinfo.programs[0].command]

        # Get parameters
        input_map1 = self.joboptions["input_map1"].get_string(
            True, "Input file missing"
        )
        input_half_map2 = self.joboptions["input_half_map2"].get_string()
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        resolution = self.joboptions["resolution"].get_number()
        ncycle = self.joboptions["n_cycle"].get_number()
        mask_radius = self.joboptions["mask_radius"].get_number()
        b_factor = self.joboptions["b_factor"].get_number()
        ncsr = self.joboptions["auto_symmetry"].get_string()
        jelly_body = self.joboptions["jelly_body"].get_boolean()
        jelly_body_sigma = self.joboptions["jelly_body_sigma"].get_number()
        jelly_body_dmax = self.joboptions["jelly_body_dmax"].get_number()

        # Set command parameters
        command += ["refine_spa", "--show_refmac_log", "--output_prefix", "refined"]

        # Half map refinement
        if input_half_map2 != "":
            command += [
                "--halfmaps",
                os.path.relpath(input_map1, self.working_dir),
                os.path.relpath(input_half_map2, self.working_dir),
            ]

        # Full map refinement
        else:
            command += ["--map", os.path.relpath(input_map1, self.working_dir)]

        command += ["--model", os.path.relpath(input_model, self.working_dir)]
        command += ["--resolution", str(resolution)]
        command += ["--ncycle", str(ncycle)]
        command += ["--mask_radius", str(mask_radius)]
        command += ["--bfactor", str(b_factor)]
        # Servalcat --ncsr option supports "local" or "global" only.  Note lower case.
        if ncsr in ["Local", "Global"]:
            command += ["--ncsr", ncsr.lower()]

        if jelly_body:
            command += [
                "--jellybody",
                "--jellybody_params",
                str(jelly_body_sigma),
                str(jelly_body_dmax),
            ]

        output_file = os.path.join(self.output_dir, "refined.pdb")
        self.output_nodes.append(
            create_node(output_file, NODE_ATOMCOORDS, ["refmac_servalcat", "refined"])
        )

        commands = [command]
        return commands

    def post_run_actions(self):
        final_mtz = os.path.join(self.output_dir, "diffmap.mtz")
        dmmrc_path = os.path.join(self.output_dir, "diffmap.mrc")
        # TODO: consider running this as an additional command in the main job
        run_subprocess(["gemmi", "sf2map", final_mtz, dmmrc_path])
        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, "diffmap.mrc"),
                NODE_DENSITYMAP,
                ["refmac_servalcat", "diffmap"],
            )
        )

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "Rmsd from ideal" in line:
                metadata_dict["RMSDFromIdeal"] = {}
                metadata_dict["RMSDFromIdeal"]["BondLengths"] = float(
                    outlines[i + 1].split()[2]
                )
                metadata_dict["RMSDFromIdeal"]["BondAngles"] = float(
                    outlines[i + 2].split()[2]
                )
            if "Map-model FSCaverages" in line:
                metadata_dict["FSCAverage"] = float(outlines[i + 1].split()[2])
            if "ADP statistics" in line:
                metadata_dict["ADPStatistics"] = {}
                stats = []
                for j in range(i + 1, len(outlines)):
                    line_j = outlines[j]
                    if line_j.strip() == "":
                        break
                    if line_j.strip().startswith("Chain"):
                        stats.append(
                            [
                                "Chain " + line_j.split()[1],
                                int(line_j.split()[2].strip("(")),
                                float(line_j.split()[5]),
                                float(line_j.split()[7]),
                                float(line_j.split()[9]),
                            ]
                        )
                    if line_j.strip().startswith("All"):
                        stats.append(
                            [
                                "All",
                                int(line_j.split()[1].strip("(")),
                                float(line_j.split()[4]),
                                float(line_j.split()[6]),
                                float(line_j.split()[8]),
                            ]
                        )
                    metadata_dict["ADPStatistics"] = stats
            if "Weight used" in line:
                metadata_dict["Weight"] = float(line.split()[2])

        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        models = os.path.join(self.output_dir, "refined.pdb")

        if self.joboptions["half_map_refinement"].get_boolean():
            map = os.path.join(self.output_dir, "diffmap.mrc")
        else:
            map = self.joboptions["input_map1"].get_string()

        return [
            make_map_model_thumb_and_display(
                maps=[map],
                maps_opacity=[0.5],
                models=[models],
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
