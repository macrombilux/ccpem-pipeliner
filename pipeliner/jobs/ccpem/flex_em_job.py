#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from shutil import which
from pathlib import Path

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    FileNameJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
)
from pipeliner.utils import get_job_script


class FlexEM(PipelinerJob):
    PROCESS_NAME = "flex_em.atomic_model_refine"
    OUT_DIR = "FlexEM"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Flex-EM"
        self.jobinfo.short_desc = "Atomic structure refinement"
        self.jobinfo.long_desc = (
            "Fitting and refinement of atomic structures guided by cryoEM density. "
            "N.B. ligands and waters are not currently supported and will be "
            "automatically removed from the input model."
        )
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        self.jobinfo.programs = [
            ExternalProgram(command="mkdssp"),
            ExternalProgram(command="ccpem-ribfind"),
            ExternalProgram(command="ccpem-flex-em"),
            ExternalProgram(command="gemmi")
            # XXX need to add test for modeller
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Topf M",
                    "Lasker B",
                    "Webb B",
                    "Wolfson H",
                    "Chiu W",
                    "Sali A",
                ],
                title=(
                    "Protien Structure Fitting and Refinement Guided by Cryo-EM Density"
                ),
                journal="Structure",
                year="2008",
                volume="16",
                issue="1",
                pages="295-307",
                doi="10.1016/j.str.2007.11.016",
            ),
            Ref(
                authors=[
                    "Joseph AP",
                    "Malhotra S",
                    "Burnley T",
                    "Wood C",
                    "Clare D",
                    "Winn M",
                    "Topf M",
                ],
                title=(
                    "Refinement of atomic models in high resolution EM "
                    "reconstructions using Flex-EM and local assessment"
                ),
                journal="Methods",
                year="2016",
                volume="100",
                issue="1",
                pages="42-49",
                doi="10.1016/j.ymeth.2016.03.007",
            ),
        ]
        self.jobinfo.documentation = "http://topf-group.ismb.lon.ac.uk/flex-em/"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be refined",
            is_required=True,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to refine the model against",
            is_required=True,
        )
        self.joboptions["iterations"] = IntJobOption(
            label="Iterations",
            default_value=3,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of Flex-EM iterations "
                "(dependant on the extent of conformational difference)"
            ),
            is_required=True,
        )
        self.joboptions["auto_ribfind"] = BooleanJobOption(
            label="Create rigid bodies",
            default_value=True,
            help_text=(
                "Automatically create rigid bodies based on resolution "
                "using Ribfind (e.g. individual secondary structures for "
                "resolutions better than 7.5)"
            ),
        )
        self.joboptions["ribfind_rigid_body_file"] = FileNameJobOption(
            label="Ribfind rigid body file",
            default_value="",
            help_text="Use Ribfind in CCP-EM 1.x if needed",
            is_required=False,
            pattern=files_exts("Text", [".txt"]),
            deactivate_if=[("auto_ribfind", "=", True)],
            required_if=[("auto_ribfind", "=", False)],
        )
        self.joboptions["phi_psi_restraints"] = BooleanJobOption(
            label="Use phi/psi restraints",
            default_value=True,
            help_text=(
                "Restrain to starting model phi-psi (see homology "
                "derived restraints in Modeller)"
            ),
        )
        self.joboptions["density_weight"] = FloatJobOption(
            label="Density weight",
            default_value=1.0,
            suggested_min=0.0,
            suggested_max=1.0,
            hard_min=0,
            step_value=0.1,
            help_text="Factor to scale density with [0.0-1.0]",
            is_required=True,
        )
        self.get_runtab_options()

    def get_commands(self):
        commands = []
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get inputs
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        resolution = self.joboptions["resolution"].get_number()
        density_weight = self.joboptions["density_weight"].get_number()

        auto_ribfind = self.joboptions["auto_ribfind"].get_boolean()
        ribfind_rigid_body_file = self.joboptions[
            "ribfind_rigid_body_file"
        ].get_string()
        phi_psi_restraints = self.joboptions["phi_psi_restraints"].get_boolean()

        # Remove waters and ligands from input model with Gemmi convert
        # also ensure .pdb is generated
        model_no_wat_lig = os.path.join(
            self.working_dir, Path(input_model).stem + "_no_wat_lig.pdb"
        )
        model_no_wat_lig_rel = os.path.relpath(model_no_wat_lig, self.working_dir)

        gemmi_command = [
            "gemmi",
            "convert",
            "--remove-lig-wat",
            os.path.relpath(input_model, self.working_dir),
            model_no_wat_lig_rel,
        ]
        commands += [gemmi_command]

        # DSSP
        dssp_out = "ribfind.dssp"
        dssp = self.jobinfo.programs[0].command
        dssp_command = [dssp, model_no_wat_lig_rel, dssp_out]
        commands += [dssp_command]

        if auto_ribfind:
            # Ribfind
            ribfind_command = [
                "ccpem-python",
                FlexEM.get_ribfind_command(),
                model_no_wat_lig_rel,
                dssp_out,
                6.5,
            ]
            commands += [ribfind_command]

            # Set expected rigid body file, direction, direction, direction...

            # Set ribfind directory
            model_stem = Path(model_no_wat_lig_rel).stem
            ribfind_dir = os.path.join(model_stem, "protein")

            # Set ribfind cutoff based on resolution
            if resolution < 7.5:
                ribfind_cutoff = "100"
            elif resolution < 15.0:
                ribfind_cutoff = "60"
            else:
                ribfind_cutoff = "30"

            # Set rigid body file
            rigid_body_file = model_stem + "_denclust_" + ribfind_cutoff + ".txt"
            ribfind_rigid_body_file = os.path.join(ribfind_dir, rigid_body_file)

        else:
            ribfind_rigid_body_file = os.path.relpath(
                ribfind_rigid_body_file, self.working_dir
            )

        # Flex-EM args
        flex_em_args_command = [
            "python3",
            get_job_script("flex_em_args.py"),
            "--working_dir",
            "",
            "--input_model",
            model_no_wat_lig_rel,
            "--input_map",
            os.path.relpath(input_map, self.working_dir),
            "--ribfind_rigid_body_file",
            ribfind_rigid_body_file,
            "--density_weight",
            str(density_weight),
            "--resolution",
            str(resolution),
        ]
        if phi_psi_restraints:
            flex_em_args_command.extend(["--phi_psi"])
        commands += [flex_em_args_command]

        # Flex-EM
        flex_em_run_command = [
            "ccpem-python",
            FlexEM.get_flex_em_command(),
            "flex_em_args.json",
        ]
        commands += [flex_em_run_command]

        # Gemmi convert to make cif output file
        gemmi_cif = [
            "gemmi",
            "convert",
            "1_MD/final1_mdcg.pdb",
            "1_MD/final1_mdcg.cif",
        ]
        commands += [gemmi_cif]

        output_file_pdb = os.path.join(self.output_dir, "1_MD/final1_mdcg.pdb")
        output_file_cif = os.path.join(self.output_dir, "1_MD/final1_mdcg.cif")

        self.output_nodes.append(
            create_node(output_file_pdb, NODE_ATOMCOORDS, ["flex_em", "refined"])
        )
        self.output_nodes.append(
            create_node(output_file_cif, NODE_ATOMCOORDS, ["flex_em", "refined"])
        )
        return commands

    @staticmethod
    def get_ribfind_command():
        ccpem_python = which("ccpem-python")
        if ccpem_python is not None:
            return ccpem_python.replace(
                "/bin/ccpem-python",
                "/lib/py2/ccpem/src/ccpem_progs/ribfind/ribfind_c_SM.pyc",
            )
        else:
            raise RuntimeError("CCP-EM v1 not found")

    @staticmethod
    def get_flex_em_command():
        ccpem_python = which("ccpem-python")
        if ccpem_python is not None:
            return ccpem_python.replace(
                "/bin/ccpem-python",
                "/lib/py2/ccpem/src/ccpem_progs/flex_em/flexem.pyc",
            )
        else:
            raise RuntimeError("CCP-EM v1 not found")

    def gather_metadata(self):
        metadata_dict = {}
        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        models = [
            self.joboptions["input_model"].get_string(),
            os.path.join(self.output_dir, "1_MD/final1_mdcg.pdb"),
        ]
        maps = [self.joboptions["input_map"].get_string()]
        return [
            make_map_model_thumb_and_display(
                maps=maps,
                maps_opacity=[0.5],
                models=models,
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
