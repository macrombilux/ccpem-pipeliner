#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
import shutil
import platform
from pathlib import Path
from typing import List, cast
from ccpem_utils.other.calc import get_skewness
import math
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionValidationResult,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import create_results_display_object
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
)
from pipeliner.utils import get_job_script


class ModelValidate(PipelinerJob):
    PROCESS_NAME = "pipeliner.validation.model_validation.evaluate"
    OUT_DIR = "Model_Validation"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Atomic model validation"
        self.jobinfo.short_desc = "Validate atomic models with multiple methods"
        self.jobinfo.long_desc = (
            "Validate protein atomic model quality and agreement with map.\n"
            "Reports outliers based on:"
            "Stereochemistry:  (Molprobity)\n"
            "Ramachandran and torsion Z-scores: (Tortoize)\n"
            "Local fit of atomic model in density: (TEMPy SMOC)\n"
            "\n\nTo run molprobity, install CCP4 versions 8.0 or above.\n"
            "To run tortoize, install from https://github.com/PDB-REDO/tortoize \n"
            "To run TEMPy, run \n"
            "pip install biotempy \n"
        )
        self.jobinfo.programs = [
            ExternalProgram("molprobity.reduce"),
            ExternalProgram("molprobity.molprobity"),
            ExternalProgram("molprobity.ramalyze"),
            ExternalProgram("molprobity.rotalyze"),
        ]
        self.version = "0.2"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Williams C et al."],
                title=(
                    "MolProbity: More and better reference data for improved"
                    " all-atom structure validation ."
                ),
                journal="Protein Sci",
                year="2018",
                volume="27",
                issue="1",
                pages="293-315",
                doi="10.1002/pro.3330",
            ),
            Ref(
                authors=["Joseph et al."],
                title=(
                    "Refinement of atomic models in high resolution "
                    "EM reconstructions using Flex-EM and local assessment."
                ),
                journal="Methods",
                year="2016",
                volume="100",
                issue="1",
                pages="42-49",
                doi="10.1016/j.ymeth.2016.03.007",
            ),
            Ref(
                authors=["Sobolev et al."],
                title=(
                    "A global Ramachandran score identifies protein structures "
                    "with unlikely stereochemistry."
                ),
                journal="Structure",
                year="2020",
                volume="28",
                issue="11",
                pages="1249-1258",
                doi="10.1016/j.str.2020.08.005",
            ),
        ]
        self.jobinfo.documentation = "https://doi.org/10.1107/S205979832101278X"

        self.outlier_plot_format = {
            "b-factor_dev": ["lines", 0, "black"],
            "molprobity": ["markers", 1, "red"],
            "smoc": ["lines", 2, "green"],
            "smoc_outlier": ["markers", 2, "red"],
            "tortoize_ramaz": ["lines", 1, "orange"],
            "tortoize_torsionz": ["lines", 1, "blue"],
        }

        self.outlier_plot_group_format = {
            0: ["B-factor deviation", "B-factor dev"],
            1: ["Geometry", "Z-score"],
            2: ["Map fit", "Score"],
        }

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be evaluated",
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map to evaluate the model against",
            is_required=False,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in Angstrom",
            required_if=[("input_map", "!=", "")],
            deactivate_if=[("input_map", "=", "")],
        )

        self.joboptions["run_molprobity"] = BooleanJobOption(
            label="Molprobity",
            default_value=False if not shutil.which("molprobity.molprobity") else True,
            help_text="Run Molprobity geometry evaluation",
        )
        self.joboptions["run_smoc"] = BooleanJobOption(
            label="TEMPy SMOC",
            default_value=False if not shutil.which("TEMPY.smoc") else True,
            help_text="Run SMOC model-map fit evaluation",
        )
        self.joboptions["run_tortoize"] = BooleanJobOption(
            label="TORTOIZE",
            default_value=(
                False
                if (
                    not shutil.which("tortoize")
                    or (
                        "ccp4" in shutil.which("tortoize").lower()
                        and platform.system() == "Darwin"
                    )
                )
                else True
            ),
            help_text="Run Tortoize model quality evaluation",
        )

        self.get_runtab_options()

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        errors = []
        if self.joboptions["run_smoc"].get_boolean() and not shutil.which("TEMPY.smoc"):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_smoc"]],
                    message=(
                        "'TEMPY.smoc' is not available in the system path, check that "
                        "it is installed"
                    ),
                )
            )
        if self.joboptions["run_tortoize"].get_boolean():
            if not shutil.which("tortoize"):
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["run_tortoize"]],
                        message=(
                            "'tortoize' is not available in the system path, "
                            "check that it is installed"
                        ),
                    )
                )
            else:
                tortoize_path = cast(str, shutil.which("tortoize"))
                if platform.system() == "Darwin" and "ccp4" in tortoize_path.lower():
                    errors.append(
                        JobOptionValidationResult(
                            result_type="error",
                            raised_by=[self.joboptions["run_tortoize"]],
                            message=("'tortoize' from CCP4 is currently broken on Mac"),
                        )
                    )
        return errors

    def encode(self):
        return self.__dict__

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get parameters
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_model = os.path.relpath(input_model, self.working_dir)
        modelid = os.path.splitext(os.path.basename(self.input_model))[0]
        self.modelid = modelid  # TODO: long files names to be trimmed ?
        input_map = self.joboptions["input_map"].get_string(False, "Input file missing")
        if input_map != "":
            self.input_map = os.path.relpath(input_map, self.working_dir)
            mapid = os.path.splitext(os.path.basename(self.input_map))[0]
            self.mapid = mapid  #

        resolution = self.joboptions["resolution"].get_number()
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        run_tortoize = self.joboptions["run_tortoize"].get_boolean()
        commands = []
        # Convert cif inputs to pdb as Molprobity does not accept cif format
        cif_ext = None
        if os.path.splitext(self.input_model)[-1].lower() == ".cif":
            cif_ext = ".cif"
        elif os.path.splitext(self.input_model)[-1].lower() == ".mmcif":
            cif_ext = ".mmcif"
        if cif_ext:
            input_model_corrected = Path(input_model).name.replace(cif_ext, ".pdb")
            gemmi_convert_model = [
                "gemmi",
                "convert",
                "--shorten",
                os.path.relpath(input_model, self.working_dir),
                input_model_corrected,
            ]
            self.input_model = input_model_corrected
            self.output_nodes.append(
                create_node(
                    os.path.join(self.output_dir, input_model_corrected),
                    NODE_ATOMCOORDS,
                    ["gemmi", "pdbconvert"],
                )
            )
            commands += [gemmi_convert_model]
        # bfactor distribution
        # pdb_num = 1  # this needs to be set for multiple model input
        bfact_command = [
            "python3",
            get_job_script("model_validation/analyse_bfactors.py"),
        ]
        bfact_command += ["-p", self.input_model]
        bfact_command += ["-mid", self.modelid]
        commands += [bfact_command]
        # molprobity
        if run_molprobity:
            # this needs fixing: reduce output pdb is printed in stdout
            # and needs to be saved in a file for molprobity input
            # running the command in a separate shell to redirect output
            reduce_command = ["sh", "-c"]
            if (
                os.path.splitext(self.input_model)[-1].lower() == ".cif"
                or os.path.splitext(self.input_model)[-1].lower() == ".mmcif"
            ):
                reduce_out = "reduce_out.cif"
            else:
                reduce_out = "reduce_out.pdb"
            # use input model if reduce run fails. TODO: check this works
            copy_reduce_command = ["cp", self.input_model, reduce_out]
            commands += [copy_reduce_command]
            reduce_run = " ".join(
                [
                    self.jobinfo.programs[0].command,
                    "-FLIP",
                    "-Quiet",
                    self.input_model,
                    ">",
                    reduce_out,
                ]
            )
            reduce_command += [reduce_run]
            # run molprobity
            molprobity_command = [self.jobinfo.programs[1].command, reduce_out]
            molprobity_command += ["output.percentiles=True"]
            # run ramalyze
            ramalyze_command = ["sh", "-c"]
            ramalyze_out = "ramalyze_out"
            ramalyze_run = " ".join(
                [self.jobinfo.programs[2].command, self.input_model, ">", ramalyze_out]
            )
            ramalyze_command += [ramalyze_run]
            # run rotalyze
            rotalyze_command = ["sh", "-c"]
            rotalyze_out = "rotalyze_out"
            rotalyze_run = " ".join(
                [self.jobinfo.programs[3].command, self.input_model, ">", rotalyze_out]
            )
            rotalyze_command += [rotalyze_run]
            # gather results
            molprobity_results_command = [
                "python3",
                get_job_script("model_validation/get_molprobity_results.py"),
                "-molp",
                "molprobity.out",
                "-rama",
                ramalyze_out,
                "-rota",
                rotalyze_out,
                "-id",
                self.modelid,
            ]
            commands += [
                reduce_command,
                molprobity_command,
                ramalyze_command,
                rotalyze_command,
                molprobity_results_command,
            ]
            self.molprobity_output = os.path.join(self.output_dir, "molprobity.out")
            self.output_nodes.append(
                create_node(
                    self.molprobity_output,
                    NODE_LOGFILE,
                    ["molprobity", "output"],
                )
            )
            self.molprobity_output = os.path.realpath(self.molprobity_output)
        # smoc
        if run_smoc and input_map:
            # pdb_num = 1  # this needs to be set for multiple model input
            smoc_command = ["TEMPy.smoc"]
            smoc_command += ["-m", self.input_map]
            smoc_command += ["-p", self.input_model]
            smoc_command += ["-r", resolution]
            smoc_command += ["--smoc-window", 1]
            smoc_command += ["--output-format", "json"]
            smoc_command += ["--output-prefix", "smoc_score"]
            smoc_results_command = [
                "python3",
                get_job_script("model_validation/get_smoc_results.py"),
                "-smoc",
                "smoc_score.json",
                "-coord",
                self.modelid + "_residue_coordinates.json",
                "-id",
                self.modelid + "_" + self.mapid,
            ]
            commands += [smoc_command, smoc_results_command]
            modelid = os.path.splitext(os.path.basename(input_model))[0]
            self.smoc_output = os.path.join(self.output_dir, "smoc_score.json")

            self.output_nodes.append(
                create_node(self.smoc_output, NODE_LOGFILE, ["tempy", "smoc_scores"])
            )
            self.smoc_output = os.path.realpath(self.smoc_output)
        if run_tortoize:
            tortoize_out = self.modelid + "_tortoize.json"
            tortoize_command = [
                "tortoize",
                self.input_model,
                tortoize_out,
            ]
            tortoize_results_command = [
                "python3",
                get_job_script("model_validation/get_tortoize_results.py"),
                "-tortoize",
                tortoize_out,
                "-id",
                self.modelid,
            ]
            commands += [tortoize_command, tortoize_results_command]

        if run_molprobity or run_smoc or run_tortoize:
            results_command = [
                "python3",
                get_job_script("model_validation/validation_results.py"),
            ]
            commands += [results_command]

            glob_output_file = "global_report.txt"
            global_output = os.path.join(self.output_dir, glob_output_file)
            self.output_nodes.append(
                create_node(
                    global_output,
                    NODE_LOGFILE,
                    ["modelval", "glob_output"],
                )
            )

            cluster_output = os.path.join(self.output_dir, "outlier_clusters.csv")
            self.output_nodes.append(
                create_node(
                    cluster_output,
                    NODE_LOGFILE,
                    ["modelval", "cluster_output"],
                )
            )

        return commands

    def gather_metadata(self):

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        metadata_dict = {}
        metadata_dict["Peptide Plane"] = {}

        for line in outlines:
            if "Ramachandran outliers" in line:
                metadata_dict["RamachandranOutliers"] = float(line.split()[3])
            if "favoured" in line:
                metadata_dict["Favoured"] = float(line.split()[2])
            if "Rotamer outliers" in line:
                metadata_dict["RotamerOutliers"] = float(line.split()[3])
            if "Cis-proline" in line:
                metadata_dict["PeptidePlane"]["CisProline"] = float(line.split()[2])
            if "Cis-general" in line:
                metadata_dict["PeptidePlane"]["CisGeneral"] = float(line.split()[2])
            if "Twisted Proline" in line:
                metadata_dict["PeptidePlane"]["TwistedProline"] = float(line.split()[3])
            if "Twisted General" in line:
                metadata_dict["PeptidePlane"]["TwistedGeneral"] = float(line.split()[3])
            if "C-beta deviations" in line:
                metadata_dict["CBetaDeviations"] = int(line.split()[3])
            if line.strip().startswith("Clashscore"):
                metadata_dict["ClashScore"] = {}
                metadata_dict["ClashScore"]["ClashScore"] = float(line.split()[2])
                metadata_dict["ClashScore"]["Percentile"] = float(line.split()[4])
                metadata_dict["ClashScore"]["N"] = int(
                    line.split()[7].split("=")[1].replace(")", "")
                )
            if "RMS(bonds)" in line:
                metadata_dict["RMSBonds"] = float(line.split()[2])
            if "RMS(angles)" in line:
                metadata_dict["RMSAngles"] = float(line.split()[2])
            if "MolProbity score" in line:
                metadata_dict["MolProbityScore"] = {}
                metadata_dict["MolProbityScore"]["MolProbityScore"] = float(
                    line.split()[3]
                )
                metadata_dict["MolProbityScore"]["Percentile"] = float(line.split()[5])
                metadata_dict["MolProbityScore"]["N"] = int(
                    line.split()[8].split("=")[1].replace(")", "")
                )
            if "Resolution" in line:
                metadata_dict["Resolution"] = float(line.split()[2])
            if "RefinementProgram" in line:
                metadata_dict["RefinementProgram"] = line.split()[3]

        return metadata_dict

    def check_bfact_distribution(self, list_bfactors):
        flag = ""
        nbins = None
        if not list_bfactors:
            flag = (
                "Warning: Atomic b-factors could not be retrieved.\n"
                "Check input model data."
            )
            return flag
        skew = get_skewness(list_bfactors)
        if math.isnan(skew):
            flag = (
                "Warning: Atomic b-factors appear unrefined (single value).\n"
                "This will affect validation results."
            )
            nbins = 3
        elif skew >= 2.0:  # severe right tailed
            flag = (
                "Warning: Residue b-factor distribution is severely skewed.\n"
                "A peak at low b-factors might indicate map over-sharpening or \n"
                "improper atomic b-factor refinement"
            )
        elif skew <= -2.0:  # severe left tailed
            flag = (
                "Warning: Residue b-factor distribution is severely skewed.\n"
                "This might indicate improper atomic b-factor refinement"
            )
        return flag, nbins

    def create_results_display(self):
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_model = os.path.relpath(input_model, self.output_dir)
        modelid = os.path.splitext(os.path.basename(self.input_model))[0]
        self.modelid = modelid  # TODO: long files names to be trimmed ?

        input_map = self.joboptions["input_map"].get_string(False, "Input file missing")
        if input_map != "":
            self.input_map = os.path.relpath(input_map, self.output_dir)
            mapid = os.path.splitext(os.path.basename(self.input_map))[0]
            self.mapid = mapid
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        run_tortoize = self.joboptions["run_tortoize"].get_boolean()

        display_objects = []
        # b-factor distribution plot
        bfact_json = os.path.join(
            self.output_dir, self.modelid + "_residue_bfactors.json"
        )
        with open(bfact_json, "r") as oj:
            bfact_data = json.load(oj)
        list_bfactors = []
        for model in bfact_data:
            if model == "max_dev":
                continue
            for chain in bfact_data[model]:
                for residue in bfact_data[model][chain]:
                    list_bfactors.append(bfact_data[model][chain][residue][0])
        flag, nbins = self.check_bfact_distribution(list_bfactors)
        plotlyhist_obj = create_results_display_object(
            "plotlyhistogram",
            data={"Residue B-factors": list_bfactors},
            # labels={"Residue B-factors": "Residue B-factors"},
            x="Residue B-factors",
            title="B-factor distribution",
            associated_data=[bfact_json],
            flag=flag,
            nbins=nbins,
        )
        display_objects.append(plotlyhist_obj)

        # make global geometry stats table
        molprob_data = []
        if run_molprobity:
            mp_json = os.path.join(
                self.output_dir, self.modelid + "_molprobity_summary.json"
            )
            with open(mp_json, "r") as mpj:
                mp_data = json.load(mpj)
            labels = ["Metric", "Score", "Expected/Percentile"]

            for metric in mp_data:
                molp_metric = " ".join(metric.split("_"))
                score = mp_data[metric][0]
                pct = mp_data[metric][1]
                if "RamachandranZ" not in molp_metric:
                    molprob_data.append([molp_metric, score, pct])
        if run_tortoize:
            tort_json = os.path.join(
                self.output_dir, self.modelid + "_tortoize_summary.json"
            )
            with open(tort_json, "r") as j:
                tor_data = json.load(j)

            labels = ["Metric", "Score", "Expected/Percentile"]
            tortoize_data = []
            for model in tor_data:
                for metric in tor_data[model]:
                    molp_metric = " ".join(metric.split("_"))
                    score = round(float(tor_data[model][metric][0]), 3)
                    pct = tor_data[model][metric][1]
                    tortoize_data.append([molp_metric, score, pct])
                break
            molprob_data += tortoize_data
        if run_molprobity or run_tortoize:
            mp_summary = create_results_display_object(
                "table",
                title="Global geometry scores",
                headers=labels,
                table_data=molprob_data,
                associated_data=[mp_json],
            )

            display_objects.append(mp_summary)
        if run_molprobity or (run_smoc and input_map) or run_tortoize:
            self.create_outlier_cluster_table(display_objects)
        if (run_smoc and input_map) or run_tortoize:
            self.create_outlier_plots(display_objects)
        return display_objects

    def create_outlier_cluster_table(self, display_objects):
        # outlier clusters table
        clusters_file = os.path.join(self.output_dir, "outlier_clusters.csv")
        clusters = []
        with open(clusters_file, "r") as cf:
            for line in cf:
                if line[0] == "#":
                    continue
                line_split = line.split(",")
                clusters.append(line_split)
        clusters_table = create_results_display_object(
            "table",
            title="Outlier clusters",
            headers=[
                "Cluster",
                "Chain_Residue",
                "Outlier Type(s)",
            ],
            table_data=clusters,
            associated_data=[clusters_file],
        )
        display_objects.append(clusters_table)

    def check_smoc_distribution(self, list_scores):
        flag = ""
        skew = get_skewness(list_scores)
        if math.isnan(skew):
            flag = (
                "Warning: SMOC score has an unexpected distribution (identical values)."
                "\nThis is likely due to an issue with input data or score calculation."
            )
        elif skew >= 2.0:  # severe right tailed
            flag = (
                "Warning: SMOC score distribution is severely skewed.\n"
                "A peak at low scores might indicate major issues with model fit.\n"
                "The outliers detected using Z-scores may not be accurate with \n"
                "severely skewed distributions"
            )
        return flag

    def create_outlier_plots(
        self,
        display_objects,
    ):
        # residue outlier plot
        outplot_json = os.path.join(self.output_dir, self.modelid + "_plot_data.json")
        with open(outplot_json, "r") as oj:
            outplot_data = json.load(oj)

        for model in outplot_data:
            for chain in outplot_data[model]:
                subplot_args = []
                subplot_order = []
                subplot_titles = []
                list_yaxes_args = []
                data = []
                list_groups = []
                for outlier in outplot_data[model][chain]:
                    list_groups.append(
                        self.outlier_plot_format[outlier.split("/")[0]][1]
                    )
                # number of subplots
                flag = ""
                x_range = None
                unique_group_num = list(set(sorted(list_groups)))
                for outlier in outplot_data[model][chain]:
                    outlier_name = outlier.split("/")[0]
                    list_x = outplot_data[model][chain][outlier][0]
                    list_y = outplot_data[model][chain][outlier][1]
                    if outlier_name == "smoc":
                        flag = self.check_smoc_distribution(list_y)
                    data.append({"x": list_x, "y": list_y})  # add data for trace
                    group_num = self.outlier_plot_format[outlier_name][1]
                    row_index = unique_group_num.index(group_num) + 1
                    subplot_order.append((row_index, 1))  # row,col for each trace
                    subplot_args.append(
                        {
                            "name": outlier,
                            "mode": self.outlier_plot_format[outlier_name][0],
                            "marker": {
                                "color": self.outlier_plot_format[outlier_name][2]
                            },
                            "line": {
                                "color": self.outlier_plot_format[outlier_name][2]
                            },
                        }
                    )  # plot trace name (legend)
                    if outlier_name != "molprobity":
                        if x_range:
                            x_range = [
                                min(min(list_x), x_range[0]),
                                max(max(list_x), x_range[1]),
                            ]
                        else:
                            x_range = [min(list_x), max(list_x)]
                # title and yaxis title for each subplot
                row_num = 0
                for group_num in unique_group_num:
                    subplot_titles.append(self.outlier_plot_group_format[group_num][0])
                    list_yaxes_args.append(
                        {
                            "title_text": self.outlier_plot_group_format[group_num][1],
                            "row": row_num + 1,
                            "col": 1,
                            "gridcolor": "lightgrey",
                        }
                    )
                    row_num += 1
                if x_range:
                    xaxes_args = {
                        "title_text": "Residue number",
                        "gridcolor": "lightgrey",
                        "range": x_range,
                    }
                else:
                    xaxes_args = {
                        "title_text": "Residue number",
                        "gridcolor": "lightgrey",
                    }
                # subplots with multi_series
                disp_obj = create_results_display_object(
                    "plotlyobj",
                    data=data,
                    plot_type="Scatter",
                    subplot=True,
                    multi_series=True,
                    subplot_size=(len(unique_group_num), 1),
                    subplot_order=subplot_order,
                    subplot_args=subplot_args,
                    layout_args={
                        "title_text": "Residue outlier plot",
                        "title_font_size": 12,
                        "height": 350 * len(unique_group_num),
                        "plot_bgcolor": "white",
                    },
                    xaxes_args=xaxes_args,
                    yaxes_args=list_yaxes_args,
                    make_subplot_args={
                        "vertical_spacing": 0.12,
                        "subplot_titles": subplot_titles,
                        "shared_xaxes": True,
                    },
                    title=f"Outliers: chain {chain}",
                    associated_data=[outplot_json],
                    flag=flag,
                )
                # po.write_image(
                #     po.from_json(disp_obj.plotlyfig), chain + ".png", format="png"
                # )
                display_objects.append(disp_obj)
