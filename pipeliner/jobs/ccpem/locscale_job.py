#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_MASK3D,
    #    NODE_LIGANDDESCRIPTION,
)


class LocalSharpen(PipelinerJob):
    PROCESS_NAME = "locscale.postprocess.local_sharpening"
    OUT_DIR = "LocScale"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "LocScale"
        self.jobinfo.short_desc = "Local sharpening of 3D maps"
        self.jobinfo.long_desc = (
            "LocScale is a reference-based local amplitude scaling tool using"
            " prior model information to improve contrast of cryo-EM density"
            " maps. It can be used as an alternative to other commonly used map"
            " sharpening methods."
        )
        self.jobinfo.programs = [ExternalProgram("locscale")]
        self.version = "2.0"
        self.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Jakobi A J", "Wilmanns M", "Sachse C"],
                title="Model-based local density sharpening of cryo-EM maps",
                journal="eLife",
                year="2017",
                volume="6",
                issue="1",
                pages="e27131",
                doi="https://doi.org/10.7554/eLife.27131",
            )
        ]
        self.jobinfo.documentation = (
            "https://gitlab.tudelft.nl/aj-lab/locscale/-/wikis/home/"
        )
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="Unsharpened and unfiltered 3D refined map",
            is_required=True,
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="Input filename mask",
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="Reference PDB with refined B-factors",
            deactivate_if=[("model_free", "=", True)],
            required_if=[("model_free", "=", False)],
            create_node=False,  # Need to set extension depending on file
        )
        # # XXX Check if this is required for LocScale2
        # self.joboptions["input_ligand"] = InputNodeJobOption(
        #     label="Input ligand",
        #     node_type=NODE_LIGANDDESCRIPTION,
        #     pattern=files_exts("Ligand definition", [".cif"]),
        #     default_value="",
        #     directory="",
        #     help_text="Input ligand dictionary",
        # )
        self.joboptions["model_free"] = BooleanJobOption(
            label="Model-free",
            default_value=False,
            help_text=("Use model-free mode of LocScale, input model not required"),
        )
        self.joboptions["n_nodes"] = IntJobOption(
            label="Nodes",
            default_value=2,
            suggested_min=1,
            suggested_max=8,
            step_value=1,
            help_text="Number of mpi nodes used for LocScale calculation",
            in_continue=True,
            is_required=True,
        )

        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Run LocScale using an existing atomic model:
        # locscale run_locscale -em path/to/emmap.mrc -mc path/to/model.pdb -res 3
        #       -v -o model_based_locscale.mrc
        #
        # Run LocScale without atomic model
        # locscale run_locscale -em path/to/emmap.mrc -res 3
        #       -v -o model_based_locscale.mrc

        # Command w/ and w/out mpi
        nr_mpi = int(self.joboptions["n_nodes"].get_number())
        if nr_mpi > 1:
            command = [
                "mpirun",
                "-np",
                str(nr_mpi),
                self.jobinfo.programs[0].command,
                "run_locscale",
            ]
        else:
            command = [self.jobinfo.programs[0].command, "run_locscale"]

        # Get parameters and set nodes
        input_map = self.joboptions["input_map"].get_string(True, "Input file missing")
        input_mask = self.joboptions["input_mask"].get_string()
        self.input_nodes.append(create_node(input_map, NODE_DENSITYMAP))
        if input_mask != "":
            self.input_nodes.append(create_node(input_mask, NODE_MASK3D))
        input_model = self.joboptions["input_model"].get_string()
        if input_model != "":
            self.input_nodes.append(create_node(input_model, NODE_ATOMCOORDS))

        resolution = self.joboptions["resolution"].get_number()

        # Input Map
        command += ["-em", os.path.relpath(input_map, self.working_dir)]

        # Input Resolution
        command += ["-res", str(resolution)]

        # Input mask
        if input_mask != "":
            command += ["-ma", os.path.relpath(input_mask, self.working_dir)]

        # Model-free / input Model (optional)
        model_free = self.joboptions["model_free"].get_boolean()
        if not model_free:
            command += ["-mc", os.path.relpath(input_model, self.working_dir)]
        # Output map
        output_file = os.path.join(self.output_dir, "locscale_output.mrc")
        self.output_nodes.append(
            create_node(output_file, NODE_DENSITYMAP, ["locscale", "localsharpen"])
        )

        # Set MPI suffix
        if nr_mpi > 1:
            command += ["-mpi"]

        commands = [command]
        return commands

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "Maximum Distance Dn" in line:
                metadata_dict["MaximumDistanceDn"] = {}
                metadata_dict["MaximumDistanceDn"]["Dn"] = float(
                    line.split()[8].strip(",")
                )
                metadata_dict["MaximumDistanceDn"]["InTail"] = float(
                    line.split()[11].strip(".")
                )
                metadata_dict["MaximumDistanceDn"]["SampleSize"] = int(line.split()[15])
            if "Anderson-Darling" in line:
                metadata_dict["AndersonDarlingTest"] = {}
                metadata_dict["AndersonDarlingTest"]["TestSummary"] = float(
                    line.split()[3].strip(".")
                )
                metadata_dict["AndersonDarlingTest"]["PValue"] = float(
                    line.split()[5].strip(".")
                )
                metadata_dict["AndersonDarlingTest"]["SampleSize"] = int(
                    line.split()[9]
                )
            if "Estimated noise statistics" in line:
                metadata_dict["EstimatedNoiseStatistics"] = {}
                metadata_dict["EstimatedNoiseStatistics"]["Mean"] = float(
                    line.split()[4]
                )
                metadata_dict["EstimatedNoiseStatistics"]["Variance"] = float(
                    line.split()[7]
                )
            if "Calculated map threshold" in line:
                metadata_dict["CalculatedMapThreshold"] = float(line.split()[3])
                metadata_dict["FDR"] = float(line.split()[8])
            if "MRC file format" in line:
                metadata_dict["MRCFileProperties"] = {}
                metadata_dict["MRCFileProperties"]["VoxelSize"] = [
                    float(outlines[i + 1].split()[2].replace("(", "").replace(",", "")),
                    float(outlines[i + 1].split()[3].strip(",")),
                    float(outlines[i + 1].split()[4].strip(")")),
                ]
                metadata_dict["MRCFileProperties"]["Origin"] = [
                    float(outlines[i + 2].split()[1].replace("(", "").replace(",", "")),
                    float(outlines[i + 2].split()[2].strip(",")),
                    float(outlines[i + 2].split()[3].strip(")")),
                ]

        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        # Get input model if available
        input_model = self.joboptions["input_model"].get_string()
        if input_model == "":
            model = None
        else:
            model = [input_model]
        # Get input and output map
        input_map = self.joboptions["input_map"].get_string()
        output_map = os.path.join(self.output_dir, "locscale_output.mrc")

        return [
            make_map_model_thumb_and_display(
                maps=[input_map, output_map],
                maps_opacity=[0.5, 1.0],
                models=model,
                title="LocScale local sharpening",
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
