#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pathlib import Path
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    BooleanJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
)


class GemmiConvert(PipelinerJob):
    PROCESS_NAME = "gemmi.atomic_model_utilities.convert"
    OUT_DIR = "GemmiConvert"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Convert PDB and CIF"
        self.jobinfo.short_desc = "PDB and CIF interconversion"
        self.jobinfo.long_desc = (
            "Uses Gemmi to convert between atomics in between CIF and PDB formats"
        )
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        self.jobinfo.programs = [
            ExternalProgram(
                command="gemmi", vers_com=["gemmi", "--version"], vers_lines=[0]
            ),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Wojdyr M"],
                title=("GEMMI: A library for structural biology"),
                journal="JOSS",
                year="2022",
                volume="7",
                issue="73",
                pages="4200",
                doi="10.21105/joss.04200",
            )
        ]
        self.jobinfo.documentation = "https://gemmi.readthedocs.io/en/latest/"
        self.joboptions["model"] = InputNodeJobOption(
            label="Model to convert",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text=(
                "Input model to convert, if PDB if given return mmCIF, if "
                "mmCIF is given return PDB"
            ),
            is_required=True,
        )

        self.joboptions["remove_waters"] = BooleanJobOption(
            label="Remove waters",
            default_value=False,
            help_text=("Remove waters"),
        )

        self.joboptions["remove_ligands_waters"] = BooleanJobOption(
            label="Remove ligands and waters",
            default_value=False,
            help_text=("Remove ligands and waters"),
        )

        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir
        command = [self.jobinfo.programs[0].command, "convert"]

        # Get parameters
        model = self.joboptions["model"].get_string(True, "Input file missing")
        remove_waters = self.joboptions["remove_waters"].get_boolean()
        remove_ligands_waters = self.joboptions["remove_ligands_waters"].get_boolean()

        # Set command parameters
        if remove_ligands_waters:
            command += ["--remove-lig-wat"]
        elif remove_waters:
            command += ["--remove-waters"]

        # Set output file
        if Path(model).suffix in [".pdb", ".ent", ".brk"]:
            model_out = Path(model).stem + ".cif"
        if Path(model).suffix in [".cif"]:
            model_out = Path(model).stem + ".pdb"
        command += [os.path.relpath(model, self.working_dir), model_out]

        # Create new node
        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, model_out),
                NODE_ATOMCOORDS,
                ["gemmi", "convert"],
            )
        )

        commands = [command]
        return commands

    def gather_metadata(self):
        metadata_dict = {}
        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        model = self.joboptions["model"].get_string()
        if Path(model).suffix in [".pdb", ".ent", ".brk"]:
            model_out = Path(model).stem + ".cif"
        if Path(model).suffix in [".cif"]:
            model_out = Path(model).stem + ".pdb"
        models = [os.path.join(self.output_dir, model_out)]
        return [
            make_map_model_thumb_and_display(
                models=models,
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
