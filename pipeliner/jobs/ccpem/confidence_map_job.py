#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    FileNameJobOption,
    IntJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    mini_montage_from_stack,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_MASK3D, NODE_LOGFILE

# Conversions between GUI labels and Max's argument values
METHODS = {
    "FDR-BY": "BY",
    "FDR-BH": "BH",
    "FWER-Holm": "Holm",
    "FWER-Hochberg": "Hochberg",
}
TEST_PROCS = {
    "Left-sided": "leftSided",
    "Right-sided": "rightSided",
    "Two-sided": "twoSided",
}


class ConfidenceMap(PipelinerJob):
    PROCESS_NAME = "confidencemap.map_analysis"
    OUT_DIR = "ConfidenceMap"

    def __init__(self):
        super().__init__()

        # Figure out where FDRcontrol.py is
        # this is a bit of a workaround until it is packaged with the pipeliner
        self.fdr_path = "FDR_NOT_FOUND"
        ccpem_path = shutil.which("ccpem-python")
        if ccpem_path:
            self.fdr_path = ccpem_path.split("/bin")[0] + "/lib/py2/FDRcontrol.py"

        self.jobinfo.display_name = "Confidence Map calculation"

        self.jobinfo.short_desc = "Thresholding by FDR control"
        self.jobinfo.long_desc = (
            "Thresholding of cryo-EM density maps by False Discovery Rate " "control."
        )
        self.jobinfo.programs = [
            ExternalProgram("ccpem-python"),
            ExternalProgram(self.fdr_path),
        ]
        self.version = "0.1"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["M. Beckers", "A. J. Jakobi", "C. Sachse"],
                title=(
                    "Thresholding of cryo-EM density maps by False Discovery "
                    "Rate control"
                ),
                journal="IUCrJ.",
                year="2019",
                volume="6",
                issue="1",
                pages="18-33",
                doi="10.1107/S2052252518014434",
            )
        ]
        self.jobinfo.documentation = "https://git.embl.de/mbeckers/FDRthresholding"

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="Raw or unmasked input map",
            is_required=True,
        )

        self.joboptions["window_size"] = IntJobOption(
            label="Window size",
            default_value=-1,
            suggested_min=10,
            suggested_max=50,
            step_value=1,
            help_text="Box size for background noise estimation (in pixels).",
            in_continue=True,
            is_required=False,
        )

        self.joboptions["noise_box_coordx"] = FloatJobOption(
            label="Noise box coordinate X",
            default_value=-1.0,
            help_text=(
                "X coordinate of centre of box for noise estimation "
                "(in pixels: x,y,z). Leave blank for default."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["noise_box_coordy"] = FloatJobOption(
            label="Noise box coordinate Y",
            default_value=-1.0,
            help_text=(
                "Y coordinate of centre of box for noise estimation "
                "(in pixels: x,y,z). Leave blank for default."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["noise_box_coordz"] = FloatJobOption(
            label="Noise box coordinate Z",
            default_value=-1.0,
            help_text=(
                "Z coordinate of centre of box for noise estimation "
                "(in pixels: x,y,z). Leave blank for default."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["apix"] = FloatJobOption(
            label="Apix",
            default_value=-1,
            suggested_min=0.1,
            suggested_max=20.0,
            step_value=0.1,
            help_text="Apix (voxel size) in Angstrom",
            is_required=False,
        )
        self.joboptions["meanMap"] = FileNameJobOption(
            label="Mean map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="3D map of noise means to be used for FDR control.",
            is_required=False,
        )

        self.joboptions["varianceMap"] = FileNameJobOption(
            label="Variance map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="3D map of noise variance to be used for FDR control.",
            is_required=False,
        )

        self.joboptions["locResMap"] = InputNodeJobOption(
            label="Local resolution map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="Input local resolution map (optional).",
        )

        self.joboptions["method"] = MultipleChoiceJobOption(
            label="Method",
            choices=["FDR-BY", "FDR-BH", "FWER-Holm", "FWER-Hochberg"],
            default_value_index=1,
            help_text=(
                "Method for multiple testing correction. FDR for "
                "False Discovery Rate or FWER for Family-Wise Error "
                "Rate. 'BY' for Benjamini-Yekutieli, 'BH' for "
                "Benjamini-Hochberg."
            ),
        )

        self.joboptions["test_proc"] = MultipleChoiceJobOption(
            label="Test procedure",
            choices=["Left-sided", "Right-sided", "Two-sided"],
            default_value_index=1,
            help_text="Choose between right, left and two-sided testing.",
        )

        self.joboptions["lowPassFilter"] = FloatJobOption(
            label="Low-pass filter to",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text=(
                "Low-pass filter the map at the given resolution prior "
                "to FDR control."
            ),
            in_continue=True,
            is_required=False,
        )

        self.joboptions["ecdf"] = BooleanJobOption(
            label="Use ECDF?",
            default_value=False,
            help_text=(
                "Use empirical cumulative distribution function instead"
                " of the standard normal distribution."
            ),
            in_continue=True,
        )

        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        command = ["ccpem-python", self.fdr_path]

        # get the input map
        input_map = self.joboptions["input_map"].get_string(
            True, "ERROR: No input map specified"
        )

        # OTHER INPUT PARAMETERS
        window_size = self.joboptions["window_size"].get_number()
        noise_box_coord_x = self.joboptions["noise_box_coordx"].get_number()
        noise_box_coord_y = self.joboptions["noise_box_coordy"].get_number()
        noise_box_coord_z = self.joboptions["noise_box_coordz"].get_number()
        # noise box coord
        noise_box_coord = [noise_box_coord_x, noise_box_coord_y, noise_box_coord_z]
        if (
            noise_box_coord[0] == -1.0
            and noise_box_coord[1] == -1.0
            and noise_box_coord[2] == -1.0
        ):
            noise_box_coord = []
        apix = self.joboptions["apix"].get_number()
        test_proc = self.joboptions["test_proc"].get_string()
        method = self.joboptions["method"].get_string()
        low_pass_filter_resolution = self.joboptions["lowPassFilter"].get_number()
        ecdf = self.joboptions["ecdf"].get_boolean()

        # get the mean map
        mean_map = self.joboptions["meanMap"].get_string()

        # get the variance map
        variance_map = self.joboptions["varianceMap"].get_string()

        # get the locres map
        locres_map = self.joboptions["locResMap"].get_string()

        # append output node types
        inp_map_basename = os.path.basename(os.path.splitext(input_map)[0])
        out_confidence_map = os.path.join(
            self.output_dir, inp_map_basename + "_confidenceMap.mrc"
        )
        self.output_nodes.append(
            create_node(out_confidence_map, NODE_MASK3D, ["confidencemap", "fdr_map"])
        )
        diag_image = os.path.join(self.output_dir, "diag_image.pdf")
        self.output_nodes.append(
            create_node(diag_image, NODE_LOGFILE, ["confidencemap", "noise_window"])
        )

        # Required args
        command += [
            "--em_map",
            os.path.relpath(input_map, self.working_dir),
            "-method",
            METHODS[method],
            "--testProc",
            TEST_PROCS[test_proc],
        ]
        if noise_box_coord is not None and len(noise_box_coord) > 0:
            command.extend(["-noiseBox"] + noise_box_coord)

        if apix is not None and apix != -1:
            command += ["--apix", apix]
        if locres_map:
            command += ["--locResMap", os.path.relpath(locres_map, self.working_dir)]
        if window_size != -1:
            command += ["--window_size", window_size]
        if mean_map:
            command += ["--meanMap", os.path.relpath(mean_map, self.working_dir)]
        if variance_map:
            command += [
                "--varianceMap",
                os.path.relpath(variance_map, self.working_dir),
            ]
        if low_pass_filter_resolution is not None and low_pass_filter_resolution != -1:
            command += ["--lowPassFilter", low_pass_filter_resolution]
        if ecdf:
            command.append("-ecdf")

        commands = [command]

        return commands

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "Maximum Distance Dn" in line:
                metadata_dict["MaximumDistanceDn"] = {}
                metadata_dict["MaximumDistanceDn"]["Dn"] = float(
                    line.split()[8].strip(",")
                )
                metadata_dict["MaximumDistanceDn"]["InTail"] = float(
                    line.split()[11].strip(".")
                )
                metadata_dict["MaximumDistanceDn"]["SampleSize"] = int(line.split()[15])
            if "Estimated noise statistics" in line:
                metadata_dict["EstimatedNoiseStatistics"] = {}
                metadata_dict["EstimatedNoiseStatistics"]["Mean"] = float(
                    line.split()[4]
                )
                metadata_dict["EstimatedNoiseStatistics"]["Variance"] = float(
                    line.split()[7]
                )
            if "*** Done ***" in line:
                metadata_dict["CalculatedMapThreshold"] = float(
                    outlines[i - 1].split()[3]
                )
                metadata_dict["FDR"] = float(outlines[i - 1].split()[8])
            if "Elapsed Time" in line:
                metadata_dict["ElapsedTime"] = float(line.split()[2])

        return metadata_dict

    def create_results_display(self):
        # Have to do this because create_results_display() is run
        # on a fresh instantiation of the job object which has not
        # had get_commands() run on it. --MGI
        input_map = self.joboptions["input_map"].get_string()
        inp_map_basename = os.path.basename(os.path.splitext(input_map)[0])
        out_confidence_map = os.path.join(
            self.output_dir, inp_map_basename + "_confidenceMap.mrc"
        )
        return [
            mini_montage_from_stack(
                out_confidence_map,
                "",
                nimg=-1,
                ncols=10,
                montagesize=640,
                title="Confidence map slices",
                cmap="turbo",
            ),
            make_map_model_thumb_and_display(
                maps=[out_confidence_map],
                title="3D Confidence map",
                outputdir=self.output_dir,
            ),
        ]
