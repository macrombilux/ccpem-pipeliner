# Standalone requirements file for building the documentation
gemmi
numpy>=1.20.0
matplotlib
networkx
mrcfile
Pillow~=9.3.0
scipy~=1.9.3
pandas
myst_parser
sphinx
sphinx-argparse
sphinx-rtd-theme
